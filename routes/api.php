<?php

use Illuminate\Http\Request;

/*
   |--------------------------------------------------------------------------
   | API Routes
   |--------------------------------------------------------------------------
   |
   | Here is where you can register API routes for your application. These
   | routes are loaded by the RouteServiceProvider within a group which
   | is assigned the "api" middleware group. Enjoy building your API!
   |
 */
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post("logout", "Api\AuthController@logout");
    Route::post("password/change", "Api\AuthController@changePassword");
    Route::post("password/reset", "Api\AuthController@resetPassword");
    Route::post("check", "Api\AuthController@checkPassword");
});

Route::group(["prefix" => "departments"], function () {
    Route::get("/", "Api\DepartmentController@getList");
    Route::get("/{id}", "Api\DepartmentController@getItem");
    Route::post("/", "Api\DepartmentController@create");
    Route::put("/{id}", "Api\DepartmentController@update");
    Route::delete("/{id}", "Api\DepartmentController@delete");
});

Route::group(["prefix" => "sub_departments"], function () {
    Route::get("/", "Api\SubDepartmentController@getList");
    Route::get("/{id}", "Api\SubDepartmentController@getItem");
    Route::post("/", "Api\SubDepartmentController@create");
    Route::put("/{id}", "Api\SubDepartmentController@update");
    Route::delete("/{id}", "Api\SubDepartmentController@delete");
});

Route::group(["prefix" => "employees"], function () {
    Route::get("/", "Api\EmployeeController@getList");
    Route::get("/{id}", "Api\EmployeeController@getItem");
    Route::post("/", "Api\EmployeeController@create");
    Route::put("/{id}", "Api\EmployeeController@update");
    Route::delete("/{id}", "Api\EmployeeController@delete");
});

Route::group(["prefix" => "employeeroles"], function () {
    Route::get("/", "Api\EmployeeRoleController@getList");
    Route::get("/{id}", "Api\EmployeeRoleController@getItem");
    Route::post("/", "Api\EmployeeRoleController@create");
    Route::put("/{id}", "Api\EmployeeRoleController@update");
    Route::delete("/{id}", "Api\EmployeeRoleController@delete");
});

Route::group(["prefix" => "roles"], function () {
    Route::get("/", "Api\RoleController@getList");
    Route::get("/{id}", "Api\RoleController@getItem");
    Route::post("/", "Api\RoleController@create");
    Route::put("/{id}", "Api\RoleController@update");
    Route::delete("/{id}", "Api\RoleController@delete");

});

Route::group(["prefix" => "rolepermissions"], function () {
    Route::get("/", "Api\RolePermissionController@getList");
    Route::get("/{id}", "Api\RolePermissionController@getItem");
    Route::post("/", "Api\RolePermissionController@create");
    Route::put("/{id}", "Api\RolePermissionController@update");
    Route::delete("/{id}", "Api\RolePermissionController@delete");
    Route::get("/role/permissions", "Api\RolePermissionController@getRoleDetail");
});

Route::group(["prefix" => "permissions"], function () {
    Route::get("/", "Api\PermissionController@getList");
    Route::get("/{id}", "Api\PermissionController@getItem");
    Route::post("/", "Api\PermissionController@create");
    Route::put("/{id}", "Api\PermissionController@update");
    Route::delete("/{id}", "Api\PermissionController@delete");
});

Route::group(["prefix" => "menus"], function () {
    Route::get("/user", "Api\MenuController@userMenu");
    Route::get("/", "Api\MenuController@getList");
    Route::get("/{id}", "Api\MenuController@getItem");
    Route::post("/", "Api\MenuController@create");
    Route::put("/{id}", "Api\MenuController@update");
    Route::delete("/{id}", "Api\MenuController@delete");

});

Route::group(["prefix" => "datas"], function () {
    Route::post("/import", "Api\DataController@import");
    Route::delete("/delete", "Api\DataController@delete");
});

Route::group(["prefix" => "settings"], function () {
    Route::get("/", "Api\SettingController@getList");
    Route::get("/{id}", "Api\SettingController@getItem");
    Route::post("/", "Api\SettingController@create");
    Route::put("/{id}", "Api\SettingController@update");
    Route::delete("/{id}", "Api\SettingController@delete");
    Route::post("/administrativeAverage/set", "Api\SettingController@administrativeAverage");
    Route::post("/administrativeAverage/get", "Api\SettingController@getAdministrativeAverage");
    Route::post("/specialPerformance/set", "Api\SettingController@specialPerformance");
    Route::post("/specialPerformance/get", "Api\SettingController@getSpecialPerformance");
});

Route::group(["prefix" => "directcost"], function () {
    Route::get("/", "Api\DirectCostController@getList");
    Route::get("/detail", "Api\DirectCostController@getDirectCostDetail");
    Route::get("/socialDetail", "Api\DirectCostController@getSocialDetail");
    Route::delete("/delData", "Api\DirectCostController@delData");

});

Route::group(["prefix" => "mainincome"], function () {
    Route::get("/", "Api\MainIncomeController@getList");
    Route::get("/{id}", "Api\MainIncomeController@getItem");
    // Route::post("/", "Api\MainIncomeController@create");
//    Route::put("/{id}", "Api\MainIncomeController@update");
    Route::delete("/{id}", "Api\MainIncomeController@delete");
});

Route::group(["prefix" => "generalizedincome"], function () {
    Route::get("/", "Api\GeneralizedIncomeController@getList");
    Route::get("/generateData", "Api\GeneralizedIncomeController@generateData");
    Route::delete("/delData", "Api\GeneralizedIncomeController@delData");

//            Route::get("/{id}", "Api\GeneralizedIncomeController@getItem");
//            Route::post("/", "Api\GeneralizedIncomeController@create");
//            Route::put("/{id}", "Api\GeneralizedIncomeController@update");
//            Route::delete("/{id}", "Api\GeneralizedIncomeController@delete");
});

Route::group(["prefix" => "employeeattendance"], function () {
    Route::get("/", "Api\EmployeeAttendanceController@getList");
    Route::get("/{id}", "Api\EmployeeAttendanceController@getItem");
    Route::post("/", "Api\EmployeeAttendanceController@create");
    Route::put("/{id}", "Api\EmployeeAttendanceController@update");
    Route::delete("/{id}", "Api\EmployeeAttendanceController@delete");
});
Route::group(["prefix" => "fixedassets"], function () {
    Route::get("/", "Api\FixedAssetsController@getList");
    Route::get("/{id}", "Api\FixedAssetsController@getItem");
    Route::post("/", "Api\FixedAssetsController@create");
    Route::put("/{id}", "Api\FixedAssetsController@update");
    Route::delete("/{id}", "Api\FixedAssetsController@delete");
});
Route::group(["prefix" => "usedsanitarymaterial"], function () {
    Route::get("/", "Api\UsedSanitaryMaterialController@getList");
    Route::get("/{id}", "Api\UsedSanitaryMaterialController@getItem");
    Route::post("/", "Api\UsedSanitaryMaterialController@create");
    Route::put("/{id}", "Api\UsedSanitaryMaterialController@update");
    Route::delete("/{id}", "Api\UsedSanitaryMaterialController@delete");
});

Route::group(["prefix" => "usedgeneralmaterial"], function () {
    Route::get("/", "Api\UsedGeneralMaterialController@getList");
    Route::get("/{id}", "Api\UsedGeneralMaterialController@getItem");
    Route::post("/", "Api\UsedGeneralMaterialController@create");
    Route::put("/{id}", "Api\UsedGeneralMaterialController@update");
    Route::delete("/{id}", "Api\UsedGeneralMaterialController@delete");
});

Route::group(["prefix" => "departmentperformance"], function () {
    Route::get("/", "Api\DepartmentPerformanceController@getList");
    Route::get("/{id}", "Api\DepartmentPerformanceController@showDetail");
    Route::post("/performance/special", "Api\DepartmentPerformanceController@specialPerformance");
});

Route::group(["prefix" => "strokecenterperformance"], function () {
    Route::get("/", "Api\StrokeCenterPerformanceController@getList");
    Route::get("/{id}", "Api\StrokeCenterPerformanceController@getItem");
    Route::post("/", "Api\StrokeCenterPerformanceController@create");
    Route::put("/{id}", "Api\StrokeCenterPerformanceController@update");
    Route::delete("/{id}", "Api\StrokeCenterPerformanceController@delete");
});

Route::group(["prefix" => "surgicalpointvalues"], function () {
    Route::get("/", "Api\SurgicalPointValuesController@getList");
    Route::get("/{id}", "Api\SurgicalPointValuesController@getItem");
    Route::post("/", "Api\SurgicalPointValuesController@create");
    Route::put("/{id}", "Api\SurgicalPointValuesController@update");
    Route::delete("/{id}", "Api\SurgicalPointValuesController@delete");
    Route::get("/check/diff", "Api\SurgicalPointValuesController@getDiff");
});

Route::group(["prefix" => "medicalpointvalue"], function () {
    Route::get("/", "Api\MedicalPointValueController@getList");
    Route::get("/{id}", "Api\MedicalPointValueController@getItem");
    Route::post("/", "Api\MedicalPointValueController@create");
    Route::put("/{id}", "Api\MedicalPointValueController@update");
    Route::delete("/{id}", "Api\MedicalPointValueController@delete");
    Route::get("/check/diff", "Api\MedicalPointValueController@getDiff");
});

Route::group(["prefix" => "dieasecatalog"], function () {
    Route::get("/", "Api\DieaseCatalogController@getList");
    Route::get("/{id}", "Api\DieaseCatalogController@getItem");
    Route::post("/", "Api\DieaseCatalogController@create");
    Route::put("/{id}", "Api\DieaseCatalogController@update");
    Route::delete("/{id}", "Api\DieaseCatalogController@delete");
});

Route::group(["prefix" => "employeeinsurancebase"], function () {
    Route::get("/", "Api\EmployeeInsuranceBaseController@getList");
    Route::get("/{id}", "Api\EmployeeInsuranceBaseController@getItem");
    Route::post("/", "Api\EmployeeInsuranceBaseController@create");
    Route::put("/{id}", "Api\EmployeeInsuranceBaseController@update");
    Route::delete("/{id}", "Api\EmployeeInsuranceBaseController@delete");
    Route::post("/check", "Api\EmployeeInsuranceBaseController@check");
});
Route::group(["prefix" => "laundrycost"], function () {
    Route::get("/", "Api\LaundryCostController@getList");
    Route::get("/{id}", "Api\LaundryCostController@getItem");
    Route::post("/", "Api\LaundryCostController@create");
    Route::put("/{id}", "Api\LaundryCostController@update");
    Route::delete("/{id}", "Api\LaundryCostController@delete");
});
Route::group(["prefix" => "disinfectcost"], function () {
    Route::get("/", "Api\DisinfectCostController@getList");
    Route::get("/{id}", "Api\DisinfectCostController@getItem");
    Route::post("/", "Api\DisinfectCostController@create");
    Route::put("/{id}", "Api\DisinfectCostController@update");
    Route::delete("/{id}", "Api\DisinfectCostController@delete");
});


Route::group(["prefix" => "employeepayroll"], function () {
    Route::get("/", "Api\EmployeePayrollController@getList");
    Route::get("/{id}", "Api\EmployeePayrollController@getItem");
    Route::post("/", "Api\EmployeePayrollController@create");
    Route::put("/{id}", "Api\EmployeePayrollController@update");
    Route::delete("/{id}", "Api\EmployeePayrollController@delete");
    Route::post("/check", "Api\EmployeePayrollController@check");
});

Route::group(["prefix" => "bedusing"], function () {
    Route::get("/", "Api\BedUsingController@getList");
    Route::get("/{id}", "Api\BedUsingController@getItem");
    Route::post("/", "Api\BedUsingController@create");
    Route::put("/{id}", "Api\BedUsingController@update");
    Route::delete("/{id}", "Api\BedUsingController@delete");
});

Route::group(["prefix" => "consultation"], function () {
    Route::get("/", "Api\ConsultationController@getList");
    Route::get("/{id}", "Api\ConsultationController@getItem");
    Route::post("/", "Api\ConsultationController@create");
    Route::put("/{id}", "Api\ConsultationController@update");
    Route::delete("/{id}", "Api\ConsultationController@delete");
});

Route::group(["prefix" => "expertdiagnosis"], function () {
    Route::get("/", "Api\ExpertDiagnosisController@getList");
    Route::get("/{id}", "Api\ExpertDiagnosisController@getItem");
    Route::post("/", "Api\ExpertDiagnosisController@create");
    Route::put("/{id}", "Api\ExpertDiagnosisController@update");
    Route::delete("/{id}", "Api\ExpertDiagnosisController@delete");
});

Route::group(["prefix" => "favourcollectinpatientfee"], function () {
    Route::get("/", "Api\FavourCollectInpatientFeeController@getList");
    Route::get("/{id}", "Api\FavourCollectInpatientFeeController@getItem");
    Route::post("/", "Api\FavourCollectInpatientFeeController@create");
    Route::put("/{id}", "Api\FavourCollectInpatientFeeController@update");
    Route::delete("/{id}", "Api\FavourCollectInpatientFeeController@delete");
});

Route::group(["prefix" => "generate"], function () {
    Route::get("/directCost", "Api\GenerateDataController@directCost");
    Route::get("/departmentPerformance", "Api\GenerateDataController@departmentPerformance");
    Route::get("/specialPerformance", "Api\GenerateDataController@specialPerformance");
    Route::delete("/clearPerformance", "Api\GenerateDataController@clearPerformance");
    Route::get("/checkStatus", "Api\GenerateDataController@checkStatus");
    Route::get("/lockData", "Api\GenerateDataController@lockPerformance");
    Route::get("/checkLockStatus", "Api\GenerateDataController@checkLockStatus");
});

Route::group(["prefix" => "employeedispatchpayroll"], function () {
    Route::get("/", "Api\EmployeeDispatchPayrollController@getList");
    Route::get("/{id}", "Api\EmployeeDispatchPayrollController@getItem");
    Route::post("/", "Api\EmployeeDispatchPayrollController@create");
    Route::put("/{id}", "Api\EmployeeDispatchPayrollController@update");
    Route::delete("/{id}", "Api\EmployeeDispatchPayrollController@delete");
});

Route::group(["prefix" => "electriccost"], function () {
    Route::get("/", "Api\ElectricCostController@getList");
    Route::get("/{id}", "Api\ElectricCostController@getItem");
    Route::post("/", "Api\ElectricCostController@create");
    Route::put("/{id}", "Api\ElectricCostController@update");
    Route::delete("/{id}", "Api\ElectricCostController@delete");
});

Route::group(["prefix" => "clinicalattendancescore"], function () {
    Route::get("/", "Api\ClinicalAttendanceScoreController@getList");
    Route::get("/{id}", "Api\ClinicalAttendanceScoreController@getItem");
    Route::post("/", "Api\ClinicalAttendanceScoreController@create");
    Route::put("/{id}", "Api\ClinicalAttendanceScoreController@update");
    Route::delete("/{id}", "Api\ClinicalAttendanceScoreController@delete");
});

Route::group(["prefix" => "medicalattendancescore"], function () {
    Route::get("/", "Api\MedicalAttendanceScoreController@getList");
    Route::get("/{id}", "Api\MedicalAttendanceScoreController@getItem");
    Route::post("/", "Api\MedicalAttendanceScoreController@create");
    Route::put("/{id}", "Api\MedicalAttendanceScoreController@update");
    Route::delete("/{id}", "Api\MedicalAttendanceScoreController@delete");
});

Route::group(["prefix" => "newtechnologyitem"], function () {
    Route::get("/", "Api\NewTechnologyItemController@getList");
    Route::get("/{id}", "Api\NewTechnologyItemController@getItem");
    Route::post("/", "Api\NewTechnologyItemController@create");
    Route::put("/{id}", "Api\NewTechnologyItemController@update");
    Route::delete("/{id}", "Api\NewTechnologyItemController@delete");
});

Route::group(["prefix" => "unitphysical"], function () {
    Route::get("/", "Api\UnitPhysicalController@getList");
    Route::get("/{id}", "Api\UnitPhysicalController@getItem");
    Route::post("/", "Api\UnitPhysicalController@create");
    Route::put("/{id}", "Api\UnitPhysicalController@update");
    Route::delete("/{id}", "Api\UnitPhysicalController@delete");
});

Route::group(["prefix" => "departmentmanagementcoefficient"], function () {
    Route::get("/", "Api\DepartmentManagementCoefficientController@getList");
    Route::get("/{id}", "Api\DepartmentManagementCoefficientController@getItem");
    Route::post("/", "Api\DepartmentManagementCoefficientController@create");
    Route::put("/{id}", "Api\DepartmentManagementCoefficientController@update");
    Route::delete("/{id}", "Api\DepartmentManagementCoefficientController@delete");
});

Route::group(["prefix" => "surgerynew"], function () {
    Route::get("/", "Api\SurgeryNewController@getList");
    Route::get("/{id}", "Api\SurgeryNewController@getItem");
    Route::post("/", "Api\SurgeryNewController@create");
    Route::put("/{id}", "Api\SurgeryNewController@update");
    Route::delete("/{id}", "Api\SurgeryNewController@delete");
});

Route::group(["prefix" => "nightperformance"], function () {
    Route::get("/", "Api\NightPerformanceController@getList");
    Route::get("/{id}", "Api\NightPerformanceController@getItem");
    Route::post("/", "Api\NightPerformanceController@create");
    Route::put("/{id}", "Api\NightPerformanceController@update");
    Route::delete("/{id}", "Api\NightPerformanceController@delete");
});

Route::group(["prefix" => "radiologydepartmentperformance"], function () {
    Route::get("/", "Api\RadiologyDepartmentPerformanceController@getList");
    Route::get("/{id}", "Api\RadiologyDepartmentPerformanceController@getItem");
    Route::post("/", "Api\RadiologyDepartmentPerformanceController@create");
    Route::put("/{id}", "Api\RadiologyDepartmentPerformanceController@update");
    Route::delete("/{id}", "Api\RadiologyDepartmentPerformanceController@delete");
});

Route::group(["prefix" => "clinicallaboratoryperformance"], function () {
    Route::get("/", "Api\ClinicalLaboratoryPerformanceController@getList");
    Route::get("/{id}", "Api\ClinicalLaboratoryPerformanceController@getItem");
    Route::post("/", "Api\ClinicalLaboratoryPerformanceController@create");
    Route::put("/{id}", "Api\ClinicalLaboratoryPerformanceController@update");
    Route::delete("/{id}", "Api\ClinicalLaboratoryPerformanceController@delete");
});

Route::group(["prefix" => "specialsurveyperformance"], function () {
    Route::get("/", "Api\SpecialSurveyPerformanceController@getList");
    Route::get("/{id}", "Api\SpecialSurveyPerformanceController@getItem");
    Route::post("/", "Api\SpecialSurveyPerformanceController@create");
    Route::put("/{id}", "Api\SpecialSurveyPerformanceController@update");
    Route::delete("/{id}", "Api\SpecialSurveyPerformanceController@delete");
});


Route::group(["prefix" => "pathologyperformance"], function () {
    Route::get("/", "Api\PathologyPerformanceController@getList");
    Route::get("/{id}", "Api\PathologyPerformanceController@getItem");
    Route::post("/", "Api\PathologyPerformanceController@create");
    Route::put("/{id}", "Api\PathologyPerformanceController@update");
    Route::delete("/{id}", "Api\PathologyPerformanceController@delete");
});

Route::group(["prefix" => "publichealthperformance"], function () {
    Route::get("/", "Api\PublicHealthPerformanceController@getList");
    Route::get("/{id}", "Api\PublicHealthPerformanceController@getItem");
    Route::post("/", "Api\PublicHealthPerformanceController@create");
    Route::put("/{id}", "Api\PublicHealthPerformanceController@update");
    Route::delete("/{id}", "Api\PublicHealthPerformanceController@delete");
});


Route::group(["prefix" => "talentreserve"], function () {
    Route::get("/", "Api\TalentReserveController@getList");
    Route::get("/{id}", "Api\TalentReserveController@getItem");
    Route::post("/", "Api\TalentReserveController@create");
    Route::put("/{id}", "Api\TalentReserveController@update");
    Route::delete("/{id}", "Api\TalentReserveController@delete");
});

Route::group(["prefix" => "pharmacyincrementalperformance"], function () {
    Route::get("/", "Api\PharmacyIncrementalPerformanceController@getList");
    Route::get("/{id}", "Api\PharmacyIncrementalPerformanceController@getItem");
    Route::post("/", "Api\PharmacyIncrementalPerformanceController@create");
    Route::put("/{id}", "Api\PharmacyIncrementalPerformanceController@update");
    Route::delete("/{id}", "Api\PharmacyIncrementalPerformanceController@delete");
});

Route::group(["prefix" => "otherperformance"], function () {
    Route::get("/", "Api\OtherPerformanceController@getList");
    Route::get("/{id}", "Api\OtherPerformanceController@getItem");
    Route::post("/", "Api\OtherPerformanceController@create");
    Route::put("/{id}", "Api\OtherPerformanceController@update");
    Route::delete("/{id}", "Api\OtherPerformanceController@delete");
});

Route::group(["prefix" => "departmentheadperformance"], function () {
    Route::get("/", "Api\DepartmentHeadPerformanceController@getList");
    Route::get("/{id}", "Api\DepartmentHeadPerformanceController@getItem");
    Route::post("/", "Api\DepartmentHeadPerformanceController@create");
    Route::put("/{id}", "Api\DepartmentHeadPerformanceController@update");
    Route::delete("/{id}", "Api\DepartmentHeadPerformanceController@delete");
});

Route::group(["prefix" => "withoutpay"], function () {
    Route::get("/", "Api\WithoutPayController@getList");
    Route::get("/{id}", "Api\WithoutPayController@getItem");
    Route::post("/", "Api\WithoutPayController@create");
    Route::put("/{id}", "Api\WithoutPayController@update");
    Route::delete("/{id}", "Api\WithoutPayController@delete");
});

Route::group(["prefix" => "export"], function () {
    Route::get("/{date}", "Api\ExportDataController@export");
    Route::get("/checkfile/{date}", "Api\ExportDataController@checkFile");
});

Route::group(["prefix" => "employeeperformance"], function () {
    Route::get("/", "Api\EmployeePerformanceController@getList");
    Route::get("/{id}", "Api\EmployeePerformanceController@getItem");
    Route::post("/", "Api\EmployeePerformanceController@create");
    Route::put("/{id}", "Api\EmployeePerformanceController@update");
    Route::delete("/{id}", "Api\EmployeePerformanceController@delete");
    Route::get("/data/generate", "Api\EmployeePerformanceController@generate");
});

Route::group(["prefix" => "corebusinesspoint"], function () {
    Route::get("/", "Api\CoreBusinessPointController@getList");
    Route::get("/{id}", "Api\CoreBusinessPointController@getItem");
    Route::post("/", "Api\CoreBusinessPointController@create");
    Route::put("/{id}", "Api\CoreBusinessPointController@update");
    Route::delete("/{id}", "Api\CoreBusinessPointController@delete");
});

Route::group(["prefix" => "specialperformancesetting"], function () {
    Route::get("/", "Api\SpecialPerformanceSettingController@getList");
    Route::get("/{id}", "Api\SpecialPerformanceSettingController@getItem");
    Route::post("/", "Api\SpecialPerformanceSettingController@create");
    Route::put("/{id}", "Api\SpecialPerformanceSettingController@update");
    Route::delete("/{id}", "Api\SpecialPerformanceSettingController@delete");
});

Route::group(["prefix" => "avgperformancesetting"], function () {
    Route::get("/", "Api\AvgPerformanceSettingController@getList");
    Route::get("/{id}", "Api\AvgPerformanceSettingController@getItem");
    Route::post("/", "Api\AvgPerformanceSettingController@create");
    Route::put("/{id}", "Api\AvgPerformanceSettingController@update");
    Route::delete("/{id}", "Api\AvgPerformanceSettingController@delete");
});

Route::group(["prefix" => "departmentheadsubtract"], function () {
    Route::get("/", "Api\DepartmentHeadSubtractController@getList");
    Route::get("/{id}", "Api\DepartmentHeadSubtractController@getItem");
    Route::post("/", "Api\DepartmentHeadSubtractController@create");
    Route::put("/{id}", "Api\DepartmentHeadSubtractController@update");
    Route::delete("/{id}", "Api\DepartmentHeadSubtractController@delete");
});

Route::group(["prefix" => "departmentinsidesubtract"], function () {
    Route::get("/", "Api\DepartmentInsideSubtractController@getList");
    Route::get("/{id}", "Api\DepartmentInsideSubtractController@getItem");
    Route::post("/", "Api\DepartmentInsideSubtractController@create");
    Route::put("/{id}", "Api\DepartmentInsideSubtractController@update");
    Route::delete("/{id}", "Api\DepartmentInsideSubtractController@delete");
});

Route::group(["prefix" => "fouritemperformance"], function () {
    Route::get("/", "Api\FourItemPerformanceController@getList");
    Route::get("/{id}", "Api\FourItemPerformanceController@getItem");
    Route::post("/", "Api\FourItemPerformanceController@create");
    Route::put("/{id}", "Api\FourItemPerformanceController@update");
    Route::delete("/{id}", "Api\FourItemPerformanceController@delete");
});

Route::group(["prefix" => "basedata"], function () {
    Route::get("/", "Api\BasedataController@getList");
    Route::get("/{id}", "Api\BasedataController@getItem");
    Route::post("/", "Api\BasedataController@create");
    Route::put("/{id}", "Api\BasedataController@update");
    Route::delete("/{id}", "Api\BasedataController@delete");
});
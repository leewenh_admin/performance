<?php

return [
    'oracle' => [
        'driver'         => 'oracle',
        'host'           => '192.168.2.10',
        'port'           => 1521,
        'service_name'   => 'ORCL',
        'database'       => 'HOSPITAL',
        'username'       => 'hospital',
        'password'       => '123456',
        'charset'        => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'         => env('DB_PREFIX', ''),
        'prefix_schema'  => env('DB_SCHEMA_PREFIX', ''),
        'server_version' => env('DB_SERVER_VERSION', '11g'),
    ],
];

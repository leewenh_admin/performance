<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 电费
 */
class ElectricCost extends Model
{
    protected $guarded = ["id"];

    public function subDepartment() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "sub_departments_id",
            "id"
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * 主要收入
 */
class MainIncome extends Model
{
    protected $guarded = ["id"];

    public function prescribe_doctor() {
        return $this->belongsTo(
            "App\Models\Employee",
            "prescribe_doctor_id",
            "id"
        );
    }

    public function execute_doctor() {
        return $this->belongsTo(
            "App\Models\Employee",
            "execute_doctor_id",
            "id"
        );
    }

    public function prescribe_department() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "prescribe_department_id",
            "id"
        );
    }

    public function execute_department() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "execute_department_id",
            "id"
        );
    }

    public function admission_department() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "admission_department_id",
            "id"
        );
    }

    public function leave_department() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "leave_department_id",
            "id"
        );
    }

    /**
     * 根据年月日期确定查询表的名字
     *
     */
    public function scopeDate($query, $date) {
        $origin_table = $this->getTable();
        $new_table = splited_table_name($origin_table, $date);
        $query->from($new_table);
    }

    public function scopeImport($query, $new_table) {
        $origin_table = $this->getTable();
        if (!Schema::hasTable($new_table))
        {
            DB::update('create table '.$new_table.' like '. $origin_table);
        }
    }

    /**
     * 重写EloquentBuilder
     * @param \Illuminate\Database\Query\Builder $query
     * @return MainIncome|MyBuilder|\Illuminate\Database\Eloquent\Builder
     */
    public function newEloquentBuilder($query)
    {
        return new MyBuilder($query);
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * 职员密码
 */
class EmployeePassword extends Model implements JWTSubject, Authenticatable {

    protected $guarded = ["id"];

    //
    public function getJWTIdentifier() {
        return $this->code;
    }

    public function getJWTCustomClaims() {
        return [];
    }

    public function getAuthIdentifierName() {
        return "code";
    }

    public function getAuthIdentifier() {
        return $this->code;
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public function getRememberToken() {
    }

    public function setRememberToken($value) {
    }

    public function getRememberTokenName() {
    }

    public function employee() {
        return $this->belongsTo('App\Models\Employee', "code", "code");
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PharmacyIncrementalPerformance extends Model
{
    protected $guarded = ["id"];
}

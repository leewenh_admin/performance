<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\Paginator;

class MyBuilder extends Builder
{
    protected $total;

    /**
     * 重写分页方法，去除总条数的查询
     * 原始的总条数查询在union的时候性能特别低
     * @param null $perPage
     * @param string[] $columns
     * @param string $pageName
     * @param null $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Pagination\LengthAwarePaginator
     */
   public function paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
   {
       $page = $page ?: Paginator::resolveCurrentPage($pageName);

       $perPage = $perPage ?: $this->model->getPerPage();

       $total = $this->total ?: $this->toBase()->getCountForPagination();

       $results = $total
           ? $this->forPage($page, $perPage)->get($columns)
           : $this->model->newCollection();

       return $this->paginator($results, $total, $perPage, $page, [
           'path' => Paginator::resolveCurrentPath(),
           'pageName' => $pageName,
       ]);
   }

    /**
     * 设置总条数
     * @param $total
     * @return $this
     */
   public function total($total){
       $this->total = $total;

       return $this;
   }

}
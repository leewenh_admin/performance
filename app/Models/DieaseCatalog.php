<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 疾病点值目录
 */
class DieaseCatalog extends Model
{
    protected $guarded = ["id"];
}

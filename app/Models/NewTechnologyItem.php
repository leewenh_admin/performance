<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 新技术业务项
 */
class NewTechnologyItem extends Model
{
    protected $guarded = ["id"];

    public function department() {
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 角色
 */
class Role extends Model
{
    protected $guarded = ["id"];
}

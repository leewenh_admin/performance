<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 管理绩效
 * Class ManagementPerformance
 * @package App\Models
 */
class ManagementPerformance extends Model
{
    protected $guarded = ["id"];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 彩超工作量统计
 */
class ColorUltrasoundWorkload extends Model
{
    protected $guarded = ["id"];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 二级科室
 */
class SubDepartment extends Model {
    protected $guarded = ["id"];

    public function department() {
        return $this->belongsTo(
            'App\Models\Department', 
            "parent_id", 
            "id"
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 管理绩效明细
 * Class DepartmentHeadPerformance
 * @package App\Models
 */
class DepartmentHeadPerformance extends Model
{
    protected $guarded = ["id"];

    public function department(){
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }

    public function employee(){
        return $this->belongsTo(
            "App\Models\Employee",
            "employee_id",
            "id"
        );
    }
}

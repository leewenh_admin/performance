<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 医共体影像中心绩效(放射科)
 * Class RadiologyDepartmentPerformance
 * @package App\Models
 */
class RadiologyDepartmentPerformance extends Model
{
    protected $guarded = ["id"];

    public function department() {
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }
}

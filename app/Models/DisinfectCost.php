<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 消毒费
 */
class DisinfectCost extends Model
{
    protected $guarded = ["id"];

    public function department() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "sub_departments_id",
            "id"
        );
    }
}

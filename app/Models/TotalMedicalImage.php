<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 影像统计
 */
class TotalMedicalImage extends Model
{
    protected $guarded = ["id"];
}

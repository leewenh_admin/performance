<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 菜单
 */
class Menu extends Model
{
    protected $guarded = ["id"];

    public function sub_menus() {
        return $this->hasMany(
            "App\Models\Menu",
            "parent_id",
            "id"
        )->orderBy('order', 'asc');
    }

    public function parent() {
        return $this->belongsTo(
            "App\Models\Menu", 
            "parent_id",
            "id"
        );
    }

    public function permission() {
        return $this->belongsTo(
            'App\Models\Permission', 
            "permissions_id", 
            "id"
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 角色权限
 */
class RolePermission extends Model
{
    protected $guarded = ["id"];

    protected $hidden = [
        "roles_id", 
        "permissions_id"
    ];

    public $timestamps = false;

    public function role() {
        return $this->belongsTo(
            'App\Models\Role', 
            "roles_id", 
            "id"
        );
    }

    public function permission() {
        return $this->belongsTo(
            'App\Models\Permission', 
            "permissions_id", 
            "id"
        );
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 固定资产
 */
class FixedAssets extends Model
{
    protected $guarded = ["id"];

    public function subDepartment() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "sub_departments_id",
            "id"
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 床位利用情况
 */
class BedUsing extends Model
{
    protected $guarded = ["id"];

    public function subDepartment() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "sub_departments_id",
            "id"
        );
    }
}

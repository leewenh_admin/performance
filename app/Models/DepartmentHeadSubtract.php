<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 科主任科室绩效核减
 * Class DepartmentHeadSubtract
 * @package App\Models
 */
class DepartmentHeadSubtract extends Model
{
    protected $guarded = ["id"];

    public function department(){
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }
}

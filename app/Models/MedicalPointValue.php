<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 医技科室点值
 */
class MedicalPointValue extends Model
{
    protected $guarded = ["id"];

    public function executeDepartment() {
        return $this->belongsTo(
            'App\Models\SubDepartment',
            "execute_department_id",
            "id"
        );
    }
}

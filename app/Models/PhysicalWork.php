<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 体检工作量
 */
class PhysicalWork extends Model
{
    protected $guarded = ["id"];
}

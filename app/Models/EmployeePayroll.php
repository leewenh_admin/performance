<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 职员工资表
 */
class EmployeePayroll extends Model
{
    protected $guarded = ["id"];

    public function employee() {
        return $this->belongsTo(
            "App\Models\Employee",
            "employees_id",
            "id"
        );
    }
}

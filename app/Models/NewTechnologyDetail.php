<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 新技术业务详情
 * Class NewTechnologyDetail
 * @package App\models
 */
class NewTechnologyDetail extends Model
{
    protected $guarded = ['id'];
}

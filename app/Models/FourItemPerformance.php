<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 单位体检绩效、卒中绩效、疫情防控、科室跑账
 * Class FourItemPerformance
 * @package App\Models
 */
class FourItemPerformance extends Model
{
    protected $guarded = ['id'];

    public function department(){
        return $this->belongsTo(Department::class);
    }
}

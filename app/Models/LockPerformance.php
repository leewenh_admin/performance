<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 绩效锁定
 * Class LockPerformance
 * @package App\Models
 */
class LockPerformance extends Model
{
    protected $guarded = ['id'];
}

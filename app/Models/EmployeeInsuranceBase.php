<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 职员保险基数
 */
class EmployeeInsuranceBase extends Model
{
    protected $guarded = ["id"];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 单位体检
 */
class UnitPhysical extends Model
{
    protected $guarded = ["id"];

    public function department(){
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }

    public function subDepartment() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "sub_department_id",
            "id"
        );
    }
}

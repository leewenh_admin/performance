<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 门诊收费室 代收住院病人费用 信息
 */
class FavourCollectInpatientFee extends Model
{
    protected $guarded = ["id"];
}

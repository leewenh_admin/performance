<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 专项绩效
 * Class SpecialPerformance
 * @package App\Models
 */
class SpecialPerformance extends Model
{
    protected $guarded = ["id"];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 专家号
 */
class ExpertDiagnosis extends Model
{
    protected $guarded = ["id"];

    public function employee() {
        return $this->belongsTo(
            "App\Models\Employee",
            "employees_id",
            "id"
        );
    }
}

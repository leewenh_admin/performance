<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 停薪人员
 * Class WithoutPay
 * @package App\Models
 */
class WithoutPay extends Model
{
    protected $guarded = ["id"];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 公卫绩效
 * Class PublicHealthPerformance
 * @package App\Models
 */
class PublicHealthPerformance extends Model
{
    protected $guarded = ["id"];

    public function subDepartment(){
        return $this->belongsTo(
            'App\Models\SubDepartment',
            "sub_department_id",
            "id"
        );
    }
}

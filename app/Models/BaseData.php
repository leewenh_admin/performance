<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 基期数据
 * Class BaseData
 * @package App\Models
 */
class BaseData extends Model
{
    protected $guarded = ["id"];

    protected $hidden = ['created_at', 'updated_at'];
}

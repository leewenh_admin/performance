<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TalentReserve extends Model
{
    protected $guarded = ["id"];

    public function employee() {
        return $this->belongsTo(
            "App\Models\Employee",
            "employees_id",
            "id"
        );
    }

    public function department(){
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }
}

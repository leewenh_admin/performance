<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 其他专项绩效
 * Class OtherPerformance
 * @package App\Models
 */
class OtherPerformance extends Model
{
    protected $guarded = ["id"];

    public function subDepartment(){
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "sub_department_id",
            "id"
        );
    }
}

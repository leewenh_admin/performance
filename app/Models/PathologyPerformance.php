<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 医共体病理中心绩效(病理科)
 * Class PathologyPerformance
 * @package App\Models
 */
class PathologyPerformance extends Model
{
    protected $guarded = ["id"];

    public function department() {
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }
}

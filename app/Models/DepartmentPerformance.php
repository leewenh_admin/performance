<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 科室绩效
 */
class DepartmentPerformance extends Model
{
    protected $guarded = ["id"];

    public function department(){
        return $this->belongsTo(
            'App\Models\Department',
            "department_id",
            "id"
        );
    }
}

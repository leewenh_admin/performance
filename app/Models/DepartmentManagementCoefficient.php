<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 科室管理系数表
 */
class DepartmentManagementCoefficient extends Model
{
    protected $guarded = ["id"];

    public function department(){
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 手术
 */
class Surgery extends Model
{
    protected $table = 'surgerys';

    protected $guarded = ["id"];
}

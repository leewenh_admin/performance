<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 一级科室
 */
class Department extends Model {
    protected $guarded = ["id"];

    public function subDepartments() {
        return $this->hasMany(
            "App\Models\SubDepartment",
            "parent_id",
            "id"
        );
    }

    public function subDepartment() {
        return $this->hasOne(SubDepartment::class, 'parent_id', 'id');
    }
}

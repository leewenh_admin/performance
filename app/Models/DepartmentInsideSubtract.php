<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 科内绩效核减
 * Class DepartmentInsideSubtract
 * @package App\Models
 */
class DepartmentInsideSubtract extends Model
{
    protected $guarded = ["id"];

    public function department(){
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 行政平均绩效
 * Class AvgPerformanceSetting
 * @package App\Models
 */
class AvgPerformanceSetting extends Model
{
    protected $guarded = ["id"];
}

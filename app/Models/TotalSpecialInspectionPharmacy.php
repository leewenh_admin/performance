<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 特检胃镜药房统计
 */
class TotalSpecialInspectionPharmacy extends Model
{
    protected $guarded = ["id"];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 权限
 */
class Permission extends Model
{
    protected $guarded = ["id"];
}

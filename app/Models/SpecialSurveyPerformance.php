<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 医共体心电中心绩效(特检科)
 * Class SpecialSurveyPerformance
 * @package App\Models
 */
class SpecialSurveyPerformance extends Model
{
    protected $guarded = ["id"];

    public function department() {
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }
}

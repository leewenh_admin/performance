<?php

namespace App\Models;

use App\Enums\SettingValueType;
use Illuminate\Database\Eloquent\Model;

/**
 * 设置
 */
class Setting extends Model
{
    protected $guarded = ["id"];

    public function getValueAttribute($value) {
        $costed = null;
        switch ($this->value_type) {
            case SettingValueType::TYPE_INTEGER:
                $costed = intval($value);
                break;
            case SettingValueType::TYPE_STRING:
                $costed = strval($value);
                break;
            case SettingValueType::TYPE_DECIMAL:
                $costed = floatval($value);
                break;
            default:
                break;
        }
        return $costed;
    }
}

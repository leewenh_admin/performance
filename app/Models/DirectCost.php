<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 直接成本
 */
class DirectCost extends Model
{
    protected $guarded = ["id"];

    public function department()
    {
        return $this->belongsTo(
            "App\Models\Department",
            "departments_id",
            "id"
        );
    }
}

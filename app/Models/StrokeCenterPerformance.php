<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 卒中中心绩效
 */
class StrokeCenterPerformance extends Model
{
    protected $guarded = ["id"];

    public function subDepartment(){
        return $this->belongsTo(
            'App\Models\SubDepartment',
            "sub_department_id",
            "id"
        );
    }

    public function employee(){
        return $this->belongsTo(
            'App\Models\Employee',
            "employee_id",
            "id"
        );
    }
}

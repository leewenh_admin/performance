<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 职员考勤表
 */
class EmployeeAttendance extends Model
{
    protected $guarded = ["id"];

    public function subDepartment() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "sub_departments_id",
            "id"
        );
    }

    public function department() {
        return $this->belongsTo(
            "App\Models\Department",
            "departments_id",
            "id"
        );
    }

    public function employee() {
        return $this->belongsTo(
            "App\Models\Employee",
            "employees_id",
            "id"
        );
    }
}

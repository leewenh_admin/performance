<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 劳务派遣职员工资表
 */
class EmployeeDispatchPayroll extends Model
{
    protected $guarded = ["id"];

    public function subDepartment() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "sub_departments_id",
            "id"
        );
    }
}

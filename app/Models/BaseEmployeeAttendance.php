<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 职员考勤基础数据（2019）
 */
class BaseEmployeeAttendance extends Model
{
    protected $table = "employee_attendances_2019";

    protected $guarded = ["id"];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 手术项目点值
 */
class SurgicalPointValue extends Model
{
    protected $guarded = ["id"];

    public function belongDepartment() {
        return $this->belongsTo(
            'App\Models\SubDepartment',
            "belong_department_id",
            "id"
        );
    }

    public function executeDepartment() {
        return $this->belongsTo(
            'App\Models\SubDepartment',
            "execute_department_id",
            "id"
        );
    }
}

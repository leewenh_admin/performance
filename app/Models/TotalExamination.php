<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 检验统计
 */
class TotalExamination extends Model
{
    protected $guarded = ["id"];
}

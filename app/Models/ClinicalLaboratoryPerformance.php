<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 医共体检验中心绩效(检验科)
 * Class ClinicalLaboratoryPerformance
 * @package App\Models
 */
class ClinicalLaboratoryPerformance extends Model
{
    protected $guarded = ["id"];

    public function department() {
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }
}

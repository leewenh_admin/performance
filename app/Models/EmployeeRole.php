<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 职员角色
 */
class EmployeeRole extends Model
{
    protected $guarded = ["id"];

    public $timestamps = false;

    protected $hidden = [
        "roles_id", 
        "employees_id"
    ];

    public function role() {
        return $this->belongsTo(
            'App\Models\Role', 
            "roles_id", 
            "id"
        );
    }

    public function employee() {
        return $this->belongsTo(
            'App\Models\Employee', 
            "employees_id", 
            "id"
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 洗涤费
 */
class LaundryCost extends Model
{
    protected $guarded = ["id"];

    public function department() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "sub_departments_id",
            "id"
        );
    }
}

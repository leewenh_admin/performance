<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 手术表(新)
 * Class SurgeryNew
 * @package App\Models
 */
class SurgeryNew extends Model
{
    protected $guarded = ["id"];

    public function subDepartment() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "sub_departments_id",
            "id"
        );
    }
}

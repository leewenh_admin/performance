<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 住院病人动态
 */
class Inpatient extends Model
{
    protected $guarded = ["id"];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 夜班绩效
 * Class NightPerformance
 * @package App\Models
 */
class NightPerformance extends Model
{
    protected $guarded = ["id"];

    public function department() {
        return $this->belongsTo(
            "App\Models\Department",
            "department_id",
            "id"
        );
    }
}

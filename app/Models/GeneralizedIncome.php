<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 广义收入
 */
class GeneralizedIncome extends Model
{
    protected $guarded = ["id"];

    public function department() {
        return $this->belongsTo(
            "App\Models\SubDepartment",
            "sub_department_id",
            "id"
        );
    }
}

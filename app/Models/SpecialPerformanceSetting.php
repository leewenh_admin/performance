<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 设置特殊科室绩效
 * Class SpecialPerformanceSetting
 * @package App\Models
 */
class SpecialPerformanceSetting extends Model
{
    protected $guarded = ["id"];

    public function department(){
        return $this->belongsTo(
            'App\Models\Department',
            'department_id',
            'id'
        );
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 医技综合考核绩效
 */
// TODO: 修改模型名称以及表名
class MedicalAttendanceScore extends Model
{
    protected $guarded = ["id"];

    public function department() {
        return $this->belongsTo(
            "App\Models\Department",
            "departments_id",
            "id"
        );
    }
}

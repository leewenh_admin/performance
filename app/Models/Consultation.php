<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 会诊统计
 */
class Consultation extends Model
{
    protected $guarded = ["id"];

    public function employee() {
        return $this->belongsTo(
            "App\Models\Employee",
            "employees_id",
            "id"
        );
    }
}

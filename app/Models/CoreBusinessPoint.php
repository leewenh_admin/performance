<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 核心业务点值
 * Class CoreBusinessPoint
 * @package App\Models
 */
class CoreBusinessPoint extends Model
{
    protected $guarded = ["id"];

    public function department(){
        return $this->belongsTo(
            'App\Models\Department',
            'department_id',
            'id'
        );
    }
}

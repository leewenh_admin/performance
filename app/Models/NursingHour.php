<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 护理时数
 */
class NursingHour extends Model
{
    protected $guarded = ["id"];
}

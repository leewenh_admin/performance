<?php

namespace App\Console\Commands;

use App\Models\BaseData;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class generateBaseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:baseData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '生成科室单价数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $exists = BaseData::query()->where('date', now()->format('Y-m'))->exists();
        if (!$exists){
            $last = BaseData::query()->where('date', now()->subMonthNoOverflow(1)->format('Y-m'))->get()
                ->makeHidden(['id', 'base_department_income', 'base_period_controllable_variable_cost',
                    'target_patient_count', 'base_use_bed_day', 'base_out_hospital', 'base_surgery_count',
                    'base_special_surgery_rate', 'base_special_surgery_count'])
                ->toArray();
            if (empty($last)){
                $this->info('暂无上月数据');
            }

            $now_date = now()->format('Y-m');
            foreach ($last as $item){
                BaseData::query()->updateOrCreate(
                    [
                        'date' => $now_date,
                        'department_id' => $item['department_id'],
                    ],
                    [
                        'point_price' => $item['point_price'],
                        'cost_effective_price' => $item['cost_effective_price'],
                        'outpatient_price' => $item['outpatient_price'],
                        'admission_price' => $item['admission_price'],
                    ]
                );
            }
            $this->info('数据生成成功');
        }
    }
}

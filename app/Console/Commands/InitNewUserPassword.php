<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use App\Models\Employee;
use App\Models\EmployeePassword;

class InitNewUserPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initpwd:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '对未进行初始化密码的用户进行密码初始化';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $no_pwd_codes = EmployeePassword::select('code')
            ->get()
            ->pluck('code')
            ->all();
        $no_pwd_employees = Employee::whereNotIn('code', $no_pwd_codes)
            ->get();

        $c = 0;
        foreach ($no_pwd_employees as $employee) {
            EmployeePassword::create([
                'code' => $employee->code,
                'password' => Hash::make('88888888')
            ]);
            $c = $c + 1;
        }

        $this->info('初始化密码帐号数量=' . $c);

    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class Generate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:generate
    {name : Class (singular) for example User}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create CRUD operations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');

        $this->controller($name);
        $this->create($name);
        $this->update($name);
        $this->delete($name);
        $this->getList($name);
        $this->getItem($name);
        $this->repository($name);

        $routeName = strtolower($name);
        $routeDetail = <<<EOF


Route::group(["prefix" => "${routeName}"], function () {
    Route::get("/", "Api\\${name}Controller@getList");
    Route::get("/{id}", "Api\\${name}Controller@getItem");
    Route::post("/", "Api\\${name}Controller@create");
    Route::put("/{id}", "Api\\${name}Controller@update");
    Route::delete("/{id}", "Api\\${name}Controller@delete");
});
EOF;
        File::append(base_path('routes/api.php'), $routeDetail);

    }

    protected function getStub($type)
    {
        return file_get_contents(resource_path("stubs/$type.stub"));
    }

    protected function controller($name)
    {
        $controllerTemplate = str_replace(
            ['{{modelName}}'],
            [$name],
            $this->getStub('Controller')
        );
        $file_path = app_path("/Http/Controllers/Api/{$name}Controller.php");
        if (!file_exists($file_path)) {
            file_put_contents($file_path, $controllerTemplate);
        }
    }

    protected function create($name)
    {
        $requestTemplate = str_replace(
            ['{{modelName}}'],
            [$name],
            $this->getStub('Create')
        );

        if(!file_exists($path = app_path('/Http/Requests/'.$name)))
            mkdir($path, 0777, true);

        $file_path = app_path("/Http/Requests/{$name}/CreateRequest.php");
        if (!file_exists($file_path)) {
            file_put_contents($file_path, $requestTemplate);
        }
    }

    protected function update($name)
    {
        $requestTemplate = str_replace(
            ['{{modelName}}'],
            [$name],
            $this->getStub('Update')
        );

        if(!file_exists($path = app_path('/Http/Requests/'.$name)))
            mkdir($path, 0777, true);

        $file_path = app_path("/Http/Requests/{$name}/UpdateRequest.php");
        if (!file_exists($file_path)) {
            file_put_contents($file_path, $requestTemplate);
        } 
    }

    protected function delete($name)
    {
        $requestTemplate = str_replace(
            ['{{modelName}}'],
            [$name],
            $this->getStub('Delete')
        );

        if(!file_exists($path = app_path('/Http/Requests/'.$name)))
            mkdir($path, 0777, true);

        $file_path = app_path("/Http/Requests/{$name}/DeleteRequest.php");
        if (!file_exists($file_path)) {
            file_put_contents($file_path, $requestTemplate);
        } 
    }

    protected function getList($name)
    {
        $requestTemplate = str_replace(
            ['{{modelName}}'],
            [$name],
            $this->getStub('GetList')
        );

        if(!file_exists($path = app_path('/Http/Requests/'.$name)))
            mkdir($path, 0777, true);

        $file_path = app_path("/Http/Requests/{$name}/GetListRequest.php");
        if (!file_exists($file_path)) {
            file_put_contents($file_path, $requestTemplate);
        } 
    }

    protected function getItem($name)
    {
        $requestTemplate = str_replace(
            ['{{modelName}}'],
            [$name],
            $this->getStub('GetItem')
        );

        if(!file_exists($path = app_path('/Http/Requests/'.$name)))
            mkdir($path, 0777, true);

        $file_path = app_path("/Http/Requests/{$name}/GetItemRequest.php");
        if (!file_exists($file_path)) {
            file_put_contents($file_path, $requestTemplate);
        } 
    }

    protected function repository($name)
    {
        $requestTemplate = str_replace(
            ['{{modelName}}'],
            [$name],
            $this->getStub('Repository')
        );

        if(!file_exists($path = app_path('/Repositories/')))
            mkdir($path, 0777, true);

        $file_path = app_path("/Repositories/{$name}RepositoryEloquent.php");
        if (!file_exists($file_path)) {
            file_put_contents($file_path, $requestTemplate);
        }
    }

}

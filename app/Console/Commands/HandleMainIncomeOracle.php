<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\MainIncomeOracleImport;

class HandleMainIncomeOracle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:oracle {date} {table_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '从Oracle数据库中导入主要收入数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $importer = new MainIncomeOracleImport();
        $date = $this->argument('date');
        $table_name = $this->argument('table_name');
        $importer->import_from_oracle($date, $table_name);
        echo 'ok';
    }
}

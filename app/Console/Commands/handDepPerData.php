<?php

namespace App\Console\Commands;

use App\Repositories\ComprehensivePerformanceRepositoryEloquent;
use App\Repositories\DepartmentPerformanceRepositoryEloquent;
use App\Repositories\DirectCostRepositoryEloquent;
use Illuminate\Console\Command;

class handDepPerData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'handle:per:data{type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        if ($type == 1) {
            $result = (new DirectCostRepositoryEloquent())->generateData('2021-03');
        }elseif ( $type == 2) {
            $result = (new ComprehensivePerformanceRepositoryEloquent())->performance('2021-03');
        } else {
            $result = (new DepartmentPerformanceRepositoryEloquent())->generatePerformanceData('2021-03');
        }
        dd('ok');
    }
}

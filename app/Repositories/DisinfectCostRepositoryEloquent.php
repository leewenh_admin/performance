<?php

namespace App\Repositories;

use App\Models\DisinfectCost;

class DisinfectCostRepositoryEloquent extends BaseRepository {
    protected $actionClass = DisinfectCost::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with("department:id,name,code");
        $query = $query->join('sub_departments', 'disinfect_costs.sub_departments_id', '=', 'sub_departments.id')
            ->leftjoin('lock_performances', 'disinfect_costs.date', '=', 'lock_performances.date');

        $sub_departments_id = request("sub_departments_id", null);
        if (!empty($sub_departments_id)){
            $query = $query->where("sub_departments_id", $sub_departments_id);
        }

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("disinfect_costs.date", $date);
        }
        $query->select('disinfect_costs.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");
        return $query;
    }
}

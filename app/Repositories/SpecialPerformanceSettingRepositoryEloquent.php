<?php

namespace App\Repositories;

use App\Exceptions\MessageException;
use App\Models\DepartmentPerformance;
use App\Models\SpecialPerformanceSetting;
use Illuminate\Support\Facades\DB;

class SpecialPerformanceSettingRepositoryEloquent extends BaseRepository {
    protected $actionClass = SpecialPerformanceSetting::class;

    public function getList(){
        $query =  parent::getList();

        $query = $query->with('department:id,name,code');

        $query = $query->join('departments', 'special_performance_settings.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'special_performance_settings.date', '=', 'lock_performances.date');
        $query->select('special_performance_settings.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if ($date){
            $query = $query->where("special_performance_settings.date", $date);
        }

        $department_id = request("department_id", null);
        if ($department_id){
            $query = $query->where("department_id", $department_id);
        }

        return $query;
    }

    public function insert($data){
        DB::beginTransaction();
        try {
            parent::insert($data);
            //更新已生成的绩效数据
            $record = DepartmentPerformance::where('date', $data['date'])->where('department_id', $data['department_id'])->first();
            if ($record){
                $total = $record->total_performances;
                $old = $record->special_performance;
                $new = $data['performance'];
                $new_total = $total - $old + $new;

                //更新科内绩效和科室分配绩效
                $d_performance = (new DepartmentPerformanceRepositoryEloquent())->getdPerformance($new_total, $record->people_count, $data['date'],$record->department_id);
                $distribution_performance = $new_total - $d_performance;

                $record->special_performance = $new;
                $record->total_performances = $new_total;
                $record->department_performance = $d_performance;
                $record->distribution_performance = $distribution_performance;
                $record->save();
            }



            DB::commit();
        } catch (\Exception $exception){
            DB::rollBack();
            throw new MessageException($exception->getMessage());
        }
    }


    public function update($id, $data){
        DB::beginTransaction();
        try {
            parent::update($id, $data);
            //更新已生成的绩效数据
            $record = DepartmentPerformance::where('date', $data['date'])->where('department_id', $data['department_id'])->first();
            if ($record){
                $total = $record->total_performances;
                $old = $record->special_performance;
                $new = $data['performance'];
                $new_total = $total - $old + $new;

                //更新科内绩效和科室分配绩效
                $d_performance = (new DepartmentPerformanceRepositoryEloquent())->getdPerformance($new_total, $record->people_count, $data['date'], $record->department_id);
                $distribution_performance = $new_total - $d_performance;

                $record->special_performance = $new;
                $record->total_performances = $new_total;
                $record->department_performance = $d_performance;
                $record->distribution_performance = $distribution_performance;
                $record->save();
            }
            DB::commit();
        } catch (\Exception $exception){
            DB::rollBack();
            throw new MessageException($exception->getMessage());
        }
    }

    public function delete($id){
        DB::beginTransaction();
        try {
            $delData = SpecialPerformanceSetting::find($id);
            $record = DepartmentPerformance::where('date', $delData->date)->where('department_id', $delData->department_id)->first();

            //更新已生成的绩效数据
            if ($record){
                $total = $record->total_performances;
                $old = $record->special_performance;
                $new = 0;
                $new_total = $total - $old + $new;

                //更新科内绩效和科室分配绩效
                $d_performance = (new DepartmentPerformanceRepositoryEloquent())->getdPerformance($new_total, $record->people_count, $record->date,$record->department_id);
                $distribution_performance = $new_total - $d_performance;

                $record->special_performance = $new;
                $record->total_performances = $new_total;
                $record->department_performance = $d_performance;
                $record->distribution_performance = $distribution_performance;
                $record->save();
            }

            $delData->delete();

            DB::commit();
        } catch (\Exception $exception){
            DB::rollBack();
            throw new MessageException($exception->getMessage());
        }
    }
}

<?php

namespace App\Repositories;

use App\Models\FavourCollectInpatientFee;

class FavourCollectInpatientFeeRepositoryEloquent extends BaseRepository {
    protected $actionClass = FavourCollectInpatientFee::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->leftjoin('lock_performances', 'favour_collect_inpatient_fees.date', '=', 'lock_performances.date');


        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("favour_collect_inpatient_fees.date", $date);
        }

        $query->select('favour_collect_inpatient_fees.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        return $query;
    }
}

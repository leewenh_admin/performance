<?php

namespace App\Repositories;

use App\Models\Menu;
use App\Models\RolePermission;

class RolePermissionRepositoryEloquent extends BaseRepository {
    protected $actionClass = RolePermission::class;

    protected function getSortStr() {
        return "id";
    }

    public function getList() {
        $query = parent::getList();
        $query = $query->with("role")->with("permission");
        return $query;
    }

    public function getItem($id) {
        $record = parent::getItem($id);
        $record = $record->load("role", "permission");
        return $record;
    }

    public function insert($data)
    {
        $class = $this->getClass();
        $roles_id = $data["roles_id"];
        $permissions_id = $data["permissions_id"];
        $class::where("roles_id", $roles_id)->delete();
        foreach ($permissions_id as $permission_id){
            $class::create([
                "roles_id" => $roles_id,
                "permissions_id" => $permission_id,
            ]);
        }
    }

    public function getRolePermission()
    {
        $query = $this->getQuery();
        $results = Menu::whereNull("parent_id")
            ->with("sub_menus")
            ->orderBy('order', 'asc')
            ->get()->toArray();

        $role = request("roles_id", null);
        if (!empty($role)){
            $permission = $query->where("roles_id", $role)
                ->get()->pluck("permissions_id")->all();

            foreach ($results as $key => $result){
                foreach ($result['sub_menus'] as $k => $sub_menus){
                    if(in_array($sub_menus['permissions_id'], $permission)){
                        $results[$key]["sub_menus"][$k]["checked"] = 1;
                    }else{
                        $results[$key]["sub_menus"][$k]["checked"] = 0;
                    }
                }
            }

        }

        return $results;
    }

}

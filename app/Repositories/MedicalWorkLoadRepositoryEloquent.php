<?php

namespace App\Repositories;

use App\Enums\DepartmentType;
use App\Models\BaseData;
use App\Models\CoreBusinessPoint;
use App\Models\MainIncome;
use App\Models\Department;
use App\Models\SubDepartment;

/**
 * 医技工作量绩效
 * Class MedicalWorkLoadRepositoryEloquent
 * @package App\Repositories
 */
class MedicalWorkLoadRepositoryEloquent extends BaseRepository {

    /**
     * 计算医技工作量绩效
     * @param $date
     * @param $department_id
     * @return false|float
     */
    public function getMedicalWorkLoad($date, $department_id){
        //查询是否设置了核心业务点数
        $data = CoreBusinessPoint::where("date", $date)->where("department_id", $department_id)->first();
        if($data){
            //核心业务点数单价
            $point_price = BaseData::query()->where('date', $date)
                ->where('department_id', $department_id)->value('point_price');

            return round($data->point * $point_price);
        }


        $department_ids = $this->getSubDepartments($department_id);
        if (empty($department_ids)){
            return 0;
        }
        //查询目标工作量和点值
        $department_data = Department::where("id", $department_id)
            ->select("target_medical_workload")
            ->first();
        //实际工作量
//        $actualWorkload = $this->getActualWorkload($date, $department_id, $department_ids);
        //实际工作量*点数
        $medicalPoint = $this->getMedicalPoint($date, $department_id);
        //超额工作量
//        $overWorklod = $actualWorkload - $department_data->target_medical_workload;
//        if($department_data->target_medical_workload == 0){
//            return 0;
//        }
//
//        //超额比例
//        $percent = $overWorklod / $department_data->target_medical_workload;
//        //超额系数
//        if($percent >= 0.1 && $percent < 0.2){
//            $coefficient = $percent * 1.1;
//        } else if($percent >= 0.2 && $percent <= 0.3){
//            $coefficient = $percent * 1.15;
//        } else if($percent > 0.3){
//            $coefficient = $percent * 1.2;
//        }
        //TODO 超额系数暂定为1
        $coefficient = 1;
        $overWorklod = 0;

        $point_price = BaseData::query()->where('date', $date)
            ->where('department_id', $department_id)->value('point_price');
        //医技工作量绩效=（实际工作量*点数+超额工作量*点数*超额系数）*点值
        $workLoad = ($medicalPoint + $overWorklod * $medicalPoint * $coefficient) * $point_price;

        return round($workLoad, 2);
    }

    /**
     * 获取医技科室下二级科室
     * @param $type
     * @param $department_id
     * @return int
     */
    public function getSubDepartments($department_id){
        //只计算检验科、放射科、CT室、磁共振、病理科、 特检科
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("type_lv1", DepartmentType::MEDICAL_TECHNICAL)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();
        return $department_ids;
    }


    /**
     * 计算实际工作量
     * @param $date
     * @param $department_id
     * @param $department_ids
     * @return mixed
     */
    public function getActualWorkload($date, $department_id, $department_ids){
        $arrDate = explode('-', $date);

        $query = MainIncome::date($date)->whereIn("execute_department_id", $department_ids)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereNotIn("item_cate_code", [8, 64, 54, 46]);//治疗费、材料费、其他、注射费

        if($department_id == 251){ //病理科 额外不包括 项目编码为 1242 的项目
            $query = $query->where("item_code", "<>", 1242);
        } else if($department_id == 252){//特检科 额外不包括 项目编码为 2665、9419、9422 的项目
            $query = $query->whereNotIn("item_code", [2665, 9419, 9422]);
        }

        $count = $query->count();

        return $count;
    }


    /**
     * 查询医技工作量点数
     * @param $date
     * @param $department_id
     * @param $department_ids
     * @return mixed
     */
    public function getMedicalPoint($date, $department_id){
        $arrDate = explode('-', $date);
        $department_ids = $this->getSubDepartments($department_id);
        if (empty($department_ids)){
            return 0;
        }

        $arrDate = explode('-', $date);
        $new_table = "main_incomes" . $arrDate[0] . $arrDate[1];

        if ($department_id == 247){
            $point = MainIncome::date($date)->whereIn('execute_department_id',$department_ids)
                ->where('item_cate_code','!=',64)
                ->count();
            return $point;
        }

        $query = MainIncome::date($date)->leftjoin(
            'medical_point_values', 'medical_point_values.code', '=', $new_table.'.item_code'
            )
            ->leftjoin(
                'surgical_point_values', 'surgical_point_values.code', '=', $new_table.'.item_code'
            )
            ->whereIn($new_table.".execute_department_id", $department_ids)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereNotIn("item_cate_code", [8, 64, 54, 46]);//治疗费、材料费、其他、注射费

        if($department_id == 251){ //病理科 额外不包括 项目编码为 1242 的项目
            $query = $query->where("item_code", "<>", 1242);
        } else if($department_id == 252){//特检科 额外不包括 项目编码为 2665、9419、9422 的项目
            $query = $query->whereNotIn("item_code", [2665, 9419, 9422]);
        }

        $point = $query->selectRaw("if(medical_point_values.performance_weights, 
        medical_point_values.performance_weights * $new_table.quantity, 
        surgical_point_values.performance_weights * $new_table.quantity)  as sub")
            ->where(function ($q){
                $q->whereNotNull('medical_point_values.performance_weights')
                    ->orWhereNotNull('surgical_point_values.performance_weights');
            })
            ->get()->sum("sub");

        return $point;
    }




}

<?php

namespace App\Repositories;

use App\Models\MainIncome;
use App\Models\MedicalPointValue;

class MedicalPointValueRepositoryEloquent extends BaseRepository {
    protected $actionClass = MedicalPointValue::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with("executeDepartment:id,name,code");
        $query = $query->join('sub_departments', 'medical_point_values.execute_department_id', '=', 'sub_departments.id');

        //项目名称
        $name = request("name", null);

        if (!empty($name)) {
            $query = $query->where("medical_point_values.name", "like", "%$name%");
        }

        //项目编码
        $project_code= request("project_code", null);

        if (!empty($project_code)) {
            $query = $query->where("project_code", "like", "%$project_code%");
        }

        //编码
        $code= request("code", null);

        if (!empty($code)) {
            $query = $query->where("medical_point_values.code", "like", "%$code%");
        }
        //国标编码
        $gb_code= request("gb_code", null);

        if (!empty($gb_code)) {
            $query = $query->where("gb_code", "like", "%$gb_code%");
        }

        //执行科室
        $execute_department_id = request("execute_department_id", null);

        if (!empty($execute_department_id)) {
            $query = $query->where("execute_department_id", $execute_department_id);
        }

        $query->select('medical_point_values.*');

        return $query;
    }

    public function getDiff(){
        $date = request("date", date("Y-m"));
        $arrDate = explode('-', $date);
        $diffData = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereNotIn("item_cate_code", [2, 3, 4, 82, 15, 17, 64, 14])//西药费、中成药、中草药、中草药、手术材料费、血费、材料费、手术费
            ->whereIn('execute_department_id',[101,106,150,152,153,154,155,158,159,160,405,462])
            ->leftJoin(\DB::raw("(select code,id from medical_point_values) as mv"),'code','=','item_code')
            ->whereNull('mv.id')
            ->select("item_code", "item_name",'mv.id')
            ->groupBy("item_code", "item_name" ,'mv.id')
            ->get();

        return $diffData;
    }

    public function parseListResult($query)
    {
        $perPage = request("perPage", 25);
        $sort_str = $this->getSortStr();
        $sort_order = request("sort_order", "desc");
        $allPage = request("allPage", 1);

        if ($sort_str == 'execute_department_code'){
            $sort_str = 'sub_departments.code';
        }

        $query = $query->orderBy($sort_str, $sort_order);

        if ($allPage == 2) {
            return $query->get();
        } else {
            return $query->paginate($perPage);
        }
    }
}

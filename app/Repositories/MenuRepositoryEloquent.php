<?php

namespace App\Repositories;

use App\Models\Menu;
use App\Models\EmployeeRole;
use App\Models\RolePermission;

class MenuRepositoryEloquent extends BaseRepository {
    protected $actionClass = Menu::class;

    protected $employeeRepository = null;

    public function __construct(
        EmployeeRepositoryEloquent $employeeRepository
    ) {
        $this->employeeRepository = $employeeRepository;
    }

    public function getList() {
        $query = parent::getList();

        $parent_id = request("parent_id", null);

        if (!empty($parent_id)) {
            $query = $query->where("parent_id", $parent_id);
        }

        // $query = $query->with("sub_menus");

        return $query;
    }

    public function getUserAllowMenuList($userId) {
        $userPermQuery = $this->employeeRepository
            ->getPermissionIdList($userId);

        $userPerms = $userPermQuery
            ->pluck("permissions_id")
            ->all();

        $result = $this->getQuery()
            ->whereNull("parent_id")
            ->whereIn("permissions_id", $userPerms)
            ->with(["sub_menus" => function ($query) use ($userPerms){
                $query->whereIn("permissions_id", $userPerms)
                ->orderBy('order', 'asc');
            }])
            ->orderBy('order', 'asc')
            ->get();

        return $result;
    }
}

<?php

namespace App\Repositories;

use App\Enums\DepartmentType;
use App\Models\WithoutPay;
use Carbon\Carbon;
use App\Models\MainIncome;
use App\Enums\MaterialType;
use App\Models\Department;
use App\Models\SubDepartment;
use App\Models\DirectCost;
use App\Models\DisinfectCost;
use App\Models\ElectricCost;
use App\Models\Employee;
use App\Models\EmployeeAttendance;
use App\Models\EmployeeDispatchPayroll;
use App\Models\EmployeeInsuranceBase;
use App\Models\EmployeePayroll;
use App\Models\FixedAssets;
use App\Models\LaundryCost;
use App\Models\UsedMaterial;

class DirectCostRepositoryEloquent extends BaseRepository {
    protected $actionClass = DirectCost::class;

    /**
     * 生成直接费用统计数据
     */
    public function generateData($date) {
        // department_id = 一级科室ID

        $departments = Department::where("is_performance", true)
            ->with('subDepartment')
            ->get();
        foreach ($departments as $department) {
            if ($department->subDepartment->type_lv1 == DepartmentType::FUNCTIONAL){
                continue;
            }

            $data = [
                "date" => $date,
                "departments_id" => $department->id,
                "base_salary" => $this->getBaseSalary($date, $department->id),
                "social_security" => $this->getSocialSecurity($date, $department->id),
                "loabor_dispatch" => $this->getLaborDispatch($date, $department->id),
                "canteen" => $this->getCanteen($date, $department->id),
                "house_fee" => $this->getHouseFee($date, $department->id),
                "device_depreciation" => $this->getDeviceDepreciation($date, $department->id),
                "property_management" => $this->getPropertyManagement($date, $department->id),
                "device_maintenance" => $this->getDeviceMaintenance($date, $department->id),
                "sewage_disposal" => $this->getSewageDisposal($date, $department->id),
                "sanitary_material" => $this->getSanitaryMaterial($date, $department->id),
                "affairs_material" => $this->getAffairsMaterial($date, $department->id),
                "disinfect" => $this->getDisinfect($date, $department->id),
                "laundry" => $this->getLaundry($date, $department->id),
                "utilities" => $this->getUtilities($date, $department->id),
                "other_maintain" => $this->getOtherMaintenance($date, $department->id),
            ];
            DirectCost::create($data);
        }
    }

    /**
     * 计算基础工资
     */
    public function getBaseSalary($date, $department_id) {
        $withoutPay = WithoutPay::where("date", $date)->get()
            ->pluck("code")
            ->all();
        // 获取一级科室职员考勤系数
        $attendances = EmployeeAttendance::where("departments_id", $department_id)
            ->where("date", $date)
            ->whereNotIn("code", $withoutPay)
            ->get()
            ->pluck("actual_attendance", "employees_id")
            ->all();
        if (empty($attendances)) {
            return floatval(0);
        }

        // 查询他们的基础工资（应发合计）
        $payrolls = EmployeePayroll::whereIn(
            "employees_id", array_keys($attendances)
        )->where("date", $date)
        ->select("total_payable", "employees_id")
        ->get()
        ->pluck("total_payable", "employees_id")
        ->all();

        // 基础工资 * 考勤系数 = 这个职员在这个部门里的基础工资
        $sum_salary = 0;

        // 遍历职员考勤系数，key=职员id, value=结合考勤系数
        foreach ($attendances as $employee_id => $attendanceDays) {
            // 基础工资
            $payroll = $payrolls[$employee_id] ?? 0;

            //当月天数
            $days = date("t", strtotime($date));
//            $days = cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($date)),date('Y',strtotime($date)));
            // 基础工资 * 考勤系数
            $employee_salary = round(
                $payroll * floatval($attendanceDays / $days),
                2
            );

            // 合并科室每个职员的工资
            $sum_salary += $employee_salary;
        }

        return $sum_salary;

    }

    /**
     * 计算五险一金
     */
    public function getSocialSecurity($date, $department_id) {
        $withoutPay = WithoutPay::where("date", $date)->get()
            ->pluck("code")
            ->all();
        // 获取一级科室职员考勤天数
        $attendances = EmployeeAttendance::leftjoin("employees", "employees.id", "=", "employee_attendances.employees_id")
            ->where("departments_id", $department_id)
            ->where("date", $date)
            ->get()
            ->pluck("actual_attendance", "employees_id")
            ->all();
        if (empty($attendances)) {
            return floatval(0);
        }
        //当月天数
         $days = date("t", strtotime($date));
        // $days = cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($date)),date('Y',strtotime($date)));

        // 查询人员保险基数
        $insurances = EmployeeInsuranceBase::whereIn(
            "employees_id", array_keys($attendances)
        )
        ->whereNotIn("code", $withoutPay)
        ->get()
        ->keyBy("employees_id")
        ->all();

        $sum_insurance = 0;

        // 遍历保险基数, key=职员ID, value=基数数据集
        foreach ($insurances as $employee_id => $bases) {
            // 养老保险 单位16%
            $endowment_insurance = round(
                $bases["endowment_insurance"] * 0.16,
                1
            );
            // 医疗保险 单位8.5%
            $medical_insurance = round(
                $bases["four_insurance"] * 0.085,
                1
            );
            // 失业保险 单位0.7%
            $unemployment_insurance = round(
                $bases["four_insurance"] * 0.007,
                1
            );

            // 工伤保险 单位0.2%
            $injury_insurance = round(
                $bases["four_insurance"] * 0.002,
                1
            );
            // 职业年金 单位8%
            $occupation_pension = round(
                $bases["occupation_pension"] * 0.08,
                1
            );
            // 大病保险 15每人
            $major_illness_insurance = 15;
            // 住房公积金 与工资表中住房公积金项一致
            $payroll = EmployeePayroll::where("date", $date)
                ->where("employees_id", $employee_id)
                ->first();
            if ($payroll) {
                $house_fund = (float) $payroll->house_fund;
            } else {
//                $house_fund = 0;
                continue;
            }

            // 科室五险一金成本合并
            $employee_insurance = array_sum([
                $endowment_insurance,
                $medical_insurance,
                $unemployment_insurance,
                $injury_insurance,
                $occupation_pension,
                $major_illness_insurance,
                $house_fund
            ]);

            $coefficient = $attendances[$employee_id] / $days;

            $sum_insurance += round(
                $employee_insurance * $coefficient,
                1
            );
        }

        return $sum_insurance;

    }

    /**
     * 计算劳务派遣人员经费
     */
    public function getLaborDispatch($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->get()
            ->pluck("id")
            ->all();
        if (empty($department_ids)) {
            return 0;
        }
        // 计算二级科室劳务派遣人员的费用（应发合计+管理费）
        return EmployeeDispatchPayroll::whereIn("sub_departments_id", $department_ids)
            ->where("date", $date)
            ->get()
            ->sum(function ($payroll){
//                return $payroll->total_payable + $payroll->management_cost;
                return $payroll->total_payable;
            });

    }

    /**
     * 计算食堂费
     */
    public function getCanteen($date, $department_id) {
        $withoutPay = WithoutPay::where("date", $date)->get()
            ->pluck("code")
            ->all();
        // 普通职员人数（通过职员考勤表计算）按考勤系数相加计算
        $staff_count = EmployeeAttendance::where(
            "departments_id", $department_id
        )->where("date", $date)
        ->whereNotIn("code", $withoutPay)
        ->sum("actual_attendance");
        //当月天数
        $days = date("t", strtotime($date));
//        $days = cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($date)),date('Y',strtotime($date)));
        // 每人500
        return ($staff_count / $days) * 500;

    }

    /**
     * 计算房屋使用费
     */
    public function getHouseFee($date, $department_id) {
        $department = Department::where("id", $department_id)->first();
        if (empty($department)) {
            return 0;
        }

        $price = $department->house_price;
        $space = $department->house_space;

        return round(
            $price * $space,
            2
        );
    }

    /**
     * 物业管理费
     */
    public function getPropertyManagement($data, $department_id) {
        // 房屋面积 * 物业管理费单价
        // 物业管理费单价=1
        $department = Department::where("id", $department_id)->first();
        if (empty($department)) {
            return 0;
        }

        $space = $department->house_space;

        return $space * 1;
    }

    /**
     * 计算设备折旧费
     */
    public function getDeviceDepreciation($date, $department_id) {
        // 价格 - 月份数 * （价格 / (5 * 12))

        // 获取一级科室的二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->get()
            ->pluck("id")
            ->all();
        if (empty($department_ids)) {
            return 0;
        }

        // 查询科室固定资产金额
        $assets = FixedAssets::whereIn(
            "sub_departments_id", $department_ids
        )->where('store_date', '<=', Carbon::parse($date)->lastOfMonth())->get();

        $dt = Carbon::createFromFormat("Y-m-d", $date . "-01");

        $sum_fee = 0;
        foreach ($assets as $item) {
            $storeDt = Carbon::createFromFormat(
                "Y-m-d", $item->store_date
            );

            $diffMonth = $dt->diffInMonths($storeDt);
            $month = 60;
            if ($item->amount >= 1000000) $month = 120;
            if ($diffMonth >= $month) {
                // 60个月后不计算成本
                continue;
            } else {
                $data = 5;
                if ($item->amount >= 1000000){
                    $data = 10;
                }
                $fee =  $item->amount / ($data * 12);
                $sum_fee += $fee;
            }
        }

        return round($sum_fee, 2);


    }

    /**
     * 计算设备维护费
     */
    public function getDeviceMaintenance($date, $department_id) {
        /* 下面的部分是折旧费的计算
        // 价格 - 月份数 * （价格 / (5 * 12))

        // 获取一级科室的二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->get()
            ->pluck("id")
            ->all();
        if (empty($department_ids)) {
            return 0;
        }

        // 查询科室固定资产金额
        $assets = FixedAssets::whereIn(
            "sub_departments_id", $department_ids
        )->get();

        $dt = Carbon::createFromFormat("Y-m-d", $date . "-01");

        $sum_fee = 0;
        foreach ($assets as $item) {
            $storeDt = Carbon::createFromFormat(
                "Y-m-d", $item->store_date
            );

            $diffMonth = $dt->diffInMonths($storeDt);
            if ($diffMonth >= 60) {
                // 60个月后不计算成本
                continue;
            } else {
                $fee = $item->amount - $diffMonth * (
                    $item->amount / (5 * 12)
                );
                $sum_fee += $fee;
            }
        }

        return round($sum_fee, 2);
        */

        // 设备维护费按照上年的计算
        $department = Department::where("id", $department_id)->first();
        if (empty($department)) {
            return 0;
        }

        return $department->device_maintain ?? 0;

    }

    /**
     * 计算设备维护费
     */
    public function getOtherMaintenance($date, $department_id) {

        $department = Department::where("id", $department_id)->first();
        if (empty($department)) {
            return 0;
        }

        return $department->other_maintain ?? 0;

    }

    /**
     * 计算污水处理费
     */
    public function getSewageDisposal($date, $department_id) {
        // 暂时不计算污水处理费
        return 0;
    }

    /**
     * 计算非计价卫生材料
     */
    public function getSanitaryMaterial($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->get()
            ->pluck("id")
            ->all();
        if (empty($department_ids)) {
            return 0;
        }

        // 查询科室材料费用
        $materials = UsedMaterial::whereIn(
            "sub_departments_id", $department_ids
        )->where("date", $date)
        ->where(
            "material_type", MaterialType::SANITARY
        )->sum("total_amount");

        //查询科室当月材料收入
        $mater_income = MainIncome::date($date)->whereIn(
            "execute_department_id", $department_ids
        )->whereIn("item_cate_code", [64, 15, 17])//"64.材料费", "15.手术材料", "17.血费"
         ->sum("amount");

        return $materials - $mater_income;
    }

    /**
     * 计算总务材料
     */
    public function getAffairsMaterial($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->get()
            ->pluck("id")
            ->all();
        if (empty($department_ids)) {
            return 0;
        }

        // 查询科室材料费用
        $materials = UsedMaterial::whereIn(
            "sub_departments_id", $department_ids
        )->where("date", $date)
        ->where(
            "material_type", MaterialType::GENERAL
        )->get();

        return $materials->sum("total_amount");
    }

    /**
     * 计算消毒费
     */
    public function getDisinfect($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->get()
            ->pluck("id")
            ->all();
        if (empty($department_ids)) {
            return 0;
        }

        // 查询科室消毒费
        $costs = DisinfectCost::whereIn(
            "sub_departments_id", $department_ids
        )->where("date", $date)
        ->get();

        return $costs->sum("amount");
    }

    /**
     * 计算洗涤费
     */
    public function getLaundry($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->get()
            ->pluck("id")
            ->all();
        if (empty($department_ids)) {
            return 0;
        }

        // 查询科室洗涤费
        $costs = LaundryCost::whereIn(
            "sub_departments_id", $department_ids
        )->where("date", $date)
        ->get();

        return $costs->sum("amount");
    }

    /**
     * 计算水电气费
     */
    public function getUtilities($date, $department_id) {
        // 暂时不计算水费和气费，仅计算电费

        // 获取一级科室的二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->get()
            ->pluck("id")
            ->all();
        if (empty($department_ids)) {
            return 0;
        }

        // 查询科室电费
        $costs = ElectricCost::whereIn(
            "sub_departments_id", $department_ids
        )->where("date", $date)
        ->get();

        // 电费按单价1元计算
        return $costs->sum("dosage") * 1;
    }

    /**
     * 获取直接费用列表
     * @return mixed
     */
    public function getList() {
        $query = parent::getList();

        $department_id = request("department_id", null);
        $query = $query->with("department:id,name,code");
        $query = $query->join('departments', 'direct_costs.departments_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'direct_costs.date', '=', 'lock_performances.date');

        $date = request("date", null);

        if (!empty($department_id)) {
            $query = $query->where("departments_id", $department_id);
        }
        if (!empty($date)) {
            $query = $query->where("direct_costs.date", $date);
        }

        $code= request("code", null);
        if(!empty($code)){
            $query = $query->where("departments.code", $code);
        }

        $query->select('direct_costs.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");
        return $query;
    }

    /**
     * 计算基础工资详情
     */
    public function getBaseSalaryDetail($date, $department_id) {
        $withoutPay = WithoutPay::where("date", $date)->get()
            ->pluck("code")
            ->all();
        $attendances = EmployeeAttendance::leftJoin('employee_payrolls', function ($join) {
            $join->on('employee_attendances.employees_id', '=', 'employee_payrolls.employees_id')
                ->on('employee_attendances.date', '=', 'employee_payrolls.date');
            })
            ->leftjoin("employees", "employees.id", "=", "employee_attendances.employees_id")
            ->where("departments_id", $department_id)
            ->where("is_retire", 0)
            ->where("employee_attendances.date", $date)
            ->whereNotIn("employee_attendances.code", $withoutPay)
            ->select("name","actual_attendance","total_payable","employees.code")
            ->get()
            ->toArray();

        foreach ($attendances as &$attendance) {
            //当月天数
            $days = date("t", strtotime($date));
            $coefficient = round($attendance["actual_attendance"] / $days, 2);
            $attendance['coefficient'] = $coefficient;
            $attendance['act_payable'] = round($attendance["actual_attendance"] / $days * $attendance['total_payable'], 2);
        }

        return $attendances;

    }

    /**
     * 计算五险一金详情
     */
    public function getSocialDetail($date, $department_id) {
        $withoutPay = WithoutPay::where("date", $date)->get()
            ->pluck("code")
            ->all();
        // 获取一级科室职员考勤天数
        $attendances = EmployeeAttendance::leftjoin("employees", "employees.id", "=", "employee_attendances.employees_id")
            ->where("departments_id", $department_id)
            ->where("date", $date)
            ->get()
            ->pluck("actual_attendance", "employees_id")
            ->all();
        if (empty($attendances)) {
            return floatval(0);
        }
        //当月天数
         $days = date("t", strtotime($date));
        // $days = cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($date)),date('Y',strtotime($date)));

        // 查询人员保险基数
        $insurances = EmployeeInsuranceBase::leftjoin("employees", "employees.id", "=", "employee_insurance_bases.employees_id")
            ->whereIn("employees_id", array_keys($attendances))
            ->whereNotIn("employee_insurance_bases.code", $withoutPay)
            ->get()
            ->keyBy("employees_id")
            ->all();

        $sum_insurance = 0;

        // 遍历保险基数, key=职员ID, value=基数数据集
        foreach ($insurances as $employee_id => $bases) {
            // 养老保险 单位16%
            $endowment_insurance = round(
                $bases["endowment_insurance"] * 0.16,
                1
            );
            // 医疗保险 单位8.5%
            $medical_insurance = round(
                $bases["four_insurance"] * 0.085,
                1
            );
            // 失业保险 单位0.7%
            $unemployment_insurance = round(
                $bases["four_insurance"] * 0.007,
                1
            );

            // 工伤保险 单位0.2%
            $injury_insurance = round(
                $bases["four_insurance"] * 0.002,
                1
            );
            // 职业年金 单位8%
            $occupation_pension = round(
                $bases["occupation_pension"] * 0.08,
                1
            );
            // 大病保险 15每人
            $major_illness_insurance = 15;
            // 住房公积金 与工资表中住房公积金项一致
            $payroll = EmployeePayroll::where("date", $date)
                ->where("employees_id", $employee_id)
                ->first();
            if ($payroll) {
                $house_fund = (float) $payroll->house_fund;
            } else {
                $house_fund = 0;
                $endowment_insurance = 0;
                $medical_insurance = 0;
                $unemployment_insurance = 0;
                $injury_insurance = 0;
                $occupation_pension = 0;
                $major_illness_insurance = 0;
            }

            // 科室五险一金成本合并
            $employee_insurance = array_sum([
                $endowment_insurance,
                $medical_insurance,
                $unemployment_insurance,
                $injury_insurance,
                $occupation_pension,
                $major_illness_insurance,
                $house_fund
            ]);


            $coefficient = $attendances[$employee_id] / $days;

            $insurances[$employee_id]["endowment_insurance"] = round($endowment_insurance * $coefficient, 2);
            $insurances[$employee_id]["medical_insurance"] = round($medical_insurance * $coefficient, 2);
            $insurances[$employee_id]["unemployment_insurance"] = round($unemployment_insurance * $coefficient, 2);
            $insurances[$employee_id]["injury_insurance"] = round($injury_insurance * $coefficient, 2);
            $insurances[$employee_id]["occupation_pension"] = round($occupation_pension * $coefficient,2);
            $insurances[$employee_id]["major_illness_insurance"] = round($major_illness_insurance * $coefficient, 2);
            $insurances[$employee_id]["house_fund"] = round($house_fund * $coefficient, 2);
            $insurances[$employee_id]["coefficient"] = round($coefficient, 2);
            $insurances[$employee_id]["sum"] = round($employee_insurance * $coefficient, 2);

        }

        return $insurances;

    }

    public function delData(){
        $date = request("date", date("Y-m"));
        DirectCost::where("date", $date)->delete();
    }
}

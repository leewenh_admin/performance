<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Models\EmployeeAttendance;
use App\Models\EmployeeInsuranceBase;

class EmployeeInsuranceBaseRepositoryEloquent extends BaseRepository {
    protected $actionClass = EmployeeInsuranceBase::class;

    public function getList()
    {
        $query = parent::getList();

        //职员名称
        $name = request("name", null);

        if (!empty($name)) {
            $query = $query->where("name", "like", "%$name%");
        }

        //职员工号
        $code= request("code", null);

        if (!empty($code)) {
            $query = $query->where("code", "like", "%$code%");
        }

        //日期
        $date= request("date", null);

        if (!empty($date)) {
            $query = $query->where("date", $date);
        }

        return $query;
    }


    /**
     * 核查人员保险基数缺失数据
     * @param $data
     * @return mixed
     */
    public function check($data){
        $date = $data['date'];

        $employee = EmployeeAttendance::from('employee_attendances as a')
            ->leftjoin('employee_insurance_bases as b', 'a.employees_id', '=', 'b.employees_id')
            ->where('a.date', $date)
            ->whereNull('b.id')
            ->pluck('a.employees_id')
            ->all();

        return Employee::whereIn('id', $employee)
            ->select('id', 'name', 'code')
            ->get();
    }
}

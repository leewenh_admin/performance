<?php

namespace App\Repositories;

use App\Enums\DepartmentType;
use App\Enums\IncomeType;
use App\Models\MainIncome;
use App\Models\NewTechnologyDetail;
use App\Models\NewTechnologyItem;
use App\Models\StrokeCenterPerformance;
use App\Models\SubDepartment;
use App\Models\UnitPhysical;

class NewTechnologyItemRepositoryEloquent extends BaseRepository {
    protected $actionClass = NewTechnologyItem::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "department:id,name,code",
        ]);
        $query = $query->join('departments', 'new_technology_items.department_id', '=', 'departments.id');

        $department_id = request("department_id", null);
        if (!empty($department_id)){
            $query = $query->where("department_id", $department_id);
        }

        $apply_year = request("apply_year", null);
        if (!empty($apply_year)){
            $query = $query->where("apply_year", $apply_year);
        }

        $item_name = request("item_name", null);
        if (!empty($item_name)){
            $query = $query->where("item_name", "like", "%$item_name%");
        }

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("b.date", $date);
        }

        $name = request("name", null);
        if (!empty($name)){
            $query = $query->where("new_technology_items.name", "like", "%$name%");
        }

        $query = $query->from('new_technology_items')->leftjoin('new_technology_details as b', 'new_technology_items.id', '=', 'b.technology_id')
            ->leftjoin('lock_performances', 'b.date', '=', 'lock_performances.date')
            ->select(['department_id','b.id', 'new_technology_items.apply_year', 'b.date', 'new_technology_items.name', 'new_technology_items.code', 'new_technology_items.item_code', 'new_technology_items.item_name', 'b.count', 'b.price', 'b.increase_count', 'b.created_at'])
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");



        return $query;
    }


    /**
     * 单项绩效计算
     * @param $date
     * @param $department_id
     * @return float|int|mixed
     */
    public function getIndividualPerformance($date, $department_id){
        //新技术、业务绩效
        $newTechnologyPerformance = $this->getNewTechnologyPerformance($date, $department_id);
//        //单位体检绩效
//        $checkPerformance = $this->getCheckPerformance($date, $department_id);
//        //卒中中心绩效
//        $strokePerformance = $this->getStrokePerformance($date, $department_id);

        return $newTechnologyPerformance;
    }

    /**
     * 新技术、业务绩效
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getNewTechnologyPerformance($date, $department_id){

        $items = NewTechnologyDetail::leftjoin('new_technology_items as b', 'new_technology_details.technology_id', '=', 'b.id')
                ->where("department_id", $department_id)
                ->where("date", $date)
                ->select(['new_technology_details.count', 'new_technology_details.price', 'apply_year'])
                ->get();

        // 科室有效收入
        $departIncome = $this->getDepartmentIncome($date, $department_id);

        $performance = 0;

        foreach ($items as $item){
            $itemDetail = [];
            //项目收入
            $income = $item->price * $item->count;

            $bonusA = $this->getBonusPerUnitA($date,$item);
            $bonusB = $this->getBonusPerUnitB($income, $departIncome);

            $performance += $item->count * ($bonusA + $bonusB);
        }

        return round($performance, 2);
    }

    /**
     * 一级科室的单位体检绩效
     * @param $date
     * @param $department_id
     * @return mixed
     */
    public function getCheckPerformance($date, $department_id){
        $data = UnitPhysical::where("date", $date)
            ->where("department_id", $department_id)
            ->sum("total_performance");

        return round($data, 2);
    }

    /**
     * 卒中中心绩效
     * @param $date
     * @param $department_id
     * @return mixed
     */
    public function getStrokePerformance($date, $department_id){
        // 获取一级科室的二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();
        $data = StrokeCenterPerformance::where("date", $date)
            ->whereIn("sub_department_id", $department_ids)
            ->sum("punish_amount");

        return round($data, 2);
    }

    /**
     * 更新体检绩效表里面的绩效合计和绩效占比
     * @param $date
     */
    public function generateCheckPerformance($date){
        $checkDatas = UnitPhysical::where("date", $date)
            ->get();

        foreach ($checkDatas as $checkData){
            $total_performance = $checkData->point * $checkData->point_price;
            $performance_percent = $total_performance / $checkData->check_income * 100;
            $data = [
                "total_performance" => round($total_performance, 2),
                "performance_percent" => round($performance_percent, 2),
            ];
            UnitPhysical::where("id", $checkData->id)
                ->update($data);
        }
    }


    /**
     * 单位奖励值a
     * @param $item
     * @return float|int
     */
    public function getBonusPerUnitA($date,$item){
        //计算是第几年扶持期
        //新项目从开始到第二年12月都按第一年计算
        $year = date("Y", strtotime($date)) - $item->apply_year ;
        if ($year == 0){
            $year = 1;
        }
        $bonus = 0;
        if($year == 1){
            $bonus = $item->price * 0.1;
        } else if($year == 2){
            $bonus = $item->price * 0.05;
        } else if($year ==3){
            $bonus = $item->price * 0.03;
        }

        return $bonus;
    }

    /**
     * 单位奖励值b
     * @param $income
     * @param $departIncome
     * @return float|int
     */
    public function getBonusPerUnitB($income, $departIncome){

        //暂时计0
        return 0;

        if ($departIncome == 0){
            return 0;
        }
        $percent = $income / $departIncome;

        $bonus = 0;
        //资产折旧
        $assetDepreciation = $this->getAssetDepreciation();
        //卫材
        $eisai = $this->getEisai();
        if($percent >= 0.1 && $percent < 0.2){
            $bonus = ($income - $assetDepreciation - $eisai) * 0.1;
        } else if($percent >= 0.2 && $percent <= 0.3){
            $bonus = ($income - $assetDepreciation - $eisai) * 0.12;
        } else if($percent > 0.3){
            $bonus = ($income - $assetDepreciation - $eisai) * 0.15;
        }

        return $bonus;
    }

    /**
     * 科室有效收入
     * @param $date
     * @param $department_id
     * @return int
     */
    public function getDepartmentIncome($date, $department_id){
        // 获取一级科室的二级科室ID列表

        $sub_departments = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get();

        $department_ids = $sub_departments->pluck("id")->all();

        if (empty($department_ids)){
            return 0;
        }

        //科室本月收入
        $total_income = MainIncome::date($date)->whereIn(
            "execute_department_id", $department_ids
        )
            ->whereNotIn("item_cate_code", [2, 3, 4, 82, 15, 17, 64])//西药费、中成药、中草药、中草药、手术材料费、血费、材料费
//            ->where('income_type', IncomeType::INPATIENT)
            ->sum("amount");

        if ($sub_departments->first()->type_lv3 == DepartmentType::SURGICAL){//手术科室有效收入加上手术费
            $surgeryInCome = (new CostEffectiveRepositoryEloquent())->getSurgeryInCome($date, $department_id);
            $total_income += $surgeryInCome;
        }

        return $total_income;
    }

    /**
     * 资产折旧
     * @return int
     */
    public function getAssetDepreciation(){
        //暂定0
        return 0;
    }

    /**
     * 卫材
     * @return int
     */
    public function getEisai(){
        //暂定0
        return 0;
    }


    public function insert($data) {
        $insert_data = parent::insert($data);

        NewTechnologyDetail::query()->create([
            'technology_id' => $insert_data->id,
            'date' => $data['date'],
            'count' => $data['count'],
            'price' => $data['price'],
            'increase_count' => $data['increase_count'],
        ]);
    }

    public function update($id,$data) {
        $detail = NewTechnologyDetail::query()->find($id);
        parent::update($detail->technology_id,$data);

        NewTechnologyDetail::query()->where('id', $id)->update([
            'date' => $data['date'],
            'count' => $data['count'],
            'price' => $data['price'],
            'increase_count' => $data['increase_count'],
        ]);
    }

    public function delete($id) {
        $detail = NewTechnologyDetail::query()->find($id);
        parent::delete($detail->technology_id);
        NewTechnologyDetail::query()->where('id', $id)->delete();
    }
}

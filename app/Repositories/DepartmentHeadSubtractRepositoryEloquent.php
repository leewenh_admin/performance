<?php

namespace App\Repositories;

use App\Exceptions\MessageException;
use App\Models\DepartmentHeadSubtract;
use App\Models\SpecialPerformance;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DepartmentHeadSubtractRepositoryEloquent extends BaseRepository {
    protected $actionClass = DepartmentHeadSubtract::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with('department:id,name,code');

        $query = $query->join('departments', 'department_head_subtracts.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'department_head_subtracts.date', '=', 'lock_performances.date');
        $query->select('department_head_subtracts.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if ($date){
            $query = $query->where("department_head_subtracts.date", $date);
        }

        $department_id = request("department_id", null);
        if ($department_id){
            $query = $query->where("department_id", $department_id);
        }

        return $query;
    }

    public function insert($data)
    {
        DB::beginTransaction();
        try {
            parent::insert($data);
            $department_header_performance = (new NightPerformanceCalculateEloquent())->getDepartmentHeaderPerformance($data['date'], $data['department_id']);
            SpecialPerformance::where('date', $data['date'])->where('department_id', $data['department_id'])->update(
                ['department_header_performance' => $department_header_performance]
            );
            DB::commit();
        } catch (\Exception $exception){
            Log::error($exception->getMessage().'--'.$exception->getFile().'--'.$exception->getLine());
            DB::rollBack();
            throw new MessageException($exception->getMessage());
        }
    }

    public function update($id, $data)
    {
        DB::beginTransaction();
        try {
            parent::update($id, $data);
            $department_header_performance = (new NightPerformanceCalculateEloquent())->getDepartmentHeaderPerformance($data['date'], $data['department_id']);
            SpecialPerformance::where('date', $data['date'])->where('department_id', $data['department_id'])->update(
                ['department_header_performance' => $department_header_performance]
            );
            DB::commit();
        } catch (\Exception $exception){
            Log::error($exception->getMessage().'--'.$exception->getFile().'--'.$exception->getLine());
            DB::rollBack();
            throw new MessageException($exception->getMessage());
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $record = DepartmentHeadSubtract::find($id);
            $date = $record->date;
            $department_id = $record->department_id;
            $record->delete();
            $department_header_performance = (new NightPerformanceCalculateEloquent())->getDepartmentHeaderPerformance($date, $department_id);
            SpecialPerformance::where('date', $date)->where('department_id', $department_id)->update(
                ['department_header_performance' => $department_header_performance]
            );
            DB::commit();
        } catch (\Exception $exception){
            Log::error($exception->getMessage().'--'.$exception->getFile().'--'.$exception->getLine());
            DB::rollBack();
            throw new MessageException($exception->getMessage());
        }
    }

}

<?php

namespace App\Repositories;

use App\Models\NightPerformance;

class NightPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = NightPerformance::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "department:id,name,code",
        ]);
        $query = $query->join('departments', 'night_performances.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'night_performances.date', '=', 'lock_performances.date');
        $query->select('night_performances.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $department_id = request("department_id", null);
        if (!empty($department_id)){
            $query = $query->where("department_id", $department_id);
        }

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("night_performances.date", $date);
        }

        $sub_department_name = request("sub_department_name", null);
        if (!empty($sub_department_name)){
            $query = $query->where("sub_department_name", "like", "%$sub_department_name%");
        }


        return $query;
    }
}

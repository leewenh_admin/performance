<?php

namespace App\Repositories;

use App\Models\FixedAssets;
use Illuminate\Support\Facades\DB;

class FixedAssetsRepositoryEloquent extends BaseRepository {
    protected $actionClass = FixedAssets::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with(["subDepartment:id,name,code"]);
        $query = $query->join('sub_departments', 'fixed_assets.sub_departments_id', '=', 'sub_departments.id');

        $query = $query->select("fixed_assets.*",DB::raw("case device_type when 1 then '一般设备' when 2 then '专业设备' when 3 then '计算机设备' 
        when 4 then '家俱设备' when 5 then '制冷设备' when 6 then '交通设备' when 7 then '电器设备' when 8 then '其他设备' end as device_type"));

        $sub_departments_id = request("sub_departments_id", null);
        if (!empty($sub_departments_id)){
            $query = $query->where("sub_departments_id", $sub_departments_id);
        }

        $name = request("name", null);
        if (!empty($name)){
            $query = $query->where("fixed_assets.name", "like", "%".$name."%");
        }

        $code = request("code", null);
        if (!empty($code)){
            $query = $query->where("fixed_assets.code", $code);
        }

        $card = request("card", null);
        if (!empty($card)){
            $query = $query->where("card", $card);
        }

        $store_date = request("store_date", null);
        if (!empty($store_date)){
            $query = $query->where("store_date", $store_date);
        }

        return $query;
    }

    public function parseListResult($query)
    {
        $perPage = request("perPage", 25);
        $sort_str = $this->getSortStr();
        $sort_order = request("sort_order", "desc");
        $allPage = request("allPage", 1);

        if ($sort_str == 'sub_department_code'){
            $sort_str = 'sub_departments.code';
        }
        $query = $query->orderBy($sort_str, $sort_order);

        if ($allPage == 2) {
            return $query->get();
        } else {
            return $query->paginate($perPage);
        }
    }
}

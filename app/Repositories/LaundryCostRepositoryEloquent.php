<?php

namespace App\Repositories;

use App\Models\LaundryCost;

class LaundryCostRepositoryEloquent extends BaseRepository {
    protected $actionClass = LaundryCost::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with("department:id,name,code");
        $query = $query->join('sub_departments', 'laundry_costs.sub_departments_id', '=', 'sub_departments.id')
            ->leftjoin('lock_performances', 'laundry_costs.date', '=', 'lock_performances.date');

        $sub_departments_id = request("sub_departments_id", null);
        if (!empty($sub_departments_id)){
            $query = $query->where("sub_departments_id", $sub_departments_id);
        }

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("laundry_costs.date", $date);
        }
        $query->select('laundry_costs.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");
        return $query;
    }
}

<?php

namespace App\Repositories;

use App\Models\DieaseCatalog;

class DieaseCatalogRepositoryEloquent extends BaseRepository {
    protected $actionClass = DieaseCatalog::class;

    public function getList()
    {
        $query = parent::getList();

        //疾病名称
        $name = request("name", null);

        if (!empty($name)) {
            $query = $query->where("name", "like", "%$name%");
        }

        //疾病编码
        $code= request("code", null);

        if (!empty($code)) {
            $query = $query->where("code", "like", "%$code%");
        }

        return $query;
    }
}

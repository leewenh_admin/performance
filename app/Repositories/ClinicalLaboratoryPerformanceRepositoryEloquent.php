<?php

namespace App\Repositories;

use App\Models\ClinicalLaboratoryPerformance;

class ClinicalLaboratoryPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = ClinicalLaboratoryPerformance::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with("department:id,name,code");
        $query = $query->join('departments', 'clinical_laboratory_performances.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'clinical_laboratory_performances.date', '=', 'lock_performances.date');
        $query->select('clinical_laboratory_performances.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("clinical_laboratory_performances.date", $date);
        }

        $department_id = request("department_id", null);
        if (!empty($department_id)){
            $query = $query->where("department_id", $department_id);
        }

        return $query;
    }
}

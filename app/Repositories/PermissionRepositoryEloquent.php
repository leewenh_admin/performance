<?php

namespace App\Repositories;

use App\Models\Permission;

class PermissionRepositoryEloquent extends BaseRepository {
    protected $actionClass = Permission::class;

    public function getNameListById($permission_id_list) {
        return Permission::whereIn("id", $permission_id_list)
            ->get()
            ->pluck("name")
            ->all();

    }
}

<?php

namespace App\Repositories\PerformanceDetail;

use App\Models\SubDepartment;
use App\Models\OtherPerformance;

/**
 * 其他单项绩效详情数据
 */
class OtherPerformanceDetail extends BaseDetail {
    public $repotitory;

    public function __construct()
    {
    }


    public function getDetail($date, $department_id) {
        $sub_departments = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("name", "id")
            ->all();
        $records = OtherPerformance::whereIn(
            "sub_department_id", array_keys($sub_departments)
            )->where("date", $date)
            ->get();

        $result = [];
        foreach ($records as $record) {
            $temp = [];
            // 工号
            $temp["code"] = $record->code;
            // 姓名
            $temp["name"] = $record->name;
            // 科室
            $temp["department_name"] = $sub_departments[$record->sub_department_id];
            // 元旦天数
            $temp["new_years_day"] = $record->new_years_day . "天";
            // 春节天数
            $temp["spring_festival_day"] = $record->spring_festival_day . "天";
            // 天数
            $temp["days"] = $record->days . "天";
            // 金额
            $temp["amount"] = number_format($record->amount, 2) . "元";
            // 描述
            $temp["description"] = $record->description;

            array_push($result, $temp);
        }

        return $result;

    }
}
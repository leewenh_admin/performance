<?php

namespace App\Repositories\PerformanceDetail;

use App\Repositories\ComprehensivePerformanceRepositoryEloquent;
use App\Models\ClinicalAttendanceScore;
use App\Models\MedicalAttendanceScore;

/**
 * 获取综合考核绩效详情
 */
class ComprehensiveDetail extends BaseDetail {
    // 综合考核绩效绩效计算类
    public $repotitory;

    public function __construct()
    {
        $this->repotitory = new ComprehensivePerformanceRepositoryEloquent();
    }


    public function getDetail($date, $department_id) {
        $result = [];
        // 临床科室综合绩效
        $clinical_part = ClinicalAttendanceScore::where("date", $date)
            ->where("departments_id", $department_id)
            ->first();
        if ($clinical_part) {
            $result["is_clinical"] = true;
            // 考勤人数
            $result["employees_count"] = ($clinical_part->employees_count ?? 0) . "人";
            // 医疗质量维度(55分)
            $result["medical_quality"] = ($clinical_part->medical_quality ?? 0) . "%";
            // 护理质量维度(15分)
            $result["care_quality"] = ($clinical_part->care_quality ?? 0) . "%";
            // 病案质量维度(10分)
            $result["medical_record_quality"] = ($clinical_part->medical_record_quality ?? 0) . "%";
            // 医德医风维度(10分)
            $result["medical_ethics_quality"] = ($clinical_part->medical_ethics_quality ?? 0). "%";
            // 医保管理(5分)
            $result["health_management"] = ($clinical_part->health_management ?? 0) . "%";
            // 科研教学(5分)
            $result["scientific_research"] = ($clinical_part->scientific_research ?? 0) . "%";
            // 总得分
            $result["total_score"] = $clinical_part->total_score ?? 0;
            // 综合绩效
            $result["comprehensive_performance"] = ($clinical_part->comprehensive_performance ?? 0) . "元";
        }

        // 医技科室综合绩效
        $medical_part = MedicalAttendanceScore::where("date", $date)
            ->where("departments_id", $department_id)
            ->first();
        if ($medical_part) {
            $result["is_medical"] = true;
            // 考勤人数
            $result["employees_count"] = ($medical_part->employees_count ?? 0) . "人";
            // 医疗质量维度(60分)
            $result["medical_quality"] = ($medical_part->medical_quality ?? 0) . "%";
            // 护理质量维度(5分)
            $result["care_quality"] = ($medical_part->care_quality ?? 0) . "%";
            // 医德医风维度(10分)
            $result["medical_ethics_quality"] = ($medical_part->medical_ethics_quality ?? 0) . "%";
            // 医保管理(5分)
            $result["health_management"] = ($medical_part->health_management ?? 0) . "%";
            // 科研教学(10分)
            $result["scientific_research"] = ($medical_part->scientific_research ?? 0) . "%";
            // 工作量管理(10分)
            $result["workload_management"] = ($medical_part->workload_management ?? 0) . "%";
            // 总得分
            $result["total_score"] = ($medical_part->total_score ?? 0);
            // 综合绩效
            $result["comprehensive_performance"] = ($medical_part->comprehensive_performance ?? 0) . "元";
        }
        if (array_key_exists("total_score", $result)) {
            // 每分值奖励额
            if($result["total_score"] > 98){
                $reward_per_point = 100;
            } else if ($result["total_score"] >= 92 &&  $result["total_score"] <= 98){
                $reward_per_point = 30;
            } else {
                $reward_per_point = 50;
            }
            $result["reward_per_point"] = $reward_per_point . "元";
        }
        

        return $result;
    }
}
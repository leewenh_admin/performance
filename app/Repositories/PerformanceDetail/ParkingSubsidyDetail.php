<?php

namespace App\Repositories\PerformanceDetail;

use App\Models\SubDepartment;
use App\Models\EmployeeAttendance;
use App\Repositories\DepartmentPerformanceRepositoryEloquent;


/**
 * 停车场补助详情数据
 */
class ParkingSubsidyDetail extends BaseDetail {
    public $repotitory;

    public function __construct()
    {
    }


    public function getDetail($date, $department_id) {
        //科室内考勤天数总和
        $attendanceCoefficient = EmployeeAttendance::where(
            "departments_id", $department_id)
            ->where("date", $date)
            ->sum("actual_attendance");

        $days = date("t", strtotime($date));
        $attendanceCoefficient = $attendanceCoefficient / $days;

        $performance = 600 * $attendanceCoefficient ;

        return [
            // 考勤绩效人数
            "attendanceCoefficient" => round($attendanceCoefficient, 2) . "人",
            // 补助单价
            "subsidy_price" => "600.00元/人/月",
            // 科室补助总额
            "performance" => number_format($performance, 2) . "元"
        ];

    }
}
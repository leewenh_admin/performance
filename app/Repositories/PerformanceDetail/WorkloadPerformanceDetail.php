<?php

namespace App\Repositories\PerformanceDetail;

use App\Enums\DepartmentType;
use App\Enums\IncomeType;
use App\Models\BaseData;
use App\Models\CoreBusinessPoint;
use App\Models\Department;
use App\Models\MainIncome;
use App\Models\EmployeeAttendance;
use App\Repositories\MedicalWorkLoadRepositoryEloquent;
use App\Repositories\WorkLoadRepositoryEloquent;
use Illuminate\Support\Facades\DB;


/**
 * 工作量绩效详情数据
 */
class WorkloadPerformanceDetail extends BaseDetail {
    public $repository;
    public function __construct()
    {
        $this->repository = new WorkLoadRepositoryEloquent();
    }


    public function getDetail($date, $department_id){
        $department = Department::where("id", $department_id)->with('subDepartment')->first();
        $data = [];
        if ($department->subDepartment->type_lv1 == DepartmentType::CLINICAL){
            $department_ids = (new WorkLoadRepositoryEloquent())->getOutPatientDepartments($department_id);

            //门急诊人次
            $outPatientCount = $this->repository->getOutPatientCount($date, $department_id);

            $price = BaseData::query()->where('date', $date)
                ->where('department_id', $department_id)->first();
            //门诊绩效单价
            $outpatientPrice = $price->outpatient_price ?? 0;
            //出院者实际占床日
//            $outHospitalBed = MainIncome::query()->date($date)
//                ->whereIn('execute_department_id',$department_ids)
//                ->whereRaw("left(charging_time,7) =  '".$date."'")
//                ->whereRaw("left(leave_time,6) =  '".str_replace('-', '', $date)."'")
//                ->where('item_cate_code',1)
//                ->whereNotIn('item_code',[1034,1031,1030,1033])
//                ->sum('quantity');
            $outHospitalBed = (new WorkLoadRepositoryEloquent())->getOutHospitalBed($date,$department_ids);
            //出院人数
            $outHospitalCount = (new WorkLoadRepositoryEloquent())->getOutHospitalCount($date, $department_ids);
            //实际平均住院日
            if ($outHospitalCount == 0){
                $avgHosDay = 0;
            }else{
                $avgHosDay = round($outHospitalBed/$outHospitalCount,2);
            }
            //实际占床日
            $inHospitalBed = (new WorkLoadRepositoryEloquent())->getBedCount($date,$department_ids, $department_id);
            //基准平均住院日
            $baseAvgHosDay = $department["base_avg_hos_days"];
            //床日系数
            if ($baseAvgHosDay == 0) return 0;
            if($avgHosDay < $baseAvgHosDay* 0.95){
                $bedDayCoefficient =  round($avgHosDay / ($baseAvgHosDay* 0.95),2);
            }elseif ($avgHosDay > $baseAvgHosDay*1.05){
                $bedDayCoefficient = 2 - round($avgHosDay/($baseAvgHosDay*1.05),2);
            }else{
                $bedDayCoefficient = 1;
            }
            //护理时数
            $nursingHour = (new WorkLoadRepositoryEloquent())->getNursingHour($date, $department_id);
            //住院绩效单价
            $inPerformancePrice = $price->admission_price ?? 0;

            $data = [
                "outPatientCount" => number_format($outPatientCount, 2)."人",
                "outpatientPrice" => number_format($outpatientPrice, 2)."元",
                "outHospitalBed" => number_format($outHospitalBed, 2)."天",
                "outHospitalCount" => number_format($outHospitalCount, 2)."人",
                "avgHosDay" => number_format($avgHosDay, 2)."天",
                "inHospitalBed" => number_format($inHospitalBed, 2)."天",
                "baseAvgHosDay" => number_format($baseAvgHosDay, 2)."天",
                "bedDayCoefficient" => number_format($bedDayCoefficient, 2),
                "nursingHour" => number_format($nursingHour, 2),
                "inPerformancePrice" => number_format($inPerformancePrice, 2).'元',
            ];
        } elseif ($department->subDepartment->type_lv1 == DepartmentType::MEDICAL_TECHNICAL){
            $obj = new MedicalWorkLoadRepositoryEloquent();
            //实际工作量点数
            $data = CoreBusinessPoint::where("date", $date)->where("department_id", $department_id)->first();
            if($data){
                //核心业务点数单价
                $medicalPoint = $data->point;
            } else {
                $medicalPoint = $obj->getMedicalPoint($date, $department_id);
            }
            //超额工作量点数
            $overPoint = 0;
            //点值
            $medicalPointPrice = BaseData::query()->where('date', $date)
                ->where('department_id', $department_id)->value('point_price');

            $data = [
                "medicalPoint" => number_format($medicalPoint, 2),
                "overPoint" => $overPoint,
                "medicalPointPrice" => number_format($medicalPointPrice, 2)."元",
            ];
        }

        return $data;

    }


    /**
     * 门诊例均费用系数
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getOutAverageCost($date, $department_id)
    {
        // 获取一级科室的门诊类型二级科室ID列表
//        $department_ids = $this->repotitory->getOutPatientDepartments($department_id);
//        if (empty($department_ids)) {
//            return 0;
//        }

        //科室内考勤的医生
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->get()
            ->pluck("employees_id")
            ->all();
        //本期门诊均例费用 = （门诊收入 / 门急诊人次）
        //计算门诊收入
        $arrDate = explode('-', $date);
        $outPatientCountIncome = MainIncome::date($date)->whereIn(
            "prescribe_doctor_id", $doctors
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::OUTPATIENT)
            ->sum("amount");

        //门诊人数
        $outPatientCount = $this->repotitory->getOutPatientCount($date, $department_id);
        if ($outPatientCount == 0) {
            return 0;
        }
        //本期门诊均例费用
        $thisAverageCost = $outPatientCountIncome / $outPatientCount;
        //目标均次费用
        $targetAverageCost = Department::where("id", $department_id)
            ->value("target_average_cost");

        if ($targetAverageCost == 0 || $thisAverageCost == 0) {
            return 0;
        }

        if ($thisAverageCost > $targetAverageCost * 1.1) {
            $outAverageCost = $targetAverageCost / $thisAverageCost;
        } else if ($thisAverageCost >= $targetAverageCost * 0.9
            && $thisAverageCost <= $targetAverageCost * 1.1) {
            $outAverageCost = 1;
        } else {
            $outAverageCost = $thisAverageCost / $targetAverageCost;
        }

        $outAverageCost = round($outAverageCost, 4);

        $data = [
            "targetAverageCost" => $targetAverageCost, //门诊目标均次费用
            "thisAverageCost" => $thisAverageCost, //门诊本期均次费用
            "outAverageCost" => $outAverageCost,    //门诊例均费用系数
        ];

        return $data;

    }


    /**
     * 门诊科室间人均效率的横向比较
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getPerCapitaHorizontalComparison($date, $department_id)
    {
        // 获取一级科室的门诊类型二级科室ID列表
        $type = DepartmentType::OUTPATIENT;
        $department_ids = $this->repotitory->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }

        //本月科室医生人数
        $departmentDoctorNum = EmployeeAttendance::leftjoin("employees", "employees.id", "employee_attendances.employees_id")
            ->whereIn("sub_departments_id", $department_ids)
            ->where("date", $date)
            ->where("employees.attendances_type", 1)
            ->whereRaw("employee_attendances.actual_attendance = (select max(t.actual_attendance) 
            from employee_attendances as t where 
            employee_attendances.employees_id = t.employees_id and t.date = '$date') ")
            ->count();

        //科室门诊人数
        $outPatientCount = $this->repotitory->getOutPatientCount($date, $department_id);
        //全院门诊人数
        $arrDate = explode('-', $date);

        $outPatientAllCount = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::OUTPATIENT)
            ->whereIn("item_code", [1002, 1003, 1006, 1008, 1010])
            ->select("serial_no")
            ->distinct()
            ->get()
            ->count();
        //全院医生人次   TODO::职员表还需要加一个职称分类来筛选考勤类型是医生
        $allDoctorNum = EmployeeAttendance::leftjoin("employees", "employees.id", "employee_attendances.employees_id")
            ->where("date", $date)
            ->where("employees.attendances_type", 1)
            ->select("employees_id")
            ->groupBy("employees_id")
            ->get()
            ->count();

        if ($departmentDoctorNum == 0 || $allDoctorNum == 0) {
            return 0;
        }
        //科室医生人均门诊人次 = 科室门诊人次 / 科室医生人数
        $departmentWorkNum = $outPatientCount / $departmentDoctorNum;
        //全院医生人均门诊人次 = 全院门诊人次 / 全院医生人次
        $hospitalWorkTime = $outPatientAllCount / $allDoctorNum;
        //门诊科室间人均效率横向比较 = 科室医生人均门诊人次 / 全院医生人均门诊人次
        if ($hospitalWorkTime == 0) {
            return 0;
        }
        $perCapitaHorizontalComparison = $departmentWorkNum / $hospitalWorkTime;

        $perCapitaHorizontalComparison = round($perCapitaHorizontalComparison, 4);

        $data = [
            "departmentWorkNum" => $departmentWorkNum,//科室医生人均门诊人次
            "hospitalWorkTime" => $hospitalWorkTime,//全院医生人均门诊人次
            "perCapitaHorizontalComparison" => $perCapitaHorizontalComparison,//门诊科室间人均效率横向比较
        ];

        return $data;
    }

    /**
     * 药品例均费用
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getEachDrugCost($date, $department_id)
    {
        // 获取一级科室的门诊类型二级科室ID列表
        $type = DepartmentType::OUTPATIENT;
        $department_ids = $this->repotitory->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        //门诊人数
        $outPatientCount = $this->repotitory->getOutPatientCount($date, $department_id);
        $arrDate = explode('-', $date);
        //药品收入,按开单科室来计算
        //TODO 目前只统计住院的药品收入实际值过小
        $drugInCome = MainIncome::date($date)->whereIn(
            "prescribe_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::OUTPATIENT)
            ->whereIn("item_cate_code", [2, 3, 4, 82])//西药费,中成药,中草药
            ->sum("amount");

        $target = Department::where("id", $department_id)
            ->select("target_average_drug_cost", "punish_amount")
            ->first();
        //出院人数
        $leaverCount = $this->repotitory->getOutHospitalCount($date, $department_ids);
        if ($leaverCount == 0) {
            return 0;
        }
        $eachDrugCost = $drugInCome / $leaverCount;
        if ($eachDrugCost > $target->target_average_drug_cost){
            $value = ($eachDrugCost - $target->target_average_drug_cost) * $outPatientCount * $target->punish_amount;
        } else {
            $value = 0;
        }

        $value = round($value, 4);
        $data = [
            "eachDrugCost" => $eachDrugCost, //本期药品例均费用
            "target_average_drug_cost" => $target->target_average_drug_cost, //目标药品例均费用
            "punish_amount" => $target->punish_amount, //扣罚金额
            "value" => $value, //药品例均费用控制
        ];

        return $data;
    }


    /**
     * 住院科室间人均效率的横向比较
     * @param $date
     * @param $department_id
     * @return
     */
    public function getInHospitalHorizontalComparison($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
        $type = DepartmentType::WARD;
        $department_ids = $this->repotitory->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        //todo
        //科室占床日总和
        $departAvgBedDayAll = $this->repotitory->getBedCount($date,$department_ids);

        //全院占床日总和
        $hospitalAvgBedDayAll = $this->repotitory->getAllBedCount($date);

        //本月科室医生人数
        $departmentDoctorNum = EmployeeAttendance::leftjoin("employees", "employees.id", "employee_attendances.employees_id")
            ->whereIn("sub_departments_id", $department_ids)
            ->where("date", $date)
            ->where("employees.attendances_type", 1)
            ->whereRaw("employee_attendances.actual_attendance = (select max(t.actual_attendance) 
            from employee_attendances as t where 
            employee_attendances.employees_id = t.employees_id and t.date = '$date') ")
            ->count();

        //全院医生人数
        $allDoctorNum = EmployeeAttendance::leftjoin("employees", "employees.id", "employee_attendances.employees_id")
            ->where("date", $date)
            ->where("employees.attendances_type", 1)
            ->select("employees_id")
            ->groupBy("employees_id")
            ->get()
            ->count();
        if ($departmentDoctorNum == 0 ||
            $hospitalAvgBedDayAll == 0 ||
            $allDoctorNum == 0) {
            return 0;
        }
        //科室医生人均床日
        $departmentAvg = $departAvgBedDayAll / $departmentDoctorNum;
        //全院医生人均床日
        $hospitalAvg = $hospitalAvgBedDayAll / $allDoctorNum;

        $horizontalComparison = round($departmentAvg / $hospitalAvg, 4);

        $data = [
            "departmentAvg" => $departmentAvg,//科室医生人均床日
            "hospitalAvg" => $hospitalAvg,//全院医生人均床日
            "horizontalComparison" => $horizontalComparison,//住院科室间人均效率的横向比较
        ];

        return $data;
    }


    /**
     * 住院科室总效率纵向比较
     * @param $date
     * @param $department_id
     * @return array|int
     */
    public function getInHospitalVerticalComparison($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
        $type = DepartmentType::WARD;
        $department_ids = $this->repotitory->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        //科室占床日总和
        $departAvgBedDayAll = $this->repotitory->getBedCount($date, $department_ids);

        //科室基期占床日
        //先查询基值表，如果没有数据就从department表查询2019年数据
        $year = date("Y",strtotime($date));
        $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
        if ($year == "2021"){//如果是2021年就查询2019年的数据
            $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
        }
        $baseData = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();
        if (empty($baseData)){
            $baseAvgBedDayAll = Department::where("id", $department_id)
                ->value("base_use_bed_day");
        }else{
            $baseAvgBedDayAll = $baseData->base_use_bed_day;
        }

        if ($baseAvgBedDayAll == 0) {
            return 0;
        }
        $verticalComparison = round($departAvgBedDayAll / $baseAvgBedDayAll, 4);

        $data = [
            "departAvgBedDayAll" => $departAvgBedDayAll, //科室本期实际占床日
            "baseAvgBedDayAll" => $baseAvgBedDayAll, //科室基期实际占床日
            "verticalComparison" => $verticalComparison, //住院科室总效率纵向比较
        ];

        return $data;
    }

    /**
     * 出院病人例均费用系数
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getLeaveAverageCost($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
        $type = DepartmentType::WARD;
        $department_ids = $this->repotitory->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        $arrDate = explode('-', $date);
        //出院病人总收入
        $leavePatientIncome = MainIncome::date($date)->whereIn(
            "leave_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereNotNull("leave_time")
            ->sum("amount");
        //出院人数
        $leaverCount = $this->repotitory->getOutHospitalCount($date, $department_ids);
        if ($leaverCount == 0) {
            return 0;
        }
        //目标人均费用
        $targetAverageLeaveCost = Department::where("id", $department_id)
            ->value("target_average_leave_cost");
        //实际出院病人均例费用
        $realAverageLeaveCost = $leavePatientIncome / $leaverCount;

        if ($targetAverageLeaveCost == 0 || $realAverageLeaveCost == 0) {
            return 0;
        }

        if ($realAverageLeaveCost > $targetAverageLeaveCost * 1.1) {
            $leaveAverageCost = $targetAverageLeaveCost / $realAverageLeaveCost;
        } else if ($realAverageLeaveCost >= $targetAverageLeaveCost * 0.9
            && $realAverageLeaveCost <= $targetAverageLeaveCost * 1.1) {
            $leaveAverageCost = 1;
        } else {
            $leaveAverageCost = $realAverageLeaveCost / $targetAverageLeaveCost;
        }

        $leaveAverageCost = round($leaveAverageCost, 4);

        $data = [
            "targetAverageLeaveCost" => $targetAverageLeaveCost,//出院病人目标均次费用
            "realAverageLeaveCost" => $realAverageLeaveCost,//出院病人本期均次费用
            "leaveAverageCost" => $leaveAverageCost,//出院病人例均费用系数
        ];

        return $data;

    }

}
<?php

namespace App\Repositories\PerformanceDetail;

use App\Models\MainIncome;
use App\Models\SubDepartment;
use App\Models\EmployeeAttendance;
use Illuminate\Support\Facades\DB;


/**
 * 会诊绩效详情数据
 */
class ConsultationDetail extends BaseDetail {

    public function __construct()
    {
    }


    public function getDetail($date, $department_id) {
//        $department_ids = SubDepartment::where("parent_id", $department_id)
//            ->where("is_performance", 1)
//            ->get()
//            ->pluck("id")
//            ->all();
        //科室内考勤的医生
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->get()
            ->pluck("employees_id")
            ->all();

        $arrDate = explode("-", $date);
        $count = MainIncome::date($date)->whereIn("prescribe_doctor_id", $doctors)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("item_cate_code", 36)//会诊费
            ->sum("quantity");

        $performance = $count * 10;

        return [
            // 会诊人次
            "consultation_count" => $count . "人",
            // 人次奖励
            "subsidy_price" => "10.00元/人次",
            // 科室会诊绩效
            "performance" => number_format($performance, 2) . "元"
        ];

    }
}
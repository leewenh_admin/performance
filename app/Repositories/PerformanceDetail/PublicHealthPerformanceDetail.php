<?php

namespace App\Repositories\PerformanceDetail;

use App\Models\SubDepartment;
use App\Models\PublicHealthPerformance;

/**
 * 公卫绩效详情数据
 */
class PublicHealthPerformanceDetail extends BaseDetail {
    public $repotitory;

    public function __construct()
    {
    }


    public function getDetail($date, $department_id) {

        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();
        $records = PublicHealthPerformance::whereIn("sub_department_id", $department_ids)
            ->where("date", $date)
            ->get();

        $details = [];
        foreach ($records as $record) {
            $detail = [];
            // 工号
            $detail["code"] = $record->code;
            // 姓名
            $detail["name"] = $record->name;
            // 科室名称
            if ($record->subDepartment) {
                $detail["sub_department_name"] = $record->subDepartment->name;
            } else {
                $detail["sub_department_name"] = null;
            }

            // 金额
            $detail["amount"] = number_format($record->amount, 2) . "元";


            // 备注
            $detail["description"] = $record->description;

            array_push($details, $detail);
        }

        return [
            // 详情数据
            "details" => $details,
            // 科室绩效
            "performance" => $records->sum("amount") . "元"
        ];
    }
}
<?php

namespace App\Repositories\PerformanceDetail;


use App\Models\TalentReserve;
use App\Repositories\NightPerformanceCalculateEloquent;



/**
 * 人才储备绩效详情数据
 */
class TalentReserveDetail extends BaseDetail {

    protected $obj_night;
    public function __construct()
    {
        $this->obj_night = new NightPerformanceCalculateEloquent();
    }


    public function getDetail($date, $department_id) {
        $records = TalentReserve::where("date", $date)
            ->where("department_id", $department_id)
            ->select("name", "code", "actual_sent")
            ->get();

        return $records;
    }
}
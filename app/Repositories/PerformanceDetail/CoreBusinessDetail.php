<?php

namespace App\Repositories\PerformanceDetail;

use App\Models\BaseData;
use App\Models\Department;
use App\Models\MainIncome;
use App\Enums\DepartmentType;
use App\Models\SurgeryNew;
use App\Repositories\CoreBusinessRepositoryEloquent;
use App\Repositories\WorkLoadRepositoryEloquent;

/**
 * 获取科室核心业务绩效详情
 */
class CoreBusinessDetail extends BaseDetail {
    public $repotitory;

    public function __construct()
    {
        $this->repotitory = new CoreBusinessRepositoryEloquent();
    }

    public function getDetail($date, $department_id) {
        $arrDate = explode('-', $date);
        $new_table = "main_incomes" . $arrDate[0] . $arrDate[1];
        // 获取二级非手术科室ID列表
        $type = DepartmentType::MEDICINE;
        $department_ids = $this->repotitory->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            $leaveWeight = 0;
            $subWeight = 0;
            $diseasePoint = 0;
            $diseasePerformance = 0;
        } else {
            $str_date = str_replace("-", "", $date);
            $arrDate = explode('-', $date);
            //出院诊断权重
            $leaveWeight = MainIncome::date($date)->leftjoin(
                'diease_catalogs', $new_table.'.leave_diagnosis_code', '=', 'diease_catalogs.code'
            )
                ->whereIn("leave_department_id", $department_ids)
                ->whereRaw("left(leave_time,6) =  '".$str_date."'")
                ->select($new_table.".serial_no", "diease_catalogs.weights")
                ->distinct()
                ->get()
                ->sum("weights");


            //次诊断权重
            $subWeight = MainIncome::date($date)->leftjoin(
                'diease_catalogs', $new_table.'.sub_diagnosis_code', '=', 'diease_catalogs.code'
            )
                ->whereIn("leave_department_id", $department_ids)
                ->whereRaw("left(leave_time,6) =  '".$str_date."'")
                ->select($new_table.".serial_no", "diease_catalogs.weights")
                ->distinct()
                ->get()
                ->sum("weights");


            //查询点值
            $diseasePoint = BaseData::query()->where('date', $date)
                ->where('department_id', $department_id)->value('point_price');
                
            // 内科疾病绩效
            $diseasePerformance = round(
                $leaveWeight * $diseasePoint + $subWeight * $diseasePoint * 0.5,
                2
            );
        }

        // 获取二级手术科室ID列表
        $type = DepartmentType::SURGICAL;
        $department_ids = $this->repotitory->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            $surgeryCount = 0;
            $leaverCount = 0;
            $surgeryRate = 0;
            $target_surgery_rate = 0;
            $surgeryChange = 0;
            $surgeryPoint = 0;
            $surgeryPunishAmount = 0;
            $surgeryRewardAmount = 0;
            $surgeryPerformance = 0;
        } else {
            //手术人数
            $surgeryCount = SurgeryNew::whereIn("sub_departments_id", $department_ids)
                ->where("date", $date)
                ->get()
                ->count();

            //出院人数
            $obj = new WorkLoadRepositoryEloquent();
            $leaverCount = $obj->getOutHospitalCount($date, $department_ids);


            $department_data = Department::where("id", $department_id)
                ->select("punish_amount", "reward_amount", "target_surgery_rate")
                ->first();

            // 实际手术率
            if ($leaverCount == 0) {
                $surgeryRate = 0;
            } else {
                $surgeryRate = $surgeryCount / $leaverCount;
            }


            // 目标手术率
            // 数据库保存的手术率是百分比
            $target_surgery_rate = $department_data->target_surgery_rate / 100;

            // 手术扣罚金额
            $surgeryPunishAmount = $department_data->punish_amount;
            // 手术奖励金额
            $surgeryRewardAmount = $department_data->reward_amount;
            // 手术率调整额
            if($target_surgery_rate == 0){
                $surgeryChange = 0;
            } else {
                if ($surgeryRate > $target_surgery_rate) {
                    $surgeryChange = ($surgeryRate - $target_surgery_rate)
                        / $target_surgery_rate * $department_data->punish_amount;
                } else {
                    $surgeryChange = ($target_surgery_rate - $surgeryRate)
                        / $target_surgery_rate * $department_data->reward_amount;
                }
            }

            $arrDate = explode('-', $date);
            //手术点数
            $surgeryPoint = MainIncome::query()->date($date)
                ->join(\DB::raw("(select code,performance_weights,belong_department_id from surgical_point_values) as pw"), 'item_code', '=', 'code')
                ->whereIn("leave_department_id", $department_ids)
                ->where("item_cate_code", 14)//手术费
                ->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->select("serial_no")
                ->selectRaw("(performance_weights * quantity) as weightCount")
                ->get()
                ->sum("weightCount");

            $point_price = BaseData::query()->where('date', $date)
                ->where('department_id', $department_id)->value('point_price');

            $surgeryPerformance = round(
                $surgeryPoint * $point_price + $surgeryChange,
                2
            );
        }

        $performance = $diseasePerformance + $surgeryPerformance;

        return [
            // 内科出院诊断权重
            "leaveWeight" => number_format($leaveWeight, 2),
            // 内科次诊断权重
            "subWeight" => number_format($subWeight, 2),
            // 内科疾病点值
            "diseasePoint" => number_format($diseasePoint,2),
            // 内科疾病绩效
            "diseasePerformance" => number_format($diseasePerformance, 2) . "元",
            // 外科手术人数
            "surgeryCount" => $surgeryCount . "人",
            // 外科出院人数
            "leaverCount" => $leaverCount . "人",
            // 外科实际手术率
            "surgeryRate" => number_format($surgeryRate * 100, 2) . "%",
            // 外科目标手术率
            "targetSurgeryRate" => number_format($target_surgery_rate * 100, 2) . "%",
            // 外科手术率扣罚金额
            "surgeryPunishAmount" => number_format($surgeryPunishAmount, 2) . "元",
            // 外科手术率奖励金额
            "surgeryRewardAmount" => number_format($surgeryRewardAmount, 2) . "元",
            // 外科手术调整额
            "surgeryChange" => number_format($surgeryChange, 2) . "元",
            // 外科手术点数
            "surgeryPoint" => number_format($surgeryPoint, 2),
            // 外科手术绩效
            "surgeryPerformance" => number_format($surgeryPerformance, 2) . "元",
            // 核心业务单价
            "point_price" => number_format($point_price??$diseasePoint, 2) . "元",
            // 核心业务绩效
            "performance" => number_format($performance, 2) . "元"
        ];
    }
}
<?php

namespace App\Repositories\PerformanceDetail;

use App\Enums\DepartmentType;
use App\Models\BaseData;
use App\Models\Department;
use App\Models\SurgeryNew;
use App\Models\SpecialPerformance;
use App\Repositories\WorkLoadRepositoryEloquent;
use App\Repositories\NewTechnologyItemRepositoryEloquent;

/**
 * 业务恢复绩效详情数据
 */
class BusinessRecoveryDetail extends BaseDetail {
    public $repotitory;

    public function __construct()
    {
    }

    public function getDetail($date, $department_id) {
        $department = Department::where("id", $department_id)->first();
        $sub_department = $department->subDepartments()->first();
        $year = date("Y",strtotime($date));
        $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
        if ($year == "2021"){//如果是2021年就查询2019年的数据
            $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
        }
        $baseData = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();

        //出院人数
        $obj = new WorkLoadRepositoryEloquent();
        $department_ids = $obj->getOutPatientDepartments($department_id);
        $leaverCount = $obj->getOutHospitalCount($date, $department_ids);
        //基期出院人数
        if (empty($baseData)){
            $base_out_hospital = Department::where("id", $department_id)->value("base_out_hospital");
        }else{
            $base_out_hospital = $baseData->base_out_hospital;
        }
        if($base_out_hospital == 0){
            $leaveCountIncrease = 0;
        } else {
            //出院人次增幅 = 实际出院人次 / 基期出院人数
            $leaveCountIncrease = $leaverCount / $base_out_hospital;
        }


        //实际占床日
        $departBedDayAll = $obj->getBedCount($date,$department_ids);

        //基期占用总床日数
        if (empty($baseData)){
            $base_use_bed_day = Department::where("id", $department_id)->value("base_use_bed_day");
        }else{
            $base_use_bed_day = $baseData->base_use_bed_day;
        }
        if($base_use_bed_day == 0){
            $useBedIncrease = 0;
        } else {
            //实际占用总床日数增幅 =  实际占用总床日数增幅/基期占用总床日数
            $useBedIncrease = $departBedDayAll / $base_use_bed_day;
        }


        //科室有效收入
        $obj2 = new NewTechnologyItemRepositoryEloquent();
        $income = $obj2->getDepartmentIncome($date, $department_id);
        //基期科室有效收入
        if (empty($baseData)){
            $base_department_income = Department::where("id", $department_id)->value("base_department_income");
        }else{
            $base_department_income = $baseData->base_department_income;
        }
        if($base_department_income == 0){
            $incomeIncrease = 0;
        } else {
            // 有效收入增幅
            $incomeIncrease = $income / $base_department_income;
        }


        $increase = 0;
        $base_surgery_count = 0;
        $surgeryCountIncrease = 0;
        $surgeryCount = 0;
        $base_special_surgery_count = 0;
        $surgeryRateIncrease = 0;
        if ($sub_department->type_lv3 == DepartmentType::MEDICINE){//非手术科室
            //工作量增幅=出院人次增幅*30%+实际占用总床日数增幅*30%+有效收入增幅*40%

            $increase = $leaveCountIncrease * 0.3 + $useBedIncrease * 0.3 + $incomeIncrease * 0.4;
        } elseif ($sub_department->type_lv3 == DepartmentType::SURGICAL){//手术科室
            //手术科室：工作量增幅=出院人次增幅*30%+手术台次增幅*30%+实际占用总床日数增幅*10%+有效收入增幅*20%+三、四级手术率增幅*10%

            //手术台次增幅
            $surgeryCount = SurgeryNew::whereIn("sub_departments_id", $department_ids)
                ->where("date", $date)
                ->count();
            if (empty($baseData)){
                $base_surgery_count = Department::where("id", $department_id)->value("base_surgery_count");
            }else{
                $base_surgery_count = $baseData->base_surgery_count;
            }
            if($base_surgery_count == 0){
                $surgeryCountIncrease = 0;
            } else {
                //手术台次增幅 =  实际手术台次/基期手术台次
                $surgeryCountIncrease = $surgeryCount / $base_surgery_count;
            }
            
            //三四级手术场次
            $surgeryCount1 = SurgeryNew::whereIn("sub_departments_id", $department_ids)
                ->where("date", $date)
                ->whereRaw("(find_in_set('3',level) or find_in_set('4',level))")
                ->count();

            $year = date("Y",strtotime($date));
            $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
            if ($year == "2021"){//如果是2021年就查询2019年的数据
                $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
            }
            $baseData = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();
            if (empty($baseData)){
                $base_special_surgery_count = Department::where("id", $department_id)->value("base_special_surgery_count");
            }else{
                $base_special_surgery_count = $baseData->base_special_surgery_count;
            }
            if($base_special_surgery_count == 0){
                $surgeryRateIncrease = 0;
            } else {
                //手术台次增幅 =  实际手术台次/基期手术台次
                $surgeryRateIncrease = $surgeryCount1 / $base_special_surgery_count;
            }



            $increase = $leaveCountIncrease * 0.3 + $surgeryCountIncrease * 0.3 + $useBedIncrease * 0.1 +
                $incomeIncrease * 0.2 + $surgeryRateIncrease * 0.1;
        }

        //区间月奖励额
        $reward = 0;
        switch ($increase) {
            case $increase >= 0.9 && $increase < 1:
                $reward = 500;
                break;
            case $increase >= 1 && $increase < 1.1:
                $reward = 1100;
                break;
            case $increase >= 1.1 && $increase < 1.2:
                $reward = 1900;
                break;
            case $increase >= 1.2 && $increase < 1.3:
                $reward = 3000;
                break;
            case $increase >= 1.3 && $increase < 1.4:
                $reward = 4500;
                break;
            case $increase >= 1.4 && $increase < 1.5:
                $reward = 6500;
                break;
            case $increase >= 1.5 && $increase < 1.6:
                $reward = 9100;
                break;
            case $increase >= 1.6 && $increase < 1.7:
                $reward = 12400;
                break;
        }

//        //本期奖励额=区间月奖励额*累计月*（实际工作量增幅/区间工作量增幅下限）-累计至上月奖励额
//        //TODO 累计月和累计至上个月奖励需要设计好专项绩效表后直接查询
//        $data = SpecialPerformance::where("department_id", $department_id)
//            ->where("date", "<", $date)
//            ->get()
//            ->pluck("business_recovery_performance")
//            ->all();
//        if (empty($data)){
//            $month = 1;
//            $total = 0;
//        } else {
//            $month = count($data) + 1;
//            $total = array_sum($data);
//        }
//        $data = $reward * $month * ($increase / 0.9) - $total;

        return [
            // 实际出院人数
            "leaverCount" => ($leaverCount ?? 0) . "人",
            // 基期出院人数
            "baseOutHospital" => ($base_out_hospital ?? 0) . "人",
            // 出院人数增幅
            "leaveCountIncrease" => number_format(
                $leaveCountIncrease * 100,
                2
            ) . "%",
            //实际占床日
            "departBedDayAll" => ($departBedDayAll ?? 0) . "天",
            //基期占用总床日数
            "baseUseBedDay" => ($base_use_bed_day ?? 0) . "天",
            //实际占用总床日数增幅
            "useBedIncrease" => number_format(
                $useBedIncrease * 100,
                2
            ) . "%",
            // 科室有效收入
            "income" => number_format($income ?? 0, 2) . "元",
            // 基期科室有效收入
            "baseDepartmentIncome" => number_format($base_department_income, 2) . "元",
            // 有效收入增幅
            "incomeIncrease" => number_format(
                $incomeIncrease * 100,
                2
            ) . "%",
            // 实际手术台次
            "surgeryCount" => ($surgeryCount ?? 0) . "台",
            // 基期手术台次
            "baseSurgeryCount" => ($base_surgery_count ?? 0) . "台",
            // 手术台次增幅
            "surgeryCountIncrease" => number_format(
                $surgeryCountIncrease * 100, 
                2
            ) . "%",
            // 三四级手术场次
            "surgeryCount1" => ($surgeryCount1 ?? 0) . "台",
            // 基期三四级手术台次
            "base_special_surgery_count" => round($base_special_surgery_count, 0) . "台",
            // 三四级手术增幅
            "surgeryRateIncrease" => number_format($surgeryRateIncrease * 100, 2) . "%",
            // 实际工作量增幅
            "increase" => number_format($increase * 100, 2) . "%",
            // 区间月奖励额
            "reward" => number_format($reward ?? 0, 2) . "元",
            // 累计月
            "month" => 0,
            // 区间工作量增幅下限
            "rangeWorkloadIncreaseLowerLimit" => 0.9,
            // 累计至上月奖励额
            "total" => 0 . "元"
        ];
    }
}
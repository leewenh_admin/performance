<?php

namespace App\Repositories\PerformanceDetail;

use App\Models\NewTechnologyItem;
use App\Repositories\NewTechnologyItemRepositoryEloquent;

/**
 * 获取新技术业务绩效详情
 */
class NewTechnologyItemDetail extends BaseDetail {
    public $repository;

    public function __construct()
    {
        $this->repository = new NewTechnologyItemRepositoryEloquent();
    }

    public function getDetail($date, $department_id) {
        $result = [];
        $items = NewTechnologyItem::where("department_id", $department_id)
                ->get();
        // 科室有效收入
        $departIncome = $this->repository->getDepartmentIncome($date, $department_id);

        $performance = 0;
        $itemDetails = [];

        foreach ($items as $item){
            $itemDetail = [];
            //项目收入
            $income = $item->price * $item->count;
            // 计算单位奖励值A
            // 计算是第几年扶持期
            $year = date("Y") - $item->apply_year + 1;

            $bonusA = 0;
            if($year == 1){
                $bonusA = $item->price * 0.1;
            } else if($year == 2){
                $bonusA = $item->price * 0.05;
            } else if($year ==3){
                $bonusA = $item->price * 0.03;
            }

            // 计算单位奖励值B

            // 项目收入占科室收入百分比
            $percent = $income / $departIncome;
            $bonusB = 0;
            //资产折旧 (暂时计为0)
            $assetDepreciation = 0;
            //卫材 (暂时计为0)
            $eisai = 0;
            if($percent >= 0.1 && $percent < 0.2){
                $bonusB = ($income - $assetDepreciation - $eisai) * 0.1;
            } else if($percent >= 0.2 && $percent <= 0.3){
                $bonusB = ($income - $assetDepreciation - $eisai) * 0.12;
            } else if($percent > 0.3){
                $bonusB = ($income - $assetDepreciation - $eisai) * 0.15;
            }
            // 计算项目绩效
            $performance += $item->count * ($bonusA + $bonusB);

            // 扶持期
            $itemDetail["year"] = "第" . $year . "年";
            // 新技术名称
            $itemDetail["name"] = $item->name;
            // 项目名称
            $itemDetail["itemName"] = $item->item_name;
            // 项目占科室收入百分比
            $itemDetail["percent"] = round($percent * 100, 2) . "%";
            // 资产折旧
            $itemDetail["assetDepreciation"] = $assetDepreciation;
            // 卫材
            $itemDetail["eisai"] = $eisai;
            // 项目例数
            $itemDetail["itemCount"] = $item->count . "例";
            // 项目收入
            $itemDetail["income"] = number_format($income, 2) . "元";
            // 单位奖励值A
            $itemDetail["bonusA"] = number_format($bonusA, 2) . "元";
            // 单位奖励值B
            $itemDetail["bonusB"] = number_format($bonusB, 2) . "元";
            // 项目绩效
            $itemDetail["performance"] = number_format($performance, 2) . "元";

            array_push($itemDetails, $itemDetail);
        }
        // 科室有效收入
        $result["departmentIncome"] = number_format($departIncome, 2) . "元";
        // 科室绩效
        $result["performance"] = number_format($performance, 2) . "元";
        // 项目详情
        $result["itemDetails"] = $itemDetails;
        return $result;
    }
}
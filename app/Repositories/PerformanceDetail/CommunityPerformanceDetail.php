<?php

namespace App\Repositories\PerformanceDetail;

use App\Models\RadiologyDepartmentPerformance;
use App\Models\ClinicalLaboratoryPerformance;
use App\Models\SpecialSurveyPerformance;
use App\Models\PathologyPerformance;

/**
 * 医共体绩效详情数据
 */
class CommunityPerformanceDetail extends BaseDetail {
    public $repotitory;

    public function __construct()
    {
    }


    public function getDetail($date, $department_id) {
        //医共体影像中心绩效(放射科)
        $data1 = RadiologyDepartmentPerformance::where("date", $date)
            ->where("department_id", $department_id)
            ->value("performance") ?? 0;
        //医共体检验中心绩效(检验科)
        $data2 = ClinicalLaboratoryPerformance::where("date", $date)
            ->where("department_id", $department_id)
            ->value("performance") ?? 0;
        //医共体心电中心绩效(特检科)
        $data3 = SpecialSurveyPerformance::where("date", $date)
            ->where("department_id", $department_id)
            ->value("performance") ?? 0;
        //医共体病理中心绩效(病理科)
        $data4 = PathologyPerformance::where("date", $date)
            ->where("department_id", $department_id)
            ->value("performance") ?? 0;

        $total = $data1 + $data2 + $data3 + $data4;

        return [
            // 医共体影像中心绩效(放射科)
            "radiology_department" => $data1 . "元",
            // 医共体检验中心绩效(检验科)
            "clinical_laboratory" => $data2 . "元",
            // 医共体心电中心绩效(特检科)
            "special_survey" => $data3 . "元",
            // 医共体病理中心绩效(病理科)
            "pathology_performance" => $data4 . "元",
            // 科室绩效
            "total" => $total . "元",
        ];

    }
}
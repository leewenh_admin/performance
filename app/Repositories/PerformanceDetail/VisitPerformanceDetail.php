<?php

namespace App\Repositories\PerformanceDetail;

use App\Models\MainIncome;
use App\Models\SubDepartment;
use App\Models\EmployeeAttendance;


/**
 * 专家号绩效详情数据
 */
class VisitPerformanceDetail extends BaseDetail {

    public function __construct()
    {
    }


    public function getDetail($date, $department_id) {
        $arrDate = explode("-", $date);
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();

        //科室内考勤的医生
        $doctors = EmployeeAttendance::leftjoin("employees", "employees.id", "=", "employee_attendances.employees_id")
            ->whereIn("sub_departments_id", $department_ids)->where("date", $date)
//            ->whereIn("professional_rank_level", [3, 4])
            ->whereRaw("employee_attendances.actual_attendance = (select max(t.actual_attendance) 
            from employee_attendances as t where 
            employee_attendances.employees_id = t.employees_id and t.date = '$date') ")
            ->pluck("employees_id")
            ->all();

        if(in_array($date,['2021-04', '2021-05', '2021-06', '2021-08'])){
            $chief_count = 0;
            $assistant_count = 0;

            //专家号数量
            $count = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->whereIn("prescribe_doctor_id", $doctors)
                ->select("serial_no", "quantity", "price")
                ->distinct()
                ->where("item_code", 1002)
                ->where("price", ">=", 10)
                ->get();

            foreach ($count as $value){
                if($value->price < 16 ){  //大于等于10 小于16 副主任
                    $assistant_count += $value->quantity;
                } else { //大于等于16 主任
                    $chief_count += $value->quantity;
                }
            }
        } else{
            //副主任专家号数量
            $assistant_count = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->whereIn("prescribe_doctor_id", $doctors)
                ->select("serial_no", "quantity", "price")
                ->distinct()
                ->where("item_code", 1002)
                ->whereIn("register_cate", [3,6])
                ->get()
                ->sum('quantity');

            //主任专家号数量
            $chief_count = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->whereIn("prescribe_doctor_id", $doctors)
                ->select("serial_no", "quantity", "price")
                ->distinct()
                ->where("item_code", 1002)
                ->whereIn("register_cate", [4,7])
                ->get()
                ->sum('quantity');

        }

        $performance = $chief_count * 9.6 + $assistant_count * 6 ;
        return [
            // 主任医师号人次
            "chief_count" => $chief_count . "人",
            // 副主任医师号人次
            "assistant_count" => $assistant_count . "人",
            // 主任医师号人次奖励
            "chief_price" => "9.60元/人次",
            // 副主任医师号人次奖励
            "assistant_price" => "6.00元/人次",
            // 科室专家号绩效
            "performance" => number_format($performance, 2) . "元"
        ];

    }
}
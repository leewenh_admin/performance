<?php

namespace App\Repositories\PerformanceDetail;

use App\Enums\DepartmentType;
use App\Models\Department;
use App\Models\DepartmentHeadPerformance;
use App\Repositories\NightPerformanceCalculateEloquent;
use Illuminate\Support\Facades\DB;

/**
 * 科主任管理绩效详情数据
 */
class DepartmentHeaderDetail extends BaseDetail {
    public function getDetail($date, $department_id) {
        $obj = new NightPerformanceCalculateEloquent();

        //当月天数
        $days = date("t",strtotime($date));
        $department = Department::query()->where('id',$department_id)
            ->with('subDepartments')
            ->first();

        $departmentHeadAward = 0; //科主任管理奖
        $departmentSubHeadAward = 0; //副主任管理奖
        $departmentNurseHeadAward = 0; //护士长管理奖
        $departmentOtherAward = 0; //其他职位科室管理奖
        //质量考核分大于或等于98， 管理绩效 = 岗位管理绩效标准*实际考勤天数占比
        //质量考核分数小于98  管理绩效 = （岗位管理绩效标准 + 岗位管理绩效标准 * （质量考核分数 - 98）/100）） *实际考勤天数占比
        $employees = DepartmentHeadPerformance::join('employee_attendances', function ($join) {
            $join->on('employee_attendances.employees_id', '=', 'department_head_performances.employee_id')
                ->on('employee_attendances.date', '=', 'department_head_performances.date');
//                ->on('department_head_performances.department_id', '=', 'employee_attendances.departments_id');
        })
            ->where("department_head_performances.department_id", $department->id)
            ->where("department_head_performances.date", $date)
            ->select("employee_attendances.employees_id", "performance_standard", DB::raw("sum(employee_attendances.actual_attendance) as actual_attendance"), "administrative_duties", "medical_percent", "expend_percent", "check_day")
            ->groupBy("employee_attendances.employees_id", "performance_standard",  "administrative_duties", "medical_percent", "expend_percent", "check_day")
            ->get();


        if ($department->subDepartments->first()->type_lv1 == DepartmentType::FUNCTIONAL) { //职能科室
            $score = 100;
            foreach ($employees as $employee) {
                $percent = ($employee->actual_attendance - $employee->check_day) / $days;
                $performance = $employee->performance_standard * $percent;

                if ($employee->administrative_duties == "科主任") {
                    $departmentHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                } elseif ($employee->administrative_duties == "副主任") {
                    $departmentSubHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                } elseif ($employee->administrative_duties == "护士长") {
                    $departmentNurseHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                } else {
                    $departmentOtherAward += $performance + $employee->medical_percent + $employee->expend_percent;
                }


            }
        } else {
            $scoreData = $obj->getScore($date, $department->id, $department->subDepartments->first()->type_lv1);
            foreach ($employees as $employee) {
                if ($employee->administrative_duties == "科主任" || $employee->administrative_duties == "副主任") {
                    $score = $scoreData->medical_quality;
                } elseif ($employee->administrative_duties == "护士长") {
                    $score = $scoreData->care_quality;
                }
                $percent = ($employee->actual_attendance - $employee->check_day) / $days;
                if ($score >= 98) {
                    $performance = $employee->performance_standard * $percent;
                } else {
                    $performance = ($employee->performance_standard + ($employee->performance_standard *
                            ($score - 98) / 100) )* $percent;
                }

                if ($employee->administrative_duties == "科主任") {
                    $departmentHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                } elseif ($employee->administrative_duties == "副主任") {
                    $departmentSubHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                } elseif ($employee->administrative_duties == "护士长") {
                    $departmentNurseHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                }


            }
        }


        return [
            // 科室主任管理奖
            "cb_departmentHeadAward" => number_format($departmentHeadAward, 2) ."元",
            // 科室副主任管理奖
            "cb_departmentSubHeadAward" => number_format($departmentSubHeadAward, 2) ."元",
            // 科室护士长管理奖
            "cb_departmentNurseHeadAward" => number_format($departmentNurseHeadAward, 2)."元",
            // 其他职位管理奖
            "cb_departmentOtherAward" => number_format($departmentOtherAward, 2) ."元",
            // 医疗质量分数
            "cb_medical_quality" => number_format(isset($scoreData)?$scoreData->medical_quality:100, 2) ."分",
            // 护理质量分数
            "cb_care_quality" => number_format(isset($scoreData)?$scoreData->care_quality:100, 2) ."分",
        ];
    }
}
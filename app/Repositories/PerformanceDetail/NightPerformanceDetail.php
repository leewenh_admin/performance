<?php

namespace App\Repositories\PerformanceDetail;

use App\Models\SubDepartment;
use App\Models\EmployeeAttendance;
use App\Models\NightPerformance;

/**
 * 夜班绩效详情数据
 */
class NightPerformanceDetail extends BaseDetail {

    public function __construct()
    {
    }


    public function getDetail($date, $department_id) {
        $record = NightPerformance::where("date", $date)
            ->where("department_id", $department_id)
            ->get();

        $daily_fixed_number = 0;
        $before_night_number = 0;
        $after_night_number = 0;
        $whole_night_number = 0;
        $dispatch_amount = 0;
        $total_night_performance = 0;

        foreach ($record as $value){
            $daily_fixed_number += $value->daily_fixed_number;
            $before_night_number += $value->before_night_number;
            $after_night_number += $value->after_night_number;
            $whole_night_number += $value->whole_night_number;
            $dispatch_amount += $value->dispatch_amount;
            $total_night_performance += $value->total_night_performance;

        }

        return [
            // 每日固定人数
            "daily_fixed_number" => $daily_fixed_number . "人",
            // 上夜班人数
            "before_night_number" => $before_night_number . "人",
            // 上夜班标准
            "before_night_price" => number_format(
                isset($record[0]) ? $record[0]->before_night_price : 0,
                2
            ) . "元",
            // 下夜班人数
            "after_night_number" => $after_night_number . "人",
            // 下夜班标准
            "after_night_price" => number_format(
                    isset($record[0]) ? $record[0]->after_night_price : 0,
                2
            ) . "元",
            // 整夜班人数
            "whole_night_number" => $whole_night_number . "人",
            // 整夜班标准
            "whole_night_price" => number_format(
                    isset($record[0]) ? $record[0]->whole_night_price : 0,
                2
            ) . "元",
            // 月天数
            "month_day" => (isset($record[0]) ? $record[0]->month_day : 0) . "天",
            // 劳务派遣人员夜班
            "dispatch_amount" => $dispatch_amount . "元",
            // 实发夜班绩效合计
            "total_night_performance" => number_format(
                    $total_night_performance,
                2
            ) . "元"
        ];

    }
}
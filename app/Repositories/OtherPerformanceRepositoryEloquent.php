<?php

namespace App\Repositories;

use App\Models\OtherPerformance;

class OtherPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = OtherPerformance::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with("subDepartment:id,name,code");

        $query = $query->join('sub_departments', 'other_performances.sub_department_id', '=', 'sub_departments.id')
            ->leftjoin('lock_performances', 'other_performances.date', '=', 'lock_performances.date');
        $query->select('other_performances.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if (!empty($date)){
            $query->where("other_performances.date", $date);
        }

        $code = request("code", null);
        if (!empty($code)){
            $query->where("sub_departments.code", "like", "%$code%");
        }

        $name = request("name", null);
        if (!empty($name)){
            $query->where("sub_departments.name", "like", "%$name%");
        }

        $sub_department_id = request("sub_department_id", null);
        if (!empty($sub_department_id)){
            $query->where("sub_department_id", $sub_department_id);
        }

        return $query;
    }
}

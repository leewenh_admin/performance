<?php

namespace App\Repositories;

use App\Models\AvgPerformanceSetting;

class AvgPerformanceSettingRepositoryEloquent extends BaseRepository {
    protected $actionClass = AvgPerformanceSetting::class;

    public function getList(){
        $query =  parent::getList();

        $query->leftjoin('lock_performances', 'avg_performance_settings.date', '=', 'lock_performances.date')
            ->select('avg_performance_settings.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if ($date){
            $query = $query->where("avg_performance_settings.date", $date);
        }

        return $query;
    }
}

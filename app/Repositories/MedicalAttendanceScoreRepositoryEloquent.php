<?php

namespace App\Repositories;

use App\Models\MedicalAttendanceScore;

class MedicalAttendanceScoreRepositoryEloquent extends BaseRepository {
    protected $actionClass = MedicalAttendanceScore::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "department:id,name,code",
        ]);
        $query = $query->join('departments', 'medical_attendance_scores.departments_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'medical_attendance_scores.date', '=', 'lock_performances.date');

        $departments_id = request("departments_id", null);
        if(!empty($departments_id)){
            $query = $query->where("departments_id", $departments_id);
        }

        $date = request("date", null);
        if(!empty($date)){
            $query = $query->where("medical_attendance_scores.date", $date);
        }
        $query->select('medical_attendance_scores.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        return $query;
    }
}

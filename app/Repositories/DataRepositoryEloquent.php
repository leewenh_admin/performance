<?php

namespace App\Repositories;

use App\Enums\ImportDataType;
use App\Exceptions\MessageException;
use App\Imports\BaseAdmissionRateImport;
use App\Imports\BaseAverageCostImport;
use App\Imports\BaseBedValueImport;
use App\Imports\BaseControllableCostImport;
use App\Imports\BaseDepartmentIncomeImport;
use App\Imports\BaseMaterialCostImport;
use App\Imports\BaseServiceIncomeImport;
use App\Imports\BaseSurgeryRateImport;
use App\Imports\BedUsdingImport;
use App\Imports\ClinicalAttendanceScoreImport;
use App\Imports\ClinicalLaboratoryPerformanceImport;
use App\Imports\ColorUltrasoundWorkloadImport;
use App\Imports\ConsultationImport;
use App\Imports\DepartCoefficientImport;
use App\Imports\DepartmentHeadPerformanceImport;
use App\Imports\DepartmentImport;
use App\Imports\FourItemPerformanceImport;
use App\Imports\NightPerformanceImport;
use App\Imports\OtherPerformanceImport;
use App\Imports\PathologyPerformanceImport;
use App\Imports\PublicHealthPerformanceImport;
use App\Imports\RadiologyPerformanceImport;
use App\Imports\SpecialSurveyPerformanceImport;
use App\Imports\SurgeryNewImport;
use App\Imports\UnitPhysicalsImport;
use App\Imports\DieaseCatalogImport;
use App\Imports\DisinfectCostImport;
use App\Imports\ElectricCostImport;
use App\Imports\EmployeeAttendance2019Import;
use App\Imports\EmployeeAttendanceImport;
use App\Imports\EmployeeAttendanceScoreImport;
use App\Imports\EmployeeDispatchPayrollImport;
use App\Imports\EmployeeImport;
use App\Imports\EmployeePayrollImport;
use App\Imports\EmployeeInsuranceBaseImport;
use App\Imports\ExpertDiagnosisImport;
use App\Imports\FixedAssetsImport;
use App\Imports\InpatientImport;
use App\Imports\LaundryCostImport;
use App\Imports\MainIncomeImport;
use App\Imports\MedicalPointValueImport;
use App\Imports\NewTechnologyItemImport;
use App\Imports\PhysicalWorkImport;
use App\Imports\SurgeryImport;
use App\Imports\SurgicalPointValueImport;
use App\Imports\TotalExaminationImport;
use App\Imports\TotalMedicalImageImport;
use App\Imports\TotalSpecialInspectionPharmacyImport;
use App\Imports\UsedGeneralMaterialImport;
use App\Imports\UsedSanitaryMaterialImport;
use App\Imports\NursingHourImport;
use App\Imports\TalentReserveImport;
use App\Imports\PharmacyIncrementalPerformanceImport;
use App\Imports\EmployeePerformanceImport;
use App\Models\BaseEmployeeAttendance;
use App\Models\BedUsing;
use App\Models\ClinicalAttendanceScore;
use App\Models\ClinicalLaboratoryPerformance;
use App\Models\ColorUltrasoundWorkload;
use App\Models\Consultation;
use App\Models\Department;
use App\Models\DepartmentHeadPerformance;
use App\Models\DepartmentManagementCoefficient;
use App\Models\FourItemPerformance;
use App\Models\NewTechnologyDetail;
use App\Models\NightPerformance;
use App\Models\OtherPerformance;
use App\Models\PathologyPerformance;
use App\Models\PublicHealthPerformance;
use App\Models\RadiologyDepartmentPerformance;
use App\Models\SpecialSurveyPerformance;
use App\Models\SurgeryNew;
use App\Models\UnitPhysical;
use App\Models\DieaseCatalog;
use App\Models\DisinfectCost;
use App\Models\ElectricCost;
use App\Models\Employee;
use App\Models\EmployeeAttendance;
use App\Models\MedicalAttendanceScore;
use App\Models\EmployeeDispatchPayroll;
use App\Models\EmployeeInsuranceBase;
use App\Models\EmployeePayroll;
use App\Models\ExpertDiagnosis;
use App\Models\FixedAssets;
use App\Models\Inpatient;
use App\Models\LaundryCost;
use App\Models\MainIncome;
use App\Models\MedicalPointValue;
use App\Models\NewTechnologyItem;
use App\Models\NursingHour;
use App\Models\PhysicalWork;
use App\Models\Surgery;
use App\Models\SurgicalPointValue;
use App\Models\TotalExamination;
use App\Models\TotalMedicalImage;
use App\Models\TotalSpecialInspectionPharmacy;
use App\Models\UsedMaterial;
use App\Models\TalentReserve;
use App\Models\PharmacyIncrementalPerformance;
use App\Models\EmployeePerformance;
use Illuminate\Support\Facades\Log;


class DataRepositoryEloquent extends BaseRepository {
    public function getDataImporter($data_type) {
        $importer_class = null;

        switch ($data_type) {
            case ImportDataType::EMPLOYEE_ATTENDANCE:
                $importer_class = EmployeeAttendanceImport::class;
                break;
            case ImportDataType::FIXED_ASSETS:
                $importer_class = FixedAssetsImport::class;
                break;
            case ImportDataType::USED_SANITARY_MATERIAL:
                $importer_class = UsedSanitaryMaterialImport::class;
                break;
            case ImportDataType::USED_GENERAL_MATERIAL:
                $importer_class = UsedGeneralMaterialImport::class;
                break;
            case ImportDataType::LAUNDRY_COST:
                $importer_class = LaundryCostImport::class;
                break;
            case ImportDataType::DISINFECT_COST:
                $importer_class = DisinfectCostImport::class;
                break;
            case ImportDataType::EMPLOYEE_PAYROLL:
                $importer_class = EmployeePayrollImport::class;
                break;
            case ImportDataType::BED_USDING:
                $importer_class = BedUsdingImport::class;
                break;
            case ImportDataType::CONSULTATION:
                $importer_class = ConsultationImport::class;
                break;
            case ImportDataType::EXPERT_DIAGNOSIS:
                $importer_class = ExpertDiagnosisImport::class;
                break;
            case ImportDataType::PHYSICAL_WORK:
                $importer_class = PhysicalWorkImport::class;
                break;
            case ImportDataType::EMPLOYEE_DISPATCH_PAYROLL:
                $importer_class = EmployeeDispatchPayrollImport::class;
                break;
            case ImportDataType::DIEASE_CATALOG:
                $importer_class = DieaseCatalogImport::class;
                break;
            case ImportDataType::TOTAL_SPECIAL_INSPECTION_PHARMACY:
                $importer_class = TotalSpecialInspectionPharmacyImport::class;
                break;
            case ImportDataType::INPATIENT:
                $importer_class = InpatientImport::class;
                break;
            case ImportDataType::COLOR_ULTRASOUND_WORKLOAD:
                $importer_class = ColorUltrasoundWorkloadImport::class;
                break;
            case ImportDataType::TOTAL_EXAMINATION:
                $importer_class = TotalExaminationImport::class;
                break;
            case ImportDataType::TOTAL_MEDICAL_IMAGE:
                $importer_class = TotalMedicalImageImport::class;
                break;
            case ImportDataType::SURGICAL_POINT_VALUE:
                $importer_class = SurgicalPointValueImport::class;
                break;
            case ImportDataType::SURGERY:
                $importer_class = SurgeryImport::class;
                break;
            case ImportDataType::MAIN_INCOME:
                $importer_class = MainIncomeImport::class;
                break;

            case ImportDataType::DEPARTMENT:
                $importer_class = DepartmentImport::class;
                break;

            case ImportDataType::EMPLOYEE:
                $importer_class = EmployeeImport::class;
                break;
            case ImportDataType::INSURANCE_BASE:
                $importer_class = EmployeeInsuranceBaseImport::class;
                break;

            case ImportDataType::ELECTRIC_COST:
                $importer_class = ElectricCostImport::class;
                break;
            case ImportDataType::NURSING_HOUR:
                $importer_class = NursingHourImport::class;
                break;

            case ImportDataType::EMPLOYEE_ATTENDANCE_2019:
                $importer_class = EmployeeAttendance2019Import::class;
                break;

            case ImportDataType::BASE_SERVICE_INCOME_TARGET:
                $importer_class = BaseServiceIncomeImport::class;
                break;

            case ImportDataType::BASE_BED:
                $importer_class = BaseBedValueImport::class;
                break;

            case ImportDataType::BASE_AVERAGE_COST:
                $importer_class = BaseAverageCostImport::class;
                break;

            case ImportDataType::BASE_ADMISSION_RATE:
                $importer_class = BaseAdmissionRateImport::class;
                break;

            case ImportDataType::BASE_CONTROLLABLE_VARIABLE_COST:
                $importer_class = BaseControllableCostImport::class;
                break;

            case ImportDataType::BASE_DEPARTMENT_INCOME:
                $importer_class = BaseDepartmentIncomeImport::class;
                break;

            case ImportDataType::BASE_AVERAGE_MATERIAL_COST:
                $importer_class = BaseMaterialCostImport::class;
                break;

            case ImportDataType::EMPLOYEE_ATTENDANCE_SCORE:
                $importer_class = EmployeeAttendanceScoreImport::class;
                break;
            case ImportDataType::MEDICAL_POINT_VALUE:
                $importer_class = MedicalPointValueImport::class;
                break;
            case ImportDataType::TARGET_SURGERY_RATE:
                $importer_class = BaseSurgeryRateImport::class;
                break;
            case ImportDataType::NEW_TECHNOLOGY_ITEM:
                $importer_class = NewTechnologyItemImport::class;
                break;
            case ImportDataType::UNIT_PHYSICAL:
                $importer_class = UnitPhysicalsImport::class;
                break;
            case ImportDataType::DEPART_COEFFICIENT:
                $importer_class = DepartCoefficientImport::class;
                break;
            case ImportDataType::SURGERY_NEW:
                $importer_class = SurgeryNewImport::class;
                break;
            case ImportDataType::NIGHT_PERFORMANCE:
                $importer_class = NightPerformanceImport::class;
                break;
            case ImportDataType::RADIOLOGY_PERFORMANCE:
                $importer_class = RadiologyPerformanceImport::class;
                break;
            case ImportDataType::CLINICAL_LABORATORY_PERFORMANCE:
                $importer_class = ClinicalLaboratoryPerformanceImport::class;
                break;
            case ImportDataType::SPECIAL_SURVEY_PERFORMANCE:
                $importer_class = SpecialSurveyPerformanceImport::class;
                break;
            case ImportDataType::PATHOLOGY_PERFORMANCE:
                $importer_class = PathologyPerformanceImport::class;
                break;
            case ImportDataType::PUBLIC_HEALTH_PERFORMANCE:
                $importer_class = PublicHealthPerformanceImport::class;
                break;
            case ImportDataType::TALENT_RESERVE:
                $importer_class = TalentReserveImport::class;
                break;
            case ImportDataType::PHARMACY_INCREMENTAL:
                $importer_class = PharmacyIncrementalPerformanceImport::class;
                break;
            case ImportDataType::OTHER_PERFORMANCE:
                $importer_class = OtherPerformanceImport::class;
                break;
            case ImportDataType::DEPARTMENT_HEAD_PERFORMANCES:
                $importer_class = DepartmentHeadPerformanceImport::class;
                break;
            case ImportDataType::CLINICAL_ATTENDANCE_SCORE:
                $importer_class = ClinicalAttendanceScoreImport::class;
                break;
            case ImportDataType::EMPLOYEE_PERFORMANCE:
                $importer_class = EmployeePerformanceImport::class;
                break;
            case ImportDataType::FOUR_ITEM_PERFORMANCE:
                $importer_class = FourItemPerformanceImport::class;
                break;
            default:
                # code...
                break;
        }

        if (!empty($importer_class)) {
            return app($importer_class);
        } else {
            return null;
        }
    }

    public function getDataCount($data_type, $startTime, $endTime) {
        $model_class = null;
        $count = null;

        switch ($data_type) {
            case ImportDataType::EMPLOYEE_ATTENDANCE:
                $model_class = EmployeeAttendance::class;
                break;
            case ImportDataType::FIXED_ASSETS:
                $model_class = FixedAssets::class;
                break;
            case ImportDataType::USED_GENERAL_MATERIAL:
            case ImportDataType::USED_SANITARY_MATERIAL:
                $model_class = UsedMaterial::class;
                break;
            case ImportDataType::LAUNDRY_COST:
                $model_class = LaundryCost::class;
                break;
            case ImportDataType::DISINFECT_COST:
                $model_class = DisinfectCost::class;
                break;
            case ImportDataType::EMPLOYEE_PAYROLL:
                $model_class = EmployeePayroll::class;
                break;
            case ImportDataType::BED_USDING:
                $model_class = BedUsing::class;
                break;
            case ImportDataType::CONSULTATION:
                $model_class = Consultation::class;
                break;
            case ImportDataType::EXPERT_DIAGNOSIS:
                $model_class = ExpertDiagnosis::class;
                break;
            case ImportDataType::PHYSICAL_WORK:
                $model_class = PhysicalWork::class;
                break;
            case ImportDataType::EMPLOYEE_DISPATCH_PAYROLL:
                $model_class = EmployeeDispatchPayroll::class;
                break;
            case ImportDataType::DIEASE_CATALOG:
                $model_class = DieaseCatalog::class;
                break;
            case ImportDataType::TOTAL_SPECIAL_INSPECTION_PHARMACY:
                $model_class = TotalSpecialInspectionPharmacy::class;
                break;
            case ImportDataType::INPATIENT:
                $model_class = Inpatient::class;
                break;
            case ImportDataType::COLOR_ULTRASOUND_WORKLOAD:
                $model_class = ColorUltrasoundWorkload::class;
                break;
            case ImportDataType::TOTAL_EXAMINATION:
                $model_class = TotalExamination::class;
                break;
            case ImportDataType::TOTAL_MEDICAL_IMAGE:
                $model_class = TotalMedicalImage::class;
                break;
            case ImportDataType::SURGICAL_POINT_VALUE:
                $model_class = SurgicalPointValue::class;
                break;
            case ImportDataType::SURGERY:
                $model_class = Surgery::class;
                break;
            case ImportDataType::MAIN_INCOME:
                $date = request("date", date("Y-m"));
                $model_class = MainIncome::class;
                $count = $model_class::date($date)->whereBetween("created_at", [$startTime, $endTime])->count();
                break;

            case ImportDataType::DEPARTMENT:
                $model_class = Department::class;
                break;

            case ImportDataType::EMPLOYEE:
                $model_class = Employee::class;
                break;
            case ImportDataType::INSURANCE_BASE:
                $model_class = EmployeeInsuranceBase::class;
                break;

            case ImportDataType::ELECTRIC_COST:
                $model_class = ElectricCost::class;
                break;
            case ImportDataType::NURSING_HOUR:
                $model_class = NursingHour::class;
                break;

            case ImportDataType::EMPLOYEE_ATTENDANCE_2019:
                $model_class = BaseEmployeeAttendance::class;
                break;

            case ImportDataType::BASE_SERVICE_INCOME_TARGET:
            case ImportDataType::BASE_BED:
            case ImportDataType::BASE_AVERAGE_COST:
            case ImportDataType::BASE_ADMISSION_RATE:
            case ImportDataType::BASE_CONTROLLABLE_VARIABLE_COST:
            case ImportDataType::BASE_DEPARTMENT_INCOME:
            case ImportDataType::BASE_AVERAGE_MATERIAL_COST:
            case ImportDataType::TARGET_SURGERY_RATE:
                $model_class = Department::class;
                $count = $model_class::whereBetween("created_at", [$startTime, $endTime])->count();
                break;
            case ImportDataType::EMPLOYEE_ATTENDANCE_SCORE:
                $model_class = MedicalAttendanceScore::class;
                break;

            case ImportDataType::MEDICAL_POINT_VALUE:
                $model_class = MedicalPointValue::class;
                break;

            case ImportDataType::NEW_TECHNOLOGY_ITEM:
                $model_class = NewTechnologyDetail::class;
                break;

            case ImportDataType::UNIT_PHYSICAL:
                $model_class = UnitPhysical::class;
                break;
            case ImportDataType::DEPART_COEFFICIENT:
                $model_class = DepartmentManagementCoefficient::class;
                break;
            case ImportDataType::SURGERY_NEW:
                $model_class = SurgeryNew::class;
                break;

            case ImportDataType::NIGHT_PERFORMANCE:
                $model_class = NightPerformance::class;
                break;
            case ImportDataType::RADIOLOGY_PERFORMANCE:
                $model_class = RadiologyDepartmentPerformance::class;
                break;
            case ImportDataType::CLINICAL_LABORATORY_PERFORMANCE:
                $model_class = ClinicalLaboratoryPerformance::class;
                break;
            case ImportDataType::SPECIAL_SURVEY_PERFORMANCE:
                $model_class = SpecialSurveyPerformance::class;
                break;
            case ImportDataType::PATHOLOGY_PERFORMANCE:
                $model_class = PathologyPerformance::class;
                break;
            case ImportDataType::PUBLIC_HEALTH_PERFORMANCE:
                $model_class = PublicHealthPerformance::class;
                break;
            case ImportDataType::TALENT_RESERVE:
                $model_class = TalentReserve::class;
                break;
            case ImportDataType::PHARMACY_INCREMENTAL:
                $model_class = PharmacyIncrementalPerformance::class;
                break;
            case ImportDataType::OTHER_PERFORMANCE:
                $model_class = OtherPerformance::class;
                break;
            case ImportDataType::DEPARTMENT_HEAD_PERFORMANCES:
                $model_class = DepartmentHeadPerformance::class;
                break;
            case ImportDataType::CLINICAL_ATTENDANCE_SCORE:
                $model_class = ClinicalAttendanceScore::class;
                break;
            case ImportDataType::EMPLOYEE_PERFORMANCE:
                $model_class = EmployeePerformance::class;
                break;
            case ImportDataType::FOUR_ITEM_PERFORMANCE:
                $model_class = FourItemPerformance::class;
                $count = $model_class::whereBetween("updated_at", [$startTime, $endTime])->count();
                break;
            default:
                # code...
                break;
        }

        if (!empty($count)){
            return $count;
        }

        if (!empty($model_class)) {
            $count = $model_class::whereBetween("updated_at", [$startTime, $endTime])->count();
            return $count;
        } else {
            return null;
        }
    }


    public function getModel($data_type) {
        $model_class = null;

        switch ($data_type) {
            case ImportDataType::EMPLOYEE_ATTENDANCE:
                $model_class = EmployeeAttendance::class;
                break;
            case ImportDataType::FIXED_ASSETS:
                $model_class = FixedAssets::class;
                break;
            case ImportDataType::USED_GENERAL_MATERIAL:
            case ImportDataType::USED_SANITARY_MATERIAL:
                $model_class = UsedMaterial::class;
                break;
            case ImportDataType::LAUNDRY_COST:
                $model_class = LaundryCost::class;
                break;
            case ImportDataType::DISINFECT_COST:
                $model_class = DisinfectCost::class;
                break;
            case ImportDataType::EMPLOYEE_PAYROLL:
                $model_class = EmployeePayroll::class;
                break;
            case ImportDataType::BED_USDING:
                $model_class = BedUsing::class;
                break;
            case ImportDataType::EMPLOYEE_DISPATCH_PAYROLL:
                $model_class = EmployeeDispatchPayroll::class;
                break;
            case ImportDataType::TOTAL_SPECIAL_INSPECTION_PHARMACY:
                $model_class = TotalSpecialInspectionPharmacy::class;
                break;
            case ImportDataType::INPATIENT:
                $model_class = Inpatient::class;
                break;
            case ImportDataType::COLOR_ULTRASOUND_WORKLOAD:
                $model_class = ColorUltrasoundWorkload::class;
                break;
            case ImportDataType::TOTAL_EXAMINATION:
                $model_class = TotalExamination::class;
                break;
            case ImportDataType::TOTAL_MEDICAL_IMAGE:
                $model_class = TotalMedicalImage::class;
                break;
            case ImportDataType::ELECTRIC_COST:
                $model_class = ElectricCost::class;
                break;
            case ImportDataType::EMPLOYEE_ATTENDANCE_SCORE:
                $model_class = MedicalAttendanceScore::class;
                break;
            case ImportDataType::NEW_TECHNOLOGY_ITEM:
                $model_class = NewTechnologyDetail::class;
                break;
            case ImportDataType::SURGERY_NEW:
                $model_class = SurgeryNew::class;
                break;
            case ImportDataType::NIGHT_PERFORMANCE:
                $model_class = NightPerformance::class;
                break;
            case ImportDataType::RADIOLOGY_PERFORMANCE:
                $model_class = RadiologyDepartmentPerformance::class;
                break;
            case ImportDataType::CLINICAL_LABORATORY_PERFORMANCE:
                $model_class = ClinicalLaboratoryPerformance::class;
                break;
            case ImportDataType::SPECIAL_SURVEY_PERFORMANCE:
                $model_class = SpecialSurveyPerformance::class;
                break;
            case ImportDataType::PATHOLOGY_PERFORMANCE:
                $model_class = PathologyPerformance::class;
                break;
            case ImportDataType::PUBLIC_HEALTH_PERFORMANCE:
                $model_class = PublicHealthPerformance::class;
                break;
            case ImportDataType::TALENT_RESERVE:
                $model_class = TalentReserve::class;
                break;
            case ImportDataType::PHARMACY_INCREMENTAL:
                $model_class = PharmacyIncrementalPerformance::class;
                break;
            case ImportDataType::OTHER_PERFORMANCE:
                $model_class = OtherPerformance::class;
                break;
            case ImportDataType::DEPARTMENT_HEAD_PERFORMANCES:
                $model_class = DepartmentHeadPerformance::class;
                break;
            case ImportDataType::CLINICAL_ATTENDANCE_SCORE:
                $model_class = ClinicalAttendanceScore::class;
                break;
            case ImportDataType::FOUR_ITEM_PERFORMANCE:
                $model_class = FourItemPerformance::class;
                break;
            default:
                $model_class = null;
                break;
        }

       return $model_class;
    }

    public function deleteData($model_class, $date){
        try {
            switch ($model_class){
                case FixedAssets::class:
                    $model_class::where('store_date', '>', $date)->delete();
                    break;
                default:
                    $model_class::where('date', $date)->delete();
                    break;
            }
        } catch (\Exception $exception){
            Log::error($exception->getMessage().'--'.$exception->getFile().'--'.$exception->getLine());
            throw new MessageException('数据删除失败');
        }
    }

}

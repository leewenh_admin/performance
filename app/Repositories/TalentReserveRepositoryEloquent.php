<?php

namespace App\Repositories;

use App\Models\TalentReserve;

class TalentReserveRepositoryEloquent extends BaseRepository {
    protected $actionClass = TalentReserve::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with("employee:id,name")->with("department:id,name,code");

        $query = $query->join('departments', 'talent_reserves.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'talent_reserves.date', '=', 'lock_performances.date');
        $query->select('talent_reserves.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $start_date = request("start_date", null);
        $end_date = request("end_date", null);
        if (!empty($start_date)){
            $query = $query->where("start_date", $start_date);
        }
        if (!empty($end_date)){
            $query = $query->where("end_date", $end_date);
        }

        $employees_id = request("employees_id", null);
        if (!empty($employees_id)){
            $query = $query->where("employees_id", $employees_id);
        }

        $code = request("code", null);
        if (!empty($code)){
            $query = $query->where("talent_reserves.code", "like", "%$code%");
        }

        $name = request("name", null);
        if (!empty($name)){
            $query = $query->where("talent_reserves.name", "like", "%$name%");
        }

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("talent_reserves.date", $date);
        }

        $department_id = request("department_id", null);
        if (!empty($department_id)){
            $query = $query->where("department_id", $department_id);
        }

        return $query;
    }
}

<?php

namespace App\Repositories;

use App\Models\ClinicalAttendanceScore;

class ClinicalAttendanceScoreRepositoryEloquent extends BaseRepository {
    protected $actionClass = ClinicalAttendanceScore::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "department:id,name,code",
        ]);
        $query = $query->join('departments', 'clinical_attendance_scores.departments_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'clinical_attendance_scores.date', '=', 'lock_performances.date');

        $departments_id = request("departments_id", null);
        if(!empty($departments_id)){
            $query = $query->where("departments_id", $departments_id);
        }

        $date = request("date", null);
        if(!empty($date)){
            $query = $query->where("clinical_attendance_scores.date", $date);
        }
        $query->select('clinical_attendance_scores.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        return $query;
    }
}

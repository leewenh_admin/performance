<?php

namespace App\Repositories;

use App\Models\DepartmentPerformance;
use App\Models\Setting;

class SettingRepositoryEloquent extends BaseRepository {
    protected $actionClass = Setting::class;

    public function administrativeAverage($data){
        Setting::updateOrCreate(
            [
                "key" => "administrativeAverage",
                "title" => $data["date"],
            ],
            [
                "value" => $data["administrativeAverage"],
                "value_type" => 1,
            ]

        );
    }

    public function getAdministrativeAverage($data){
        $data = Setting::where("key", "administrativeAverage")
            ->where("title", $data["date"])->first();
        $administrativeAverage = 0;
        if($data){
            $administrativeAverage = $data->value;
        }

        return $administrativeAverage;
    }


    public function specialPerformance($data){
        $performance =  DepartmentPerformance::where("department_id", $data["department_id"])
            ->where("date", $data["date"])->first();
        if (!$performance){
            return "请先生成科室绩效数据";
        }

        $p = $performance->total_performances - $performance->special_performance + $data["performance"] ;

        DepartmentPerformance::where("department_id", $data["department_id"])
            ->where("date", $data["date"])
            ->update([
                "total_performances" => $p,
                "special_performance" => $data["performance"],
            ]);

        return null;
    }

    public function getSpecialPerformance($data){
        $performance =  DepartmentPerformance::where("department_id", $data["department_id"])
            ->where("date", $data["date"])->first();
        $data = null;
        if($performance){
            $data = $performance->special_performance;
        }

        return $data;
    }
}

<?php

namespace App\Repositories;

use App\Exceptions\MessageException;
use App\Models\ClinicalAttendanceScore;
use App\Models\ClinicalLaboratoryPerformance;
use App\Models\DepartmentHeadPerformance;
use App\Models\DepartmentPerformance;
use App\Models\DirectCost;
use App\Models\DisinfectCost;
use App\Models\ElectricCost;
use App\Models\EmployeeAttendance;
use App\Models\EmployeeDispatchPayroll;
use App\Models\EmployeeInsuranceBase;
use App\Models\EmployeePayroll;
use App\Models\LaundryCost;
use App\Models\MainIncome;
use App\Models\MedicalAttendanceScore;
use App\Models\NewTechnologyItem;
use App\Models\NightPerformance;
use App\Models\PathologyPerformance;
use App\Models\PharmacyIncrementalPerformance;
use App\Models\RadiologyDepartmentPerformance;
use App\Models\SpecialSurveyPerformance;
use App\Models\SurgeryNew;
use App\Models\UnitPhysical;
use App\Models\UsedMaterial;
use Illuminate\Support\Facades\Hash;

class CheckDataRepositoryEloquent extends BaseRepository {
    /**
     * 核查固定成本所需数据
     * @param $date
     * @return string
     */
    public function checkDirectCost($date){
        $arrDate = explode('-', $date);
        //主要收入
        $num1 = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->count();
        if ($num1 == 0){
            return "error1";
        }
        //消毒费
        $num2 = DisinfectCost::where("date", $date)
            ->count();
        if ($num2 == 0){
            return "error2";
        }
        //电费
        $num3 = ElectricCost::where("date", $date)
            ->count();
        if ($num3 == 0){
            return "error3";
        }
        //考勤
        $num4 = EmployeeAttendance::where("date", $date)
            ->count();
        if ($num4 == 0){
            return "error4";
        }
        //劳务派遣职员工资表
        $num5 = EmployeeDispatchPayroll::where("date", $date)
            ->count();
        if ($num5 == 0){
            return "error5";
        }
        //职员保险基数
        $num6 = EmployeeInsuranceBase::count();
        if ($num6 == 0){
            return "error6";
        }
        //职员工资表
        $num7 = EmployeePayroll::where("date", $date)
            ->count();
        if ($num7 == 0){
            return "error7";
        }
        //洗涤费
        $num8 = LaundryCost::where("date", $date)
            ->count();
        if ($num8 == 0){
            return "error8";
        }
        //领用材料
        $num9 = UsedMaterial::where("date", $date)
            ->count();
        if ($num9 == 0){
            return "error9";
        }

        return "success";
    }

    /**
     * 核查科室绩效所需数据
     * @param $date
     * @return string
     */
    public function checkDepartmentPerformance($date){
        //考勤
        $num1 = EmployeeAttendance::where("date", $date)
            ->count();
        if ($num1 == 0){
            return "error1";
        }

        $arrDate = explode('-', $date);
        //主要收入
        $num2 = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->count();
        if ($num2 == 0){
            return "error2";
        }

        //固定成本
        $num3 = DirectCost::where("date", $date)
            ->count();
        if ($num3 == 0){
            return "error3";
        }

        //手术明细
        $num4 = SurgeryNew::where("date", $date)
            ->count();
        if ($num4 == 0){
            return "error4";
        }

        //临床综合考核绩效
        $num5 = ClinicalAttendanceScore::where("date", $date)
            ->count();
        if ($num5 == 0){
            return "error5";
        }

        //医技综合考核绩效
        $num6 = MedicalAttendanceScore::where("date", $date)
            ->count();
        if ($num6 == 0){
            return "error5";
        }

        //新技术业务项
        $num7 = NewTechnologyItem::count();
        if ($num7 == 0){
            return "error7";
        }

        //单位体检
        $num8 = UnitPhysical::where("date", $date)
            ->count();
        if ($num8 == 0){
            return "error8";
        }

        //药剂科增量绩效
        $num9 = PharmacyIncrementalPerformance::where("date", $date)
            ->count();
        if ($num9 == 0){
            return "error9";
        }

        return "success";
    }

    /**
     * 核查专项绩效所需数据
     * @param $date
     * @return string
     */
    public function checkSpecialPerformance($date){
        //管理绩效明细
        $num1 = DepartmentHeadPerformance::where("date", $date)
            ->count();
        if ($num1 == 0){
            return "error1";
        }

        //临床综合考核绩效
        $num2 = ClinicalAttendanceScore::where("date", $date)
            ->count();
        if ($num2 == 0){
            return "error2";
        }

        //医共体检验中心绩效
        $num3 = ClinicalLaboratoryPerformance::where("date", $date)
            ->count();
        if ($num3 == 0){
            return "error3";
        }

        //医共体病理中心绩效
        $num4 = PathologyPerformance::where("date", $date)
            ->count();
        if ($num4 == 0){
            return "error4";
        }

        //医共体影像中心绩效
        $num5 = RadiologyDepartmentPerformance::where("date", $date)
            ->count();
        if ($num5 == 0){
            return "error5";
        }

        //医共体心电中心绩效
        $num6 = SpecialSurveyPerformance::where("date", $date)
            ->count();
        if ($num6 == 0){
            return "error6";
        }

        //考勤
        $num7 = EmployeeAttendance::where("date", $date)
            ->count();
        if ($num7 == 0){
            return "error7";
        }

        $arrDate = explode('-', $date);
        //主要收入
        $num8 = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->count();
        if ($num8 == 0){
            return "error8";
        }

        //医技综合考核绩效
        $num9 = MedicalAttendanceScore::where("date", $date)
            ->count();
        if ($num9 == 0){
            return "error9";
        }

        //手术明细
        $num10 = SurgeryNew::where("date", $date)
            ->count();
        if ($num10 == 0){
            return "error10";
        }

        //科室绩效
        $num11 = DepartmentPerformance::where("date", $date)
            ->count();
        if ($num11 == 0){
            return "error11";
        }

        //科室绩效
        $num12 = NightPerformance::where("date", $date)
            ->count();
        if ($num12 == 0){
            return "error12";
        }

        return "success";

    }

    /**
     * 验证密码
     * @param $password
     * @throws MessageException
     */
    public function checkPassword($password){
        if (!Hash::check($password, auth()->user()->getAuthPassword())){
            throw new  MessageException('密码验证错误');
        }
    }
}

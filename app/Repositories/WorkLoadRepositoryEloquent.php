<?php

namespace App\Repositories;

use App\Enums\DepartmentType;
use App\Models\BaseData;
use App\Models\BedUsing;
use App\Models\EmployeeAttendance;
use App\Models\MainIncome;
use App\Models\Department;
use App\Models\Setting;
use App\Models\SubDepartment;
use App\Enums\IncomeType;
use Illuminate\Support\Facades\DB;


class WorkLoadRepositoryEloquent extends BaseRepository
{

    /**
     * 临床工作量绩效
     * @param $date
     * @param $department_id
     * @param $department
     * @return false|float
     */
    public function getWorkLoadPerformance($date, $department_id ,$department)
    {
        $price = BaseData::query()->where('date', $date)
            ->where('department_id', $department_id)->first();
        $data = $this->getOutPatientPerformance($date, $department_id) * ($price->outpatient_price ?? 0) +
            $this->getInHospitalPerformance($date, $department_id,$department) * ($price->admission_price ?? 0);

        return $data;
    }

    /**
     * 门诊工作量绩效
     * @param $date
     * @param $department_id
     * @return false|float
     */
    public function getOutPatientPerformance($date, $department_id)
    {
//        //急诊人数
//        $outPatientCount = $this->getOutPatientCount($date, $department_id);
//        //绩效单价
//        $performancePrice = $this->getOutPerformancePrice($date, $department_id);
//        //门诊例均费用系数
//        $outAverageCost = $this->getOutAverageCost($date, $department_id);
//        //收治率系数
//        $admissionRate = $this->getAdmissionRate($date, $department_id);
//        //科室间人均效率的横向比较
//        $perCapitaHorizontalComparison = $this->getPerCapitaHorizontalComparison($date, $department_id);
//        //本科室总效率的纵向比较
//        $totalVerticalComparison = $this->getTotalVerticalComparison($date, $department_id);
//        //药品例均费用控制
//        $getEachDrugCost = $this->getEachDrugCost($date, $department_id);
//
//        $outPatientPerformance = $outPatientCount * $performancePrice * (
//                $outAverageCost * 0.35 + $admissionRate * 0.35 +
//                sqrt($perCapitaHorizontalComparison * $totalVerticalComparison) * 0.3
//            ) - $getEachDrugCost;
        //急诊人数
        $outPatientCount = $this->getOutPatientCount($date, $department_id);
        //绩效单价
        $performancePrice = $this->getOutPerformancePrice($date, $department_id);
        $outPatientPerformance = $outPatientCount * $performancePrice;
        return round($outPatientPerformance, 2);
    }

    /**
     * 住院工作量绩效
     * @param $date
     * @param $department_id
     * @param $department
     * @return false|float
     */
    public function getInHospitalPerformance($date, $department_id, $department)
    {
//        //住院床日
//        $inHospitalBed = $this->getInHospitalBed($date, $department_id);
//        //护理时数
//        $nursingHour = $this->getNursingHour($date, $department_id);
//        //绩效单价
//        $inPerformancePrice = $this->getInPerformancePrice($date, $department_id);
//        //出院病人例均费用系数
//        $leaveAverageCost = $this->getLeaveAverageCost($date, $department_id);
//        //科室每床日均次费用
//        $departmentBedDayCost = $this->getDepartmentBedDayCost($date, $department_id);
//        //医院每床日均次费用
//        $hospitalBedDayCost = $this->getHospitalBedDayCost($date, $department_id);
//        //出院病人例均药品费用系数
//        $drugAverageCost = $this->getDrugAverageCost($date, $department_id);
//        //出院病人例均耗材费用系数
//        $meterialAverageCost = $this->getMaterialAverageCost($date, $department_id);
//        //住院科室间人均效率的横向比较
//        $inHospitalHorizontalComparison = $this->getInHospitalHorizontalComparison($date, $department_id);
//        //住院科室总效率纵向比较
//        $inHospitalVerticalComparison = $this->getInHospitalVerticalComparison($date, $department_id);
//        //术前待床日调整奖励
//        $getStayBedDay = $this->getStayBedDay($date, $department_id);
//
//        if ($hospitalBedDayCost == 0) {
//            return 0;
//        }
//
//        $performance = $inHospitalBed * $nursingHour  * $inPerformancePrice *
//            ($leaveAverageCost * ($departmentBedDayCost / $hospitalBedDayCost) * 0.25 +
//                $drugAverageCost * 0.3 + $meterialAverageCost * 0.25 +
//                sqrt($inHospitalHorizontalComparison * $inHospitalVerticalComparison) * 0.2 ) -
//                $getStayBedDay;
        $department_ids = $this->getOutPatientDepartments($department_id);
//        $bedUsing = BedUsing::query()
//            ->where("date", $date)
//            ->whereIn('sub_departments_id',$department_ids)
//            ->get();
        //出院者实际占床日
//        $outHospitalBed = MainIncome::query()->date($date)
//            ->whereIn('execute_department_id',$department_ids)
//            ->whereRaw("left(charging_time,7) =  '".$date."'")
////            ->whereRaw("left(leave_time,6) =  '".str_replace('-', '', $date)."'")
//            ->where('item_cate_code',1)
//            ->whereNotIn('item_code',[1034,1031,1030,1033])
//            ->sum('quantity');
        $outHospitalBed = $this->getOutHospitalBed($date,$department_ids);
        //出院人数
        $outHospitalCount = $this->getOutHospitalCount($date, $department_ids);
        //实际平均住院日
        if ($outHospitalCount == 0){
            $avgHosDay = 0;
        }else{
            $avgHosDay = round($outHospitalBed/$outHospitalCount,2);
        }
        //实际占床日
        $inHospitalBed = $this->getBedCount($date,$department_ids);
        //基准平均住院日
        $baseAvgHosDay = $department["base_avg_hos_days"];
        //床日系数
        if ($baseAvgHosDay == 0) return 0;
        if($avgHosDay < $baseAvgHosDay* 0.95){
            $bedDayCoefficient =  round($avgHosDay / ($baseAvgHosDay* 0.95),2);
        }elseif ($avgHosDay > $baseAvgHosDay*1.05){
            $bedDayCoefficient = 2 - round($avgHosDay/($baseAvgHosDay*1.05),2);
        }else{
            $bedDayCoefficient = 1;
        }
        //护理时数
        $nursingHour = $this->getNursingHour($date, $department_id);
        //绩效单价
        $inPerformancePrice = $this->getInPerformancePrice($date, $department_id);

        $performance = $inHospitalBed * $nursingHour * $inPerformancePrice * $bedDayCoefficient;
        return round($performance, 2);

    }

    /**
     * 获取不同类型的二级科室
     * @param $type
     * @param $department_id
     * @return int
     */
    public function getOutPatientDepartments($department_id)
    {
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();

        return $department_ids;
    }

    /**
     * 门急诊人次
     */
    public function getOutPatientCount($date, $department_id)
    {
        // 获取一级科室的门诊类型二级科室ID列表
//        $department_ids = $this->getOutPatientDepartments($department_id);
//        if (empty($department_ids)) {
//            return 0;
//        }

        //类型 为 门诊, 项目类别  = 挂号费
        $arrDate = explode('-', $date);

        //科室内考勤的医生
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->get()
            ->pluck("employees_id")
            ->all();

        $outPatientCount = MainIncome::date($date)->whereIn(
            "prescribe_doctor_id", $doctors
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::OUTPATIENT)
            ->whereIn("item_code", [1002, 1003, 1006, 1008, 1010])
            ->select("serial_no")
            ->distinct()
            ->get()
            ->count();

        return $outPatientCount;

    }


    /**
     * 门诊绩效单价
     * @param $date
     * @param $department_id
     * @return int
     */
    public function getOutPerformancePrice($date, $department_id)
    {
        //绩效单价暂定3元
        return 1;
    }


    /**
     * 门诊例均费用系数
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getOutAverageCost($date, $department_id)
    {
        // 获取一级科室的门诊类型二级科室ID列表
//        $department_ids = $this->getOutPatientDepartments($department_id);
//        if (empty($department_ids)) {
//            return 0;
//        }

        //科室内考勤的医生
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->get()
            ->pluck("employees_id")
            ->all();
        //本期门诊均例费用 = （门诊收入 / 门急诊人次）
        //计算门诊收入
        $arrDate = explode('-', $date);
        $outPatientCountIncome = MainIncome::date($date)->whereIn(
            "prescribe_doctor_id", $doctors
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::OUTPATIENT)
            ->sum("amount");

        //门诊人数
        $outPatientCount = $this->getOutPatientCount($date, $department_id);
        if ($outPatientCount == 0) {
            return 0;
        }
        //本期门诊均例费用
        $thisAverageCost = $outPatientCountIncome / $outPatientCount;
        //目标均次费用
        $targetAverageCost = Department::where("id", $department_id)
            ->value("target_average_cost");

        if ($targetAverageCost == 0 || $thisAverageCost == 0) {
            return 0;
        }

        if ($thisAverageCost > $targetAverageCost * 1.1) {
            $outAverageCost = $targetAverageCost / $thisAverageCost;
        } else if ($thisAverageCost >= $targetAverageCost * 0.9
            && $thisAverageCost <= $targetAverageCost * 1.1) {
            $outAverageCost = 1;
        } else {
            $outAverageCost = $thisAverageCost / $targetAverageCost;
        }

        return round($outAverageCost, 4);

    }

    /**
     * 收治率系数
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getAdmissionRate($date, $department_id)
    {
        // 获取一级科室的门诊类型二级科室ID列表
        $type = DepartmentType::OUTPATIENT;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }

        //查询入院人次
        $arrDate = explode("-", $date);
        $leftDate = str_replace('-', '', $date);
        $inHospitalData = MainIncome::date($date)->whereIn("admission_department_id", $department_ids)
            ->where("income_type", IncomeType::INPATIENT)
            ->whereRaw("left(admission_time,6) = '$leftDate'")
            ->select("serial_no")
            ->distinct()
            ->get();
        $inpatientsCount = count($inHospitalData);

        //门诊人数
        $outPatientCount = $this->getOutPatientCount($date, $department_id);
        if ($outPatientCount == 0) {
            return 0;
        }
        //收治率=入院人次/门诊人次
        $admissionRate = $inpatientsCount / $outPatientCount;

        //目标收治率
        $targetAdmissionRate = Department::where("id", $department_id)
            ->value("target_admission_rate");
        //数据库存储的是百分比
        $targetAdmissionRate = $targetAdmissionRate / 100;

        if ($targetAdmissionRate == 0 || $admissionRate == 0) {
            return 0;
        }

        if ($admissionRate > $targetAdmissionRate * 1.1) {
            $admission = $targetAdmissionRate / $admissionRate;
        } else if ($admissionRate >= $targetAdmissionRate * 0.9
            && $admissionRate <= $targetAdmissionRate * 1.1) {
            $admission = 1;
        } else {
            $admission = $admissionRate / $targetAdmissionRate;
        }
        return round($admission, 4);
    }

    /**
     * 门诊科室间人均效率的横向比较
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getPerCapitaHorizontalComparison($date, $department_id)
    {
        // 获取一级科室的门诊类型二级科室ID列表
        $type = DepartmentType::OUTPATIENT;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }

        //本月科室医生人数
        $departmentDoctorNum = EmployeeAttendance::leftjoin("employees", "employees.id", "employee_attendances.employees_id")
            ->whereIn("sub_departments_id", $department_ids)
            ->where("date", $date)
            ->where("employees.attendances_type", 1)
            ->whereRaw("employee_attendances.actual_attendance = (select max(t.actual_attendance) 
            from employee_attendances as t where 
            employee_attendances.employees_id = t.employees_id and t.date = '$date') ")
            ->count();

        //科室门诊人数
        $outPatientCount = $this->getOutPatientCount($date, $department_id);
        //全院门诊人数
        $arrDate = explode('-', $date);

        $outPatientAllCount = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::OUTPATIENT)
            ->whereIn("item_code", [1002, 1003, 1006, 1008, 1010])
            ->select("serial_no")
            ->distinct()
            ->get()
            ->count();
        //全院医生人次   TODO::职员表还需要加一个职称分类来筛选考勤类型是医生
        $allDoctorNum = EmployeeAttendance::leftjoin("employees", "employees.id", "employee_attendances.employees_id")
            ->where("date", $date)
            ->where("employees.attendances_type", 1)
            ->select("employees_id")
            ->groupBy("employees_id")
            ->get()
            ->count();

        if ($departmentDoctorNum == 0 || $allDoctorNum == 0) {
            return 0;
        }
        //科室医生人均门诊人次 = 科室门诊人次 / 科室医生人数
        $departmentWorkNum = $outPatientCount / $departmentDoctorNum;
        //全院医生人均门诊人次 = 全院门诊人次 / 全院医生人次
        $hospitalWorkTime = $outPatientAllCount / $allDoctorNum;
        //门诊科室间人均效率横向比较 = 科室医生人均门诊人次 / 全院医生人均门诊人次
        if ($hospitalWorkTime == 0) {
            return 0;
        }
        $perCapitaHorizontalComparison = $departmentWorkNum / $hospitalWorkTime;

        return round($perCapitaHorizontalComparison, 4);
    }

    /**
     * 门诊科室总效率纵向比较
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getTotalVerticalComparison($date, $department_id)
    {
        // 获取一级科室的门诊类型二级科室ID列表
        $type = DepartmentType::OUTPATIENT;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        //科室门诊人数
        $outPatientCount = $this->getOutPatientCount($date, $department_id);
        BaseData::updateOrCreate(
            [
                "date" => $date ,
                "department_id" => $department_id
            ],
            [
                "target_patient_count" => $outPatientCount,
            ]
        );
        //科室基期门急诊人次
        //先查询基值表，如果没有数据就从department表查询2019年数据
        $year = date("Y",strtotime($date));
        $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
        if ($year == "2021"){//如果是2021年就查询2019年的数据
            $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
        }
        $baseOutPatientCount = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();
        if (empty($baseOutPatientCount)){
            $baseOutPatientCount = Department::where("id", $department_id)->value("target_patient_count");
        } else {
            $baseOutPatientCount = $baseOutPatientCount->target_patient_count;
        }
        if ($baseOutPatientCount == 0) {
            return 0;
        }
        //门诊科室总效率纵向比较
        $totalVerticalComparison = $outPatientCount / $baseOutPatientCount;

        return round($totalVerticalComparison, 4);
    }

    /**
     * 药品例均费用控制
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getEachDrugCost($date, $department_id)
    {
        //todo   药品例均费用控制 暂时返回0
        return 0;

        // 获取一级科室的门诊类型二级科室ID列表
        $type = DepartmentType::OUTPATIENT;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        //门诊人数
        $outPatientCount = $this->getOutPatientCount($date, $department_id);
        $arrDate = explode('-', $date);
        //药品收入,按开单科室来计算
        //TODO 目前只统计门诊的药品收入实际值过小
        $drugInCome = MainIncome::date($date)->whereIn(
            "prescribe_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::OUTPATIENT)
            ->whereIn("item_cate_code", [2, 3, 4, 82])//西药费,中成药,中草药
            ->sum("amount");

        $target = Department::where("id", $department_id)
            ->select("target_average_drug_cost", "punish_amount")
            ->first();
        //出院人数
        $leaverCount = $this->getOutHospitalCount($date, $department_ids);
        if ($leaverCount == 0) {
            return 0;
        }
        $eachDrugCost = $drugInCome / $leaverCount;
        if ($eachDrugCost > $target->target_average_drug_cost){
            $value = ($eachDrugCost - $target->target_average_drug_cost) * $outPatientCount * $target->punish_amount;
        } else {
            $value = 0;
        }

        return $value;
    }


    /**
     * 住院床日
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getInHospitalBed($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
        $type = DepartmentType::WARD;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        //改为科室总占床日
        return $this->getBedCount($date,$department_ids);

//        //出院人数
//        $leaverCount = $this->getOutHospitalCount($date, $department_ids);
//        if ($leaverCount == 0) return 0;
//        //实际平均住院日
//        $arrDate = explode("-", $date);
//        $inHospitalDay = MainIncome::date($date)->whereIn("admission_department_id", $department_ids)
//            ->where("income_type", IncomeType::INPATIENT)
//            ->whereYear("charging_time", $arrDate[0])
//            ->whereMonth("charging_time", $arrDate[1])
//            ->selectRaw("TIMESTAMPDIFF(DAY,admission_time , leave_time) as day, serial_no")
//            ->distinct()
//            ->get()
//            ->toArray();
//        if(count($inHospitalDay) == 0) {
//            return 0;
//        }
//        $sum = array_sum(array_map(function($val){return $val['day'];}, $inHospitalDay));
//        $avgInHospitalDay = $sum / $leaverCount;
//
////        $avgInHospitalDay = BedUsing::whereIn("sub_departments_id", $department_ids)
////            ->where("date", $date)
////            ->avg("leaver_avg_day");
//        //目标平均住院日
//        $targetInHospitalDay = Department::where("id", $department_id)->value("target_in_hospital_day");
//
//        if ($targetInHospitalDay == 0) {
//            return 0;
//        }
//
//        if ($avgInHospitalDay >= 0 && $avgInHospitalDay <= $targetInHospitalDay * 0.9) {
//            $inHospitalBed = $leaverCount * $avgInHospitalDay *
//                ($avgInHospitalDay / ($targetInHospitalDay * 0.9));
//        } else if ($avgInHospitalDay >= $targetInHospitalDay * 0.9
//            && $avgInHospitalDay <= $targetInHospitalDay * 1.1) {
//            $inHospitalBed = $leaverCount * $avgInHospitalDay;
//        } else {
//            $inHospitalBed = $leaverCount * $avgInHospitalDay * 1.1 *
//                (2 - $avgInHospitalDay / ($targetInHospitalDay * 1.1));
//        }
//
//        return round($inHospitalBed, 2);
    }

    /**
     * 住院绩效单价
     * @param $date
     * @param $department_id
     * @return int
     */
    public function getInPerformancePrice($date, $department_id)
    {
        //绩效单价暂定3元
        return 1;
    }

    /**
     * 出院病人例均费用系数
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getLeaveAverageCost($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
        $type = DepartmentType::WARD;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        $arrDate = explode('-', $date);
        //出院病人总收入
        $leavePatientIncome = MainIncome::date($date)->whereIn(
            "leave_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereNotNull("leave_time")
            ->sum("amount");
        //出院人数
        $leaverCount = $this->getOutHospitalCount($date, $department_ids);
        if ($leaverCount == 0) {
            return 0;
        }
        //目标人均费用
        $targetAverageLeaveCost = Department::where("id", $department_id)
            ->value("target_average_leave_cost");
        //实际出院病人均例费用
        $realAverageLeaveCost = $leavePatientIncome / $leaverCount;

        if ($targetAverageLeaveCost == 0 || $realAverageLeaveCost == 0) {
            return 0;
        }

        if ($realAverageLeaveCost > $targetAverageLeaveCost * 1.1) {
            $leaveAverageCost = $targetAverageLeaveCost / $realAverageLeaveCost;
        } elseif ($realAverageLeaveCost >= $targetAverageLeaveCost * 0.9
            && $realAverageLeaveCost <= $targetAverageLeaveCost * 1.1) {
            $leaveAverageCost = 1;
        } else {
            $leaveAverageCost = $realAverageLeaveCost / $targetAverageLeaveCost;
        }

        return round($leaveAverageCost, 4);

    }

    /**
     * 出院病人例均药品费用系数
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getDrugAverageCost($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
        $type = DepartmentType::WARD;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        $arrDate = explode('-', $date);
        //出院病人药品总收入

        $leavePatientDrugIncome = MainIncome::date($date)->whereIn(
            "leave_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereNotNull("leave_time")
            ->whereIn("item_cate_code", [2, 3, 4, 82])//西药费,中成药,中草药
            ->sum("amount");
        //出院人数
        $leaverCount = $this->getOutHospitalCount($date, $department_ids);
        if ($leaverCount == 0) {
            return 0;
        }
        //目标人均费用
        $targetAverageDrugCost = Department::where("id", $department_id)
            ->value("target_average_drug_cost");
        //实际出院病人均例费用
        $realAverageDrugCost = $leavePatientDrugIncome / $leaverCount;

        if ($targetAverageDrugCost == 0 || $realAverageDrugCost == 0) {
            return 0;
        }

        if ($realAverageDrugCost > $targetAverageDrugCost * 1.1) {
            $leaveAverageDrugCost = $targetAverageDrugCost / $realAverageDrugCost;
        } else if ($realAverageDrugCost >= $targetAverageDrugCost * 0.9
            && $realAverageDrugCost <= $targetAverageDrugCost * 1.1) {
            $leaveAverageDrugCost = 1;
        } else {
            $leaveAverageDrugCost = $realAverageDrugCost / $targetAverageDrugCost;
        }

        return round($leaveAverageDrugCost, 4);

    }

    /**
     * 出院病人例均耗材费用系数
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getMaterialAverageCost($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
        $type = DepartmentType::WARD;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        $arrDate = explode('-', $date);
        //出院病人药品总收入

        $leavePatientMaterialIncome = MainIncome::date($date)->whereIn(
            "leave_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereNotNull("leave_time")
            ->whereIn("item_cate_code", [15, 64, 17])//手术材料费,材料费
            ->sum("amount");
        //出院人数
        $leaverCount = $this->getOutHospitalCount($date, $department_ids);
        if ($leaverCount == 0) {
            return 0;
        }
        //目标人均耗材费用
        $targetAverageMaterialCost = Department::where("id", $department_id)
            ->value("target_average_material_cost");
        //实际出院病人均例耗材费用
        $realAverageMaterialCost = $leavePatientMaterialIncome / $leaverCount;

        if ($targetAverageMaterialCost == 0 || $realAverageMaterialCost == 0) {
            return 0;
        }

        if ($realAverageMaterialCost > $targetAverageMaterialCost * 1.1) {
            $leaveAverageDrugCost = $targetAverageMaterialCost / $realAverageMaterialCost;
        } else if ($realAverageMaterialCost >= $targetAverageMaterialCost * 0.9
            && $realAverageMaterialCost <= $targetAverageMaterialCost * 1.1) {
            $leaveAverageDrugCost = 1;
        } else {
            $leaveAverageDrugCost = $realAverageMaterialCost / $targetAverageMaterialCost;
        }

        return round($leaveAverageDrugCost, 4);

    }

    /**
     * 住院科室间人均效率的横向比较
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getInHospitalHorizontalComparison($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
        $type = DepartmentType::WARD;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        //todo
        //科室占床日总和
        $departAvgBedDayAll = $this->getBedCount($date,$department_ids);
//        $departAvgBedDayAll = BedUsing::whereIn("sub_departments_id", $department_ids)
//            ->where("date", $date)
//            ->sum("leaver_used_bed_day");
        //全院占床日总和
        $hospitalAvgBedDayAll = $this->getAllBedCount($date);
//        $hospitalAvgBedDayAll = BedUsing::where("date", $date)
//            ->sum("leaver_used_bed_day");

        //本月科室医生人数
        $departmentDoctorNum = EmployeeAttendance::leftjoin("employees", "employees.id", "employee_attendances.employees_id")
            ->whereIn("sub_departments_id", $department_ids)
            ->where("date", $date)
            ->where("employees.attendances_type", 1)
            ->whereRaw("employee_attendances.actual_attendance = (select max(t.actual_attendance) 
            from employee_attendances as t where 
            employee_attendances.employees_id = t.employees_id and t.date = '$date') ")
            ->count();

        //全院医生人数
        $allDoctorNum = EmployeeAttendance::leftjoin("employees", "employees.id", "employee_attendances.employees_id")
            ->where("date", $date)
            ->where("employees.attendances_type", 1)
            ->select("employees_id")
            ->groupBy("employees_id")
            ->get()
            ->count();
        if ($departmentDoctorNum == 0 ||
            $hospitalAvgBedDayAll == 0 ||
            $allDoctorNum == 0) {
            return 0;
        }

        $horizontalComparison = ($departAvgBedDayAll / $departmentDoctorNum) / ($hospitalAvgBedDayAll / $allDoctorNum);

        return round($horizontalComparison, 4);
    }

    /**
     * 住院科室总效率纵向比较
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getInHospitalVerticalComparison($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
        $type = DepartmentType::WARD;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        //科室占床日总和
        $departAvgBedDayAll = $this->getBedCount($date, $department_ids);

        BaseData::updateOrCreate(
            [
                "date" => $date ,
                "department_id" => $department_id
            ],
            [
                "base_use_bed_day" => $departAvgBedDayAll,
            ]
        );
//        $departAvgBedDayAll = BedUsing::whereIn("sub_departments_id", $department_ids)
//            ->where("date", $date)
//            ->sum("leaver_used_bed_day");
        //科室基期占床日
        //先查询基值表，如果没有数据就从department表查询2019年数据
        $year = date("Y",strtotime($date));
        $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
        if ($year == "2021"){//如果是2021年就查询2019年的数据
            $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
        }
        $baseData = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();
        if (empty($baseData)){
            $baseAvgBedDayAll = Department::where("id", $department_id)
                ->value("base_use_bed_day");
        }else{
            $baseAvgBedDayAll = $baseData->base_use_bed_day;
        }

        if ($baseAvgBedDayAll == 0) {
            return 0;
        }
        $VerticalComparison = $departAvgBedDayAll / $baseAvgBedDayAll;

        return round($VerticalComparison, 4);
    }

    /**
     * 科室每床日均次费用
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getDepartmentBedDayCost($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
//        $type = DepartmentType::WARD;
        $department_ids = $this->getOutPatientDepartments($department_id);
//        if (empty($department_ids)) {
//            return 0;
//        }

        //科室内考勤的医生
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->get()
            ->pluck("employees_id")
            ->all();
        //计算住院科室收入
        $arrDate = explode('-', $date);
        $departmentIncome = MainIncome::date($date)->whereIn(
            "prescribe_doctor_id", $doctors
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::INPATIENT)
            ->sum("amount");
        //科室占床日总和
        $departAvgBedDayAll = $this->getBedCount($date, $department_ids);
//        $departAvgBedDayAll = BedUsing::whereIn("sub_departments_id", $department_ids)
//            ->where("date", $date)
//            ->sum("leaver_used_bed_day");
        if ($departAvgBedDayAll == 0) {
            return 0;
        }

        $bedDayCost = $departmentIncome / $departAvgBedDayAll;

        return round($bedDayCost, 2);

    }

    /**
     * 医院每床日均次费用
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getHospitalBedDayCost($date, $department_id)
    {
        //计算住院科室收入
        $arrDate = explode('-', $date);
        $hospitalIncome = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::INPATIENT)
            ->sum("amount");
        //科室占床日总和
        $hospitalAvgBedDayAll = $this->getAllBedCount($date);
//        $hospitalAvgBedDayAll = BedUsing::where("date", $date)
//            ->sum("leaver_used_bed_day");
        if ($hospitalAvgBedDayAll == 0) {
            return 0;
        }

        $bedDayCost = $hospitalIncome / $hospitalAvgBedDayAll;

        return round($bedDayCost, 2);

    }

    /**
     * 术前待床日调整奖励
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getStayBedDay($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
        $type = DepartmentType::WARD;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
//        //查询科室手术的时间和入院时间
//        $day = Surgery::leftjoin('main_incomes', 'main_incomes.card_no', '=', 'surgerys.inpatient_code')
//            ->where("surgerys.date", $date)
//            ->whereIn("sub_departments_id", $department_ids)
//            ->whereNotNull("surgerys.surgery_time")
//            ->whereNotNull("main_incomes.admission_time")
//            ->groupBy("surgerys.inpatient_code","surgerys.surgery_time","main_incomes.admission_time")
//            ->select("surgerys.inpatient_code","surgerys.surgery_time","main_incomes.admission_time")
//            ->get()
//            ->toArray();
//
//        if (empty($day)){
//            return 0;
//        }
//        $allDays = 0;
//        foreach ($day as $value){
//            $surgery_time = Carbon::createFromFormat('YmdHisu', $value['surgery_time']);
//            $admission_time = Carbon::createFromFormat('YmdHisu', $value['admission_time']);
//            $diffDay = $surgery_time->diffInDays($admission_time);
//            $diffHour = $surgery_time->diffInHours($admission_time);
//            $miniDay = round($diffHour % 24 /24, 2);
//            $diff = $diffDay + $miniDay;
//            $allDays += $diff;
//        }
//        $preBedDay = $allDays / count($day);

        //目标术前待床日
        $targetStayBedDay = Department::where("id", $department_id)
            ->select("target_stay_bed_day", "punish_amount")
            ->first();
        $stayBedDay = 0;
        //暂时按照1计算
        $stayBedDay = 1 * $targetStayBedDay->punish_amount;
//        if($preBedDay > $targetStayBedDay->target_stay_bed_day){
//            $stayBedDay = ($preBedDay - $targetStayBedDay->target_stay_bed_day) * $targetStayBedDay->punish_amount;
//
//        }
        return $stayBedDay;
    }

    /**
     * 护理时数
     * @param $date
     * @param $department_id
     * @return int
     */
    public function getNursingHour($date, $department_id)
    {
        // 获取一级科室的住院类型二级科室ID列表
        $type = DepartmentType::WARD;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
//        $hours = NursingHour::whereIn("sub_departments_id", $department_ids)
//            ->where("date", $date)
//            ->sum("part_result");
        //本月特级护理人数
        $arrDate = explode('-', $date);
        $num1 = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereIn("execute_department_id", $department_ids)
            ->whereIn("item_code", [1044, 1045, 1049])
            ->sum("quantity");

        //项目编号 ：1046   项目名称  Ⅰ级护理*    一级护理
        $num2 = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereIn("execute_department_id", $department_ids)
            ->where("item_code", 1046)
            ->sum("quantity");
        //项目编号 ：1047   项目名称  Ⅱ级护理*    二级护理
        $num3 = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereIn("execute_department_id", $department_ids)
            ->where("item_code", 1047)
            ->sum("quantity");
        //项目编号 ：1048   项目名称  Ⅲ级护理*    三级护理
        $num4 = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereIn("execute_department_id", $department_ids)
            ->where("item_code", 1048)
            ->sum("quantity");
        if( ($num1 + $num2 + $num3 + $num4) == 0){
            return 0;
        }

        if($department_id == 239){
            $price = 10.89;
        } else {
            $price = 8.17;
        }

        $hours = ($num1 * $price + $num2 * 2.44 + $num3 * 1.82 + $num4 * 0.32) /
            ($num1 + $num2 + $num3 + $num4);

        return round($hours, 2);
    }

    /**
     * 从setting表里面查询值
     * @param $type
     * @param $name
     * @return mixed
     */
    public function getSettingVal($type, $name)
    {
        if (is_array($name)) {
            $value = Setting::where("key", $type)
                ->whereIn("title", $name)
                ->get()
                ->pluck("value")
                ->all();
            return $value;
        } else {
            $value = Setting::where("key", $type)
                ->where("title", $name)
                ->first();
            return $value->value;
        }

    }

    /**
     * 出院人数
     * @param $date
     * @param $department_ids 二级科室id数组
     * @return mixed
     */
    public function getOutHospitalCount($date, $department_ids)
    {
        if (in_array(120, $department_ids)){ //血液透析室出院人次单独计算
            $data = MainIncome::date($date)
                ->whereIn('item_code', [7666,7667,7668])
                ->select("serial_no")
                ->sum('quantity');
        } else {
            //        $arrDate = explode("-", $date);
            $new_date = str_replace("-", "", $date);
            $data = MainIncome::date($date)->whereIn("leave_department_id", $department_ids)
                ->where("income_type", IncomeType::INPATIENT)
                ->whereRaw("left(leave_time,6) = '$new_date'")
//            ->whereYear("charging_time", $arrDate[0])
//            ->whereMonth("charging_time", $arrDate[1])
                ->where("is_have_case", 1)
                ->select("serial_no")
                ->distinct()
                ->get();
            $data = count($data);
        }

        return $data;
    }

//    /**
//     * 获取科室占床日
//     *
//     * @param $date
//     * @param $department_ids
//     * @return int|mixed
//     */
//    public function getBedCount($date, $department_ids) {
//        //先查询床位表，没有数据则自己计算
//        $data = BedUsing::query()->where('date', $date)
//            ->whereIn('sub_departments_id', $department_ids)
//            ->select('total_use_bed_day')
//            ->get();
//        if (count($data)>0){
//            return $data->sum('total_use_bed_day');
//        }
//
//        //当月第一天
//        $firstDay = date('Ym01', strtotime($date));
//        $lastDay = date('Ymd', strtotime("$firstDay +1month -1day"));
//        $firstDayStr = $firstDay . '000000000';
//        $lastDayStr = $lastDay . '000000000';
//        $year = date('Y', strtotime($date));
//        $data = MainIncome::query()->date($date)
//            ->whereIn('admission_department_id',$department_ids)
//            ->where('income_type',IncomeType::INPATIENT)
//            ->where('leave_time','like',"%{$year}%")
//            ->whereRaw("left(charging_time,7) =  '".$date."'")
//            ->select('admission_department_id','serial_no','admission_time','leave_time')
//            ->addSelect(\DB::raw("DATEDIFF(IF(leave_time > $lastDayStr,'$lastDayStr',leave_time),IF(admission_time < $firstDayStr,'$firstDayStr',admission_time)) as bed"))
//            ->distinct()
//            ->get();
//        $sum =  $data->sum('bed');
//        return $sum;
//    }

    /**
     * 获取科室占床日
     *
     * @param $date
     * @param $department_ids
     * @return int|mixed
     */
    public function getBedCount($date, $department_ids) {

        if (in_array(120, $department_ids)){ //血液透析室占床日单独计算
            $data = MainIncome::date($date)
                ->whereIn('item_code', [7666,7667,7668])
                ->select("serial_no")
                ->sum('quantity');
        }else {
            $data = MainIncome::query()->date($date)
                ->whereIn('execute_department_id',$department_ids)
                ->whereRaw("left(charging_time,7) =  '".$date."'")
                ->where('item_cate_code',1)
                ->whereNotIn('item_code',[1034,1031,1030,1033])
                ->sum('quantity');
        }
        return $data;
    }

//    /**
//     * 获取全院占床日
//     *
//     * @param $date
//     * @return int|mixed
//     */
//    public function getAllBedCount($date) {
//        $firstDay = date('Ym01', strtotime($date));
//        $lastDay = date('Ymd', strtotime("$firstDay +1month -1day"));
//        $firstDayStr = $firstDay . '000000000';
//        $lastDayStr = $lastDay . '000000000';
//        $data = MainIncome::query()->date($date)
//            ->where('income_type',IncomeType::INPATIENT)
//            ->whereRaw("left(charging_time,7) =  '".$date."'")
//            ->select('admission_department_id','serial_no','admission_time','leave_time')
//            ->addSelect(\DB::raw("DATEDIFF(IF(leave_time > $lastDayStr,'$lastDayStr',leave_time),IF(admission_time < $firstDayStr,'$firstDayStr',admission_time)) as bed"))
//            ->distinct()
//            ->get();
//        $sum =  $data->sum('bed');
//        $count = $data->count();
//        return $sum + $count;
//    }
    /**
     * 获取全院占床日
     *
     * @param $date
     * @return int|mixed
     */
    public function getAllBedCount($date) {

        $data = MainIncome::query()->date($date)
            ->whereRaw("left(charging_time,7) =  '".$date."'")
            ->where('item_cate_code',1)
            ->whereNotIn('item_code',[1034,1031,1030,1033])
            ->sum('quantity');

        $data += MainIncome::date($date)
            ->whereIn('item_code', [7666,7667,7668])
            ->select("serial_no")
            ->sum('quantity');
        return $data;
    }

    /**
     * 出院者实际占床日
     *
     * @param $date
     * @param $department_ids
     * @return
     */
     public function getOutHospitalBed($date,$department_ids){
         $new_date = str_replace("-", "", $date);
         $data = MainIncome::date($date)->whereIn("leave_department_id", $department_ids)
             ->where("income_type", IncomeType::INPATIENT)
             ->whereRaw("left(leave_time,6) = '$new_date'")
             ->where("is_have_case", 1)
             ->select("serial_no",\DB::raw("DATEDIFF(leave_time,admission_time) as day"))
             ->distinct()
             ->get()->sum('day');
         return $data;
     }


}

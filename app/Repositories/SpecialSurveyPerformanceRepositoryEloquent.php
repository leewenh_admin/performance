<?php

namespace App\Repositories;

use App\Models\SpecialSurveyPerformance;

class SpecialSurveyPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = SpecialSurveyPerformance::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with("department:id,name,code");

        $query = $query->join('departments', 'special_survey_performances.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'special_survey_performances.date', '=', 'lock_performances.date');
        $query->select('special_survey_performances.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("special_survey_performances.date", $date);
        }

        $department_id = request("department_id", null);
        if (!empty($department_id)){
            $query = $query->where("department_id", $department_id);
        }

        return $query;
    }
}

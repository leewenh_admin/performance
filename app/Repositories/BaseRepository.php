<?php

namespace App\Repositories;

use App\Exceptions\MessageException;
use App\Models\Setting;

class BaseRepository {
    protected $actionClass = null;

    protected function getClass() {
        if (empty($this->actionClass)) {
            throw new MessageException("未设置actionClass");
        }
        return $this->actionClass;
    }

    public function getQuery() {
        return $this->getClass()::query();
    }

    public function getItem($id) {
        return $this->getQuery()->findOrFail($id);
    }

    public function getList() {
        return $this->getQuery();
    }

    public function delete($id) {
        $this->getItem($id)->delete();
    }

    public function update($id, $data) {
        $record = $this->getItem($id);

        $record->fill($data)->save();
    }

    public function insert($data) {
        $class = $this->getClass();
        return $class::create($data);
    }

    public function insertGetId($data) {
        $class = $this->getClass();
        return $class::insertGetId($data);
    }

    public function parseListResult($query) {
        $perPage = request("perPage", 25);
        $sort_str = $this->getSortStr();
        $sort_order = request("sort_order", "desc");
        $allPage = request("allPage", 1);

        $query = $query->orderBy($sort_str, $sort_order);

        if ($allPage == 2) {
            return $query->get();
        } else {
            return $query->paginate($perPage);
        }
    }

    protected function getSortStr() {
        return request("sort_str", "created_at");
    }

    /**
     * 从setting表里面查询值
     * @param $type
     * @param $name
     * @return mixed
     */
    public function getSettingVal($type, $name){
        if(is_array($name)){
            $value = Setting::where("key", $type)
                ->whereIn("title", $name)
                ->get()
                ->pluck("value")
                ->all();
            return $value;
        }else{
            $value = Setting::where("key", $type)
                ->where("title", $name)
                ->first();
            return  $value->value;
        }

    }
}

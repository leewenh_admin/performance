<?php

namespace App\Repositories\ExportDetail;

use App\Enums\EmployeeAttendanceType;
use App\Models\Department;
use App\Models\EmployeeAttendance;

class ParkingPerformanceExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department = Department::where("id", $department_id)->value("name");

        //科室内考勤系数总和
        $attendanceCoefficient = EmployeeAttendance::leftJoin('employees', 'employees.id', '=', 'employee_attendances.employees_id')
            ->where("departments_id", $department_id)
            ->where("date", $date)
            ->select("employees.code", "name", "actual_attendance", "attendance_coefficient", "sub_departments_id", "employees_id")
            ->get();

        $details = [];
        $i = 0;
        $days = date("t", strtotime($date));

        $type = [
            EmployeeAttendanceType::NONE => "无",
            EmployeeAttendanceType::DOCTOR => "医生",
            EmployeeAttendanceType::NURSE => "护理",
            EmployeeAttendanceType::TECHNICIAN => "技师",
            EmployeeAttendanceType::MANAGER => "行管",
            EmployeeAttendanceType::PHARMACIST => "药师",
        ];

        foreach ($attendanceCoefficient as $record){
            $detail = [];
            $detail["num"] = ++$i;

            $detail["department_name"] = $department;
            // 科室名称
            if ($record->subDepartment) {
                $detail["sub_department_name"] = $record->subDepartment->name;
            } else {
                $detail["sub_department_name"] = null;
            }
            // 工号
            $detail["code"] = $record->code;
            // 姓名
            $detail["name"] = $record->name;

            $detail["type"] = $type[$record->employee->attendances_type];
            // 实际出去天数
            $detail["actual_attendance"] = $record->actual_attendance;

            // 备注
            $detail["amount"] = round(600 * ($record->actual_attendance / $days));

            array_push($details, $detail);
        }

        $strDate = date("Y年m月", strtotime($date));
        $cellData = [
            [$strDate.'院外停车绩效明细表', '', '', '', '', '', '',  '附10表'],
            ['单位：协和京山医院', '', '', '', '', '', '',  ''],
            ['编号', '所属科室', '科室','工号', '姓名', '类别','实际出勤天数','停车补助'],
        ];
        $cellData = array_merge($cellData, $details);

        return $cellData;

    }
}
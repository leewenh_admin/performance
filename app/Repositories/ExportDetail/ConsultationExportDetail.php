<?php

namespace App\Repositories\ExportDetail;

use App\Models\Department;
use App\Models\EmployeeAttendance;
use App\Models\MainIncome;
use App\Models\SubDepartment;
use Illuminate\Support\Facades\DB;

class ConsultationExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department = Department::where("id", $department_id)->value("name");
//        $department_ids = SubDepartment::where("parent_id", $department_id)
//            ->where("is_performance", 1)
//            ->get()
//            ->pluck("id")
//            ->all();
        //科室内考勤的医生
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->get()
            ->pluck("employees_id")
            ->all();

        $arrDate = explode("-", $date);
        $data = MainIncome::date($date)->leftJoin('employees', 'employees.id', '=', 'prescribe_doctor_id')
            ->whereIn("prescribe_doctor_id", $doctors)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("item_cate_code", 36)//会诊费
            ->select(DB::raw("'$department' as department_name"), "code", "employees.name", DB::raw("sum(quantity)"), DB::raw("sum(amount)"))
            ->groupBy('department_name', 'code', 'employees.name')
            ->get()
            ->toArray();

        $strDate = date("Y年m月", strtotime($date));
        $cellData = [
            [$strDate.'会诊绩效明细表', '', '', '',  '附04表'],
            ['单位：协和京山医院', '', '', '',  '单位：元'],
            ['归属科室', '工号', '医生', '数量', '金额'],
        ];
        $cellData = array_merge($cellData, $data);

        return $cellData;

    }
}
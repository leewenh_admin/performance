<?php

namespace App\Repositories\ExportDetail;

use App\Enums\DepartmentType;
use App\Models\ClinicalAttendanceScore;
use App\Models\Department;
use App\Models\DepartmentHeadPerformance;
use App\Models\NightPerformance;
use App\Repositories\NightPerformanceCalculateEloquent;
use Illuminate\Support\Facades\DB;

class NightPerformanceExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department = Department::where("id", $department_id)->value("name");
        $nightData = NightPerformance::where("department_id", $department_id)
            ->where("date", $date)
            ->select("department_type",DB::raw("'$department' as department_name"),"sub_department_name", "daily_fixed_number", "before_night_number",
                "before_night_price", "after_night_number", "after_night_price", "whole_night_number",
                "whole_night_price", "month_day", "dispatch_amount", "float_performance","total_night_performance")
            ->get()
            ->toArray();
        $strDate = date("Y年m月", strtotime($date));
        $cellData = [
            [$strDate.'夜班绩效明细表', '', '', '', '', '','', '', '','', '', '', '附04表'],
            ['单位：协和京山医院', '', '', '', '', '','', '', '','', '', '', '单位：元'],
            ['科室类别', '归属科室', '科室', '每日固定人数', '护理人员', '','', '', '医生及其他整夜班人员','', '月天数', '其中：','浮动夜班绩效', '实发夜班绩效合计'],
            ['', '', '', '', '上夜班人数', '上夜班标准', '下夜班人数',
                '下夜班标准', '整夜班人数' ,'整夜班标准', '月天数','劳务派遣人员夜班', '','']
        ];

        $cellData = array_merge($cellData, $nightData);

        return $cellData;

    }
}
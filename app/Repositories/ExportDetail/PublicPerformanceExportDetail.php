<?php

namespace App\Repositories\ExportDetail;

use App\Models\PublicHealthPerformance;
use App\Models\SubDepartment;

class PublicPerformanceExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();
        $records = PublicHealthPerformance::whereIn("sub_department_id", $department_ids)
            ->where("date", $date)
            ->get();

        $details = [];
        foreach ($records as $record) {
            $detail = [];
            // 工号
            $detail["code"] = $record->code;
            // 姓名
            $detail["name"] = $record->name;
            // 科室名称
            if ($record->subDepartment) {
                $detail["sub_department_name"] = $record->subDepartment->name;
            } else {
                $detail["sub_department_name"] = null;
            }

            // 金额
            $detail["amount"] = number_format($record->amount, 2) . "元";


            // 备注
            $detail["description"] = $record->description;

            array_push($details, $detail);
        }

        $strDate = date("Y年m月", strtotime($date));
        $cellData = [
            [$strDate.'公卫绩效', '', '', '', '附09表'],
            ['单位：协和京山医院', '', '', '',   '单位：元'],
            ['工号', '姓名', '科室','金额','备注'],
        ];
        $cellData = array_merge($cellData, $details);

        return $cellData;

    }
}
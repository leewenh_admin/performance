<?php

namespace App\Repositories\ExportDetail;

use App\Enums\DepartmentType;
use App\Models\Department;
use App\Models\DepartmentHeadPerformance;
use App\Repositories\NightPerformanceCalculateEloquent;
use Illuminate\Support\Facades\DB;

class ManagementExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $obj = new NightPerformanceCalculateEloquent();

        //当月天数
        $days = date("t",strtotime($date));
        $department = Department::query()->where('id',$department_id)
            ->with('subDepartments')
            ->first();
        //质量考核分大于或等于98， 管理绩效 = 岗位管理绩效标准*实际考勤天数占比
        //质量考核分数小于98  管理绩效 = （岗位管理绩效标准 + 岗位管理绩效标准 * （质量考核分数 - 98）/100）） *实际考勤天数占比
        $employees = DepartmentHeadPerformance::join('employee_attendances', function ($join) {
            $join->on('employee_attendances.employees_id', '=', 'department_head_performances.employee_id')
                ->on('employee_attendances.date', '=', 'department_head_performances.date');
//                ->on('department_head_performances.department_id', '=', 'employee_attendances.departments_id');
            })
            ->join('employees', 'employees.id', '=', 'department_head_performances.employee_id')
            ->where("department_head_performances.department_id", $department->id)
            ->where("department_head_performances.date", $date)
            ->select("employees.id", "performance_standard", DB::raw("sum(employee_attendances.actual_attendance) as actual_attendance"),
                "administrative_duties", "employees.name", "employees.position_date", "employees.code", "administrative_time", "medical_percent", "expend_percent", "check_day")
            ->groupBy("employees.id", "performance_standard",
                "administrative_duties", "employees.name", "employees.position_date", "employees.code", "administrative_time", "medical_percent", "expend_percent", "check_day")
            ->get();

        if ($department->subDepartments->first()->type_lv1 == DepartmentType::FUNCTIONAL) { //职能科室
            $department_type = "职能";
        } elseif ($department->subDepartments->first()->type_lv1 == DepartmentType::CLINICAL){
            $department_type = "临床";
        } elseif ($department->subDepartments->first()->type_lv1 == DepartmentType::MEDICAL_TECHNICAL){
            $department_type = "医技";
        }

        $data = [];
        $scoreData = $obj->getScore($date, $department->id, $department->subDepartments->first()->type_lv1);
        foreach ($employees as $employee) {
            if (empty($scoreData)){
                $score = 100 ;
            }else {
                if ($employee->administrative_duties == "科主任" || $employee->administrative_duties == "副主任") {
                    $score = $scoreData->medical_quality;
                } elseif ($employee->administrative_duties == "护士长") {
                    $score = $scoreData->care_quality ;
                }
            }

            $percent = ($employee->actual_attendance - $employee->check_day) / $days;

            if ($score >= 98) {
                $performance = $employee->performance_standard * $percent;
            } else {
                $performance = ($employee->performance_standard + ($employee->performance_standard *
                        ($score - 98) / 100)) * $percent;
            }

            $data[$employee->employees_id]["department_name"] = $department->name;
            $data[$employee->employees_id]["code"] = $employee->code;
            $data[$employee->employees_id]["name"] = $employee->name;
            $data[$employee->employees_id]["administrative_duties"] = $employee->administrative_duties;
            $data[$employee->employees_id]["position_date"] = $employee->administrative_time ?? $employee->position_date;
            $data[$employee->employees_id]["department_type"] = $department_type;
            $data[$employee->employees_id]["performance_standard"] = $employee->performance_standard;
            $data[$employee->employees_id]["score"] = $score;
            $data[$employee->employees_id]["actual_attendance"] = $employee->actual_attendance - $employee->check_day;
            $data[$employee->employees_id]["performance"] = round($performance);

        }
        $strDate = date("Y年m月", strtotime($date));
        $cellData = [
            [$strDate.'管理绩效明细表', '', '', '', '', '','', '', '', '附03表'],
            ['单位：协和京山医院', '', '', '', '', '','', '', '', '单位：元'],
            ['所在科室', '工号', '姓名', '行政职务', '行政职务任职时间',
                '科室类型', '管理绩效标准' ,'质量考核分数', '出勤天数','管理绩效合计'],
        ];

        $cellData = array_merge($cellData, $data);


        return $cellData;

    }
}
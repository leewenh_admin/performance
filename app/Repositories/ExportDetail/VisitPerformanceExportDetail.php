<?php

namespace App\Repositories\ExportDetail;

use App\Models\Department;
use App\Models\EmployeeAttendance;
use App\Models\MainIncome;
use App\Models\SubDepartment;
use Illuminate\Support\Facades\DB;

class VisitPerformanceExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department = Department::where("id", $department_id)->value("name");
        $arrDate = explode("-", $date);
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();

        $doctors = EmployeeAttendance::leftjoin("employees", "employees.id", "=", "employee_attendances.employees_id")
            ->whereIn("sub_departments_id", $department_ids)->where("date", $date)
//            ->whereIn("professional_rank_level", [3, 4])
            ->whereRaw("employee_attendances.actual_attendance = (select max(t.actual_attendance) 
            from employee_attendances as t where 
            employee_attendances.employees_id = t.employees_id and t.date = '$date') ")
            ->select("employees_id", "employees.name", "employees.code")
            ->get();

        $employee = [];
        foreach ($doctors as $doctor){
            $employee[$doctor->employees_id] = $doctor;
        }

        $chief_count = 0;
        $assistant_count = 0;

        if(in_array($date,['2021-04', '2021-05', '2021-06', '2021-08'])){
            $count = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->whereIn("prescribe_doctor_id", array_keys($employee))
                ->select("serial_no", "quantity", "price", "prescribe_doctor_id")
                ->distinct()
                ->where("item_code", 1002)
                ->where("price", ">=", 10)
                ->get();
        } else {
            $count = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->whereIn("prescribe_doctor_id", array_keys($employee))
                ->select("serial_no", "quantity", "price", "prescribe_doctor_id","register_cate")
                ->distinct()
                ->where("item_code", 1002)
                ->whereIn("register_cate", [3,4,6,7])
                ->get();
        }




        $data = [];
        foreach ($count as $value){
            $data[$value->prescribe_doctor_id]['department'] = $department;
            $data[$value->prescribe_doctor_id]['code'] = $employee[$value->prescribe_doctor_id]->code;
            $data[$value->prescribe_doctor_id]['name'] = $employee[$value->prescribe_doctor_id]->name;
            $data[$value->prescribe_doctor_id]['assistant_count'] = $data[$value->prescribe_doctor_id]['assistant_count'] ?? 0;
            $data[$value->prescribe_doctor_id]['assistant_sum'] = $data[$value->prescribe_doctor_id]['assistant_sum'] ?? 0;
            $data[$value->prescribe_doctor_id]['chief_count'] = $data[$value->prescribe_doctor_id]['chief_count'] ?? 0;
            $data[$value->prescribe_doctor_id]['chief_sum'] = $data[$value->prescribe_doctor_id]['chief_sum'] ?? 0;
            $data[$value->prescribe_doctor_id]['count_sum'] = $data[$value->prescribe_doctor_id]['count_sum'] ?? 0;
            $data[$value->prescribe_doctor_id]['price_sum'] = $data[$value->prescribe_doctor_id]['price_sum'] ?? 0;
            $data[$value->prescribe_doctor_id]['performance_sum'] = $data[$value->prescribe_doctor_id]['performance_sum'] ?? 0;

            $data[$value->prescribe_doctor_id]['count_sum'] += $value->quantity;
            $data[$value->prescribe_doctor_id]['price_sum'] += $value->price;
            if(in_array($date,['2021-04', '2021-05', '2021-06', '2021-08'])){
                if($value->price < 16 ){  //大于等于10 小于16 副主任
                    $data[$value->prescribe_doctor_id]['assistant_count'] += $value->quantity;
                    $data[$value->prescribe_doctor_id]['assistant_sum'] += $value->price;
                    $data[$value->prescribe_doctor_id]['performance_sum'] += $value->quantity * 6;
                } else { //大于等于16 主任
                    $data[$value->prescribe_doctor_id]['chief_count'] += $value->quantity;
                    $data[$value->prescribe_doctor_id]['chief_sum'] += $value->price;
                    $data[$value->prescribe_doctor_id]['performance_sum'] += $value->quantity * 9.6;
                }
            } else{
                if($value->register_cate == 3 || $value->register_cate == 6){  //大于等于10 小于16 副主任
                    $data[$value->prescribe_doctor_id]['assistant_count'] += $value->quantity;
                    $data[$value->prescribe_doctor_id]['assistant_sum'] += $value->price;
                    $data[$value->prescribe_doctor_id]['performance_sum'] += $value->quantity * 6;
                } else { //大于等于16 主任
                    $data[$value->prescribe_doctor_id]['chief_count'] += $value->quantity;
                    $data[$value->prescribe_doctor_id]['chief_sum'] += $value->price;
                    $data[$value->prescribe_doctor_id]['performance_sum'] += $value->quantity * 9.6;
                }
            }
        }

        $strDate = date("Y年m月", strtotime($date));
        $cellData = [
            [$strDate.'专家号绩效明细表', '', '', '', '', '', '', '', '',  '附04表'],
            ['单位：协和京山医院', '', '', '', '', '', '', '', '', '单位：元'],
            ['归属科室', '工号', '医生', '副主任号人次', '副主任号金额', '主任号人次', '主任号金额', '总人次', '总金额', '绩效合计'],
        ];
        $cellData = array_merge($cellData, $data);

        return $cellData;

    }
}
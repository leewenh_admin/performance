<?php

namespace App\Repositories\ExportDetail;

use App\Models\Department;
use App\Models\DepartmentPerformance;
use App\Models\SpecialPerformance;
use Maatwebsite\Excel\Facades\Excel;
ini_set('memory_limit',-1);

class BaseExportDetail {
    public function __construct(){

    }

    public function export($date){
        $departmentExport = new DepartmentExportDetail();
        $comprehensiveExport = new ComprehensiveExportDetail();
        $managementExport = new ManagementExportDetail();
        $nightPerformanceExport = new NightPerformanceExportDetail();
        $consultationExport = new ConsultationExportDetail();
        $visitPerformanceExport = new VisitPerformanceExportDetail();
        $communityPerformanceExport = new CommunityPerformanceExportDetail();
        $otherPerformanceExport = new OtherPerformanceExportDetail();
        $publicPerformanceExport = new PublicPerformanceExportDetail();
        $parkingPerformanceExport = new ParkingPerformanceExportDetail();
        $talentReserveExport = new TalentReserveExportDetail();
        $payrollExport = new PayrollExportDetail();
        $recoverExport = new RecoverExportDetail();
        $departments = Department::where("is_performance", 1)->get()->pluck("id")->all();
//        $departments = Department::where("id", 216)->get()->pluck("id")->all();
        $time_stamp = date("Ymdhis",time());
        foreach ($departments as $department_id){
            $department = Department::where("id", $department_id)->value("name");
            $data1 = $departmentExport->getExportDetail($date, $department_id);
            $data2 = $comprehensiveExport->getExportDetail($date, $department_id);
            $data3 = $managementExport->getExportDetail($date, $department_id);
            $data4 = $nightPerformanceExport->getExportDetail($date, $department_id);
            $data5 = $consultationExport->getExportDetail($date, $department_id);
            $data6 = $visitPerformanceExport->getExportDetail($date, $department_id);
            $data7 = $communityPerformanceExport->getExportDetail($date, $department_id);
            $data8 = $otherPerformanceExport->getExportDetail($date, $department_id);
            $data9 = $publicPerformanceExport->getExportDetail($date, $department_id);
            $data10 = $parkingPerformanceExport->getExportDetail($date, $department_id);
            $data11 = $talentReserveExport->getExportDetail($date, $department_id);
            $data12 = $recoverExport->getExportDetail($date, $department_id);
            $data13 = $payrollExport->getExportDetail($date, $department_id);

            Excel::create($department.'绩效工资详情', function ($excel) use ($data1, $data2, $data3, $data4, $data5,
            $data6, $data7, $data8, $data9, $data10, $data11, $data12, $data13) {
                $excel->sheet('绩效 (正01表)', function ($sheet) use ($data13) {
                    $sheet->rows($data13);
                    $sheet->mergeCells('A1:U1', 'left');
                    $sheet->mergeCells('A2:V2', 'left');
                    $sheet->mergeCells('G3:H3', 'center');
                    $sheet->mergeCells('B3:E3', 'center');
                    $sheet->mergeCells('J3:S3', 'center');
                    $sheet->setMergeColumn(array(
                        'columns' => array('A'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ), 'center');
                    $sheet->setMergeColumn(array(
                        'columns' => array('F'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ), 'center');
                    $sheet->setMergeColumn(array(
                        'columns' => array('T'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ), 'center');
                    $sheet->setMergeColumn(array(
                        'columns' => array('U'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ), 'center');
                    $sheet->setMergeColumn(array(
                        'columns' => array('V'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ), 'center');

                    $sheet->setMergeColumn(array(
                        'columns' => array('I'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ), 'center');

                    $sheet->cells('A1:V1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:V2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:V3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                    $sheet->cells('A4:V4', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                    $sheet->setWidth(setSheetWidth('A', 'U'));
                });
                $excel->sheet('附12表  业务恢复绩效', function ($sheet) use ($data12) {
                    $sheet->rows($data12);
                    $sheet->mergeCells('D3:H3');
                    $sheet->mergeCells('I3:M3');
                    $sheet->mergeCells('N3:S3');
                    $sheet->mergeCells('A1:S1', 'left');
                    $sheet->mergeCells('A2:T2', 'left');
                    $sheet->setWidth(setSheetWidth('A', 'T'));
                    $sheet->setMergeColumn(array(
                        'columns' => array('A'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ));
                    $sheet->setMergeColumn(array(
                        'columns' => array('B'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ));

                    $sheet->setMergeColumn(array(
                        'columns' => array('C'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ));
                    $sheet->setMergeColumn(array(
                        'columns' => array('T'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ));


                    $sheet->cells('A1:T1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:T2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:T3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                    $sheet->cells('A4:T4', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });

                });
                $excel->sheet('附11表 人才储备绩效', function ($sheet) use ($data11) {
                    $sheet->rows($data11);
                    $sheet->mergeCells('A1:H1', 'left');
                    $sheet->mergeCells('A2:I2', 'left');

                    $sheet->cells('A1:I1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:I2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:I3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });

                    $sheet->setWidth(setSheetWidth('A', 'I'));
                });
                $excel->sheet('附10表 院外停车绩效', function ($sheet) use ($data10) {
                    $sheet->rows($data10);
                    $sheet->mergeCells('A1:G1', 'left');
                    $sheet->mergeCells('A2:H2', 'left');

                    $sheet->cells('A1:H1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:H2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:H3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });

                    $sheet->setWidth(setSheetWidth('A', 'H'));
                });
                $excel->sheet('附09表 公卫绩效', function ($sheet) use ($data9) {
                    $sheet->rows($data9);
                    $sheet->mergeCells('A1:D1', 'left');
                    $sheet->mergeCells('A2:D2', 'left');
                    $sheet->cells('A1:E1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:E2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:E3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });

                    $sheet->setWidth(setSheetWidth('A', 'E'));
                });
                $excel->sheet('附08表 其他单项', function ($sheet) use ($data8) {
                    $sheet->rows($data8);
                    $sheet->mergeCells('A1:E1', 'left');
                    $sheet->mergeCells('A2:E2', 'left');

                    $sheet->cells('A1:F1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:F2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:F3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                    $sheet->setWidth(setSheetWidth('A', 'F'));
                });
                $excel->sheet('附07表 医共体专项', function ($sheet) use ($data7) {
                    $sheet->rows($data7);
                    $sheet->mergeCells('A1:C1', 'left');
                    $sheet->mergeCells('A2:D2', 'left');

                    $sheet->cells('A1:D1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:D2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:D3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                    $sheet->setWidth(setSheetWidth('A', 'D'));
                });
                $excel->sheet('附06表 专家号', function ($sheet) use ($data6) {
                    $sheet->rows($data6);
                    $sheet->mergeCells('A1:I1', 'left');
                    $sheet->mergeCells('A2:I2', 'left');

                    $sheet->cells('A1:J1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:J2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:J3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                    $sheet->setWidth(setSheetWidth('A', 'J'));
                });
                $excel->sheet('附05表 会诊绩效', function ($sheet) use ($data5) {
                    $sheet->rows($data5);
                    $sheet->mergeCells('A1:D1', 'left');
                    $sheet->mergeCells('A2:D2', 'left');

                    $sheet->cells('A1:E1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:E2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:E3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                    $sheet->setWidth(setSheetWidth('A', 'E'));
                });
                $excel->sheet('附04表 夜班绩效', function ($sheet) use ($data4) {
                    $sheet->rows($data4);
                    $sheet->mergeCells('E3:H3');
                    $sheet->mergeCells('I3:J3');
                    $sheet->mergeCells('I3:J3');
                    $sheet->mergeCells('A1:L1', 'left');
                    $sheet->mergeCells('A2:L2', 'left');
                    $sheet->setWidth(setSheetWidth('A', 'N'));
                    $sheet->setMergeColumn(array(
                        'columns' => array('A'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ));
                    $sheet->setMergeColumn(array(
                        'columns' => array('B'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ));
                    $sheet->setMergeColumn(array(
                        'columns' => array('C'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ));
                    $sheet->setMergeColumn(array(
                        'columns' => array('D'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ));
                    $sheet->setMergeColumn(array(
                        'columns' => array('K'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ));
                    $sheet->setMergeColumn(array(
                        'columns' => array('M'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ));
                    $sheet->setMergeColumn(array(
                        'columns' => array('N'),
                        'rows' => array(
                            array(3, 4),
                        )
                    ));

                    $sheet->cells('A1:N1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:N2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:N3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                    $sheet->cells('A4:N4', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                });
                $excel->sheet('附03表 科主任管理绩效 ', function ($sheet) use ($data3) {
                    $sheet->rows($data3);
                    $sheet->mergeCells('A1:I1', 'left');
                    $sheet->mergeCells('A2:I2', 'left');
                    $sheet->cells('A1:J1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:J2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:J3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                    $sheet->setWidth(setSheetWidth('A', 'J'));
                });
                $excel->sheet('附02表 综合目标绩效', function ($sheet) use ($data2) {
                    $sheet->rows($data2);
                    $sheet->mergeCells('A1:I1', 'left');
                    $sheet->mergeCells('A2:H2', 'left');
                    $sheet->cells('A1:M1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:M2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:M3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                    $sheet->setWidth(setSheetWidth('A', 'M'));
                });
                $excel->sheet('附01表 临床科室', function ($sheet) use ($data1) {
                    $sheet->rows($data1);
                    $sheet->mergeCells('A1:F1', 'left');
                    $sheet->mergeCells('A2:G2', 'left');

                    $sheet->cells('A1:G1', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A2:G2', function($cells) {
                        $cells->setFontWeight('bold');
                    });
                    $sheet->cells('A3:G3', function($cells) {
                        $cells->setFontWeight('bold');
                        $cells->setAlignment('center');
                    });
                    $sheet->setWidth(setSheetWidth('A', 'G'));
                });

                $excel->setActiveSheetIndex(0);
            })->store('xlsx', storage_path('excel/exports/'.$time_stamp));
        }

        return $time_stamp;
    }

    public function checkData($date){
        $res = DepartmentPerformance::where("date", $date)
            ->count();
        if (empty($res)){
            return [
                "status" => "error",
                "message" => "请先生成科室绩效",
            ];
        }

        $res2 = SpecialPerformance::where("date", $date)
            ->count();
        if (empty($res2)){
            return [
                "status" => "error",
                "message" => "请先生成科室绩效",
            ];
        }
    }
    
}
<?php

namespace App\Repositories\ExportDetail;

use App\Enums\DepartmentType;
use App\Models\ClinicalAttendanceScore;
use App\Models\Department;
use App\Models\MedicalAttendanceScore;

class ComprehensiveExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department = Department::query()->where('id',$department_id)
            ->with('subDepartments')
            ->first();

        if ($department->subDepartments->first()->type_lv1 == DepartmentType::CLINICAL){
            $departData = ClinicalAttendanceScore::where("departments_id", $department_id)
                ->where("date", $date)
                ->select("employees_count", "medical_quality", "care_quality", "medical_ethics_quality",
                    "health_management", "scientific_research", "medical_record_quality", "total_score",
                    "comprehensive_performance")
                ->get()
                ->toArray();
            $herder =  ['科室', '人数', '医疗质量维度（55分）', '护理质量维度（15分）', '医德医风维度（5分）',
                '医保管理（5分）', '病案质量（10分）' ,'科研教学（10分）', '得分','综合目标绩效','', '科主任考核得分', '护士长考核得分'];
        } elseif ($department->subDepartments->first()->type_lv1 == DepartmentType::MEDICAL_TECHNICAL){
            $departData = MedicalAttendanceScore::where("departments_id", $department_id)
                ->where("date", $date)
                ->select("employees_count", "medical_quality", "care_quality", "medical_ethics_quality",
                    "health_management", "scientific_research", "workload_management", "total_score",
                    "comprehensive_performance")
                ->get()
                ->toArray();
            $herder =  ['科室', '人数', '医疗质量维度（55分）', '护理质量维度（15分）', '医德医风维度（5分）',
                '医保管理（5分）', '病案质量（10分）' ,'科研教学（10分）', '得分','综合目标绩效','', '科主任考核得分', '护士长考核得分'];
        } else{
            $herder =  ['科室', '人数', '医疗质量维度（55分）', '护理质量维度（15分）', '医德医风维度（5分）',
                '医保管理（5分）', '病案质量（10分）' ,'科研教学（10分）', '得分','综合目标绩效','', '科主任考核得分', '护士长考核得分'];
            $departData = [];
        }

        foreach ($departData as $k => $v){
            $departData[$k] = array_prepend($departData[$k], $department->name,  'name');
            array_push($departData[$k], '');
            array_push($departData[$k], $v['medical_quality']);
            array_push($departData[$k], $v['care_quality']);
        }

        $strDate = date("Y年m月", strtotime($date));

        $cellData = [
            [$strDate.'综合目标绩效（临床科室）明细表', '', '', '', '', '','', '', '', '附02表','', '', ''],
            ['单位：协和京山医院', '', '', '', '', '','', '', '单位：元', '','', '', ''],
            $herder
        ];

        $cellData = array_merge($cellData, $departData);


        return $cellData;

    }
}
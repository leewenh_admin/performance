<?php

namespace App\Repositories\ExportDetail;

use App\Models\Department;
use App\Models\OtherPerformance;
use App\Models\SubDepartment;
use Illuminate\Support\Facades\DB;

class OtherPerformanceExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department = Department::where("id", $department_id)->value("name");
        $sub_departments = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("name", "id")
            ->all();
        $records = OtherPerformance::whereIn(
            "sub_department_id", array_keys($sub_departments)
        )->where("date", $date)
            ->select(DB::raw("'$department' as department_name"), "code", "name",
            "days", "amount", "description")
            ->get()
            ->toArray();

        $strDate = date("Y年m月", strtotime($date));
        $cellData = [
            [$strDate.'其他单项绩效明细表',  '', '', '', '',  '附08表'],
            ['单位：协和京山医院', '',  '', '', '',  '单位：元'],
            ['科室', '工号', '姓名', '天数','金额','备注'],
        ];
        $cellData = array_merge($cellData, $records);

        return $cellData;

    }
}
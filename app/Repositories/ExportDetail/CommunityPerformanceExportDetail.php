<?php

namespace App\Repositories\ExportDetail;

use App\Models\ClinicalLaboratoryPerformance;
use App\Models\Department;
use App\Models\PathologyPerformance;
use App\Models\RadiologyDepartmentPerformance;
use App\Models\SpecialSurveyPerformance;

class CommunityPerformanceExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department = Department::where("id", $department_id)->value("name");
        $data = [];
        if ($department_id == 248){
            //医共体影像中心绩效(放射科)
            $total = RadiologyDepartmentPerformance::where("date", $date)
                    ->where("department_id", $department_id)
                    ->value("performance") ?? 0;
            $data = [
                'date' => $date,
                'department' => $department,
                'total' => $total,
            ];
        } elseif ($department_id == 247){
            //医共体检验中心绩效(检验科)
            $total = ClinicalLaboratoryPerformance::where("date", $date)
                    ->where("department_id", $department_id)
                    ->value("performance") ?? 0;
            $data = [
                'date' => $date,
                'department' => $department,
                'total' => $total,
            ];
        } elseif ($department_id == 252){
            //医共体心电中心绩效(特检科)
            $total = SpecialSurveyPerformance::where("date", $date)
                    ->where("department_id", $department_id)
                    ->value("performance") ?? 0;
            $data = [
                'date' => $date,
                'department' => $department,
                'total' => $total,
            ];
        } elseif ($department_id == 251){
            //医共体病理中心绩效(病理科)
            $total = PathologyPerformance::where("date", $date)
                    ->where("department_id", $department_id)
                    ->value("performance") ?? 0;
            $data = [
                'date' => $date,
                'department' => $department,
                'total' => $total,
            ];
        }



        $strDate = date("Y年m月", strtotime($date));
        $cellData = [
            [$strDate.'医共体绩效明细表', '', '',  '附07表'],
            ['单位：协和京山医院', '', '', ''],
            ['月份', '科室', '绩效合计', '备注'],
            $data
        ];

        return $cellData;

    }
}
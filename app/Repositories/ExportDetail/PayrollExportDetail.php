<?php

namespace App\Repositories\ExportDetail;

use App\Models\Department;
use App\Models\DepartmentPerformance;

class PayrollExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department = Department::where("id", $department_id)->value("name");

        $department_data = DepartmentPerformance::leftjoin("special_performances", "special_performances.department_id", "department_performances.department_id")
            ->where("department_performances.department_id", $department_id)
            ->where("department_performances.date", $date)
            ->select("department_performances.*", "management_performance", "night_performance", "consultation_performance",
                "visit_performance", "community_performance", "public_health_performance", "talent_performance" ,"business_recovery_performance",
                "parking_performance","other_performance","total_performance","department_header_performance")
            ->selectRaw("department_performances.cost_effective + department_performances.special_performance as cost_effective")
            ->get()
            ->toArray();

        if (empty($department_data)){
            $data = [];
        } else {
            $department_data = $department_data[0];
            $data = [
                'department' => $department,
                'workload' => $department_data['workload'] + $department_data['cost_effective'] + $department_data['core_business'] + $department_data['individual'],
                'percent1' => '',
                'percent2' => '',
                'comprehensive' => $department_data['comprehensive'],
                'total_performances' => $department_data['total_performances'],
                'department_performance' => $department_data['department_performance'],
                'distribution_performance' => $department_data['distribution_performance'],
                'department_header_performance' => $department_data['department_header_performance'],
                'management_performance' => $department_data['management_performance'],
                'night_performance' => $department_data['night_performance'],
                'consultation_performance' => $department_data['consultation_performance'],
                'visit_performance' => $department_data['visit_performance'],
                'community_performance' => $department_data['community_performance'],
                'other_performance' => $department_data['other_performance'],
                'public_health_performance' => $department_data['public_health_performance'],
                'parking_performance' => $department_data['parking_performance'],
                'talent_performance' => $department_data['talent_performance'],
                'business_recovery_performance' => $department_data['business_recovery_performance'],
                'total_performance' => $department_data['total_performance'],
                'total' => $department_data['total_performance'] + $department_data['total_performances'],
                'sign' => ''
            ];
        }



        $strDate = date("Y年m月", strtotime($date));
        $cellData = [
            [$strDate.'绩效工资分配表', '', '','', '', '','','', '', '','', '', '','','','','', '','','','','正01表'],
            ['单位：协和京山医院', '', '', '','', '', '','','', '', '','', '', '','','','','', '','','','',''],
            ['科室', '科室绩效', '','', '', '科室绩效合计', '其中：','','科主任科室绩效',
                '专项绩效','', '', '','', '', '',
                '','','','专项绩效合计', '应发绩效总计','签名'],
            ['',  '工作量绩效', '耗占比考核','药占比考核', '综合考核绩效', '','科内绩效','科室分配绩效','',
                '科主任管理绩效','夜班绩效', '会诊绩效', '专家号绩效','医共体绩效', '其他单项绩效', '公卫绩效',
                '院外停车绩效','人才储备绩效','业务恢复绩效','', '',''],
            $data
        ];

        return $cellData;

    }
}
<?php

namespace App\Repositories\ExportDetail;

use App\Models\Department;
use App\Models\DepartmentPerformance;
use App\Repositories\NewTechnologyItemRepositoryEloquent;

class DepartmentExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department = Department::where("id", $department_id)->value("name");
        $departPerformance = DepartmentPerformance::where("department_id", $department_id)
            ->where("date", $date)
            ->first();
        //工作量绩效
        $workloadPerformance = $departPerformance ? $departPerformance->workload + $departPerformance->cost_effective + $departPerformance->core_business + + $departPerformance->individual : 0;

        $objNewTechnology = new NewTechnologyItemRepositoryEloquent();

        //新技术、业务绩效
        $newTechnologyPerformance = $objNewTechnology->getNewTechnologyPerformance($date, $department_id);
        //单位体检绩效
        $physical_performance = $departPerformance ? $departPerformance->physical_performance : 0;
        //科室跑账
        $running_performance = $departPerformance ? $departPerformance->running_performance : 0;
        //疫情防控
        $control_performance = $departPerformance ? $departPerformance->control_performance : 0;
        //科室绩效合计
        $totalPerformance = $workloadPerformance + $newTechnologyPerformance + $physical_performance +
            $running_performance + $control_performance;

        $totalPerformance = round($totalPerformance, 2);
        $data = [
            "name" => $department,
            "workloadPerformance" => $workloadPerformance,
            "newTechnologyPerformance" => $newTechnologyPerformance,
            "physical_performance" => $physical_performance,
            "control_performance" => $control_performance,
            "running_performance" => $running_performance,
            "totalPerformance" => $totalPerformance,
        ];
        $strDate = date("Y年m月", strtotime($date));
        $cellData = [
            [$strDate.'科室绩效明细表', '', '', '', '', '', '附01表'],
            ['单位：协和京山医院', '', '', '', '', '', ''],
            ['科室', '工作量绩效', '新技术/新业务绩效', '单位体检绩效', '疫情防控', '科室跑账', '科室绩效合计'],
            $data
        ];



        return $cellData;

    }
}
<?php

namespace App\Repositories\ExportDetail;

use App\Enums\DepartmentType;
use App\Enums\EmployeeProfessionalRankLevel;
use App\Models\Department;
use App\Models\EmployeeAttendance;
use App\Models\SubDepartment;
use App\Models\TalentReserve;
use App\Repositories\SpecialDepartmentPerformanceRepositoryEloquent;
use Carbon\Carbon;

class TalentReserveExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department = Department::where("id", $department_id)->first();

        $records = TalentReserve::where("date", $date)
            ->where("department_id", $department_id)
            ->get();

        $details = [];

        foreach ($records as $record) {
            $detail = [];
            $detail["department_name"] = $department->name;
            // 工号
            $detail["code"] = $record->code;
            // 姓名
            $detail["name"] = $record->name;

            $detail["rank_level"] = $record->professional_rank_level;

            $detail["professional_rank_title"] = $record->professional_rank_title;
            $detail["total"] = $record->standard;
            $detail["act_total"] = $record->actual_sent;

            $detail["day"] = '';

            // 备注
            $detail["description"] = $record->remark;

            array_push($details, $detail);
        }


        $strDate = date("Y年m月", strtotime($date));
        $cellData = [
            [$strDate.'人才储备绩效明细表', '', '', '','', '', '','', '附11表'],
            ['单位：协和京山医院', '', '', '','', '', '','', ''],
            ['所属科室', '工号', '姓名', '职称级别','技术职称', '发放标准（元）', '本月应发（元）','请假天数', '备注'],
        ];
        $cellData = array_merge($cellData, $details);

        return $cellData;

    }
}
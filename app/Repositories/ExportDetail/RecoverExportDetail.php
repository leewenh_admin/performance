<?php

namespace App\Repositories\ExportDetail;

use App\Enums\EmployeePosition;
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeeAttendance;
use App\Models\SpecialPerformance;
use App\Repositories\PerformanceDetail\BusinessRecoveryDetail;

class RecoverExportDetail extends BaseExportDetail {

    public function __construct(){
    }

    public function getExportDetail($date, $department_id){
        $department = Department::where("id", $department_id)->value("name");

        $rec_data  = (new BusinessRecoveryDetail())->getDetail($date, $department_id);
        $performance = SpecialPerformance::where('date', $date)
            ->where('department_id', $department_id)->first();
        $header = EmployeeAttendance::where('departments_id', $department_id)
            ->where('date', $date)
            ->whereHas('employee', function ($q){
                $q->where('position', EmployeePosition::DEPARTMENT_HEAD);
            })->first();


        $data = [
            'code' => $header?$header->code: '',
            'name' => $header?$header->employee->name:"",
            'department' => $department,
            'leaverCount' => mb_substr($rec_data['leaverCount'], 0, -1),
            'surgeryCount' => mb_substr($rec_data['surgeryCount'], 0, -1),
            'departBedDayAll' => mb_substr($rec_data['departBedDayAll'], 0, -1),
            'income' => mb_substr($rec_data['income'], 0, -1),
            'specialSurgery' => mb_substr($rec_data['surgeryCount1'], 0, -1),
            'baseOutHospital' => mb_substr($rec_data['baseOutHospital'], 0, -1),
            'baseSurgeryCount' => mb_substr($rec_data['baseSurgeryCount'], 0, -1),
            'baseUseBedDay' => mb_substr($rec_data['baseUseBedDay'], 0, -1),
            'baseDepartmentIncome' => mb_substr($rec_data['baseDepartmentIncome'], 0, -1),
            'baseSpecialSurgery' => mb_substr($rec_data['base_special_surgery_count'], 0, -1),
            'leaverCount1' => round(str_replace('%', '', $rec_data['leaveCountIncrease'])),
            'surgeryCount1' => round(str_replace('%', '', $rec_data['surgeryCountIncrease'])),
            'departBedDayAll1' => round(str_replace('%', '', $rec_data['useBedIncrease'])),
            'income1' => round(str_replace('%', '', $rec_data['incomeIncrease'])),
            'specialSurgeryRate1' => round(str_replace('%', '', $rec_data['surgeryRateIncrease'])),
            'recover_degree' => round(str_replace('%', '', $rec_data['increase'])),
            'total' => $performance?$performance->business_recovery_performance: '',
        ];

        $strDate = date("Y年m月", strtotime($date));
        $lastYear = date("Y年m月",strtotime("-1 years",strtotime($date)));

        $cellData = [
            [$strDate.'业务恢复绩效表', '', '', '','', '', '','','', '', '','', '','', '','','','','', '附12表'],
            ['单位：协和京山医院','', '', '','', '', '','','', '', '','', '','', '','','','','', ''],
            ['工号', '姓名', '科室', $strDate,'', '', '','',$lastYear, '', '','', '','恢复情况', '','','','','','恢复绩效'],
            ['', '', '', '出院人数','手术台次', '实际占用总床日数', '科室有效收入','三四级手术','出院人数', '手术台次', '实际占用总床日数','科室有效收入','三四级手术', '出院人数', '手术台次','实际占用总床日数','科室有效收入','三四级手术','恢复度',''],

            $data
        ];

        return $cellData;

    }
}
<?php

namespace App\Repositories;

use App\Enums\DepartmentType;
use App\Enums\EmployeeAttendanceType;
use App\Models\AvgPerformanceSetting;
use App\Models\BaseData;
use App\Models\Department;
use App\Models\DepartmentPerformance;
use App\Models\DisinfectCost;
use App\Models\EmployeeAttendance;
use App\Models\FavourCollectInpatientFee;
use App\Models\MainIncome;
use App\Models\PharmacyIncrementalPerformance;
use App\Models\Setting;
use App\Models\SpecialPerformanceSetting;
use App\Models\SubDepartment;
use Illuminate\Support\Facades\DB;

/**
 * 特殊科室绩效
 */
class SpecialDepartmentPerformanceRepositoryEloquent extends BaseRepository {
    // 成本效益绩效计算逻辑类
    protected $costEffective;
    // 临床工作量绩效计算逻辑类
    protected $workLoad;
    // 医技工作两绩效计算逻辑类
    protected $medicalWorkLoad;

    public function __construct(){
        $this->costEffective = new CostEffectiveRepositoryEloquent();
        $this->workLoad = new WorkLoadRepositoryEloquent();
        $this->medicalWorkLoad = new MedicalWorkLoadRepositoryEloquent();

    }

    public function updateSpecialDepartment($date, $department_id){
        //如果设置了特殊科室绩效就取设置的值
        $data = SpecialPerformanceSetting::where("department_id", $department_id)->where("date", $date)->first();
        if($data){
//            $total_performances = DepartmentPerformance::where("department_id", $department_id)
//                ->where("date", $date)->value("total_performances");
            $department_performances = DepartmentPerformance::where("department_id", $department_id)
                ->where("date", $date)->first();

            //更新科内绩效和科室分配绩效
            $d_performance = (new DepartmentPerformanceRepositoryEloquent())->getdPerformance($data->performance + $department_performances->total_performances, $department_performances->people_count,$date, $department_performances->department_id);
            $distribution_performance = $data->performance + $department_performances->total_performances - $d_performance;

            DepartmentPerformance::where("department_id", $department_id)
                ->where("date", $date)
                ->update([
                    "total_performances" => $data->performance + $department_performances->total_performances,
                    "special_performance" => $data->performance,
                    "department_performance" => $d_performance,
                    "distribution_performance" => $distribution_performance
                ]);
            return ;
        }
        switch ($department_id){
            case 242 ://急诊科
                $this->getEmergencyPerformance($date);
                break;
            case 249 ://药剂科
                $this->getPharmacyPerformance($date);
                break;
            case 253 ://输血科
                $this->getBloodTransfusionPerformance($date);
                break;
            case 282 ://供应室
                $this->getSupplyPerformance($date);
                break;
            case 319 ://门诊收费室
                $this->getOutpatientCaisherPerformance($date);
                break;
            case 320 ://住院收费室
                $this->getInpatientCaisherPerformance($date);
                break;
            case 218 ://介入科
                $this->getInterventionPerformance($date);
                break;
            case 216 ://感染性疾病科
                $this->getInfectiousPerformance($date);
                break;
            case 255 ://发热门诊
                $this->getFeverClinics($date);
                break;
            case 489 ://门岗及核酸采集
                $this->getNucleicAcidPerformance($date);
                break;
            case 254 ://缓冲病区
                $this->getBufferWardPerformance($date);
                break;
        }

        $department_performances = DepartmentPerformance::where("department_id", $department_id)
            ->where("date", $date)->first();
        //更新科内绩效和科室分配绩效
        $d_performance = (new DepartmentPerformanceRepositoryEloquent())->getdPerformance($department_performances->total_performances, $department_performances->people_count,$date, $department_performances->department_id);
        $distribution_performance = $department_performances->total_performances - $d_performance;

        DepartmentPerformance::where("department_id", $department_id)
            ->where("date", $date)
            ->update([
                "total_performances" => $department_performances->total_performances,
                "department_performance" => $d_performance,
                "distribution_performance" => $distribution_performance
            ]);
    }

    /**
     * 计算行政平均绩效
     * 
     */
    public function getAdministrativeAverage($date){
        $data = AvgPerformanceSetting::where("date", $date)->first();

        if ($data && $data->performance){
            return $data->performance;
        }
        // 行政平均绩效 = (((非手术科室工作量- 发热门诊工作量 -  缓冲病区工作量)  + (门诊科室工作量 - 急诊科工作量) + (医技科室工作量 - 药剂科工作量)) /0.88 *0.12) / (职能科室总人数 - 住院收费人数 - 门诊收费室人数)
        // 非手术科室工作量
        $medicines = [
            "感染性疾病科",
            "心血管内科",
            "呼吸与危重症医学科",
            "神经内科",
            "消化内科",
            "内分泌·肾病·血液科",
            "儿科",
            "肿瘤科",
            "康复医学科·中医科",
            "血液透析室"
        ];

        //手术科室
        $surgerys = [
            "骨外Ⅰ科",
            "骨外Ⅱ科",
            "骨外Ⅲ科",
            "普外Ⅰ科",
            "普外Ⅱ科",
            "神经外科·烧伤科·血管外科",
            "泌尿外科",
            "妇科",
            "产科",
            "耳鼻喉科",
            "眼科",
            "重症医学科",
        ];

        // 发热门诊工作量 = 0
        // 缓冲病区工作量 = 0
        // 门诊科室工作量
        $outpatients = [
            "外科门诊", 
            "口腔科", 
            "皮肤科",
            "中医科",
            "门岗及核酸采集"
        ];
        // 急诊科工作量 = 0
        // 医技科室工作量
        $medical_skills = [
            "麻醉科", 
            "检验科", 
            "放射科", 
            "超声影像科", 
            "病理科", 
            "特检科", 
//            "输血科",
            "介入科",
            "消化内镜室",
//            "供应室"
        ];
        // 药剂科工作量 = 0

        $departments = Department::whereIn(
            "name", 
            array_merge($medicines, $outpatients, $medical_skills, $surgerys)
        )
            ->get()
            ->pluck("id");
        $sum_point = DepartmentPerformance::whereIn(
            "department_id", $departments
        )
            ->selectRaw("sum(workload + cost_effective + core_business + individual) as performance")
            ->where("date", $date)
            ->first()
            ->performance;

        $sum_performance = $sum_point / 0.88 * 0.12;

        // 职能科室总人数 - 住院收费人数 - 门诊收费室人数
        $functional_subs = SubDepartment::where("type_lv1", DepartmentType::FUNCTIONAL)
            ->select("parent_id")
            ->distinct("parent_id")
            ->get()
            ->pluck("parent_id");
        $functionals = Department::whereIn("id", $functional_subs)
            ->whereNotIn("name", ["住院收费室", "门诊收费室"])
            ->select("id")
            ->get()
            ->pluck("id")
            ->all();

        $people_count = EmployeeAttendance::whereIn("departments_id", $functionals)
            ->where("date", $date)
            ->sum("attendance_coefficient");

        return round($sum_performance / $people_count, 2);
    }

    /**
     * 急诊科绩效
     */
    public function getEmergencyPerformance($date){
        $arrDate = explode('-', $date);

        // 急诊科绩效=行政平均绩效*0.8*人数+接/转诊绩效+工作量绩效+变动成本控制
        $department = Department::where("name", "急诊科")->first();
        if (empty($department)) {
            return 0;
        }

        $subs = $this->costEffective->getSubDepartments($department->id);

        // 行政平均成绩
        $administrative_average = $this->getAdministrativeAverage($date);
        // 人数
        $people_count = 29;

        // 接诊次数
        $recevie_data = MainIncome::date($date)->where("item_code", 1014)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->selectRaw("sum(quantity) as quantity_sum")
            ->first();
        $receive_count = $recevie_data->quantity_sum;
        // 接诊绩效
        // 接诊标准：护理人员20元/次，医生20元/次，司机40元/次。（参考以前年度）
        $receive_performance = (20 + 20 + 40) * $receive_count;

        // 转诊次数
        // 市内转诊
        $move_data1 = MainIncome::date($date)->where("item_code", 10208)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->selectRaw("sum(quantity) as quantity_sum")
            ->first();
        $move_count1 = $move_data1->quantity_sum;
        // 市外转诊
        $move_data2 = MainIncome::date($date)->where("item_code", 10209)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->selectRaw("sum(quantity) as quantity_sum")
            ->first();
        $move_count2 = $move_data2->quantity_sum;
        // 转诊绩效
        // 编码10208,85元/次；编码10209,200元/次
        $move_performance = $move_count1 * 85 + $move_count2 * 200;
        // 接转诊绩效
        $receive_move_performance = $receive_performance + $move_performance;

        // 工作量绩效：（项目点数-固定成本点数）*单价+门急诊人次*单价+留观人次*单价
        // 医疗服务项目点数
        $medical_point = $this->costEffective->getMedicalPoint($date, $department->id);
        // 辅检项目点数
        $auxiliary_point = $this->costEffective->getAuxiliaryPoint($date, $department->id);
        // 手术项目点数
        $surgery_point = $this->costEffective->getSurgeryPoint($date, $department->id);
        // 固定成本点数
        $fixed_costs_point = $this->costEffective->getFixedCostsPoint($date, $department->id);
        // 绩效单价
        $performance_price = $this->costEffective->getPerformancePrice($date, $department->id);
        // 变动成本控制因素
        $variable_cost_control = $this->costEffective->getVariableCostControl($date, $department->id);

        // 医疗服务项目点数*100%+辅检项目点数*20%+手术项目点数*50%

        // 项目点数-固定成本
        $point_count = $medical_point * 1 + $auxiliary_point * 0.2 + $surgery_point * 0.5 - $fixed_costs_point;

        // 门急诊人次
        $outpatient_count = $this->workLoad->getOutPatientCount($date, $department->id);
        // 留观人次
        $watch_count = MainIncome::date($date)->where("item_code", 1009)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->select("card_no")
            ->groupBy("card_no")
            ->get()
            ->count();
        // 工作量绩效：（项目点数-固定成本点数）*单价+门急诊人次*单价+留观人次*单价
        $work_load_performance = $point_count * $performance_price +
            $outpatient_count * $performance_price + 
            $watch_count * $performance_price;

        // 急诊科绩效=行政平均绩效*0.8*人数+接/转诊绩效+工作量绩效+变动成本控制
        $p = $administrative_average * 0.8 * $people_count +
             $receive_move_performance + $work_load_performance 
             + $variable_cost_control;

        $total_performances = DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)->value("total_performances");

        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->update([
                "total_performances" => round($p) + $total_performances,
                "special_performance" => round($p),
            ]);

        return round($p);
        
    }

    /**
     * 药剂科绩效
     */
    public function getPharmacyPerformance($date) {
        // 药剂科绩效=基础绩效+增量绩效*（2-本期药占比/基期药占比）
        $department = Department::where("name", "药剂科")->first();

        if (empty($department)) {
            return 0;
        }

        // 基础绩效=行政平均绩效*科室实际计算绩效人数

        // 行政平均绩效
        $administrative_average = $this->getAdministrativeAverage($date);
        // 科室实际计算绩效人数
        $performance_people_count = $this->getAttendancePeopleCount($date, $department->id);

        // 基础绩效
        $base_performance = $administrative_average * $performance_people_count;

        // 增量绩效总和
        $record = PharmacyIncrementalPerformance::where("quota", "本月增量绩效合计")
            ->where("date", $date)
            ->first();
        if (empty($record)) {
            $incremental_performance = 0;
        } else {
            $incremental_performance = $record->incremental;
        }


        $total_performances = DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)->value("total_performances");

        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->update([
                "total_performances" => round($base_performance + $incremental_performance) + $total_performances,
                "special_performance" => round($base_performance + $incremental_performance),
            ]);

        return round($base_performance + $incremental_performance);
    }

    /**
     * 输血科绩效
     */
    public function getBloodTransfusionPerformance($date) {
        // 输血科绩效=行政平均绩效*1.2*绩效人数
        $department = Department::where("name", "输血科")->first();

        if (empty($department)) {
            return 0;
        }

        // 行政平均绩效
        $administrative_average = $this->getAdministrativeAverage($date);
        // 科室实际计算绩效人数
        $performance_people_count = $this->getAttendancePeopleCount($date, $department->id);

        $p = round($administrative_average * 1.2 * $performance_people_count);

        $total_performances = DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)->value("total_performances");

        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->update([
                "total_performances" => $p + $total_performances,
                "special_performance" => $p,
            ]);

        return $p;
    }

    /**
     * 供应室绩效
     */
    public function getSupplyPerformance($date) {
        // 供应室绩效=供应室工作量*[?]%-成本控制因素
        $department = Department::where("name", "供应室")->first();
        if (empty($department)) {
            return 0;
        }
        // 消毒费合计
        $amount = DisinfectCost::where("date", $date)->sum("amount");

        // 成本控制因素
        $variable_cost_control = $this->costEffective->getVariableCostControl($date, $department->id);

        $cost_effective_price = BaseData::query()->where('date', $date)
            ->where('department_id', $department->id)->value('cost_effective_price');
        // [?]% = 系数先按1
        $performance = ($amount * 0.3 - $variable_cost_control) * $cost_effective_price;
        $p = round($performance);


        $total_performances = DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)->value("total_performances");

        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->update([
                "total_performances" => $p + $total_performances,
                "special_performance" => $p ,
            ]);

        return $p;
    }

    /**
     * 门诊收费室绩效
     */
    public function getOutpatientCaisherPerformance($date) {
        $department = Department::where("name", "门诊收费室")
            ->first();
        if (empty($department)) {
            return 0;
        }

        // 门诊收费室=行政平均绩效*1.1*绩效人数+代收住院病人*5

        // 行政平均绩效
        $administrative_average = $this->getAdministrativeAverage($date);

        // 门诊收费室绩效人数
        $people_count = $this
            ->getAttendancePeopleCount($date, $department->id);

        // 门诊收费室绩效
        $base_performance = $administrative_average * 1.1 * $people_count;

        // 代收住院病人人数
        $substitute_obj = FavourCollectInpatientFee::where("date", $date)
            ->first();
        if (!empty($substitute_obj)) {
            $substitute_count = $substitute_obj->amount;
        } else {
            $substitute_count = 0;
        }
        // 代收住院病人绩效
        $substitute_performance = $substitute_count * 5;

        $p = round($base_performance + $substitute_performance);

        $total_performances = DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)->value("total_performances");

        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->update([
                "total_performances" => $p + $total_performances,
                "special_performance" => $p,
            ]);

        return $p;
    }

    /**
     * 住院收费室绩效
     */
    public function getInpatientCaisherPerformance($date) {
        $department = Department::where("name", "住院收费室")
            ->first();
        if (empty($department)) {
            return 0;
        }

        // 住院收费室绩效=行政平均绩效*1.1*绩效人数
        $people_count = $this
            ->getAttendancePeopleCount($date, $department->id);
        // 行政平均绩效
        $administrative_average = $this->getAdministrativeAverage($date);
        // 住院收费室绩效人数
        $inpatient_people = $this
            ->getAttendancePeopleCount($date, $department->id);

        $p = round($administrative_average * 1.1 * $people_count);

        $total_performances = DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)->value("total_performances");

        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->update([
                "total_performances" => $p + $total_performances,
                "special_performance" => $p,
            ]);
        // 住院收费室绩效
        return $p;
    }

    /**
     * 介入科绩效
     */
    public function getInterventionPerformance($date) {
        $arrDate = explode('-', $date);
        $department = Department::where("name", "介入科")->first();
        if (empty($department)) {
            return 0;
        }
        $subs = $department->subDepartments->pluck("id")->all();
        // 2805*3+检查例数*100元/例+介入手术台次*100元/台
        // 检查例数除根据收入表中流水号分组统计，手术台次根据收入表中类型为手术费的统计
        // 检查例数
        $checking_count = MainIncome::date($date)->whereIn("execute_department_id", $subs)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->select("serial_no")
            ->groupBy("serial_no")
            ->get()
            ->count();
        // 手术台次
        $surgery_count = MainIncome::date($date)->whereIn("execute_department_id", $subs)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("item_cate", "手术费")
            ->count();

        $p = round(2805 * 3 + $checking_count * 100 + $surgery_count * 100);

        $total_performances = DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)->value("total_performances");

        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->update([
                "total_performances" => $p + $total_performances,
                "special_performance" => $p,
            ]);
        
        return $p;
    }

    /**
     * 感染性疾病科绩效
     */
    public function getInfectiousPerformance($date) {
        $department = Department::where("name", "感染性疾病科")->first();
        // 每月在科室绩效基础上额外增加“内科人平绩效*4”的绩效。
        // 仅返回额外增加的部分
        // 内科人平绩效 = 内科科室总绩效/内科科室总考勤人数

        // 非手术科室（内科)
        $medicines = [
            "感染性疾病科",
            "心血管内科",
            "呼吸与危重症医学科",
            "神经内科",
            "消化内科",
            "内分泌·肾病·血液科",
            "儿科",
            "肿瘤科",
            "康复医学科·中医科",
            "血液透析室"
        ];

        $medicine_department_ids = Department::whereIn(
            "name", $medicines
            )->get()
            ->pluck("id");
        $performances = DepartmentPerformance::whereIn(
            "department_id", $medicine_department_ids
            )->selectRaw("total_performances - special_performance as total_performances")
            ->get();
        // 内科科室总绩效
        $total_performance = $performances->sum("total_performances");
        
        // 内科科室总考勤人数
        $people_count = $this->getAttendancePeopleCount(
            $date,
            $medicine_department_ids
        );


        if ($people_count == 0){
            return 0;
        }

        $p = round(($total_performance / $people_count) * 4);

        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->increment('total_performances', $p);

        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->update([
                "special_performance" => $p,
            ]);


        return $p;

    }

    /**
     * 发热门诊绩效
     */
    public function getFeverClinics($date) {
        $department = Department::where("name", "发热门诊")->first();
        if (empty($department)) {
            return 0;
        }
        $record = EmployeeAttendance::leftJoin("employees",
            "employee_attendances.employees_id", 
            "=", 
            "employees.id"
        )->selectRaw(
            "count(employees.id) as c, employees.attendances_type as t"
        )
        ->where("date", $date)
        ->where("employee_attendances.departments_id", $department->id)
        ->groupBy("employees.attendances_type")
        ->get()
        ->keyBy("t")
        ->all();

        $doctor_count = $record[EmployeeAttendanceType::DOCTOR]["c"];
        $nurse_count = $record[EmployeeAttendanceType::NURSE]["c"];

        // 行政平均绩效
        $administrative_average = $this->getAdministrativeAverage($date);

        $p = round($doctor_count * $administrative_average + $nurse_count * $administrative_average);

        $total_performances = DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)->value("total_performances");
        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->update([
                "total_performances" => $p + $total_performances,
                "special_performance" => $p,
            ]);

        return $p;

    }

    /**
     * 核酸采集人员绩效
     */
    public function getNucleicAcidPerformance($date) {
        // 核酸采集人员按行政平均绩效标准发放。
        $department = Department::where("name", "门岗及核酸采集")->first();

        if (empty($department)) {
            return 0;
        }

        // 核酸采集人员考勤人数
        $people_count = $this->getAttendancePeopleCount($date, $department->id);

        // 行政平均绩效
        $administrative_average = $this->getAdministrativeAverage($date);
        $p = round($people_count * $administrative_average);

        $total_performances = DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)->value("total_performances");

        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->update([
                "total_performances" => $p + $total_performances,
                "special_performance" => $p,
            ]);

        return $p;
    }

    /**
     * 缓冲病区绩效
     */
    public function getBufferWardPerformance($date) {
        // 缓冲病区护士按临床科室平均绩效标准发放
        // 缓冲病区医生按临床科室平均绩效*1.1标准发放

        $department = Department::where("name", "缓冲病区")->first();
        if (empty($department)) {
            return 0;
        }

        $clinicals = DepartmentPerformance::where(
            "type", DepartmentType::CLINICAL
            )->selectRaw("total_performances - special_performance as total_performances")
            ->get();

        // 临床科室总绩效
        $total_performance = $clinicals->sum("total_performances");
        
        // 临床科室总考勤人数
        $clinical_department_ids = $clinicals->pluck("department_id");
        $people_count = $this->getAttendancePeopleCount(
            $department->id,
            $clinical_department_ids
        );

        if ($people_count == 0){
            return 0;
        }
        // 临床科室平均绩效
        $clinical_performance_avg = $total_performance / $people_count;

        
        $record = EmployeeAttendance::leftJoin("employees",
            "employee_attendances.employees_id", 
            "=", 
            "employees.id"
        )->selectRaw(
            "count(employees.id) as c, employees.attendances_type as t"
        )
        ->where("date", $date)
        ->where("employee_attendances.departments_id", $department->id)
        ->groupBy("employees.attendances_type")
        ->get()
        ->keyBy("t")
        ->all();

        $doctor_count = $record[EmployeeAttendanceType::DOCTOR]["c"];
        $nurse_count = $record[EmployeeAttendanceType::NURSE]["c"];

        $p = round(
            ($nurse_count * $clinical_performance_avg) +
            ($doctor_count * $clinical_performance_avg * 1.1)
        );

        $total_performances = DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)->value("total_performances");

        DepartmentPerformance::where("department_id", $department->id)
            ->where("date", $date)
            ->update([
                "total_performances" => $p + $total_performances,
                "special_performance" => $p,
            ]);

        return $p;
    }

    /**
     * 计算一个或者多个科室的考勤人数（绩效人数）
     * 即这些人的结合考勤系数的总和
     */
    public function getAttendancePeopleCount($date, $departments_id) {
        if (!is_object($departments_id) && !is_array($departments_id)) {
            $departments_id = [$departments_id];
        }

        $attendance = EmployeeAttendance::whereIn("departments_id", $departments_id)
            ->where("date", $date)
            ->sum("attendance_coefficient");;


        return $attendance;
    }
}

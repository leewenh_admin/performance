<?php

namespace App\Repositories;

use App\Models\BaseData;

class BasedataRepositoryEloquent extends BaseRepository {
    protected $actionClass = BaseData::class;

    public function getList() {
        $query = parent::getList();

        $query->leftjoin('departments', 'base_datas.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'base_datas.date', '=', 'lock_performances.date');

        $department_id = request('department_id', null);
        if ($department_id){
            $query->where('department_id', $department_id);
        }

        $date = request('date', null);
        if ($date){
            $query->where('base_datas.date', $date);
        }

        $query->select('base_datas.*', 'departments.name as department_name', 'departments.code as department_code')
                ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        return $query;
    }

    public function parseListResult($query) {
        $perPage = request("perPage", 25);
        $sort_str = request("sort_str", null);
        $sort_order = request("sort_order", "desc");
        $allPage = request("allPage", 1);

        if (is_null($sort_str)){
            $query = $query->orderBy('date', 'desc')
                ->orderBy('code', 'asc');
        } else {
            $query = $query->orderBy($sort_str, $sort_order);
        }


        if ($allPage == 2) {
            return $query->get();
        } else {
            return $query->paginate($perPage);
        }
    }
}

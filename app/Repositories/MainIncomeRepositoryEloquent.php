<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\MainIncome;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MainIncomeRepositoryEloquent extends BaseRepository {
    protected $actionClass = MainIncome::class;

    public function getQuery() {
        return $this->getClass()::date(
            date('Y-m')
        );
    }



    public function getList() {
        $query = parent::getList();

//        $query = $query->with([
//            "prescribe_doctor:id,name",
//            "execute_doctor:id,name",
//            "prescribe_department:id,name",
//            "execute_department:id,name",
//            "admission_department:id,name",
//            "leave_department:id,name",
//        ]);
        //病人名称
        $patient = request("patient", null);

        if (!empty($patient)) {
            $query = $query->where("name", "like", "%$patient%");
        }
        //开单医生
        $prescribe_doctor = request("prescribe_doctor", null);
        if (!empty($prescribe_doctor)) {
            $query = $query->whereHas('prescribe_doctor', function ($query) use ($prescribe_doctor){
                if (!empty($prescribe_doctor)) {
                    $query->where("name", "like", "%$prescribe_doctor%");
                }
            });
        }
        //执行医生
        $execute_doctor = request("execute_doctor", null);
        if (!empty($execute_doctor)) {
            $query = $query->whereHas('execute_doctor', function ($query) use ($execute_doctor){
                if (!empty($execute_doctor)) {
                    $query->where("name", "like", "%$execute_doctor%");
                }
            });
        }

        //开单科室
        $prescribe_department_id = request("prescribe_department_id", null);
        if (!empty($prescribe_department_id)) {
            $query = $query->where("prescribe_department_id", $prescribe_department_id);
        }

        //执行科室
        $execute_department_id = request("execute_department_id", null);
        if (!empty($execute_department_id)) {
            $query = $query->where("execute_department_id", $prescribe_department_id);
        }

        //缴费时间
        $start_time = request("start_time", null);
        $end_time = request("end_time", null);
        //计算数据的总条数
        $count = 0;
        if (!empty($start_time) && !empty($end_time)) {
            $start_date = new Carbon($start_time);
            $end_date = new Carbon($end_time);

            $diff_months = diff_in_months($start_time, $end_time);
            $query_cursor = null;

            for ($i = 0;$i <= $diff_months; $i++) {
                $sub_query = (clone $query);
                $table_date = (clone $end_date)->subMonths($i);
                $table_name = 'main_incomes' . $table_date->format('Ym');
                if (!Schema::hasTable($table_name)){
                    continue;
                }

                $sub_query = $sub_query->from($table_name);
                $sub_query = $sub_query->whereBetween('charging_time', [$start_time, $end_time]);
                $count += $sub_query->count();
                if (empty($query_cursor)) {
                    $query_cursor = $sub_query;
                } else {
                    $query_cursor = $query_cursor->unionall($sub_query);
                }

            }
            if (empty($query_cursor)){
                $query_cursor = (clone $query)->whereBetween('charging_time', [$start_time, $end_time]);
            }

            $unionQuerys = $query_cursor->toSql();
            $total_query = MainIncome::from(DB::raw("($unionQuerys) as a"))
                ->mergeBindings($query_cursor->getQuery())
                ->select("a.*",DB::raw("case income_type when 1 then '门诊' when 2 then '住院' end as income_type"));

            return [$total_query, $count];

        }
        $query = $query->select("*",DB::raw("case income_type when 1 then '门诊' when 2 then '住院' end as income_type"));
        $count = $query->count();
        return [$query, $count];
    }

    public function parseListResult($query){
        $total = $query[1];
        $query = $query[0];
        $perPage = request("perPage", 25);
        $sort_str = $this->getSortStr();
        $sort_order = request("sort_order", null);
        $allPage = request("allPage", 1);

        if ($sort_str =='prescribe_doctor_name'){
            $sort_str = 'prescribe_doctor_id';
        } else if ($sort_str =='prescribe_department_name'){
            $sort_str = 'prescribe_department_id';
        }else if ($sort_str =='execute_doctor_name'){
            $sort_str = 'execute_doctor_id';
        }else if ($sort_str =='execute_department_name'){
            $sort_str = 'execute_department_id';
        }else if ($sort_str =='admission_department_name'){
            $sort_str = 'admission_department_id';
        }else if ($sort_str =='leave_department_name'){
            $sort_str = 'leave_department_id';
        }

        $query = $query->orderBy($sort_str, $sort_order);


        if ($allPage == 2) {
            return $query->get();
        } else {
            return $query->total($total)->paginate($perPage);
        }
    }
}

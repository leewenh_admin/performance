<?php

namespace App\Repositories;

use App\Models\MainIncome;
use App\Models\SurgicalPointValue;

class SurgicalPointValuesRepositoryEloquent extends BaseRepository {
    protected $actionClass = SurgicalPointValue::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
           "belongDepartment:id,name,code",
           "executeDepartment:id,name,code",
        ]);
        $query = $query->join('sub_departments as a', 'surgical_point_values.belong_department_id', '=', 'a.id')
            ->join('sub_departments as b', 'surgical_point_values.execute_department_id', '=', 'b.id');

        //项目名称
        $name = request("name", null);

        if (!empty($name)) {
            $query = $query->where("surgical_point_values.name", "like", "%$name%");
        }

        //项目编码
        $project_code= request("project_code", null);

        if (!empty($project_code)) {
            $query = $query->where("project_code", "like", "%$project_code%");
        }

        //编码
        $code= request("code", null);

        if (!empty($code)) {
            $query = $query->where("surgical_point_values.code", "like", "%$code%");
        }
        //国标编码
        $gb_code= request("gb_code", null);

        if (!empty($gb_code)) {
            $query = $query->where("gb_code", "like", "%$gb_code%");
        }

        //所属科室
        $belong_department_id = request("belong_department_id", null);

        if (!empty($belong_department_id)) {
            $query = $query->where("belong_department_id", $belong_department_id);
        }

        //执行科室
        $execute_department_id = request("execute_department_id", null);

        if (!empty($execute_department_id)) {
            $query = $query->where("execute_department_id", $execute_department_id);
        }

        $query->select('surgical_point_values.*');

        return $query;
    }

    public function getDiff(){
        $date = request("date", date("Y-m"));
        $arrDate = explode('-', $date);

        $diffData = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("item_cate_code", 14)//手术费
            ->leftjoin("surgical_point_values as sv", "item_code", "=", "code")
            ->whereNull("sv.id")
            ->select("item_code", "item_name")
            ->groupBy("item_code", "item_name")
            ->get();

        return $diffData;
    }

    public function parseListResult($query)
    {
        $perPage = request("perPage", 25);
        $sort_str = $this->getSortStr();
        $sort_order = request("sort_order", "desc");
        $allPage = request("allPage", 1);

        if ($sort_str == 'belong_department_code'){
            $sort_str = 'a.code';
        } elseif ($sort_str == 'execute_department_code'){
            $sort_str = 'b.code';
        }

        $query = $query->orderBy($sort_str, $sort_order);

        if ($allPage == 2) {
            return $query->get();
        } else {
            return $query->paginate($perPage);
        }
    }
}

<?php

namespace App\Repositories;

use App\Models\EmployeeAttendance;

class EmployeeAttendanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = EmployeeAttendance::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "subDepartment:id,name,code",
            "department:id,name,code",
            "employee:id,name",
            ]);

        $query = $query->join('departments', 'employee_attendances.departments_id', '=', 'departments.id')
            ->join('sub_departments', 'employee_attendances.sub_departments_id', '=', 'sub_departments.id')
            ->leftjoin('lock_performances', 'employee_attendances.date', '=', 'lock_performances.date');

        $employees_id = request("employees_id", null);
        if (!empty($employees_id)){
            $query = $query->where("employees_id", $employees_id);
        }

        $code = request("code", null);
        if (!empty($code)){
            $query = $query->where("employee_attendances.code", $code);
        }

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("employee_attendances.date", $date);
        }

        $departments_id = request("departments_id", null);
        if (!empty($departments_id)){
            $query = $query->where("departments_id", $departments_id);
        }

        $sub_departments_id = request("sub_departments_id", null);
        if (!empty($sub_departments_id)){
            $query = $query->where("sub_departments_id", $sub_departments_id);
        }
        $query->select('employee_attendances.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        return $query;
    }

    public function parseListResult($query)
    {
        $perPage = request("perPage", 25);
        $sort_str = $this->getSortStr();
        $sort_order = request("sort_order", "desc");
        $allPage = request("allPage", 1);

        if ($sort_str == 'departments_code'){
            $sort_str = 'departments.code';
        } elseif ($sort_str == 'sub_departments_code'){
            $sort_str = 'sub_departments.code';
        }
        $query = $query->orderBy($sort_str, $sort_order);

        if ($allPage == 2) {
            return $query->get();
        } else {
            return $query->paginate($perPage);
        }
    }
}

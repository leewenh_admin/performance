<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Hash;
use App\Exceptions\MessageException;
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeePassword;
use App\Models\EmployeeRole;
use App\Models\RolePermission;

class EmployeeRepositoryEloquent extends BaseRepository {
    protected $actionClass = Employee::class;
    
    public function setPassword($employee_code, $new_password) {
        $record = EmployeePassword::where("code", $employee_code)->first();

        if (!$record) {
            // throw new MessageException("职员记录未找到");
            $record = new EmployeePassword();
            $record->code = $employee_code;
        } 

        $record->password = Hash::make($new_password);
        $record->save();

        return true;
    }

    public function getList() {
        $query = parent::getList();

        $code = request("code", null);

        if (!empty($code)) {
            $query = $query->where("code", $code);
        }

        $department_id = request("department_id", null);

        if (!empty($department_id)) {
            $department = Department::where("id", $department_id)->first();
            if ($department) {
                $sub_departments = $department->subDepartments->pluck("id");
                $query = $query->whereIn("sub_departments_id", $sub_departments);
            } else {
                $query = $query->where("sub_departments_id", 0);
            }
        }

        $sub_department_id = request("sub_department_id", null);

        if (!empty($sub_department_id)) {
            $query = $query->where("sub_departments_id", $sub_department_id);
        }

        $name = request("name", null);

        if (!empty($name)) {
            $query = $query->where("name", "like", "%$name%");
        }

        $employee_type = request("type", null);

        if (!empty($employee_type)) {
            $query = $query->where("type", $employee_type);
        }

        $position = request("position", null);

        if (!empty($position)) {
            $query = $query->where("position", $position);
        }

        $query = $query->selectRaw("*, case when professional_rank_level = 0 then '无' when  professional_rank_level = 1 then '初级'
         when professional_rank_level = 2 then '中级' when professional_rank_level = 3 then '副高' 
         when professional_rank_level = 4 then '正高' when professional_rank_level = 5 then '返聘' end as professional_rank_level_name ,
        case when attendances_type = 0 then '无' when attendances_type = 1 then '医生' when attendances_type = 2 then '护理' 
        when attendances_type = 3 then '技师' when attendances_type = 4 then '药师' 
        when attendances_type = 5 then '行管' end as attendances_type_name, 
        case when position = 1 then '院长' when position = 2 then '副院长' when position = 3 then '院长助理' 
        when position = 4 then '科主任' when position = 5 then '副主任' when position = 6 then '护士长'
        when position = 7 then '职员' end as position_name,
        case when professional_rank_type = 1 then '护理' when professional_rank_type = 2 then '医疗' 
        when professional_rank_type = 3 then '经统会' when professional_rank_type = 4 then '医技' 
        when professional_rank_type = 5 then '药剂' when professional_rank_type = 6 then '工勤'
        end as professional_rank_type_name,
        case when sex = 1 then '男' when sex = 2 then '女' end as sex_name");

        return $query;
    }

    public function getPermissionIdList($userId) {
        $userRoles = EmployeeRole::where(
            "employees_id", $userId
        )->get()->pluck("roles_id")->all();

        $userPerms = RolePermission::whereIn(
            "roles_id", $userRoles
        )->get();

        return $userPerms;
    }
}

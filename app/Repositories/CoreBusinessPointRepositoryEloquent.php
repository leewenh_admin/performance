<?php

namespace App\Repositories;

use App\Models\CoreBusinessPoint;

class CoreBusinessPointRepositoryEloquent extends BaseRepository {
    protected $actionClass = CoreBusinessPoint::class;

    public function getList(){
        $query =  parent::getList();

        $query = $query->with('department:id,name,code');

        $query = $query->join('departments', 'core_business_points.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'core_business_points.date', '=', 'lock_performances.date');
        $query->select('core_business_points.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if ($date){
            $query = $query->where("core_business_points.date", $date);
        }

        $department_id = request("department_id", null);
        if ($department_id){
            $query = $query->where("department_id", $department_id);
        }

        return $query;
    }
}

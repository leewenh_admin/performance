<?php

namespace App\Repositories;

use App\Models\EmployeeRole;

class EmployeeRoleRepositoryEloquent extends BaseRepository {
    protected $actionClass = EmployeeRole::class;

    protected function getSortStr() {
        return "id";
    }

    public function getList() {
        $query = parent::getList();
        $query = $query->with("role")->with("employee");
        return $query;
    }

    public function getItem($id) {
        $record = parent::getItem($id);
        $record = $record->load("role", "employee");
        return $record;
    }
}

<?php

namespace App\Repositories;

use App\Models\StrokeCenterPerformance;

class StrokeCenterPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = StrokeCenterPerformance::class;

    public function getList()
    {
        $query =  parent::getList();
        $query = $query->with(["subDepartment:id,name,code","employee:id,name"]);
        $query = $query->join('sub_departments', 'stroke_center_performances.sub_department_id', '=', 'sub_departments.id');

        $sub_department_id = request("sub_department_id", null);
        if (!empty($sub_department_id)){
            $query = $query->where("sub_department_id", $sub_department_id);
        }

        $employee_id = request("employee_id", null);
        if (!empty($employee_id)){
            $query = $query->where("employee_id", $employee_id);
        }


        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("date", $date);
        }
        $query->select('stroke_center_performances.*');
        return $query;

    }
}

<?php

namespace App\Repositories;

use App\Enums\MaterialType;
use App\Models\UsedMaterial;

class UsedSanitaryMaterialRepositoryEloquent extends BaseRepository {
    protected $actionClass = UsedMaterial::class;

    public function getList()
    {
        $query =  parent::getList();
        $query = $query->with(["subDepartment:id,name,code"])->where("material_type", MaterialType::SANITARY);
        $query = $query->join('sub_departments', 'used_materials.sub_departments_id', '=', 'sub_departments.id')
            ->leftjoin('lock_performances', 'used_materials.date', '=', 'lock_performances.date');
        $sub_departments_id = request("sub_departments_id", null);
        if (!empty($sub_departments_id)){
            $query = $query->where("sub_departments_id", $sub_departments_id);
        }

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("used_materials.date", $date);
        }
        $query->select('used_materials.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        return $query;
    }

}

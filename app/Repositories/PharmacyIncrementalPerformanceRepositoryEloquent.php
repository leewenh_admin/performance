<?php

namespace App\Repositories;

use App\Models\PharmacyIncrementalPerformance;

class PharmacyIncrementalPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = PharmacyIncrementalPerformance::class;

    public function getList()
    {
        $query = parent::getList();

        $query->leftjoin('lock_performances', 'pharmacy_incremental_performances.date', '=', 'lock_performances.date')
              ->select('pharmacy_incremental_performances.*')
              ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("pharmacy_incremental_performances.date", $date);
        }

        return $query;
    }
}

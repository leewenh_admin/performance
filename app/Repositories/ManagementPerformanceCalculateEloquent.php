<?php

namespace App\Repositories;

use App\Enums\DepartmentType;
use App\Enums\EmployeePosition;
use App\Models\BedUsing;
use App\Models\DepartmentManagementCoefficient;
use App\Models\DepartmentPerformance;
use App\Models\Employee;
use App\Models\EmployeeAttendance;
use App\Models\ExpertDiagnosis;
use App\Models\Department;
use App\Models\ManagementPerformance;
use App\Models\SubDepartment;
use App\Models\SurgeryNew;
use Illuminate\Support\Facades\DB;

/**
 * 管理绩效计算
 * Class PerformanceManagementCalculateEloquent
 * @package App\Repositories
 */
class ManagementPerformanceCalculateEloquent extends BaseRepository {

    /**
     * 生成科室管理绩效表
     * @param $date
     */
    public function generateManagementPerformance($date){
        //参与绩效的所有科室
        $departments = Department::leftjoin(
            'sub_departments', 'departments.id', '=', 'sub_departments.parent_id'
        )->where("sub_departments.type_lv1", "<>", 0)
            ->where("departments.is_performance", 1)
            ->select("departments.id","type_lv1")
            ->distinct()
            ->get();

        foreach ($departments as $department){
            $departmentHeadAward = 0;
            $departmentSubHeadAward = 0;
            $departmentNurseHeadAward = 0;
            if($department->type_lv1 == DepartmentType::CLINICAL ||
                $department->type_lv1 == DepartmentType::MEDICAL_TECHNICAL){
                //临床医技科室负责人管理奖
                $departmentHeadAward = $this->getDepartmentHeadAward($date, $department->id);
                //临床医技科室副主任管理奖
                $departmentSubHeadAward = $this->getDepartmentSubHeadAward($departmentHeadAward);
                //临床医技科室护士长管理奖
                $departmentNurseHeadAward = $this->getDepartmentNurseHeadAward($departmentHeadAward);
            } elseif($department->type_lv1 == DepartmentType::FUNCTIONAL){
                //职能科室平均奖
                $departmentAverageAward = $this->getFunctionDepartmentAverageAward($date, $department->id);
                //职能科室主任管理奖
                $departmentHeadAward = $this->getFunctionDepartmentHeadAward($departmentAverageAward);
                //职能科室副主任管理奖
                $departmentSubHeadAward = $this->getFunctionSubDepartmentHeadAward($departmentAverageAward);
                $departmentNurseHeadAward = 0 ;

            }

            $data = [
                "date" => $date,
                "department_id" => $department->id,
                "type_lv1" => $department->type_lv1,
                "department_head_award" => $departmentHeadAward,
                "department_sub_head_award" => $departmentSubHeadAward,
                "department_nurse_head_award" => $departmentNurseHeadAward,
            ];

            ManagementPerformance::create($data);
        }
    }


    /**
     * 生成科室的绩效排名
     * @param $date
     */
    public function generateEfficiencyWeight($date){
        $departments = DepartmentManagementCoefficient::where("date", $date)
            ->get();
        $arrEfficiency = [];
        foreach ($departments as $department){
            $result = $this->getPercapitaEfficiency($date, $department->department_id);
            $arrEfficiency[$department->department_id] = $result;
        }
        arsort($arrEfficiency);
        $weight = 0;
        $lastWeight = 0;
        foreach ($arrEfficiency as $key => $item){
            if(isset($lastVal)){
                if($item == $lastVal){
                    $weight = $lastWeight;
                } else {
                    $weight += 1;
                    $lastVal = $item;
                    $lastWeight = $weight;
                }
            } else {
                $weight += 1;
                $lastVal = $item;
                $lastWeight = $weight;
            }
            DepartmentManagementCoefficient::where("date", $date)
                ->where("department_id", $key)
                ->update(["efficiency_ranking" => $weight ]);
        }
    }

    /**
     * 临床医技科室负责人管理奖
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getDepartmentHeadAward($date, $department_id){
        //查询科室系数
        $coefficientData = $this->getManagementCoefficientData($date, $department_id);
        if (empty($coefficientData)){
            return 0;
        }
        //科室负责人管理奖=科室平均奖*（科主任岗位系数+科室规模系数+科室重要程度系数+科室人均效率排名系数+科主任专家号绩效占比系数）-科室平均奖*1.6
        //科室平均奖
        $departmentAverageAward = $this->getDepartmentAverageAward($date, $department_id);
        //科主任岗位系数
        $postCoefficient = $coefficientData->post_coefficient;
        //科室规模系数
        $scaleCoefficient = $coefficientData->scale_coefficient;
        //科室重要程度系数
        $brandCoefficient = $coefficientData->brand_coefficient;
        //科室人均效率排名系数
        $efficiencyRanking = $this->getDepartmentEfficiencyWeight($date, $department_id);
        //科主任专家号绩效占比系数
        $visitPerformance = $this->getVisitPerformance($date, $department_id);

        $departmentHeadAward = $departmentAverageAward * ($postCoefficient + $scaleCoefficient +
                $brandCoefficient + $efficiencyRanking + $visitPerformance) - $departmentAverageAward * 1.6;
        return round($departmentHeadAward, 2);
    }


    /**
     * 临床医技科室副主任管理奖
     * @param $departmentHeadAward 科室负责人管理奖
     * @return false|float
     */
    public function getDepartmentSubHeadAward($departmentHeadAward){
        //科室副主任管理奖 = 科室副主任管理奖 * 0.2
        $departmentSubHeadAward = $departmentHeadAward * 0.2;
        return round($departmentSubHeadAward, 2);
    }


    /**
     * 临床医技科室护士长管理奖
     * @param $departmentHeadAward 科室负责人管理奖
     * @return false|float
     */
    public function getDepartmentNurseHeadAward($departmentHeadAward){
        //科室护士长管理奖 = 科室副主任管理奖 * 0.2
        $departmentSubHeadAward = $departmentHeadAward * 0.2;
        return round($departmentSubHeadAward, 2);
    }


    /**
     * 职能科室科室平均奖
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getFunctionDepartmentAverageAward($date, $department_id){
        //查询科室的绩效
        $departPerformance = DepartmentPerformance::where("department_id", $department_id)
            ->where("date", $date)
            ->first();
        if (empty($departPerformance)){
            return 0;
        }

        //查询二级科室
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();
        //查询考勤医生人数
        $doctors = EmployeeAttendance::leftjoin("employees", "employees.id", "employee_attendances.employees_id")
            ->whereIn("sub_departments_id", $department_ids)
            ->where("date", $date)
            ->where("employees.attendances_type", 1)
            ->whereRaw("employee_attendances.actual_attendance = (select max(t.actual_attendance)
            from employee_attendances as t where
            employee_attendances.employees_id = t.employees_id and t.date = '$date') ")
            ->count();

        if(empty($doctors)){
            return 0;
        }


        $departmentAverageAward = $departPerformance->total_performances / $doctors;

        return round($departmentAverageAward, 2);
    }



    /**
     * 职能科室主任管理奖
     * @param $departmentAverageAward 职能科室科室平均奖
     * @return false|float|int
     */
    public function getFunctionDepartmentHeadAward($departmentAverageAward){
        $departmentHeadAward = $departmentAverageAward * 0.8;

        return round($departmentHeadAward, 2);
    }

    /**
     * 职能科室副主任管理奖
     * @param $departmentAverageAward 职能科室科室平均奖
     * @return false|float|int
     */
    public function getFunctionSubDepartmentHeadAward($departmentAverageAward){
        $departmentHeadAward = $departmentAverageAward * 0.5;

        return round($departmentHeadAward, 2);
    }

    /**
     * 职能科室副院长绩效
     * @param $date
     * @return false|float
     */
    public function getVicePresidentPerformance($date){
        //副院长绩效按照临床医技科室科主任前五名平均绩效计算
        $vicePresidentPerformance = ManagementPerformance::where("date", $date)
                ->whereIn("type_lv1", [DepartmentType::CLINICAL, DepartmentType::MEDICAL_TECHNICAL])
                ->limit(5)
                ->orderBy("department_head_award", "desc")
                ->avg("department_head_award");

        return round($vicePresidentPerformance, 2);
    }

    /**
     * 职能科室院长绩效
     * @param $vicePresidentPerformance
     * @return false|float
     */
    public function getDeanPresidentPerformance($vicePresidentPerformance){
        //院长绩效总额按照副院长1.15倍计算
        $deanPresidentPerformance = $vicePresidentPerformance * 1.15;
        return round($deanPresidentPerformance, 2);
    }


    /**
     * 职能科室院长助理绩效
     * @param $vicePresidentPerformance
     * @return false|float
     */
    public function getDeanAssistantPerformance($vicePresidentPerformance){
        //院长助理绩效按副院长80%计算
        $DeanAssistantPerformance = $vicePresidentPerformance * 0.8;
        return round($DeanAssistantPerformance, 2);
    }



    /**
     * 查询科室的管理相关系数
     * @param $date
     * @param $department_id
     * @return mixed
     */
    public function getManagementCoefficientData($date, $department_id){
        $data = DepartmentManagementCoefficient::where("department_id", $department_id)
            ->where("date", $date)
            ->first();

        return $data;
    }

    /**
     * 临床医技科室平均奖
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getDepartmentAverageAward($date, $department_id){
        //查询科室的绩效
        $departPerformance = DepartmentPerformance::where("department_id", $department_id)
                    ->where("date", $date)
                    ->first();


        if (empty($departPerformance)){
            return 0;
        }

        $coefficientData = $this->getManagementCoefficientData($date, $department_id);
        if (empty($coefficientData)){
            return 0;
        }
        $departmentAverageAward = $departPerformance->total_performances / $coefficientData->employees_number;

        return round($departmentAverageAward, 2);
    }

    /**
     * 临床医技科室科室人均效率计算
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getPercapitaEfficiency($date, $department_id){
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();

        $coefficientData = $this->getManagementCoefficientData($date, $department_id);
        if (empty($coefficientData)){
            return 0;
        }

        $workload_obj = new WorkLoadRepositoryEloquent();
        //科室门诊人次
        $outPatientCount = $workload_obj->getOutPatientCount($date, $department_id);
        //科室医生人均门诊人次 = 科室门诊人次 / 科室医生人数
        $departmentWorkNum = $outPatientCount / $coefficientData->employees_number;
        //科室占床日总和
        $obj = new WorkLoadRepositoryEloquent();
        $departAvgBedDayAll = $obj->getBedCount($date, $department_ids);
//        $departAvgBedDayAll = BedUsing::whereIn("sub_departments_id", $department_ids)
//            ->where("date", $date)
//            ->sum("leaver_used_bed_day");
        //人均出院床日数
        $departmentPerBedNum = $departAvgBedDayAll / $coefficientData->employees_number;
        //科室手术数量
        $surgerys = SurgeryNew::whereIn("sub_departments_id", $department_ids)
            ->where("date", $date)
            ->count();
        //人均手术数量
        $departmentPerSurgerysNum = $surgerys / $coefficientData->employees_number;
        //科室人均效率
        $percapitaEfficiency = $departmentWorkNum * 0.3 + $departmentPerBedNum * 0.5 + $departmentPerSurgerysNum * 0.2;

        return $percapitaEfficiency;

    }


    /**
     * 生成临床医技科室的人均效率排名绩效
     * @param $date
     * @param $department_id
     * @return float|int|mixed
     */
    public function getDepartmentEfficiencyWeight($date, $department_id){
        $coefficientData = $this->getManagementCoefficientData($date, $department_id);
        if (empty($coefficientData)){
            return 0;
        }

        if ($coefficientData->efficiency_ranking = 1 && $coefficientData->efficiency_ranking <= 10){
            return 0.2;
        } elseif ($coefficientData->efficiency_ranking >= 11 && $coefficientData->efficiency_ranking <= 20){
            return 0.1;
        } else {
            return 0;
        }

    }

    /**
     * 临床医技科室科主任专家号绩效占比系数
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getVisitPerformance($date, $department_id){

        //科室内的科主任
        $doctors = $this->getDepartmentHead($date, $department_id);
        //科室主任专家号数量
        $visitNum = ExpertDiagnosis::where("date", $date)
            ->whereIn("employees_id", $doctors)
            ->sum("total_count");
        //科室主任专家号绩效
        $visitAmount = $visitNum * 9.8;
        //查询科室的绩效
        $departPerformance = DepartmentPerformance::where("department_id", $department_id)
            ->where("date", $date)
            ->first();
        if(empty($departPerformance)){
            return 0;
        }
        //z专家号绩效占比
        $expert_number_rate = $visitAmount / $departPerformance->total_performances;
        $coefficient = 0;
        if($expert_number_rate < 0.01){
            $coefficient = 0.1;
        } elseif ($expert_number_rate >= 0.01 && $expert_number_rate < 0.02){
            $coefficient = 0;
        } elseif ($expert_number_rate >= 0.02 && $expert_number_rate < 0.03){
            $coefficient = -0.1;
        } elseif ($expert_number_rate >= 0.03 && $expert_number_rate < 0.15){
            $coefficient = -0.3;
        } elseif ($expert_number_rate >= 0.15 && $expert_number_rate < 0.25){
            $coefficient = -0.5;
        } elseif ($expert_number_rate >= 0.25){
            $coefficient = -0.7;
        }

        return $coefficient;
    }

    /**
     * 查询科室内的科主任
     * @param $date
     * @param $department_id
     * @return mixed
     */
    public function getDepartmentHead($date, $department_id){
//        $department_ids = SubDepartment::where("parent_id", $department_id)
//            ->where("is_performance", 1)
//            ->get()
//            ->pluck("id")
//            ->all();
        //查询所有的科主任
        $departmentHeader = Employee::where("position", EmployeePosition::DEPARTMENT_HEAD)
            ->get()
            ->pluck("id")
            ->all();
        //科室内考勤的科主任
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->whereIn("employees_id", $departmentHeader)
            ->get()
            ->pluck("employees_id")
            ->all();

        return $doctors;
    }







}

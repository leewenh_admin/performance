<?php

namespace App\Repositories;

use App\Models\WithoutPay;

class WithoutPayRepositoryEloquent extends BaseRepository {
    protected $actionClass = WithoutPay::class;

    public function getList()
    {
        $query = parent::getList();

        $name = request("name", null);
        if (!empty($name)){
            $query = $query->where("name", "like", "%$name%");
        }

        $code = request("code", null);
        if (!empty($code)){
            $query = $query->where("code", "like", "%$code%");
        }

        return $query;
    }
}

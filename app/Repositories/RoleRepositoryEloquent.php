<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepositoryEloquent extends BaseRepository {
    protected $actionClass = Role::class;
}

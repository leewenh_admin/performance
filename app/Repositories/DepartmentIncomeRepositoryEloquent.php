<?php

namespace App\Repositories;

use App\Enums\BusinessIncomeType;
use App\Enums\DepartmentType;
use App\Enums\EmployeeProfessionalRankType;
use App\Enums\IncomeType;
use App\Enums\SettingValueType;
use App\Models\Employee;
use App\Models\EmployeeAttendance;
use App\Models\GeneralizedIncome;
use App\Models\MainIncome;
use App\Models\Setting;
use App\Models\SubDepartment;
use Illuminate\Support\Facades\DB;
use function foo\func;

/**
 * 计算科室的广义收入和狭义收入
 * Class DepartmentIncomeRepositoryEloquent
 * @package App\Repositories
 */
class DepartmentIncomeRepositoryEloquent extends BaseRepository {


    /**
     * 生成二级部门的广义收入
     */
    public function generateData($date, $type) {
        if($type == BusinessIncomeType::CLINICAL_DEPARTMENT){
            $departments = SubDepartment::where("type_lv1", DepartmentType::CLINICAL)->get();
            $arrNurse = array();

        } else if($type == BusinessIncomeType::MEDICAL_DEPARTMENT){
            $departments = SubDepartment::where("type_lv1", DepartmentType::MEDICAL_TECHNICAL)->get();
            $arrNurse = array();

        } else if($type == BusinessIncomeType::NURSE_GROUP){
            $departments = SubDepartment::whereIn("type_lv1", [DepartmentType::CLINICAL, DepartmentType::MEDICAL_TECHNICAL])->get();
            $arrNurse = $this->getNursing();
        }

        foreach ($departments as $department) {
            $data = [
                "date" => $date,
                "sub_department_id" => $department->id,
                "type" => $type,
                "diagnosis_income" => $this->getDiagnosisIncome($date, $department->id, $arrNurse),
                "check_income" => $this->getCheckIncome($date, $department->id, $arrNurse),
                "treatment_income" => $this->getTreatmentIncome($date, $department->id, $arrNurse),
                "assay_income" => $this->getAssayIncome($date, $department->id, $arrNurse),
                "bed_income" => $this->getBedIncome($date, $department->id, $arrNurse),
                "nursing_income" => $this->getNursingIncome($date, $department->id, $arrNurse),
                "material_income" => $this->getMaterialIncome($date, $department->id, $arrNurse),
                "drug_income" => $this->getDrugIncome($date, $department->id, $arrNurse),
                "surgery_income" => $this->getSurgeryIncome($date, $department->parent_id, $department->id, $arrNurse),
            ];
            GeneralizedIncome::create($data);
        }
    }


    /**
     * 查询所有的护理组成员
     * @param $sub_department_id
     * @return mixed
     */
    public function getNursing(){
        $data = Employee::where("professional_rank_type", EmployeeProfessionalRankType::NURSE)
            ->get()
            ->pluck("id")
            ->all();

        return $data;
    }



    /**
     * 二级科室诊收入
     * @param $date
     * @param $sub_department_id
     * @return mixed
     */
    public function getDiagnosisIncome($date, $sub_department_id, $arrNurse){
        $arrDate = explode('-', $date);

        //诊察收入 = 住院诊察费、诊查费、门诊诊查
        $query = MainIncome::date($date)->where("execute_department_id", $sub_department_id)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereIn("item_cate_code", [7, 42, 71]);
        if (!empty($arrNurse)){
            $query = $query->whereIn("execute_doctor_id", $arrNurse);
        }

        $amount = $query->sum("amount");

        return $amount;
    }

    /**
     * 二级科室检查收入
     * @param $date
     * @param $sub_department_id
     * @return string
     */
    public function getCheckIncome($date, $sub_department_id, $arrNurse){
        $arrDate = explode('-', $date);
        //检查收入 = 介入治疗、11.放射费、23.心电图、病理检查、26.B超、电子胃镜、电子结肠镜、29.心血管、32运动平板
        //、35.CT、45.超声波、肺功能测定、60.磁共振、乳透、75.脑电图、78.呼气试验、电子纤支镜

        $query = MainIncome::date($date)->where("execute_department_id", $sub_department_id)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereIn("item_cate_code", [11, 23, 26, 29, 32, 35, 45, 60, 75, 78]);

        if (!empty($arrNurse)){
            $query = $query->whereIn("execute_doctor_id", $arrNurse);
        }

        $amount = $query->sum("amount");

        return $amount;
    }

    /**
     * 治疗收入
     * @param $date
     * @param $sub_department_id
     * @return string
     */
    public function getTreatmentIncome($date, $sub_department_id, $arrNurse){
        $arrDate = explode('-', $date);
        //治疗收入 = 8.治疗费、5.输液费、12.理疗费、三维理疗、16.换药费、18.输氧费、19.接生、温箱、25.配镜、
        //31.口腔、住院妇检、结核菌试验、36.会诊、激光治疗、38.输血化验、39.碎石、77.急诊、耳鼻喉、眼科、
        //46.注射、47.雾化、体疗、50.特治、妇检、门外处置、55.手术1、皮肤科治疗、耳鼻喉检查、59.出诊费、
        //医疗整形、煎药费、婴儿出生证、66.空调费、67.暖气费、ICU费、72.院前急救费、光子嫩肤、77.急诊观察费

        $query = MainIncome::date($date)->where("execute_department_id", $sub_department_id)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereIn("item_cate_code", [8, 5, 12, 16, 18, 19, 25, 31, 36, 38, 39, 77, 46, 47, 50, 55, 59, 66, 67, 72, 77]);

        if (!empty($arrNurse)){
            $query = $query->whereIn("execute_doctor_id", $arrNurse);
        }

        $amount = $query->sum("amount");

        return $amount;
    }

    /**
     * 二级科室化验收入
     * @param $date
     * @param $sub_department_id
     * @return string
     */
    public function getAssayIncome($date, $sub_department_id, $arrNurse){
        $arrDate = explode('-', $date);
        //化验收入 = 化验费

        $amount = MainIncome::date($date)->where("execute_department_id", $sub_department_id)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("item_cate_code", 9)//化验费
            ->when(!empty($arrNurse), function ($query) use ($arrNurse){
                return $query->whereIn("execute_doctor_id", $arrNurse);
            })
            ->sum("amount");

        return $amount;
    }


    /**
     * 二级科室床位收入
     * @param $date
     * @param $sub_department_id
     * @return string
     */
    public function getBedIncome($date, $sub_department_id, $arrNurse){
        $arrDate = explode('-', $date);
        //床位收入 = 床位费

        $amount = MainIncome::date($date)->where("execute_department_id", $sub_department_id)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("item_cate_code", 1)//床位费
            ->when(!empty($arrNurse), function ($query) use ($arrNurse){
                return $query->whereIn("execute_doctor_id", $arrNurse);
            })
            ->sum("amount");

        return $amount;
    }

    /**
     * 二级科室护理收入
     * @param $date
     * @param $sub_department_id
     * @return string
     */
    public function getNursingIncome($date, $sub_department_id, $arrNurse){
        $arrDate = explode('-', $date);
        //护理收入 = 护理费、陪护、婴护

        $amount = MainIncome::date($date)->where("execute_department_id", $sub_department_id)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("item_cate_code", 65)//"65.护理费", "陪护", "婴护"
            ->when(!empty($arrNurse), function ($query) use ($arrNurse){
                return $query->whereIn("execute_doctor_id", $arrNurse);
            })
            ->sum("amount");

        return $amount;
    }

    /**
     * 二级科室材料收入
     * @param $date
     * @param $sub_department_id
     * @return string
     */
    public function getMaterialIncome($date, $sub_department_id, $arrNurse){
        $arrDate = explode('-', $date);
        //材料收入 = 材料费、手术材料、血费、特材

        $amount = MainIncome::date($date)->where("execute_department_id", $sub_department_id)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereIn("item_cate_code", [64, 15, 17])//"64.材料费", "15.手术材料", "17.血费"
            ->when(!empty($arrNurse), function ($query) use ($arrNurse){
                return $query->whereIn("execute_doctor_id", $arrNurse);
            })
            ->sum("amount");

        return $amount;
    }

    /**
     * 二级科室药品收入
     * @param $date
     * @param $sub_department_id
     * @return string
     */
    public function getDrugIncome($date, $sub_department_id, $arrNurse){
        $arrDate = explode('-', $date);

        $amount = MainIncome::date($date)->where(
            "prescribe_department_id", $sub_department_id
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::OUTPATIENT)
            ->whereIn("item_cate_code", [2, 3, 4, 82])//西药费,中成药,中草药
            ->when(!empty($arrNurse), function ($query) use ($arrNurse){
                return $query->whereIn("prescribe_doctor_id", $arrNurse);
            })
            ->sum("amount");


        return $amount;
    }


    /**
     * 二级科室手术收入
     * @param $date
     * @param $department_id
     * @param $sub_department_id
     * @return string
     */
    public function getSurgeryIncome($date, $department_id, $sub_department_id, $arrNurse){
        $arrDate = explode('-', $date);
        $surgery_income = MainIncome::date($date)->where("prescribe_department_id", $sub_department_id)
            ->where("execute_department_id", 150)//麻醉科
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::INPATIENT)
            ->when(!empty($arrNurse), function ($query) use ($arrNurse){
                return $query->whereIn("execute_doctor_id", $arrNurse);
            })
            ->where("item_cate_code", 14) //手术费
            ->sum("amount");

        //心血管内科和神经外科·烧伤科·血管外科 累加介入科的手术费
        if ($department_id == 233) {
            $surgery_income += MainIncome::date($date)->where("prescribe_department_id", $sub_department_id)
                ->where("execute_department_id", 101)//执行科室为介入科
                ->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->where("income_type", IncomeType::INPATIENT)
                ->when(!empty($arrNurse), function ($query) use ($arrNurse){
                    return $query->whereIn("execute_doctor_id", $arrNurse);
                })
                ->where("item_cate_code", 14)//手术费
                ->sum("amount");
        }elseif ($department_id == 217){
            //科室内考勤的医生
            $doctors = EmployeeAttendance::query()->where("date", $date)
                ->where("sub_departments_id", $sub_department_id)
                ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                    $join->on('employee_attendances.code', 'b.code')
                        ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
                })
                ->when(!empty($arrNurse), function ($query) use ($arrNurse){
                    return $query->whereIn("employees_id", $arrNurse);
                })
                ->get()
                ->pluck("actual_attendance", "employees_id")
                ->all();
            $doctor_ids = array_keys($doctors);
            $surgery_income += MainIncome::date($date)->where("prescribe_doctor_id", $doctor_ids)
                ->where("execute_department_id", 101)//执行科室为介入科
                ->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->where("income_type", IncomeType::INPATIENT)
                ->where("item_cate_code", 14)//手术费
                ->sum("amount");
        }

        return $surgery_income;
    }


}

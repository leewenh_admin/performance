<?php

namespace App\Repositories;

use App\Enums\DepartmentType;
use App\Enums\IncomeType;
use App\Models\BaseData;
use App\Models\CoreBusinessPoint;
use App\Models\Department;
use App\Models\MainIncome;
use App\Models\SubDepartment;
use App\Models\SurgeryNew;

class CoreBusinessRepositoryEloquent extends BaseRepository
{


    /**
     * 核心绩效计算
     * @param $date
     * @param $department_id
     * @return false|float
     */
    public function getCoreBusinessPerformance($date, $department_id)
    {
        //查询是否设置了核心业务点数
        $data = CoreBusinessPoint::where("date", $date)->where("department_id", $department_id)->first();
        if($data){
            //核心业务点数单价
            $point_price = BaseData::query()->where('date', $date)
                ->where('department_id', $department_id)->value('point_price');

            return round($data->point * $point_price);
        }





        //内科核心绩效
        $diseasePerformance = $this->getDiseasePerformance($date, $department_id);
        //外科核心绩效
        $surgeryPerformance = $this->getSurgeryPerformance($date, $department_id);

        $performance = round($diseasePerformance + $surgeryPerformance, 2);

        return $performance;
    }

    /**
     * 获取二级科室
     * @param $type 1=>内科科室(非手术科室) 2=>手术科室
     * @param $department_id
     * @return int
     */
    public function getOutPatientDepartments($department_id, $type)
    {
        // 获取二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("type_lv3", $type)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();

        return $department_ids;
    }

    /**
     * 内科核心绩效
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getDiseasePerformance($date, $department_id)
    {
        // 获取二级非手术科室ID列表
        $type = DepartmentType::MEDICINE;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }

        $arrDate = explode('-', $date);
        $new_table = "main_incomes" . $arrDate[0] . $arrDate[1];

        $str_date = str_replace("-", "", $date);
        //出院诊断权重
        $leaveWeight = MainIncome::date($date)->leftjoin(
            'diease_catalogs', $new_table.'.leave_diagnosis_code', '=', 'diease_catalogs.code'
        )
            ->whereIn("leave_department_id", $department_ids)
            ->whereRaw("left(leave_time,6) =  '".$str_date."'")
            ->where("is_have_case", 1)
            ->select($new_table.".serial_no", "diease_catalogs.weights")
            ->distinct()
            ->get()
            ->sum("weights");



        //次诊断权重
        $subWeight = MainIncome::date($date)->leftjoin(
            'diease_catalogs', $new_table.'.sub_diagnosis_code', '=', 'diease_catalogs.code'
        )
            ->whereIn("leave_department_id", $department_ids)
            ->whereRaw("left(leave_time,6) =  '".$str_date."'")
            ->where("is_have_case", 1)
            ->select($new_table.".serial_no", "diease_catalogs.weights")
            ->distinct()
            ->get()
            ->sum("weights");


        //查询点值
        $point = BaseData::query()->where('date', $date)
            ->where('department_id', $department_id)->value('point_price');
    
        $diseasePerformance = round(
            $leaveWeight * $point + $subWeight * $point * 0.5,
            2
        );

    
        return $diseasePerformance;
    }

    /**
     * 手术核心绩效
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getSurgeryPerformance($date, $department_id)
    {
        // 获取二级手术科室ID列表
        $type = DepartmentType::SURGICAL;
        $department_ids = $this->getOutPatientDepartments($department_id, $type);
        if (empty($department_ids)) {
            return 0;
        }
        //手术人数
        $surgeryCount = SurgeryNew::whereIn("sub_departments_id", $department_ids)
            ->where("date", $date)
            ->get()
            ->count();


        //出院人数
        $obj = new WorkLoadRepositoryEloquent();
        $leaverCount = $obj->getOutHospitalCount($date, $department_ids);


        if ($leaverCount == 0) {
            $surgeryRate = 0;
        } else {
            // 实际手术率
            $surgeryRate = $surgeryCount / $leaverCount;
        }

        $department_data = Department::where("id", $department_id)
            ->select("punish_amount", "reward_amount", "target_surgery_rate")
            ->first();


        // 目标手术率
        // 数据库保存的手术率是百分比
        $target_surgery_rate = $department_data->target_surgery_rate / 100;


        // 手术率调整额
        if($target_surgery_rate == 0){
            $surgeryChange = 0;
        } else {
            if ($surgeryRate > $target_surgery_rate) {
                $surgeryChange = ($surgeryRate - $target_surgery_rate)
                    / $target_surgery_rate * $department_data->punish_amount;
            } else {
                $surgeryChange = ($target_surgery_rate - $surgeryRate)
                    / $target_surgery_rate * $department_data->reward_amount;
            }
        }


        $arrDate = explode('-', $date);
        $new_table = "main_incomes" . $arrDate[0] . $arrDate[1];

        $arrDate = explode('-', $date);
        //手术点数
        $surgeryPoint = MainIncome::query()->date($date)
            ->join(\DB::raw("(select code,performance_weights,belong_department_id from surgical_point_values) as pw"), 'item_code', '=', 'code')
            ->whereIn("prescribe_department_id", $department_ids)
            ->whereNotIn("execute_department_id", $department_ids)
            ->where("item_cate_code", 14)//手术费
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->select("serial_no")
            ->selectRaw("(performance_weights * quantity) as weightCount")
            ->get()
            ->sum("weightCount");

        //神经外科·烧伤科·血管外科 累加介入科的手术费
        $subDepartments1 = $this->getSubDepartments(218);
        if ($department_id == 233) {
            $surgeryPoint += MainIncome::date($date)
                ->join(\DB::raw("(select code,performance_weights,belong_department_id from surgical_point_values) as pw"), 'item_code', '=', 'code')
                ->whereIn("prescribe_department_id", $department_ids)
                ->whereIn("execute_department_id", $subDepartments1)//执行科室为介入科
                ->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->where("income_type", IncomeType::INPATIENT)
                ->where("item_cate_code", 14)//手术费
                ->select("serial_no")
                ->selectRaw("(performance_weights * quantity) as weightCount")
                ->get()
                ->sum("weightCount");
        }

        $point_price = BaseData::query()->where('date', $date)
            ->where('department_id', $department_id)->value('point_price');

        $surgeryPerformance = round(
            $surgeryPoint * $point_price + $surgeryChange,
            2
        );

        return $surgeryPerformance;
    }

    /**
     * 查询一级科室下的二级科室
     * @param $department_id
     * @return mixed
     */
    public function getSubDepartments($department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();

        return $department_ids;
    }


}

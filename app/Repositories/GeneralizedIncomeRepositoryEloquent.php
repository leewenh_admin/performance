<?php

namespace App\Repositories;

use App\Enums\BusinessIncomeType;
use App\Models\GeneralizedIncome;
use App\Models\MainIncome;
use Illuminate\Support\Facades\DB;

class GeneralizedIncomeRepositoryEloquent extends BaseRepository {
    protected $actionClass = GeneralizedIncome::class;

    public function getList() {
        $query = parent::getList();

        $query = $query->with(["department:id,name,code"]);
        $query = $query->join('sub_departments', 'generalized_incomes.sub_department_id', '=', 'sub_departments.id')
            ->leftjoin('lock_performances', 'generalized_incomes.date', '=', 'lock_performances.date');



        //收入时间
        $charging_time = request("date", null);

        if (!empty($charging_time)) {
            $query = $query->where("generalized_incomes.date", $charging_time);
        }

        //收入类型
        $type = request("type", null);

        if (!empty($type)) {
            $query = $query->where("type", $type);
        }

        $code= request("code", null);
        if(!empty($code)){
            $query = $query->where("sub_departments.code", $code);
        }

        //二级科室
        $sub_department_id = request("sub_department_id", null);

        if (!empty($sub_department_id)) {
            $query = $query->where("sub_department_id", $sub_department_id);
        }

        $query = $query->select("generalized_incomes.*",DB::raw("case type when 1 then '医疗组' when 2 then '医技科室' when 3 then '护理组' end as type"))
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");


        return $query;
    }

    public function generateData($date){
        $obj = new DepartmentIncomeRepositoryEloquent();
        $obj->generateData($date, BusinessIncomeType::CLINICAL_DEPARTMENT);
        $obj->generateData($date, BusinessIncomeType::MEDICAL_DEPARTMENT);
        $obj->generateData($date, BusinessIncomeType::NURSE_GROUP);
    }

    public function parseListResult($query) {
        $perPage = request("perPage", 25);
        $sort_str = $this->getSortStr();
        $sort_order = request("sort_order", "desc");
        $allPage = request("allPage", 1);

        $query = $query->orderBy($sort_str, $sort_order);

        if ($allPage == 2) {
            return $query->get();
        } else {
            $query = $query->orderBy('sub_department_id','asc');
            return $query->paginate($perPage);
        }
    }

    public function delData(){
        $date = request("date", date("Y-m"));
        GeneralizedIncome::where("date", $date)->delete();
    }

    public function checkData($date){
        $data = MainIncome::date($date)
            ->whereRaw("left(charging_time, 7) = '$date'")
            ->count();
        if (empty($data)){
            return "尚未导入主要收入数据";
        }

        return "success";
    }
}

<?php

namespace App\Repositories;

use App\Models\DepartmentManagementCoefficient;

class DepartmentManagementCoefficientRepositoryEloquent extends BaseRepository {
    protected $actionClass = DepartmentManagementCoefficient::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "department:id,name,code",
        ]);
        $query = $query->join('departments', 'department_management_coefficients.department_id', '=', 'departments.id');

        $department_id = request("department_id", null);
        if (!empty($department_id)){
            $query = $query->where("department_id", $department_id);
        }

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("date", $date);
        }

        $type_lv1 = request("type_lv1", null);
        if (!empty($type_lv1)){
            $query = $query->where("department_management_coefficients.type_lv1", $type_lv1);
        }

        $query = $query->selectRaw("department_management_coefficients.*, case when type_lv1 = 11 then '临床科室' when type_lv1 = '12' then '医技科室' 
         when type_lv1 = 13 then '职能科室' end as type_lv1_name");
        return $query;
    }
}

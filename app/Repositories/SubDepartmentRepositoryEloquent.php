<?php

namespace App\Repositories;

use App\Models\SubDepartment;

class SubDepartmentRepositoryEloquent extends BaseRepository {
    protected $actionClass = SubDepartment::class;

    public function getList() {
        $query = parent::getList();

        $query = $query->with("department:id,name,code");

        $query = $query->join('departments', 'sub_departments.parent_id', '=', 'departments.id');

        $department_id = request("department_id", null);

        if (!empty($department_id)) {
            $query = $query->where("parent_id", $department_id);
        }

        $name = request("name", null);

        if (!empty($name)) {
            $query = $query->where("sub_departments.name", "like", "%$name%");
        }

        $code = request("code", null);

        if (!empty($code)) {
            $query = $query->where("sub_departments.code", $code);
        }

        $type_lv1 = request("type_lv1", null);

        if (!empty($type_lv1)) {
            $query = $query->where("type_lv1", $type_lv1);
        }

        $type_lv2 = request("type_lv2", null);

        if (!empty($type_lv2)) {
            $query = $query->where("type_lv2", $type_lv2);
        }

        $type_lv3 = request("type_lv3", null);

        if (!empty($type_lv3)) {
            $query = $query->where("type_lv3", $type_lv3);
        }

        $is_performance = request("is_performance", false);
    
        if (!empty($is_performance)) {
            $query = $query->where("sub_departments.is_performance", $is_performance);
        }
        $query->select('sub_departments.*');
        return $query;
    }

    public function parseListResult($query)
    {
        $perPage = request("perPage", 25);
        $sort_str = $this->getSortStr();
        $sort_order = request("sort_order", "desc");
        $allPage = request("allPage", 1);
        if ($sort_str == 'parent_code'){
            $sort_str = 'departments.code';
        }

        $query = $query->orderBy($sort_str, $sort_order);

        if ($allPage == 2) {
            return $query->get();
        } else {
            return $query->paginate($perPage);
        }
    }
}

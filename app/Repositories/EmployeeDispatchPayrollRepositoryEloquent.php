<?php

namespace App\Repositories;

use App\Models\EmployeeDispatchPayroll;

class EmployeeDispatchPayrollRepositoryEloquent extends BaseRepository {
    protected $actionClass = EmployeeDispatchPayroll::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "subDepartment:id,name,code",
        ]);
        $query = $query->join('sub_departments', 'employee_dispatch_payrolls.sub_departments_id', '=', 'sub_departments.id')
            ->leftjoin('lock_performances', 'employee_dispatch_payrolls.date', '=', 'lock_performances.date');

        $name = request("name", null);
        if(!empty($name)){
            $query = $query->where("employee_dispatch_payrolls.name", "like", "%$name%");
        }

        $sub_departments_id = request("sub_departments_id", null);
        if(!empty($sub_departments_id)){
            $query = $query->where("sub_departments_id", $sub_departments_id);
        }

        $date = request("date", null);
        if(!empty($date)){
            $query = $query->where("employee_dispatch_payrolls.date", $date);
        }
        $query->select('employee_dispatch_payrolls.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        return $query;
    }
}

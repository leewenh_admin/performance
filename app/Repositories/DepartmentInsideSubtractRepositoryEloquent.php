<?php

namespace App\Repositories;

use App\Exceptions\MessageException;
use App\Models\DepartmentInsideSubtract;
use App\Models\DepartmentPerformance;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DepartmentInsideSubtractRepositoryEloquent extends BaseRepository {
    protected $actionClass = DepartmentInsideSubtract::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with('department:id,name,code');

        $query = $query->join('departments', 'department_inside_subtracts.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'department_inside_subtracts.date', '=', 'lock_performances.date');
        $query->select('department_inside_subtracts.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if ($date){
            $query = $query->where("department_inside_subtracts.date", $date);
        }

        $department_id = request("department_id", null);
        if ($department_id){
            $query = $query->where("department_id", $department_id);
        }

        return $query;
    }

    public function insert($data)
    {
        DB::beginTransaction();
        try {
            parent::insert($data);
            $performance = DepartmentPerformance::where('date', $data['date'])->where('department_id', $data['department_id'])->first();
            if ($performance) {
                $d_performance = (new DepartmentPerformanceRepositoryEloquent())
                    ->getdPerformance($performance->total_performances, $performance->people_count, $performance->date, $performance->department_id);
                $distribution_performance = $performance->total_performances - $d_performance;

                DepartmentPerformance::where("id", $performance->id)
                    ->update([
                        "department_performance" => $d_performance,
                        "distribution_performance" => $distribution_performance
                    ]);
            }
            DB::commit();
        } catch (\Exception $exception){
            Log::error($exception->getMessage().'--'.$exception->getFile().'--'.$exception->getLine());
            DB::rollBack();
            throw new MessageException($exception->getMessage());
        }
    }

    public function update($id, $data)
    {
        DB::beginTransaction();
        try {
            parent::update($id, $data);

            $performance = DepartmentPerformance::where('date', $data['date'])->where('department_id', $data['department_id'])->first();
            if ($performance) {
                $d_performance = (new DepartmentPerformanceRepositoryEloquent())
                    ->getdPerformance($performance->total_performances, $performance->people_count, $performance->date, $performance->department_id);
                $distribution_performance = $performance->total_performances - $d_performance;

                DepartmentPerformance::where("id", $performance->id)
                    ->update([
                        "department_performance" => $d_performance,
                        "distribution_performance" => $distribution_performance
                    ]);
            }
            DB::commit();
        } catch (\Exception $exception){
            Log::error($exception->getMessage().'--'.$exception->getFile().'--'.$exception->getLine());
            DB::rollBack();
            throw new MessageException($exception->getMessage());
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $record = DepartmentInsideSubtract::find($id);
            $date = $record->date;
            $department_id = $record->department_id;
            $record->delete();
            $performance = DepartmentPerformance::where('date', $date)->where('department_id', $department_id)->first();
            if ($performance) {
                $d_performance = (new DepartmentPerformanceRepositoryEloquent())
                    ->getdPerformance($performance->total_performances, $performance->people_count, $performance->date, $performance->department_id);
                $distribution_performance = $performance->total_performances - $d_performance;

                DepartmentPerformance::where("id", $performance->id)
                    ->update([
                        "department_performance" => $d_performance,
                        "distribution_performance" => $distribution_performance
                    ]);
            }
            DB::commit();
        } catch (\Exception $exception){
            Log::error($exception->getMessage().'--'.$exception->getFile().'--'.$exception->getLine());
            DB::rollBack();
            throw new MessageException($exception->getMessage());
        }
    }
}

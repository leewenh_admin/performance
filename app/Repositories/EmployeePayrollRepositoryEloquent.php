<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Models\EmployeeAttendance;
use App\Models\EmployeePayroll;

class EmployeePayrollRepositoryEloquent extends BaseRepository {
    protected $actionClass = EmployeePayroll::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "employee" => function ($q) {
                $q->select('id', 'name', 'code');
            },
        ]);
        $query = $query->leftjoin('lock_performances', 'employee_payrolls.date', '=', 'lock_performances.date');


        $employee_id = request("employees_id", null);
        if (!empty($employee_id)){
            $query = $query->where("employees_id", $employee_id);
        }


        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("employee_payrolls.date", $date);
        }

        $code = request("code", null);
        if (!empty($code)){
            $query = $query->whereHas("employee", function ($q)use ($code){
                $q->where('code', $code);
            });
        }

        $query->select('employee_payrolls.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");
        return $query;
    }


    /**
     * 核查基本工资缺失数据
     * @param $data
     * @return mixed
     */
    public function check($data){
        $date = $data['date'];

        $employee = EmployeeAttendance::from('employee_attendances as a')
            ->leftjoin('employee_payrolls as b', function ($join) {
                $join->on('a.employees_id', '=', 'b.employees_id')
                    ->on('a.date', '=', 'b.date');
            })
            ->where('a.date', $date)
            ->whereNull('b.id')
            ->pluck('a.employees_id')
            ->all();

        return Employee::whereIn('id', $employee)
            ->select('id', 'name', 'code')
            ->get();
    }
}

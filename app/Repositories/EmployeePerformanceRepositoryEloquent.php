<?php

namespace App\Repositories;

use App\Models\DepartmentPerformance;
use App\Models\EmployeePerformance;
use App\Models\SpecialPerformance;

class EmployeePerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = EmployeePerformance::class;

    public function getList()
    {
        $query = parent::getList();
        $name = request("name", null);
        if (!empty($name)){
            $query = $query->where("name", "like", "%$name%");
        }

        $code = request("code", null);
        if (!empty($code)){
            $query = $query->where("code", "like", "%$code%");
        }

        $department = request("department", null);
        if (!empty($department)){
            $query = $query->where("department", "like", "%$department%");
        }

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("date", $date);
        }

        return $query;
    }

    public function checkData($date){
        //个人绩效
        $data1 = EmployeePerformance::where("date", $date)
            ->count();
        if (empty($data1)){
            return "尚未导入个人绩效数据";
        }
        //科室绩效
        $data2 = DepartmentPerformance::where("date", $date)
            ->count();
        if (empty($data2)){
            return "尚未生成科室绩效数据";
        }
        //专项绩效
        $data3 = SpecialPerformance::where("date", $date)
            ->count();
        if (empty($data3)){
            return "尚未生成专项绩效数据";
        }
        return "success";
    }
}

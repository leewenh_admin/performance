<?php

namespace App\Repositories;

use App\Models\PathologyPerformance;

class PathologyPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = PathologyPerformance::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with("department:id,name,code");

        $query = $query->join('departments', 'pathology_performances.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'pathology_performances.date', '=', 'lock_performances.date');
        $query->select('pathology_performances.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("pathology_performances.date", $date);
        }

        $department_id = request("department_id", null);
        if (!empty($department_id)){
            $query = $query->where("department_id", $department_id);
        }

        return $query;
    }
}

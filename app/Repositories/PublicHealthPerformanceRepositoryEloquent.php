<?php

namespace App\Repositories;

use App\Models\PublicHealthPerformance;

class PublicHealthPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = PublicHealthPerformance::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with("subDepartment:id,name,code");

        $query = $query->join('sub_departments', 'public_health_performances.sub_department_id', '=', 'sub_departments.id')
            ->leftjoin('lock_performances', 'public_health_performances.date', '=', 'lock_performances.date');
        $query->select('public_health_performances.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("public_health_performances.date", $date);
        }

        $sub_department_id = request("sub_department_id", null);
        if (!empty($sub_department_id)){
            $query = $query->where("sub_department_id", $sub_department_id);
        }

        $code = request("code", null);
        if (!empty($code)){
            $query = $query->where("public_health_performances.code", "like", "%$code%");
        }

        $name = request("name", null);
        if (!empty($name)){
            $query = $query->where("public_health_performances.name", "like", "%$name%");
        }

        return $query;
    }
}

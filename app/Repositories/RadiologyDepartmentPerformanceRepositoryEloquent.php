<?php

namespace App\Repositories;

use App\Models\RadiologyDepartmentPerformance;

class RadiologyDepartmentPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = RadiologyDepartmentPerformance::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "department:id,name,code",
        ]);

        $query = $query->join('departments', 'radiology_department_performances.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'radiology_department_performances.date', '=', 'lock_performances.date');
        $query->select('radiology_department_performances.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("radiology_department_performances.date", $date);
        }

        return $query;
    }
}

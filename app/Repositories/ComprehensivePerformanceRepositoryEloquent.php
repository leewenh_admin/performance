<?php

namespace App\Repositories;

use App\Models\ClinicalAttendanceScore;
use App\Models\MedicalAttendanceScore;

/**
 * 综合考核绩效计算
 * Class ComprehensivePerformanceRepositoryEloquent
 * @package App\Repositories
 */
class ComprehensivePerformanceRepositoryEloquent extends BaseRepository {

    /**
     * 临床科室综合绩效
     * @param $date
     * @param $department
     * @return int
     */
    public function getClinicalComprehensivePerformance($date, $department){
        $data = ClinicalAttendanceScore::where("date", $date)
            ->where("departments_id", $department)
            ->first();
        if (empty($data)){
            return 0;
        }else{
            return $data->comprehensive_performance;
        }
    }


    /**
     * 医技科室综合绩效
     * @param $date
     * @param $department
     * @return int
     */
    public function getMedicalComprehensivePerformance($date, $department){
        $data = MedicalAttendanceScore::where("date", $date)
            ->where("departments_id", $department)
            ->first();
        if (empty($data)){
            return 0;
        }else{
            return $data->comprehensive_performance;
        }
    }

    /**
     * 科室综合绩效计算
     * @param $date
     */
    public function performance($date){
        $this->getMedicalPerformance($date);
        $this->getClinicalPerformance($date);
    }


    /**
     * 医技科室考核绩效计算
     * @param $date
     * @return int
     */
    public function getMedicalPerformance($date){
        $datas = MedicalAttendanceScore::where("date", $date)
            ->get();

        if (empty($datas)){
            return 0;
        }
        $targetScore = $this->getTargetScore();
        foreach ($datas as $data){
            $total_score = $data->total_score;

            if(!$total_score){
                $total_score = $data->medical_quality * 0.6 + $data->care_quality*0.05 + $data->medical_ethics_quality*0.05 +
                    $data->health_management * 0.05 + $data->workload_management * 0.15 + $data->scientific_research * 0.1;
            }

            if($total_score >= 98){
                $reward_amount = 100;
            } else if ($total_score >= 92 &&  $total_score < 98){
                $reward_amount = 30;
            } else {
                $reward_amount = 50;
            }



            $performance = ($total_score - $targetScore) * $reward_amount * $data->employees_count;

            MedicalAttendanceScore::where("id", $data->id)->update(["comprehensive_performance" => round($performance, 0)]);
        }


    }

    /**
     * 临床科室考核绩效计算
     * @param $date
     * @return int
     */
    public function getClinicalPerformance($date){
        $datas = ClinicalAttendanceScore::where("date", $date)
            ->get();

        if (empty($datas)){
            return 0;
        }
        $targetScore = $this->getTargetScore();

        foreach ($datas as $data){
            $total_score = $data->total_score;
            if(!$total_score){
                $total_score = $data->medical_quality * 0.55 + $data->care_quality * 0.15 + $data->medical_ethics_quality * 0.05 +
                    $data->health_management * 0.05 + $data->medical_record_quality * 0.1 + $data->scientific_research * 0.1;
            }

            if($total_score >= 98){
                $reward_amount = 100;
            } else if ($total_score >= 92 &&  $total_score < 98){
                $reward_amount = 30;
            } else {
                $reward_amount = 50;
            }
            $performance = ($total_score - $targetScore) * $reward_amount * $data->employees_count;

            ClinicalAttendanceScore::where("id", $data->id)->update(["comprehensive_performance" => round($performance, 0)]);
        }
    }


    /**
     * 目标考核分
     * @return int
     */
    public function getTargetScore(){
        //暂定98
        return 98;
    }

}

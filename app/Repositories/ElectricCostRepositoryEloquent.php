<?php

namespace App\Repositories;

use App\Models\ElectricCost;

class ElectricCostRepositoryEloquent extends BaseRepository {
    protected $actionClass = ElectricCost::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "subDepartment:id,name,code",
        ]);
        $query = $query->join('sub_departments', 'electric_costs.sub_departments_id', '=', 'sub_departments.id')
            ->leftjoin('lock_performances', 'electric_costs.date', '=', 'lock_performances.date');

        $date = request("date", null);
        if(!empty($date)){
            $query = $query->where("electric_costs.date", $date);
        }

        $sub_departments_id = request("sub_departments_id", null);
        if(!empty($sub_departments_id)){
            $query = $query->where("sub_departments_id", $sub_departments_id);
        }
        $query->select('electric_costs.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        return $query;
    }
}

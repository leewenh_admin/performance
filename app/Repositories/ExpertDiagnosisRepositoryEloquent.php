<?php

namespace App\Repositories;

use App\Models\ExpertDiagnosis;

class ExpertDiagnosisRepositoryEloquent extends BaseRepository {
    protected $actionClass = ExpertDiagnosis::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "employee:id,name",
        ]);

        return $query;
    }
}

<?php

namespace App\Repositories;

use App\Enums\DepartmentType;
use App\Enums\IncomeType;
use App\Models\BaseData;
use App\Models\EmployeeAttendance;
use App\Models\MainIncome;
use App\Models\Department;
use App\Models\SubDepartment;
use App\Models\DirectCost;
use Illuminate\Support\Facades\DB;


class CostEffectiveRepositoryEloquent extends BaseRepository {

    /**
     * 查询一级科室下的二级科室
     * @param $department_id
     * @return mixed
     */
    public function getSubDepartments($department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();

        return $department_ids;
    }

    /**
     * 计算医疗服务项目点数
     */
    public function getMedicalPoint($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);

        if (empty($department_ids)) {
            return 0;
        }


        //查询科室当月医疗服务项目收入（执行收入）
        //直接按执行科室统计
        //医疗服务项目不应包含药品费和材料费
        $arrDate = explode('-', $date);
        //住院
        $medical_income = MainIncome::date($date)->whereIn("execute_department_id", $department_ids)
            ->where("income_type", IncomeType::INPATIENT)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where('item_cate_code', '!=', 2)//西药费
            ->where('item_cate_code', '!=', 3)//中成药
            ->where('item_cate_code', '!=', 4)// 中草药
            ->where('item_cate_code', '!=', 82)// 中草药
            ->where('item_cate_code', '!=', 15)// 手术材料费
            ->where('item_cate_code', '!=', 17)// 血费
            ->where('item_cate_code', '!=', 64)// 材料费
            ->sum("amount");
        if ($department_id == 217) {
            $medical_income += MainIncome::date($date)
                ->whereIn('prescribe_department_id',$department_ids)
                ->where("execute_department_id", 101)
                ->where("income_type", IncomeType::INPATIENT)
                ->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->where('item_cate_code', '!=', 2)//西药费
                ->where('item_cate_code', '!=', 3)//中成药
                ->where('item_cate_code', '!=', 4)// 中草药
                ->where('item_cate_code', '!=', 82)// 中草药
                ->where('item_cate_code', '!=', 15)// 手术材料费
                ->where('item_cate_code', '!=', 17)// 血费
                ->where('item_cate_code', '!=', 64)// 材料费
                ->where('item_cate_code', '!=', 14)//
                ->sum("amount");
        }

        //门诊执行收入
        //科室内考勤的医生
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->get()
            ->pluck("actual_attendance", "employees_id")
            ->all();
        $doctor_ids = array_keys($doctors);
        $medical_income += MainIncome::date($date)
            ->whereIn("execute_department_id", $department_ids)
            ->where("income_type", IncomeType::OUTPATIENT)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where('item_cate_code', '!=', 2)//西药费
            ->where('item_cate_code', '!=', 3)//中成药
            ->where('item_cate_code', '!=', 4)// 中草药
            ->where('item_cate_code', '!=', 82)// 中草药
            ->where('item_cate_code', '!=', 15)// 手术材料费
            ->where('item_cate_code', '!=', 17)// 血费
            ->where('item_cate_code', '!=', 64)// 材料费
            ->sum("amount");

        $medical_income += MainIncome::date($date)
            ->whereIn('prescribe_department_id',[166,484])
            ->whereIn('execute_department_id',[166,484])
            ->whereIn("prescribe_doctor_id", $doctor_ids)
            ->where("income_type", IncomeType::OUTPATIENT)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where('item_cate_code', '!=', 2)//西药费
            ->where('item_cate_code', '!=', 3)//中成药
            ->where('item_cate_code', '!=', 4)// 中草药
            ->where('item_cate_code', '!=', 82)// 中草药
            ->where('item_cate_code', '!=', 15)// 手术材料费
            ->where('item_cate_code', '!=', 17)// 血费
            ->where('item_cate_code', '!=', 64)// 材料费
            ->sum("amount");
        return round($medical_income / 10, 2); //点数计算为医疗服务项目收入/10

    }

    /**
     * 计算门诊医疗服务项目点数
     */
    public function getOutpatientMedicalPoint($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);

        if (empty($department_ids)) {
            return 0;
        }


        //查询科室当月医疗服务项目收入（执行收入）
        //直接按执行科室统计
        //医疗服务项目不应包含药品费和材料费
        $arrDate = explode('-', $date);
        //门诊执行收入
        //科室内考勤的医生
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->get()
            ->pluck("actual_attendance", "employees_id")
            ->all();
        $doctor_ids = array_keys($doctors);
        $medical_income = MainIncome::date($date)
            ->whereIn("execute_department_id", $department_ids)
            ->where("income_type", IncomeType::OUTPATIENT)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where('item_cate_code', '!=', 2)//西药费
            ->where('item_cate_code', '!=', 3)//中成药
            ->where('item_cate_code', '!=', 4)// 中草药
            ->where('item_cate_code', '!=', 82)// 中草药
            ->where('item_cate_code', '!=', 15)// 手术材料费
            ->where('item_cate_code', '!=', 17)// 血费
            ->where('item_cate_code', '!=', 64)// 材料费
            ->sum("amount");

        $medical_income += MainIncome::date($date)
            ->whereIn('prescribe_department_id',[166,484])
            ->whereIn('execute_department_id',[166,484])
            ->whereIn("prescribe_doctor_id", $doctor_ids)
            ->where("income_type", IncomeType::OUTPATIENT)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where('item_cate_code', '!=', 2)//西药费
            ->where('item_cate_code', '!=', 3)//中成药
            ->where('item_cate_code', '!=', 4)// 中草药
            ->where('item_cate_code', '!=', 82)// 中草药
            ->where('item_cate_code', '!=', 15)// 手术材料费
            ->where('item_cate_code', '!=', 17)// 血费
            ->where('item_cate_code', '!=', 64)// 材料费
            ->sum("amount");
        return round($medical_income / 10, 2); //点数计算为医疗服务项目收入/10

    }

    /**
     * 计算住院医疗服务项目点数
     */
    public function getInHospitalMedicalPoint($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);

        if (empty($department_ids)) {
            return 0;
        }


        //查询科室当月医疗服务项目收入（执行收入）
        //直接按执行科室统计
        //医疗服务项目不应包含药品费和材料费
        $arrDate = explode('-', $date);
        //住院
        $medical_income = MainIncome::date($date)->whereIn("execute_department_id", $department_ids)
            ->where("income_type", IncomeType::INPATIENT)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where('item_cate_code', '!=', 2)//西药费
            ->where('item_cate_code', '!=', 3)//中成药
            ->where('item_cate_code', '!=', 4)// 中草药
            ->where('item_cate_code', '!=', 82)// 中草药
            ->where('item_cate_code', '!=', 15)// 手术材料费
            ->where('item_cate_code', '!=', 17)// 血费
            ->where('item_cate_code', '!=', 64)// 材料费
            ->sum("amount");
        if ($department_id == 217) {
            $medical_income += MainIncome::date($date)
                ->whereIn('prescribe_department_id',$department_ids)
                ->where("execute_department_id", 101)
                ->where("income_type", IncomeType::INPATIENT)
                ->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->where('item_cate_code', '!=', 2)//西药费
                ->where('item_cate_code', '!=', 3)//中成药
                ->where('item_cate_code', '!=', 4)// 中草药
                ->where('item_cate_code', '!=', 82)// 中草药
                ->where('item_cate_code', '!=', 15)// 手术材料费
                ->where('item_cate_code', '!=', 17)// 血费
                ->where('item_cate_code', '!=', 64)// 材料费
                ->where('item_cate_code', '!=', 14)//
                ->sum("amount");
        }
        return round($medical_income / 10, 2); //点数计算为医疗服务项目收入/10

    }


    /**
     * 计算手术项目点数
     */
    public function getSurgeryPoint($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);

        if (empty($department_ids)) {
            return 0;
        }

        //查询科室当月手术项目收入
        $medica_income = $this->getSurgeryInCome($date, $department_id);

        return round($medica_income / 10, 2); //点数计算为收入/10

    }


    /**
     * 计算辅检项目点数
     */
    public function getAuxiliaryPoint($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);
        if (empty($department_ids)) {
            return 0;
        }
        $subDepartments = $this->getSubDepartments(246);//麻醉科
        $subDepartments1 = $this->getSubDepartments(218);//介入科
        $subDepartments2 = $this->getSubDepartments(225);//肿瘤科

        $departments = array_merge($subDepartments, $subDepartments1);
        //科室内考勤的医生
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->get()
            ->pluck("actual_attendance", "employees_id")
            ->all();
        $doctor_ids = array_keys($doctors);

        //查询科室当辅检项目收入
        //判断条件为开单科室和执行科室不一致，并且不是手术项目
        $arrDate = explode('-', $date);

        //住院类型按照开单科室统计
        $medical_income = MainIncome::date($date)->whereIn(
            "prescribe_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereNotIn("execute_department_id", $department_ids)
            ->where("income_type", IncomeType::INPATIENT)//住院类型
            ->where('item_cate_code', '!=', 2)//西药费
            ->where('item_cate_code', '!=', 3)//中成药
            ->where('item_cate_code', '!=', 4)// 中草药
            ->where('item_cate_code', '!=', 82)// 中草药
            ->where('item_cate_code', '!=', 15)// 手术材料费
            ->where('item_cate_code', '!=', 17)// 血费
            ->where('item_cate_code', '!=', 64)// 材料费
            ->where(function ($query) use ($subDepartments1) {
                $query->whereNotIn("execute_department_id", $subDepartments1);
            })
            ->sum("amount");
        $medical_income -= MainIncome::date($date)->whereIn(
            "prescribe_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereIn("execute_department_id", $subDepartments)
            ->where("income_type", IncomeType::INPATIENT)//住院类型
            ->where('item_cate_code', '=', 14)
            ->sum("amount");

        //门诊类型按照开单医生统计
        $medical_income += MainIncome::date($date)
            ->whereIn("prescribe_doctor_id", $doctor_ids)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereNotIn("execute_department_id", $department_ids)
            ->whereNotIn("execute_department_id", [166,484])
            ->where("income_type", IncomeType::OUTPATIENT)//门诊类型
            ->where('item_cate_code', '!=', 2)//西药费
            ->where('item_cate_code', '!=', 3)//中成药
            ->where('item_cate_code', '!=', 4)// 中草药
            ->where('item_cate_code', '!=', 82)// 中草药
            ->where('item_cate_code', '!=', 15)// 手术材料费
            ->where('item_cate_code', '!=', 17)// 血费
            ->where('item_cate_code', '!=', 64)// 材料费
            ->sum("amount");
        return round($medical_income / 10, 2); //点数计算为收入/10

    }

    /**
     * 计算门诊辅检项目点数
     */
    public function getOutpatientAuxiliaryPoint($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);
        if (empty($department_ids)) {
            return 0;
        }

        //科室内考勤的医生
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->get()
            ->pluck("actual_attendance", "employees_id")
            ->all();
        $doctor_ids = array_keys($doctors);

        //查询科室当辅检项目收入
        //判断条件为开单科室和执行科室不一致，并且不是手术项目
        $arrDate = explode('-', $date);
        //门诊类型按照开单医生统计
        $medical_income = MainIncome::date($date)
            ->whereIn("prescribe_doctor_id", $doctor_ids)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereNotIn("execute_department_id", $department_ids)
            ->whereNotIn("execute_department_id", [166,484])
            ->where("income_type", IncomeType::OUTPATIENT)//门诊类型
            ->where('item_cate_code', '!=', 2)//西药费
            ->where('item_cate_code', '!=', 3)//中成药
            ->where('item_cate_code', '!=', 4)// 中草药
            ->where('item_cate_code', '!=', 82)// 中草药
            ->where('item_cate_code', '!=', 15)// 手术材料费
            ->where('item_cate_code', '!=', 17)// 血费
            ->where('item_cate_code', '!=', 64)// 材料费
            ->sum("amount");
        return round($medical_income / 10, 2); //点数计算为收入/10

    }

    /**
     * 计算住院辅检项目点数
     */
    public function getInHospitalAuxiliaryPoint($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);
        if (empty($department_ids)) {
            return 0;
        }
        $subDepartments = $this->getSubDepartments(246);//麻醉科
        $subDepartments1 = $this->getSubDepartments(218);//介入科



        //查询科室当辅检项目收入
        //判断条件为开单科室和执行科室不一致，并且不是手术项目
        $arrDate = explode('-', $date);

        //住院类型按照开单科室统计
        $medical_income = MainIncome::date($date)->whereIn(
            "prescribe_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereNotIn("execute_department_id", $department_ids)
            ->where("income_type", IncomeType::INPATIENT)//住院类型
            ->where('item_cate_code', '!=', 2)//西药费
            ->where('item_cate_code', '!=', 3)//中成药
            ->where('item_cate_code', '!=', 4)// 中草药
            ->where('item_cate_code', '!=', 82)// 中草药
            ->where('item_cate_code', '!=', 15)// 手术材料费
            ->where('item_cate_code', '!=', 17)// 血费
            ->where('item_cate_code', '!=', 64)// 材料费
            ->where(function ($query) use ($subDepartments1) {
                $query->whereNotIn("execute_department_id", $subDepartments1);
            })
            ->sum("amount");
        $medical_income -= MainIncome::date($date)->whereIn(
            "prescribe_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->whereIn("execute_department_id", $subDepartments)
            ->where("income_type", IncomeType::INPATIENT)//住院类型
            ->where('item_cate_code', '=', 14)
            ->sum("amount");

        return round($medical_income / 10, 2); //点数计算为收入/10

    }

    /**
     * 获取固定成本
     * @param $date
     * @param $department_id
     * @return mixed
     */
    public function getFixedCostsPoint($date, $department_id) {
        $fixedCosts = DirectCost::where("departments_id", $department_id)
            ->where("date", $date)
            ->selectRaw("(base_salary + social_security + loabor_dispatch + canteen + house_fee
                    + device_depreciation + property_management + device_maintenance + sewage_disposal + other_maintain) as total")
            ->value('total');

        return round($fixedCosts / 10, 2);
    }


    /**
     * 获取绩效单价
     * @param $date
     * @param $department_id
     * @return int
     */
    public function getPerformancePrice($date, $department_id) {
        // 绩效单价暂定3元
        return 1;
    }


    /**
     * 获取医疗服务收入占比系数
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getMedicalPercent($date, $department_id) {
        //暂时设定为1
        return 1;
        //科内收入(有效)
        $departmentIncome = $this->getDepartInCome($date, $department_id);
        //材料收入
//        $materialInCome = $this->getMaterialInCome($date, $department_id);
        //药品收入
//        $drugInCome = $this->getDrugInCome($date, $department_id);
        //手术收入
        $surgeryInCome = $this->getSurgeryInCome($date, $department_id);
        //总收入
        $totalInCome = $this->getTotalInCome($date, $department_id);

        //获取科室的医疗服务收入目标值
        $medical_target = Department::where("id", $department_id)
            ->value('service_income_target_value');
        if ($totalInCome == 0 || $medical_target == 0) {
            return 0;
        }
        $medical_target = $medical_target / 100;
        //医疗服务收入占比={科内收入（执行科室）-材料收入（执行科室）-药品收入（执行科室） + 手术收入}/总收入（开单科室）
        //医疗服务收入占比={科内有效收入（执行有效科室 + 手术收入}/总收入（开单科室）
        $medical_income = round(($departmentIncome + $surgeryInCome) / $totalInCome ,4);

        if ($medical_income < $medical_target * 0.95) {
            $medicalPercent = 1 - ($medical_target - $medical_income) / $medical_target;
        } elseif ($medical_income >= $medical_target * 0.95 && $medical_income < $medical_target) {
            $medicalPercent = 1;
        } else {
            $medicalPercent = 1 + ($medical_income - $medical_target) / $medical_target;
        }

        return round($medicalPercent, 4);
    }

    /**
     * 科室材料收入
     * @param $date
     * @param $department_id
     * @return int|string
     */
    public function getMaterialInCome($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);

        if (empty($department_ids)) {
            return 0;
        }

        $arrDate = explode('-', $date);
        $material_income = MainIncome::date($date)->whereIn(
            "execute_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::INPATIENT)
            ->whereIn("item_cate_code", [64, 17, 15]) //材料费, 血费, 手术材料费
            ->sum("amount");

        return $material_income;
    }

    /**
     * 科室药品收入
     * @param $date
     * @param $department_id
     * @return int|string
     */
    public function getDrugInCome($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);

        if (empty($department_ids)) {
            return 0;
        }

        $arrDate = explode('-', $date);
        $drug_income = MainIncome::date($date)->whereIn(
            "execute_department_id", $department_ids
        )->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::INPATIENT)
            ->whereIn("item_cate_code", [2, 3, 4, 82])//西药费, 中成药, 中草药
            ->sum("amount");

        return $drug_income;

    }

    /**
     * 手术收入
     * @param $date
     * @param $department_id
     * @return int|string
     */
    public function getSurgeryInCome($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);
        if (empty($department_ids)) {
            return 0;
        }

        $subDepartments1 = $this->getSubDepartments(218);
        $subDepartments2 = $this->getSubDepartments(246);


        $arrDate = explode('-', $date);
        $surgery_income = MainIncome::date($date)->whereIn("prescribe_department_id", $department_ids)
            ->whereIn("execute_department_id", $subDepartments2)//麻醉科
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::INPATIENT)
            ->where("item_cate_code", 14) //手术费
            ->sum("amount");

        //心血管内科和神经外科·烧伤科·血管外科 累加介入科的手术费
        if ($department_id == 233) {
            //科室内考勤的医生
            $doctors = EmployeeAttendance::query()->where("date", $date)
                ->where("departments_id", $department_id)
                ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                    $join->on('employee_attendances.code', 'b.code')
                        ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
                })
                ->get()
                ->pluck("actual_attendance", "employees_id")
                ->all();
            $doctor_ids = array_keys($doctors);
//            $surgery_income += MainIncome::date($date)->whereIn("prescribe_department_id", $department_ids)
            $surgery_income += MainIncome::date($date)->whereIn("prescribe_doctor_id", $doctor_ids)
                ->whereIn("execute_department_id", $subDepartments1)//执行科室为介入科
                ->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->where("income_type", IncomeType::INPATIENT)
                ->where("item_cate_code", 14)//手术费
                ->sum("amount");
        }elseif ($department_id == 217){
            //科室内考勤的医生
            $doctors = EmployeeAttendance::query()->where("date", $date)
                ->where("departments_id", $department_id)
                ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                    $join->on('employee_attendances.code', 'b.code')
                        ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
                })
                ->get()
                ->pluck("actual_attendance", "employees_id")
                ->all();
            $doctor_ids = array_keys($doctors);
            $surgery_income += MainIncome::date($date)->whereIn("prescribe_doctor_id", $doctor_ids)
                ->whereIn("execute_department_id", $subDepartments1)//执行科室为介入科
                ->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->where("income_type", IncomeType::INPATIENT)
                ->where("item_cate_code", 14)//手术费
                ->sum("amount");
        }

        return $surgery_income;
    }

    /**
     * 总收入
     * @param $date
     * @param $department_id
     * @return int
     */
    public function getTotalInCome($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);
        if (empty($department_ids)) {
            return 0;
        }
        $arrDate = explode('-', $date);
        $total_income = MainIncome::date($date)->whereIn("prescribe_department_id", $department_ids)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("income_type", IncomeType::INPATIENT)
            ->sum("amount");

        return $total_income;
    }

    /**
     * 科内收入
     * @param $date
     * @param $department_id
     * @return int
     */
    public function getDepartInCome($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);
        if (empty($department_ids)) {
            return 0;
        }
        $arrDate = explode('-', $date);
        $income = MainIncome::date($date)->whereIn("execute_department_id", $department_ids)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where('item_cate_code', '!=', 2)
            ->where('item_cate_code', '!=', 3)
            ->where('item_cate_code', '!=', 4)
            ->where('item_cate_code', '!=', 82)
            ->where('item_cate_code', '!=', 15)
            ->where('item_cate_code', '!=', 17)
            ->where('item_cate_code', '!=', 64)
            ->where("income_type", IncomeType::INPATIENT)
            ->sum("amount");

        return $income;
    }


    /**
     * 获取变动成本控制因素
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getVariableCostControl($date, $department_id) {
        // 获取一级科室的二级科室ID列表
        $department_ids = $this->getSubDepartments($department_id);

        if (empty($department_ids)) {
            return 0;
        }


        $arrDate = explode('-', $date);
        //科室本月收入
//        $total_income = MainIncome::date($date)->whereIn(
//            "execute_department_id", $department_ids
//        )->whereYear("charging_time", $arrDate[0])
//            ->whereMonth("charging_time", $arrDate[1])
//            ->get()->sum("amount");
        //科室本月有效收入
        $total_income = $this->getDepartInCome($date, $department_id);

        //获取科室的相关数据
        //先查询基值表，如果没有数据就从department表查询2019年数据
        $year = date("Y",strtotime($date));
        $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
        if ($year == "2021"){//如果是2021年就查询2019年的数据
            $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
        }
        $departMentData = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();
        if (empty($departMentData)){
            $departMentData = Department::where("id", $department_id)->select("current_period_controllable_variable_cost", "base_department_income", "base_period_controllable_variable_cost", "variable_cost_control_deduction_rate")->first()->toArray();
        }

        if ($departMentData['base_department_income'] == 0) {
            return 0;
        }
        $current_period_controllable_variable_cost = DirectCost::query()
            ->where('date', $date)
            ->where('departments_id', $department_id)
            ->select('sanitary_material', 'affairs_material', 'disinfect', 'laundry', 'utilities')->first()->toArray();
        $current_period_controllable_variable_cost = array_sum($current_period_controllable_variable_cost);

        if ($current_period_controllable_variable_cost == 0) {
            return 0;
        }
        BaseData::updateOrCreate(
            [
                "date" => $date ,
                "department_id" => $department_id
            ],
            [
                "base_department_income" => $total_income,
                "base_period_controllable_variable_cost" => $current_period_controllable_variable_cost
            ]
        );

        //计算变动成本控制
        if ($total_income == 0) {
            $currentRate = 0;
        }else {
            $currentRate = $current_period_controllable_variable_cost / $total_income;
        }
        $variableCostControl = ($currentRate - ($departMentData['base_period_controllable_variable_cost'] * 12) / ($departMentData['base_department_income'] * 12)) * $current_period_controllable_variable_cost * $departMentData['variable_cost_control_deduction_rate'];
        //变动成本控制只罚不奖
        if ($variableCostControl > 0) return 0;
        return round($variableCostControl, 2);
    }


    /**
     * 计算成本效益绩效
     * @param $date
     * @param $department_id
     * @param bool $isMedical
     * @return false|float
     */
    public function getCostEffective($date, $department_id, $isMedical = false) {
//        $department = Department::where("id", $department_id)->first();
//        if ($department->subDepartment->type_lv3 == DepartmentType::SURGICAL){
//            $percent = 1;
//        } else{
//            $percent = 0.5;
//        }
        $percent = 1;
        //医疗服务项目点数
        $medicalPoint = $this->getMedicalPoint($date, $department_id);
        //辅检项目点数 （消化内科为0.15）
        $auxiliaryPoint = $this->getAuxiliaryPoint($date, $department_id);
        if ($department_id == 222){
            $auxiliaryPoint = $auxiliaryPoint * 0.15;
        }else {
            $auxiliaryPoint = $auxiliaryPoint * 0.2;
        }
        //手术项目点数
        $surgeryPoint = $this->getSurgeryPoint($date, $department_id) * $percent;
        //固定成本点数
        $fixedCostsPoint = $this->getFixedCostsPoint($date, $department_id);
        //绩效单价
        $performancePrice = $this->getPerformancePrice($date, $department_id);
        //医疗服务收入系数  医技科室成本效益为1
        if ($isMedical) {
            $medicalPercent = 1;
        }else {
            $medicalPercent = $this->getMedicalPercent($date, $department_id);
        }

        //变动成本控制因素
        $variableCostControl = $this->getVariableCostControl($date, $department_id);

        //计算成本效益
        //成本效益绩效=（医疗服务项目点数+辅检项目点数+手术项目点数-固定成本点数）*绩效单价*医疗服务收入系数+变动成本控制因素
        $costEffective = ($medicalPoint + $auxiliaryPoint + $surgeryPoint - $fixedCostsPoint) * $performancePrice * $medicalPercent + $variableCostControl;
        return round($costEffective, 2);
    }


}

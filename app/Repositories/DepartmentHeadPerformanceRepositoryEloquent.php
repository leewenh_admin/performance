<?php

namespace App\Repositories;

use App\Models\DepartmentHeadPerformance;

class DepartmentHeadPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = DepartmentHeadPerformance::class;

    public function getList()
    {
        $query = parent::getList();
        $query = $query->with([
            "department:id,name,code",
            "employee:id,name",
        ]);

        $query = $query->join('departments', 'department_head_performances.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'department_head_performances.date', '=', 'lock_performances.date');
        $query->select('department_head_performances.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("department_head_performances.date", $date);
        }

        $code = request("code", null);
        if (!empty($code)){
            $query = $query->where("department_head_performances.code", $code);
        }

        $department_id = request("department_id", null);
        if (!empty($department_id)){
            $query = $query->where("department_id", $department_id);
        }

        $employee_id = request("employee_id", null);
        if (!empty($employee_id)){
            $query = $query->where("employee_id", $employee_id);
        }
        return $query;
    }
}

<?php

namespace App\Repositories;

use App\Models\BedUsing;

class BedUsingRepositoryEloquent extends BaseRepository {
    protected $actionClass = BedUsing::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "subDepartment:id,name,code",
        ]);
        $query = $query->join('sub_departments', 'bed_usings.sub_departments_id', '=', 'sub_departments.id')
            ->leftjoin('lock_performances', 'bed_usings.date', '=', 'lock_performances.date');

        $sub_departments_id = request("sub_departments_id", null);
        if (!empty($sub_departments_id)){
            $query = $query->where("sub_departments_id", $sub_departments_id);
        }


        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("bed_usings.date", $date);
        }
        $query->select('bed_usings.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        return $query;
    }
}

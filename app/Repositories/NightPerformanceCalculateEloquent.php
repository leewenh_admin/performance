<?php

namespace App\Repositories;

use App\Enums\DepartmentType;
use App\Enums\EmployeePosition;
use App\Enums\EmployeeProfessionalRankLevel;
use App\Models\BaseData;
use App\Models\ClinicalAttendanceScore;
use App\Models\ClinicalLaboratoryPerformance;
use App\Models\Department;
use App\Models\DepartmentHeadPerformance;
use App\Models\DepartmentHeadSubtract;
use App\Models\DepartmentPerformance;
use App\Models\Employee;
use App\Models\EmployeeAttendance;
use App\Models\MainIncome;
use App\Models\MedicalAttendanceScore;
use App\Models\NightPerformance;
use App\Models\OtherPerformance;
use App\Models\PathologyPerformance;
use App\Models\PublicHealthPerformance;
use App\Models\RadiologyDepartmentPerformance;
use App\Models\SpecialPerformance;
use App\Models\SpecialSurveyPerformance;
use App\Models\SurgeryNew;
use App\Models\TalentReserve;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * 夜班绩效计算 || 会诊绩效 || 专家号绩效
 * Class NightPerformanceCalculateEloquent
 * @package App\Repositories
 */
class NightPerformanceCalculateEloquent extends BaseRepository
{

    /**
     * 更新夜班绩效表的总绩效
     * @param $date
     * @param $department_id
     * @return int
     */
    public function generatePerformance($date, $department_id)
    {
        $datas = NightPerformance::where("date", $date)
            ->where("department_id", $department_id)
            ->get()
            ->all();

        if (empty($datas)) {
            return 0;
        }

        foreach ($datas as $data) {
            $performance = $this->getNightPerformance($data);
            NightPerformance::where("id", $data->id)
                ->update(["total_night_performance" => $performance]);
        }
    }

    /**
     * 计算科室的夜班绩效
     * @param $data 夜班绩效表的单条绩效数据
     * @return false|float|int
     */
    public function getNightPerformance($data)
    {
        $total = ($data->before_night_number * $data->before_night_price +
                $data->after_night_number * $data->after_night_price +
                $data->whole_night_number * $data->whole_night_price) *
            $data->month_day - $data->dispatch_amount + $data->float_performance;

        return round($total, 2);
    }


    /**
     * 会诊绩效
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getConsultationPerformance($date, $department_id)
    {
        //当月考勤医生人数
        $doctors = $this->getAttendanceDoctors($date, $department_id);
        $arrDate = explode("-", $date);
        $count = MainIncome::date($date)->whereIn("prescribe_doctor_id", $doctors)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("item_cate_code", 36)//会诊费
            ->where("item_code", 1042)//会诊费
            ->sum("quantity");

        $performance = $count * 10;

        return $performance;
    }

    /**
     * 专家号绩效
     * @param $date
     * @param $department_id
     * @return false|float|int
     */
    public function getVisitPerformance($date, $department_id)
    {
        $arrDate = explode("-", $date);
        $obj = new CostEffectiveRepositoryEloquent();
        $department_ids = $obj->getSubDepartments($department_id);
        if (empty($department_ids)) {
            return 0;
        }
        //专家号不计算院领导
        $specialPeople = (new DepartmentPerformanceRepositoryEloquent())->getSpecialEmployee();

        //科室内考勤的医生
        $doctors = EmployeeAttendance::leftjoin("employees", "employees.id", "=", "employee_attendances.employees_id")
            ->whereIn("sub_departments_id", $department_ids)->where("date", $date)
//            ->whereIn("professional_rank_level", [3, 4])
            ->whereRaw("employee_attendances.actual_attendance = (select max(t.actual_attendance)
            from employee_attendances as t where
            employee_attendances.employees_id = t.employees_id and t.date = '$date') ")
            ->whereNotIn("employee_attendances.employees_id", $specialPeople)
            ->pluck("employees_id")
            ->all();

        if(in_array($date,['2021-04', '2021-05', '2021-06', '2021-08'])){
            $chief_count = 0;
            $assistant_count = 0;

            //专家号数量
            $count = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->whereIn("prescribe_doctor_id", $doctors)
                ->select("serial_no", "quantity", "price")
                ->distinct()
                ->where("item_code", 1002)
                ->where("price", ">=", 10)
                ->get();

            foreach ($count as $value){
                if($value->price < 16 ){  //大于等于10 小于16 副主任
                    $assistant_count += $value->quantity;
                } else { //大于等于16 主任
                    $chief_count += $value->quantity;
                }
            }
        } else{
            //副主任专家号数量
            $assistant_count = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->whereIn("prescribe_doctor_id", $doctors)
                ->select("serial_no", "quantity", "price")
                ->distinct()
                ->where("item_code", 1002)
                ->whereIn("register_cate", [3,6])
                ->get()
                ->sum('quantity');

            //主任专家号数量
            $chief_count = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
                ->whereMonth("charging_time", $arrDate[1])
                ->whereIn("prescribe_doctor_id", $doctors)
                ->select("serial_no", "quantity", "price")
                ->distinct()
                ->where("item_code", 1002)
                ->whereIn("register_cate", [4,7])
                ->get()
                ->sum('quantity');
        }

        $performance = $chief_count * 9.6 + $assistant_count * 6 ;

        return round($performance, 2);


    }

    /**
     * 查询科室的考勤医生
     * @param $date
     * @param $department_id
     * @return int
     */
    public function getAttendanceDoctors($date, $department_id)
    {
//        $obj = new CostEffectiveRepositoryEloquent();
//        $department_ids = $obj->getSubDepartments($department_id);
//        if (empty($department_ids)) {
//            return 0;
//        }

        //科室内考勤的医生
        $doctors = EmployeeAttendance::query()->where("date", $date)
            ->where("departments_id", $department_id)
            ->join(DB::raw("(select code ,max(actual_attendance) as actual_attendance from 
            employee_attendances  where date = '$date' GROUP BY code) as b"), function ($join){
                $join->on('employee_attendances.code', 'b.code')
                    ->on('employee_attendances.actual_attendance', 'b.actual_attendance');
            })
            ->get()
            ->pluck("employees_id")
            ->all();

        return $doctors;
    }

    /**
     * 人才储备绩效，通过科室发放部分
     */
    public function getTalentReserveByDepartment($date, $department_id) {
//        $employees = $this->getAttendanceDoctors($date, $department_id);

        return TalentReserve::where("date", $date)
            ->where("department_id", $department_id)
            ->sum("actual_sent");

    }

    /**
     * 人才储备绩效，通过个人发放部分
     */
    public function getTalentReserveByEmployee($date, $employee_id) {
        $records = TalentReserve::where("date", $date)
            ->where("employees_id", $employee_id)
            ->first();

        if (empty($records)) {
            return 0;
        }

        return $records->actual_sent;
    }


    /**
     * 出院人次增幅
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getLeaveCountIncrease($date, $department_id){
        //出院人数
        $obj = new WorkLoadRepositoryEloquent();
        $department_ids = $obj->getOutPatientDepartments($department_id);
        $leaverCount = $obj->getOutHospitalCount($date, $department_ids);
        BaseData::updateOrCreate(
            [
                "date" => $date ,
                "department_id" => $department_id
            ],
            [
                "base_out_hospital" => $leaverCount,
            ]
        );
        //基期出院人数
        //先查询基值表，如果没有数据就从department表查询2019年数据
        $year = date("Y",strtotime($date));
        $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
        if ($year == "2021"){//如果是2021年就查询2019年的数据
            $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
        }
        $baseData = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();
        if (empty($baseData)){
            $base_out_hospital = Department::where("id", $department_id)->value("base_out_hospital");
        }else{
            $base_out_hospital = $baseData->base_out_hospital;
        }
        if($base_out_hospital == 0){
            return 0;
        }

        //出院人次增幅 = 实际出院人次 / 基期出院人数
        $increase = $leaverCount / $base_out_hospital;

        return $increase;
    }


    /**
     * 实际占用总床日数增幅
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getUseBedIncrease($date, $department_id){
        //实际占床日
        $obj = new WorkLoadRepositoryEloquent();
        $department_ids = $obj->getOutPatientDepartments($department_id);
        $departBedDayAll = $obj->getBedCount($date,$department_ids);
        BaseData::updateOrCreate(
            [
                "date" => $date ,
                "department_id" => $department_id
            ],
            [
                "base_use_bed_day" => $departBedDayAll,
            ]
        );

        //基期占床日
        //先查询基值表，如果没有数据就从department表查询2019年数据
        $year = date("Y",strtotime($date));
        $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
        if ($year == "2021"){//如果是2021年就查询2019年的数据
            $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
        }
        $baseData = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();
        if (empty($baseData)){
            $base_use_bed_day = Department::where("id", $department_id)->value("base_use_bed_day");
        }else{
            $base_use_bed_day = $baseData->base_use_bed_day;
        }
        if($base_use_bed_day == 0){
            return 0;
        }
        //实际占用总床日数增幅 =  实际占用总床日数增幅/基期占用总床日数
        $increase = $departBedDayAll / $base_use_bed_day;

        return $increase;
    }

    /**
     * 有效收入增幅
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getIncomeIncrease($date, $department_id){
        //科室有效收入
        $obj = new NewTechnologyItemRepositoryEloquent();
        $income = $obj->getDepartmentIncome($date, $department_id);
        BaseData::updateOrCreate(
            [
                "date" => $date ,
                "department_id" => $department_id
            ],
            [
                "base_department_income" => $income,
            ]
        );
        //基期科室有效收入
        //先查询基值表，如果没有数据就从department表查询2019年数据
        $year = date("Y",strtotime($date));
        $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
        if ($year == "2021"){//如果是2021年就查询2019年的数据
            $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
        }
        $baseData = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();
        if (empty($baseData)){
            $base_department_income = Department::where("id", $department_id)->value("base_department_income");
        }else{
            $base_department_income = $baseData->base_department_income;
        }
        if($base_department_income == 0){
            return 0;
        }
        //占用总床日数增幅 =  实际占用总床日数/基期占用总床日数
        $increase = $income / $base_department_income;

        return $increase;
    }

    /**
     * 手术台次增幅
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getSurgeryCountIncrease($date, $department_id){
        $obj = new WorkLoadRepositoryEloquent();
        $department_ids = $obj->getOutPatientDepartments($department_id);
        $surgeryCount = SurgeryNew::whereIn("sub_departments_id", $department_ids)
            ->where("date", $date)
            ->count();
        BaseData::updateOrCreate(
            [
                "date" => $date ,
                "department_id" => $department_id
            ],
            [
                "base_surgery_count" => $surgeryCount,
            ]
        );

        //先查询基值表，如果没有数据就从department表查询2019年数据
        $year = date("Y",strtotime($date));
        $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
        if ($year == "2021"){//如果是2021年就查询2019年的数据
            $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
        }
        $baseData = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();
        if (empty($baseData)){
            $base_surgery_count = Department::where("id", $department_id)->value("base_surgery_count");
        }else{
            $base_surgery_count = $baseData->base_surgery_count;
        }
        if($base_surgery_count == 0){
            return 0;
        }
        //手术台次增幅 =  实际手术台次/基期手术台次
        $increase = $surgeryCount / $base_surgery_count;

        return $increase;
    }

    /**
     * 三、四级手术台次增幅
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getSurgeryRateIncrease($date, $department_id){
        //三四级手术场次
        $obj = new WorkLoadRepositoryEloquent();
        $department_ids = $obj->getOutPatientDepartments($department_id);
        $surgeryCount = SurgeryNew::whereIn("sub_departments_id", $department_ids)
            ->where("date", $date)
            ->whereRaw("(find_in_set('3',level) or find_in_set('4',level))")
            ->count();


        BaseData::updateOrCreate(
            [
                "date" => $date ,
                "department_id" => $department_id
            ],
            [
                "base_special_surgery_count" => $surgeryCount,
            ]
        );

        //先查询基值表，如果没有数据就从department表查询2019年数据
        $year = date("Y",strtotime($date));
        $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
        if ($year == "2021"){//如果是2021年就查询2019年的数据
            $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
        }
        $baseData = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();
        if (empty($baseData)){
            $base_special_surgery_count = Department::where("id", $department_id)->value("base_special_surgery_count");
        }else{
            $base_special_surgery_count = $baseData->base_special_surgery_count;
        }
        if($base_special_surgery_count == 0){
            return 0;
        }

        //手术台次增幅 =  实际手术台次/基期手术台次
        $increase = $surgeryCount / $base_special_surgery_count;

        return $increase;
    }

    /**
     * 非手术科室增幅
     * @param $date
     * @param $department_id
     * @return false|float
     */
    public function getNotSurgeryDepartIncrease($date, $department_id){
        //工作量增幅=出院人次增幅*30%+实际占用总床日数增幅*30%+有效收入增幅*40%
        //出院人次增幅
        $leaveCountIncrease = $this->getLeaveCountIncrease($date, $department_id);
        //实际占用总床日数增幅
        $useBedIncrease = $this->getUseBedIncrease($date, $department_id);
        //有效收入增幅
        $incomeIncrease = $this->getIncomeIncrease($date, $department_id);

        $increase = $leaveCountIncrease * 0.3 + $useBedIncrease * 0.3 + $incomeIncrease * 0.4;

        return round($increase, 2);
    }

    /**
     * 手术科室增幅
     * @param $date
     * @param $department_id
     * @return false|float
     */
    public function getSurgeryDepartIncrease($date, $department_id){
        //手术科室：工作量增幅=出院人次增幅*30%+手术台次增幅*30%+实际占用总床日数增幅*10%+有效收入增幅*20%+三、四级手术率增幅*10%
        //出院人次增幅
        $leaveCountIncrease = $this->getLeaveCountIncrease($date, $department_id);
        //手术台次增幅
        $surgeryCountIncrease = $this->getSurgeryCountIncrease($date, $department_id);
        //实际占用总床日数增幅
        $useBedIncrease = $this->getUseBedIncrease($date, $department_id);
        //有效收入增幅
        $incomeIncrease = $this->getIncomeIncrease($date, $department_id);
        //三、四级手术台次增幅
        $surgeryRateIncrease = $this->getSurgeryRateIncrease($date, $department_id);

        $increase = $leaveCountIncrease * 0.3 + $surgeryCountIncrease * 0.3 + $useBedIncrease * 0.1 +
            $incomeIncrease * 0.2 + $surgeryRateIncrease * 0.1;

        return round($increase, 2);

    }

    /**
     * 业务恢复绩效
     * @param $date
     * @param $department_id
     * @return false|float
     */
    public function getBusinessRecoveryPerformance($date, $department_id){
        $department = Department::where("id", $department_id)->first();
        $sub_department = $department->subDepartments()->first();
        $increase = 0;
        if($sub_department->type_lv1 != DepartmentType::CLINICAL){
            return 0;
        }
        if ($sub_department->type_lv3 == DepartmentType::MEDICINE){//非手术科室
            $increase = $this->getNotSurgeryDepartIncrease($date, $department_id);
        } elseif ($sub_department->type_lv3 == DepartmentType::SURGICAL){//手术科室
            $increase = $this->getSurgeryDepartIncrease($date, $department_id);
        }

        if ($increase == 0){
            return 0;
        }
        //区间月奖励额

        $reward = 0;
        switch ($increase) {
            case $increase >= 0.9 && $increase < 1:
                $reward = 500;
                break;
            case $increase >= 1 && $increase < 1.1:
                $reward = 1100;
                break;
            case $increase >= 1.1 && $increase < 1.2:
                $reward = 1900;
                break;
            case $increase >= 1.2 && $increase < 1.3:
                $reward = 3000;
                break;
            case $increase >= 1.3 && $increase < 1.4:
                $reward = 4500;
                break;
            case $increase >= 1.4 && $increase < 1.5:
                $reward = 6500;
                break;
            case $increase >= 1.5 && $increase < 1.6:
                $reward = 9100;
                break;
            case $increase >= 1.6 && $increase < 1.7:
                $reward = 12400;
                break;
        }

        return $reward;
        //本期奖励额=区间月奖励额*累计月*（实际工作量增幅/区间工作量增幅下限）-累计至上月奖励额
//        $data = SpecialPerformance::where("department_id", $department_id)
//                ->where("date", "<", $date)
//                ->get()
//                ->pluck("business_recovery_performance")
//                ->all();
//        if (empty($data)){
//            $month = 1;
//            $total = 0;
//        } else {
//            $month = count($data) + 1;
//            $total = array_sum($data);
//        }
////        $data = $reward * $month * ($increase / 0.9) - $total;
//        $data = $reward * $month - $total;
//
//        return round($data, 2);
    }


    /**
     * 停车补助
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getParkingSubsidy($date, $department_id){

        //科室内考勤天数总和
        //不包含院长
        $yz = Employee::query()->where('position',1)->first();
        if ($yz){
            $id = $yz->id;
        }else {
            $id = 0;
        }
        $attendanceCoefficient = EmployeeAttendance::where("departments_id", $department_id)
            ->where("date", $date)
            ->where("employees_id",'!=' ,$id)
            ->sum("actual_attendance");

        $days = date("t", strtotime($date));
        $attendanceCoefficient = $attendanceCoefficient / $days;

        $performance = 600 * $attendanceCoefficient ;

        return $performance;
    }

    /**
     * 其他专项绩效
     * @param $date
     * @param $department_id
     * @return mixed
     */
    public function getOtherPerformance($date, $department_id){
        $obj = new WorkLoadRepositoryEloquent();
        $department_ids = $obj->getOutPatientDepartments($department_id);

        $data = OtherPerformance::whereIn("sub_department_id", $department_ids)
            ->where("date", $date)
            ->sum("amount");

        return $data;
    }


    /**
     * 获取科主任管理绩效
     * @param $date
     * @param $department_id
     * @return mixed
     */
    public function getManagementPerformance($date, $department_id){
        //当月天数
        $days = date("t",strtotime($date));
        $department = Department::query()->where('id',$department_id)
            ->with('subDepartments')
            ->first();
        $departmentHeadAward = 0; //科主任管理奖
        $departmentSubHeadAward = 0; //副主任管理奖
        $departmentNurseHeadAward = 0; //护士长管理奖
        $departmentOtherAward = 0; //其他职位科室管理奖
        //质量考核分大于或等于98， 管理绩效 = 岗位管理绩效标准*实际考勤天数占比
        //质量考核分数小于98  管理绩效 = （岗位管理绩效标准 + 岗位管理绩效标准 * （质量考核分数 - 98）/100）） *实际考勤天数占比
        $employees = DepartmentHeadPerformance::leftjoin('employee_attendances', function ($join) {
            $join->on('employee_attendances.employees_id', '=', 'department_head_performances.employee_id')
                ->on('employee_attendances.date', '=', 'department_head_performances.date');
//                ->on('department_head_performances.department_id', '=', 'employee_attendances.departments_id');
        })
            ->where("department_head_performances.department_id", $department->id)
            ->where("department_head_performances.date", $date)
            ->select("employee_attendances.employees_id", "performance_standard", DB::raw("sum(employee_attendances.actual_attendance) as actual_attendance"), "administrative_duties", "medical_percent", "expend_percent", "check_day")
            ->groupBy("employee_attendances.employees_id", "performance_standard",  "administrative_duties", "medical_percent", "expend_percent", "check_day")
            ->get();

        if ($department->subDepartments->first()->type_lv1 == DepartmentType::FUNCTIONAL) { //职能科室
            $score = 100;
            foreach ($employees as $employee) {
                $percent = ($employee->actual_attendance - $employee->check_day) / $days;
                $performance = $employee->performance_standard * $percent;

                if ($employee->administrative_duties == "科主任") {
                    $departmentHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                } elseif ($employee->administrative_duties == "副主任") {
                    $departmentSubHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                } elseif ($employee->administrative_duties == "护士长") {
                    $departmentNurseHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                } else {
                    $departmentOtherAward += $performance + $employee->medical_percent + $employee->expend_percent;
                }
            }
        } else {
            $scoreData = $this->getScore($date, $department->id, $department->subDepartments->first()->type_lv1);
            foreach ($employees as $employee) {
                if (empty($scoreData)){
                    continue;
                }
                if ($employee->administrative_duties == "科主任" || $employee->administrative_duties == "副主任") {
                    $score = $scoreData->medical_quality;
                } elseif ($employee->administrative_duties == "护士长") {
                    $score = $scoreData->care_quality;
                }
                $percent = ($employee->actual_attendance - $employee->check_day) / $days;
                if ($score >= 98) {
                    $performance = $employee->performance_standard * $percent;
                } else {
                    $performance = ($employee->performance_standard + ($employee->performance_standard *
                            ($score - 98) / 100)) * $percent;
                }

                if ($employee->administrative_duties == "科主任") {
                    $departmentHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                } elseif ($employee->administrative_duties == "副主任") {
                    $departmentSubHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                } elseif ($employee->administrative_duties == "护士长") {
                    $departmentNurseHeadAward += $performance + $employee->medical_percent + $employee->expend_percent;
                }

            }
        }

        $data = $departmentHeadAward + $departmentSubHeadAward + $departmentNurseHeadAward + $departmentOtherAward;

        return round($data, 2);

    }

    public function getScore($date, $department, $type){
        $data = [];
        if ($type == DepartmentType::CLINICAL){
            $data = ClinicalAttendanceScore::where("departments_id", $department)
                ->where("date", $date)
                ->first();
        } elseif ($type == DepartmentType::MEDICAL_TECHNICAL){
            $data = MedicalAttendanceScore::where("departments_id", $department)
                ->where("date", $date)
                ->first();
        }

        return $data;
    }


    /**
     * 获取夜班绩效
     * @param $date
     * @param $department_id
     * @return mixed
     */
    public function getNightPerformanceData($date, $department_id){
        $data = NightPerformance::where("date", $date)
            ->where("department_id", $department_id)
            ->sum("total_night_performance");

        return $data;
    }

    /**
     * 医共体绩效
     * @param $date
     * @param $department_id
     * @return mixed
     */
    public function getCommunityPerformance($date, $department_id){
        //医共体影像中心绩效(放射科)
        $data1 = RadiologyDepartmentPerformance::where("date", $date)
            ->where("department_id", $department_id)
            ->value("performance");
        //医共体检验中心绩效(检验科)
        $data2 = ClinicalLaboratoryPerformance::where("date", $date)
            ->where("department_id", $department_id)
            ->value("performance");
        //医共体心电中心绩效(特检科)
        $data3 = SpecialSurveyPerformance::where("date", $date)
            ->where("department_id", $department_id)
            ->value("performance");
        //医共体病理中心绩效(病理科)
        $data4 = PathologyPerformance::where("date", $date)
            ->where("department_id", $department_id)
            ->value("performance");

        return $data1 + $data2 + $data3 + $data4;
    }


    /**
     * 公卫绩效
     * @param $date
     * @param $department_id
     * @return mixed
     */
    public function getPublicPerformance($date, $department_id){
        $obj = new WorkLoadRepositoryEloquent();
        $department_ids = $obj->getOutPatientDepartments($department_id);
        $data = PublicHealthPerformance::whereIn("sub_department_id", $department_ids)
            ->where("date", $date)
            ->sum("amount");

        return $data;
    }

    /**
     * 计算科主任科室绩效
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getDepartmentHeaderPerformance($date, $department_id){
        //如果科室无考勤科主任则不计算科主任科室绩效
        $attendance_header = EmployeeAttendance::leftjoin('employees', 'employee_attendances.employees_id', '=', 'employees.id')
            ->where("employee_attendances.departments_id", $department_id)
            ->where("employee_attendances.date", $date)
            ->where("employees.position", EmployeePosition::DEPARTMENT_HEAD)
            ->first();

        if (!$attendance_header){
            return 0;
        }
        $sub_data = DepartmentHeadSubtract::where('date', $date)->where('department_id', $department_id)->first();

        if ($sub_data) {
            $sub_people = $sub_data->sub_value;
        } else {
            $sub_people = 0;
        }

        $performance = DepartmentPerformance::where('date', $date)->where('department_id', $department_id)->first();
        if (!$performance){
            return 0;
        }
        return round($performance->total_performances / ($performance->people_count + $sub_people) * 1.8
            * ($attendance_header->actual_attendance / date("t", strtotime($date))));

    }


    /**
     * 生成专项绩效
     * @param $date
     */
    public function generateSpecialPerformance($date){
        $obj = new DepartmentPerformanceRepositoryEloquent();
        $departments = Department::where("is_performance", 1)->get();

        foreach ($departments as $department){
            //生成夜班绩效
            $this->generatePerformance($date, $department["id"]);

            if(SpecialPerformance::where('date',$date)->where('department_id', $department["id"])->first()) continue;
            //管理绩效
            $managementPerformance = $this->getManagementPerformance($date, $department->id);
            //夜班绩效
            $nightPerformance = $this->getNightPerformanceData($date, $department->id);
            //会诊绩效
            $consultationPerformance = $this->getConsultationPerformance($date, $department->id);
            //专家号绩效
            $visitPerformance = $this->getVisitPerformance($date, $department->id);
            //医共体绩效
            $communityPerformance = $this->getCommunityPerformance($date, $department->id);
            //公卫绩效
            $publicPerformance = $this->getPublicPerformance($date, $department->id);
            //人才储备绩效
            $talentPerformance = $this->getTalentReserveByDepartment($date, $department->id);
            //业务恢复绩效
            $businessRecoveryPerformance = $this->getBusinessRecoveryPerformance($date, $department->id);
            //职工院外停车补助
            $parkingPerformance = $this->getParkingSubsidy($date, $department->id);
            //其他专项绩效
            $otherPerformance = $this->getOtherPerformance($date, $department->id);
            //考勤人数
            $people_count = $obj->getAttendancePeopleCount($date, $department->id);
            $sub_department = $department->subDepartments()->first();
            $total = round($managementPerformance) + round($nightPerformance) + round($consultationPerformance) + round($visitPerformance) +
                round($communityPerformance) + round($publicPerformance) + round($talentPerformance) + round($businessRecoveryPerformance) +
                round($parkingPerformance) + round($otherPerformance);

            //科主任科室绩效
            $department_header_performance = 0;
            if ($sub_department->type_lv1 == DepartmentType::CLINICAL || $sub_department->type_lv1 == DepartmentType::MEDICAL_TECHNICAL){
                $department_header_performance = $this->getDepartmentHeaderPerformance($date, $department->id);
            }

            $data = [
                'date' => $date,
                'department_id' => $department->id,
                'type' => $sub_department->type_lv1,
                'people_count' => $people_count,
                'management_performance' => round($managementPerformance) ?? 0,
                'night_performance' => round($nightPerformance) ?? 0,
                'consultation_performance' => round($consultationPerformance) ?? 0,
                'visit_performance' => round($visitPerformance) ?? 0,
                'community_performance' => round($communityPerformance) ?? 0,
                'public_health_performance' => round($publicPerformance) ?? 0,
                'talent_performance' => round($talentPerformance) ?? 0,
                'business_recovery_performance' => round($businessRecoveryPerformance) ?? 0,
                'parking_performance' => round($parkingPerformance) ?? 0,
                'other_performance' => round($otherPerformance) ?? 0,
                'total_performance' => round($total) ?? 0,
                'department_header_performance' => round($department_header_performance) ?? 0,
            ];

            SpecialPerformance::create($data);
        }
    }
}

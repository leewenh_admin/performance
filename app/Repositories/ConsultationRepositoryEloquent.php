<?php

namespace App\Repositories;

use App\Models\Consultation;

class ConsultationRepositoryEloquent extends BaseRepository {
    protected $actionClass = Consultation::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "employee:id,name",
        ]);

        return $query;
    }
}

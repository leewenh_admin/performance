<?php

namespace App\Repositories;

use App\Models\Department;

class DepartmentRepositoryEloquent extends BaseRepository {
    protected $actionClass = Department::class;

    public function getList() {
        $query = parent::getList();

        $query = $query->with("subDepartments");

        $name = request("name", null);

        if (!empty($name)) {
            $query = $query->where("name", "like", "%$name%");
        }

        $code= request("code", null);
        if(!empty($code)){
            $query = $query->where("departments.code", $code);
        }

        $is_performance = request("is_performance", false);

        if (!empty($is_performance)) {
            $query = $query->where("is_performance", $is_performance);
        }

        return $query;
    }
}

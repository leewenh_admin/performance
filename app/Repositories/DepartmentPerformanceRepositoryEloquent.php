<?php

namespace App\Repositories;

use App\Enums\DepartmentType;
use App\Enums\EmployeePosition;
use App\Enums\PerformanceDetailType;
use App\Exceptions\MessageException;
use App\Models\BaseData;
use App\Models\Department;
use App\Models\DepartmentHeadPerformance;
use App\Models\DepartmentInsideSubtract;
use App\Models\DepartmentPerformance;
use App\Models\DirectCost;
use App\Models\Employee;
use App\Models\EmployeeAttendance;
use App\Models\FourItemPerformance;
use App\Models\LockPerformance;
use App\Models\Setting;
use App\Models\SpecialPerformance;
use App\Models\SpecialPerformanceSetting;
use App\Models\SubDepartment;
use App\Repositories\PerformanceDetail\BusinessRecoveryDetail;
use App\Repositories\PerformanceDetail\CommunityPerformanceDetail;
use App\Repositories\PerformanceDetail\ConsultationDetail;
use App\Repositories\PerformanceDetail\CoreBusinessDetail;
use App\Repositories\PerformanceDetail\ComprehensiveDetail;
use App\Repositories\PerformanceDetail\DepartmentHeaderDetail;
use App\Repositories\PerformanceDetail\NewTechnologyItemDetail;
use App\Repositories\PerformanceDetail\NightPerformanceDetail;
use App\Repositories\PerformanceDetail\OtherPerformanceDetail;
use App\Repositories\PerformanceDetail\ParkingSubsidyDetail;
use App\Repositories\PerformanceDetail\PublicHealthPerformanceDetail;
use App\Repositories\PerformanceDetail\TalentReserveDetail;
use App\Repositories\PerformanceDetail\VisitPerformanceDetail;
use App\Repositories\PerformanceDetail\WorkloadPerformanceDetail;
use Illuminate\Support\Facades\DB;

class DepartmentPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = DepartmentPerformance::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with(["department:id,name,code"]);

        $department_id = request("department_id", null);
        if(!empty($department_id)){
            $query = $query->where("department_performances.department_id", $department_id);
        }

        $code= request("code", null);
        if(!empty($code)){
            $query = $query->where("departments.code", $code);
        }

        $type = request("type", null);
        if(!empty($type)){
            $query = $query->where("department_performances.type", $type);
        }

        $date = request("date", null);
        if(!empty($date)){
            $query = $query->where("department_performances.date", $date);
        }
        $query = $query->leftJoin('special_performances', function ($join) {
                    $join->on('special_performances.department_id', '=', 'department_performances.department_id')
                        ->on('special_performances.date', '=', 'department_performances.date');
                })
                ->leftjoin('departments', 'department_performances.department_id', '=', 'departments.id')
                ->leftjoin('lock_performances', 'department_performances.date', '=', 'lock_performances.date')
                ->select("department_performances.*", "management_performance", "night_performance", "consultation_performance",
                    "visit_performance", "community_performance", "public_health_performance", "talent_performance" ,"business_recovery_performance",
                    "parking_performance","other_performance","total_performance","department_header_performance")
                ->selectRaw("department_performances.cost_effective + department_performances.special_performance as cost_effective,
                 IF(lock_performances.id is not null,'true','false') as haslock");
        return $query;
    }

    public function parseListResult($query) {
        $perPage = request("perPage", 25);
        $sort_str = $this->getSortStr();
        $sort_order = request("sort_order", "desc");
        $allPage = request("allPage", 1);

        $query = $query->orderBy($sort_str, $sort_order);

        if ($allPage == 2) {
            $query = $query->orderBy('department_performances.department_id','asc');
            $data = $query->get()->toArray();
            $arr_key = [];
            foreach ($data as $key => $performance){
                //消化内科
                if ($performance['department']['code'] == '1005'){
                    $arr_key[$performance['date']]['first_key'] = $key;
                }
                //消化内镜室
                if ($performance['department']['code'] == '4011'){
                    $arr_key[$performance['date']]['last_key'] = $key;
                }
            }
            //消化内科和消化内镜室合并
            if (!empty($arr_key)){
                $fields = [
                    'business_recovery_performance',
                    'community_performance',
                    'comprehensive',
                    'consultation_performance',
                    'control_performance',
                    'core_business',
                    'cost_effective',
                    'department_header_performance',
                    'department_performance',
                    'distribution_performance',
                    'individual',
                    'management_performance',
                    'night_performance',
                    'other_performance',
                    'parking_performance',
                    'people_count',
                    'physical_performance',
                    'public_health_performance',
                    'running_performance',
                    'special_performance',
                    'talent_performance',
                    'total_performance',
                    'total_performances',
                    'visit_performance',
                    'workload'
                ];

                foreach ($arr_key as $replace_key){
                    foreach ($fields as $field){
                        $data[$replace_key['first_key']][$field] += $data[$replace_key['last_key']][$field];
                    }
                    unset($data[$replace_key['last_key']]);
                }

                return array_values($data);

            }
            return $data;

        } else {
            $query = $query->orderBy('department_performances.department_id','asc');
            return $query->paginate($perPage);
        }
    }

    public function getDetail($date, $record_id, $detail_type=0) {
        $record = DepartmentPerformance::find($record_id);
        $department_id = $record->department_id;
        switch ($detail_type) {
            case PerformanceDetailType::CORE_BUSINESS:
                return (new CoreBusinessDetail())
                    ->getDetail($date, $department_id);
                break;
            case PerformanceDetailType::WORKLOAD:
                return (new WorkloadPerformanceDetail())
                    ->getDetail($date, $department_id);
            case PerformanceDetailType::COMPREHENSIVE:
                return (new ComprehensiveDetail())
                    ->getDetail($date, $department_id);

                break;
            case PerformanceDetailType::NEWTECHNOLOGYITEM:
                return (new NewTechnologyItemDetail())
                    ->getDetail($date, $department_id);
                break;
            case PerformanceDetailType::PARKING_SUBSIDY:
                return (new ParkingSubsidyDetail())
                    ->getDetail($date, $department_id);
            case PerformanceDetailType::DEPARTMENT_HEADER:
                return (new DepartmentHeaderDetail())
                    ->getDetail($date, $department_id);
                break;
            case PerformanceDetailType::NIGHT_PERFORMANCE:
                return (new NightPerformanceDetail())
                    ->getDetail($date, $department_id);
                break;
            case PerformanceDetailType::CONSULTATION_PERFORMANCE:
                return (new ConsultationDetail())
                    ->getDetail($date, $department_id);
                break;
            case PerformanceDetailType::VISIT_PERFORMANCE:
                return (new VisitPerformanceDetail())
                    ->getDetail($date, $department_id);
                break;
            case PerformanceDetailType::COMMUNITY_PERFORMANCE:
                return (new CommunityPerformanceDetail())
                    ->getDetail($date, $department_id);
                break;
            case PerformanceDetailType::OTHER_PERFORMANCE:
                return (new OtherPerformanceDetail())
                    ->getDetail($date, $department_id);
                break;
            case PerformanceDetailType::PUBLIC_HEALTH_PERFORMANCE:
                return (new PublicHealthPerformanceDetail())
                    ->getDetail($date, $department_id);
                break;
            case PerformanceDetailType::TALENT_RESERVE_PERFORMANCE:
                return (new TalentReserveDetail())
                    ->getDetail($date, $department_id);
                break;
            case PerformanceDetailType::BUSINESS_RECOVERY_PERFORMANCE:
                return (new BusinessRecoveryDetail())
                    ->getDetail($date, $department_id);
                break;
            default:
                # code...
                break;
        }

        if($record->type == DepartmentType::FUNCTIONAL){
            $avg_performance = Setting::where("key", "avg_performance")
                ->where("title", $date)->first();
            $avg_performance = $avg_performance ? $avg_performance->value : 0;

            $data = [
                'avg_performance' => number_format($avg_performance,2),
                'people_count' => number_format($record->people_count,2),
                'function_performance' => number_format($record->total_performances,2),
                "special_performance" => number_format($record->special_performance,2)
            ];
        } else {

            $department = Department::query()->where('id',$department_id)
                ->with('subDepartments')
                ->first();
            $cost_effective_obj = new CostEffectiveRepositoryEloquent();
            //执行收入
            //门诊执行收入
            $outpatientMedicalMoney= $cost_effective_obj->getOutpatientMedicalPoint($date, $department_id) * 10;
            //住院执行收入
            $inHospitalMedicalMoney= $cost_effective_obj->getInHospitalMedicalPoint($date, $department_id) * 10;
//        辅检项目
            //门诊辅检收入
            $outpatientAuxiliaryMoney = $cost_effective_obj->getOutpatientAuxiliaryPoint($date, $department_id) * 10;
            //住院辅检收入
            $inHospitalAuxiliaryMoney = $cost_effective_obj->getInHospitalAuxiliaryPoint($date, $department_id) * 10;
//        手术收入
            $surgeryInComeMoney = $cost_effective_obj->getSurgeryInCome($date, $department_id);
            //手术项目点数
//        $surgeryPoint = round($surgeryInComeMoney / 10, 2);
            //固定成本
            $fixedCostsMoney = $cost_effective_obj->getFixedCostsPoint($date, $department_id) * 10;
            //单价
            $prices = BaseData::query()->where('date', $date)
                ->where('department_id', $department_id)->value('cost_effective_price');
            //有效收入
            $departmentIncomeMoney = $cost_effective_obj->getDepartInCome($date, $department_id);
            //总收入
            $totalInComeMoney = $cost_effective_obj->getTotalInCome($date, $department_id);
            //医疗服务收入占比目标值
            $medical_target = $department->service_income_target_value;
            //医疗服务收入占比实际值
            if ($medical_target == 0 || $totalInComeMoney == 0){
                $medicalPercent = 0;
                if ($department->subDepartments->first() && $department->subDepartments->first()->type_lv1 == DepartmentType::MEDICAL_TECHNICAL) {
                    $medicalPercent = 1;
                }
                $medical_income = 0;
            }else{
                $medical_income = ($departmentIncomeMoney + $surgeryInComeMoney) / $totalInComeMoney * 100;
                //医疗服务收入占比系数
                if ($department->subDepartments->first() && $department->subDepartments->first()->type_lv1 == DepartmentType::MEDICAL_TECHNICAL) {
                    $medicalPercent = 1;
                }else{
                    if ($medical_income < $medical_target * 0.95) {
                        $medicalPercent = 1 - ($medical_target - $medical_income) / $medical_target;
                    } elseif ($medical_income >= $medical_target * 0.95 && $medical_income <= $medical_target) {
                        $medicalPercent = 1;
                    } else {
                        $medicalPercent = 1 + ($medical_income - $medical_target) / $medical_target;
                    }
                }

            }
            //本期可控变动成本
            $current_period_controllable_variable_cost = DirectCost::query()->where('departments_id', $department_id)
                ->where('date', $date)
                ->select('sanitary_material', 'affairs_material', 'disinfect', 'laundry', 'utilities')
                ->first();
            if($current_period_controllable_variable_cost){
                $current_period_controllable_variable_cost = $current_period_controllable_variable_cost->toArray();
                $current_period_controllable_variable_cost = array_sum($current_period_controllable_variable_cost);
            } else {
                $current_period_controllable_variable_cost = 0;
            }

            if ($current_period_controllable_variable_cost == 0) {
                $variableCostControl = 0;
            }else {
                //计算变动成本控制
                if ($departmentIncomeMoney == 0) {
                    $currentRate = 0;
                }else {
                    $currentRate = $current_period_controllable_variable_cost / $departmentIncomeMoney;
                }
                $year = date("Y",strtotime($date));
                $lastYear = date("Y-m",strtotime("-1 years",strtotime($date)));
                if ($year == "2021"){//如果是2021年就查询2019年的数据
                    $lastYear = date("Y-m",strtotime("-2 years",strtotime($date)));
                }
                $baseData = BaseData::where("date", $lastYear)->where("department_id", $department_id)->first();
                if (!empty($baseData)){
                    $department->base_period_controllable_variable_cost = $baseData->base_period_controllable_variable_cost;
                    $department->base_department_income = $baseData->base_department_income;
                }
                if ($department->base_department_income == 0) {
                    $baseRate = 0;
                }else {
                    $baseRate = ($department->base_period_controllable_variable_cost * 12) / ($department->base_department_income * 12);
                }

                $variableCostControl = ($currentRate  - $baseRate) * $current_period_controllable_variable_cost * $department['variable_cost_control_deduction_rate'];
            }
            //医疗服务占比系数暂定1
            $medicalPercent = 1;
            $data = [
//                'medicalMoney' => number_format($medicalMoney,2)  . '元',
                'outpatientMedicalMoney' => number_format($outpatientMedicalMoney,2)  . '元',
                'inHospitalMedicalMoney' => number_format($inHospitalMedicalMoney,2)  . '元',
//                'auxiliaryMoney' => number_format($auxiliaryMoney,2) . '元',
                'outpatientAuxiliaryMoney' => number_format($outpatientAuxiliaryMoney,2) . '元',
                'inHospitalAuxiliaryMoney' => number_format($inHospitalAuxiliaryMoney,2) . '元',
//            'surgeryPoint' => $surgeryPoint,
                'fixedCostsMoney' => number_format($fixedCostsMoney,2) . '元',
                'prices' => number_format($prices,2) . '元',
                'departmentIncomeMoney' => number_format($departmentIncomeMoney,2) . '元',
                'surgeryInComeMoney' => number_format($surgeryInComeMoney,2) . '元',
                'totalInComeMoney' => number_format($totalInComeMoney,2) . '元',
                'medical_target' => number_format($medical_target,2).'%',
                'medical_income' => number_format($medical_income,2).'%',
                'medicalPercent' => number_format($medicalPercent * 100, 2).'%',
                'currentPeriodControllableVariableCost' => number_format($current_period_controllable_variable_cost,2) . '元',
                'baseDepartmentIncome' => number_format($department->base_department_income,2) . '元',
                'basePeriodControllableVariableCost' => number_format($department->base_period_controllable_variable_cost,2) . '元',
                'variableCostControlDeductionRate' => number_format($department->variable_cost_control_deduction_rate * 100,2).'%',
                'variableCostControl' => number_format($variableCostControl,2).'（只扣不奖，正数计0）',
                "special_performance" => number_format($record->special_performance,2)
            ];

        }

        return $data;


    }


    /**
     * 科室绩效数据生成
     * @param $date
     */
    public function generatePerformanceData($date){
        //查询需要计算绩效的科室
        $departments = $this->getDepartment();
        $cost_effective_obj = new CostEffectiveRepositoryEloquent();
        $workload_obj = new WorkLoadRepositoryEloquent();
        $core_business_obj = new CoreBusinessRepositoryEloquent();
        $comprehensive_obj = new ComprehensivePerformanceRepositoryEloquent();
        $individual_obj = new NewTechnologyItemRepositoryEloquent();
        $medical_workload_obj = new MedicalWorkLoadRepositoryEloquent();

        $total = 0;
        //特殊科室id
        //$special = [242, 249, 253, 282, 319, 320, 218, 216, 255, 489, 254];

        //生成综合考勤绩效
        $comprehensive_obj->performance($date);

        foreach ($departments as $department){
            $cost_effective = 0;
            $workload = 0;
            $core_business = 0;
            $comprehensive = 0;
            $individual = 0;
            $performance = 0;
            if(DepartmentPerformance::where('date',$date)->where('department_id', $department["id"])->first()) continue;
            $people_count = $this->getAttendancePeopleCount($date, $department["id"]);

            $cost_effective_price = BaseData::query()->where('date', $date)
                ->where('department_id', $department["id"])->value('cost_effective_price');

            if($department["type_lv1"] == DepartmentType::CLINICAL){//临床科室
                //成本效益绩效
                $cost_effective = $cost_effective_obj->getCostEffective($date, $department["id"]) * $cost_effective_price;
                //工作量绩效
                $workload = $workload_obj->getWorkLoadPerformance($date, $department["id"] ,$department);
                //核心业务绩效
                $core_business = $core_business_obj->getCoreBusinessPerformance($date, $department["id"]);
                //综合考核绩效
                $comprehensive = $comprehensive_obj->getClinicalComprehensivePerformance($date, $department["id"]);
                //单项类绩效
                $individual = $individual_obj->getIndividualPerformance($date, $department["id"]);
                //绩效总和
                $performance = round($cost_effective)  + round($workload)  + round($core_business)  +
                    round($comprehensive) + round($individual);

                $total += $performance;
            } elseif ($department["type_lv1"] == DepartmentType::MEDICAL_TECHNICAL){//医技科室
                $core_business = 0;
                //成本效益绩效
                $cost_effective = $cost_effective_obj->getCostEffective($date, $department["id"], true) * $cost_effective_price;
                //工作量绩效
                $workload = $medical_workload_obj->getMedicalWorkLoad($date, $department["id"]);
                //综合考核绩效
                $comprehensive = $comprehensive_obj->getMedicalComprehensivePerformance($date, $department["id"]);
                //单项类绩效
                $individual = $individual_obj->getIndividualPerformance($date, $department["id"]);
                $performance = round($cost_effective)  + round($workload)  + round($comprehensive) + round($individual);

                $total += $performance;
            }
            //体检绩效
            $physical_performance = 0;
            //疫情防控绩效
            $control_performance = 0;
            //科室跑账绩效
            $running_performance = 0;
            //查询是否导入了以上3种绩效
            $other = FourItemPerformance::where('date', $date)->where('department_id', $department["id"])->first();
            if($other){
                $physical_performance = round($other->physical_performance);
                $control_performance = round($other->control_performance);
                $running_performance = round($other->running_performance);
            }

            $total = $total + $physical_performance + $control_performance + $running_performance;

            $d_performance = 0;
            $distribution_performance = 0;
            if ($department["type_lv1"] == DepartmentType::CLINICAL || $department["type_lv1"] == DepartmentType::MEDICAL_TECHNICAL){
                $d_performance = $this->getdPerformance(round($performance), $people_count, $date, $department["id"]);
                $distribution_performance = round($performance) - $d_performance;
            }

            $data = [
                "date" => $date,
                "department_id" => $department["id"],
                "type" => $department["type_lv1"],
                "cost_effective" => round($cost_effective),
                "workload" => round($workload),
                "core_business" => round($core_business),
                "comprehensive" => round($comprehensive),
                "individual" => round($individual),
                "physical_performance" => $physical_performance,
                "control_performance" => $control_performance,
                "running_performance" => $running_performance,
                "total_performances" => round($performance),
                "people_count" => $people_count,
                "department_performance" => $d_performance,
                "distribution_performance" => $distribution_performance
            ];

            DepartmentPerformance::create($data);
        }
        //更新职能科室绩效
        $this->generateFunctionalPerformanceData($date);

        //更新特殊科室绩效
        $obj_special = new SpecialDepartmentPerformanceRepositoryEloquent();
        foreach ($departments as $department){
            $obj_special->updateSpecialDepartment($date, $department['id']);
        }

    }

    /**
     * 更新职能科室的绩效
     * @param $date
     */
    public function generateFunctionalPerformanceData($date){

        $departments = DepartmentPerformance::where("type", 13)
            ->get()
            ->all();

        //行政平均绩效
        $avg_performance = (new SpecialDepartmentPerformanceRepositoryEloquent())->getAdministrativeAverage($date);
        Setting::updateOrCreate(
            [
                "key" => "avg_performance",
                "title" => $date,
            ],
            [
                "value" => $avg_performance,
                "value_type" => 1,
            ]
        );


        foreach ($departments as $department){
            if ($department->type == DepartmentType::FUNCTIONAL){//职能科室
                $performance = $this->getFunctionalData($date, $department->department_id, $avg_performance);
                DepartmentPerformance::where("id", $department->id)
                    ->update([
                        "total_performances" => round($performance),
                        "cost_effective" => round($performance),
                    ]);
            }
        }
    }
    /**
     * 查询一级科室的分类
     * @return mixed
     */
    public function getDepartment(){
        $data = Department::leftjoin(
            'sub_departments', 'departments.id', '=', 'sub_departments.parent_id'
            )->where("sub_departments.type_lv1", "<>", 0)
            ->where("departments.is_performance", 1)
            ->select("departments.id","type_lv1","base_avg_hos_days")
            ->distinct()
            ->get()
            ->toArray();

        return $data;
    }

    /**
     * 临床科室绩效金额
     * @param $date
     * @param $department_id
     * @return false|float
     */
    public function getClinicalDepartmentsData($date, $department_id){
        //临床科室绩效金额=成本效益绩效（50%）+工作量绩效（30%）+核心业务绩效（20%）+综合考核绩效+单项类绩效
        //成本效益绩效
        $cost_effective_obj = new CostEffectiveRepositoryEloquent();
        $cost_effective = $cost_effective_obj->getCostEffective($date, $department_id);
        //工作量绩效
        $workload_obj = new WorkLoadRepositoryEloquent();
        $workload = $workload_obj->getWorkLoadPerformance($date, $department_id);
        //核心业务绩效
        $core_business_obj = new CoreBusinessRepositoryEloquent();
        $core_business = $core_business_obj->getCoreBusinessPerformance($date, $department_id);
        //综合考核绩效
        $comprehensive_obj = new ComprehensivePerformanceRepositoryEloquent();
        $comprehensive = $comprehensive_obj->getClinicalComprehensivePerformance($date, $department_id);
        //单项类绩效
        $individual_obj = new NewTechnologyItemRepositoryEloquent();
        $individual = $individual_obj->getIndividualPerformance($date, $department_id);

        $performance = $cost_effective * 0.5 + $workload * 0.3 + $core_business * 0.2 +
            $comprehensive + $individual;

        return round($performance, 2);
    }

    /**
     * 医技科室绩效金额
     * @param $date
     * @param $department_id
     * @return false|float
     */
    public function getMedicalDepartmentsData($date, $department_id){
        //医技科室绩效金额=成本效益绩效（50%）+工作量绩效（50%）+综合考核绩效+单项类绩效
        //成本效益绩效
        $cost_effective_obj = new CostEffectiveRepositoryEloquent();
        $cost_effective = $cost_effective_obj->getCostEffective($date, $department_id, true);
        //工作量绩效
        $workload_obj = new MedicalWorkLoadRepositoryEloquent();
        $workload = $workload_obj->getMedicalWorkLoad($date, $department_id);
        //综合考核绩效
        $comprehensive_obj = new ComprehensivePerformanceRepositoryEloquent();
        $comprehensive = $comprehensive_obj->getMedicalComprehensivePerformance($date, $department_id);
        //单项类绩效
        $individual_obj = new NewTechnologyItemRepositoryEloquent();
        $individual = $individual_obj->getIndividualPerformance($date, $department_id);

        $performance = $cost_effective * 0.5 + $workload * 0.5 + $comprehensive + $individual;

        return round($performance, 2);
    }

    /**
     * 职能科室绩效
     * @param $date
     * @param $department_id
     * @param $avg_performance
     * @return false|float|int
     */
    public function getFunctionalData($date, $department_id, $avg_performance){
        //本月科室医生人数
        $employee = $this->getAttendancePeopleCount($date, $department_id);

        $functionalData = $avg_performance * $employee;
        return round($functionalData, 2);
    }

    /**
     * 查询所有的职能科室
     * @return mixed
     */
    public function getFunctionalDepartments(){
        $department_ids = SubDepartment::where("type_lv1", DepartmentType::FUNCTIONAL)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();

        return $department_ids;
    }

    /**
     * 查询一级科室下参与绩效的全部科室
     * @param $department_id
     * @return mixed
     */
    public function getSubDepartment($department_id){
        $department_ids = SubDepartment::where("parent_id", $department_id)
            ->where("is_performance", 1)
            ->get()
            ->pluck("id")
            ->all();

        return $department_ids;
    }

    /**
     * 计算一个或者多个科室的考勤人数（绩效人数）
     * 即这些人的结合考勤系数的总和
     */
    public function getAttendancePeopleCount($date, $department_id) {

        $people_count = EmployeeAttendance::where("departments_id", $department_id)
            ->where("date", $date)
            ->select(["attendance_coefficient"])
            ->get()
            ->pluck("attendance_coefficient")
            ->sum();

        return round($people_count, 2);
    }

    /**
     * 清空绩效数据
     * @param $date
     */
    public function clearPerformance($date){
        DepartmentPerformance::where("date", $date)
            ->delete();
        SpecialPerformance::where("date", $date)
            ->delete();
        LockPerformance::query()->where("date", $date)
            ->delete();
    }


    /**
     * 查询院长，副院长，院长助理
     * @return mixed
     */
    public function getSpecialEmployee(){
        $data = Employee::whereIn("position", [1, 2, 3])
            ->get()
            ->pluck("id")
            ->all();

        return $data;
    }


    /**
     * 修改特殊科室绩效
     * @param $data
     */
    public function specialPerformance($data){
        DB::beginTransaction();

        try {
            //更新绩效数据
            $record = DepartmentPerformance::where('id', $data['id'])->first();
            $total = $record->total_performances;
            $old = $record->special_performance;
            $new = $data['special_performance'];
            $new_total = $total - $old + $new;

            $record->special_performance = $new;
            $record->total_performances = $new_total;

            $record->save();

            //保存特殊科室设置表
            SpecialPerformanceSetting::updateOrCreate(
                [
                    'date' => $record->date,
                    'department_id' => $record->department_id,
                ],
                [
                    'performance' => $data['special_performance']
                ]
            );
            DB::commit();
        } catch (\Exception $exception){
            DB::rollBack();
            throw new MessageException($exception->getMessage());
        }

    }

    /**
     * 计算科内绩效
     * @param $total_performance
     * @param $people_count
     * @param $date
     * @param $department_id
     * @return float|int
     */
    public function getdPerformance($total_performance, $people_count, $date, $department_id){
        if($department_id == '253'){ //输血科不计算
            return 0;
        }

        //如果科室无考勤科主任则不计算科主任科室绩效
        $attendance_header = EmployeeAttendance::leftjoin('employees', 'employee_attendances.employees_id', '=', 'employees.id')
            ->where("employee_attendances.departments_id", $department_id)
            ->where("employee_attendances.date", $date)
            ->where("employees.position", EmployeePosition::DEPARTMENT_HEAD)
            ->first();

        if (!$attendance_header){
            return 0;
        }
        //查询核减数量
        $sub_data = DepartmentInsideSubtract::where('date', $date)->where('department_id', $department_id)->first();

        $standard = DepartmentHeadPerformance::where('date', $date)->where('employee_id', $attendance_header->employees_id)->first();
        if(!$standard){
            return 0;
        }

        if ($sub_data) {
            $sub = $sub_data->sub_value;
        } else {
            $sub = 0;
        }

        $performance = $total_performance / $people_count * 2.5 - $standard->performance_standard + $people_count * $sub;


        return round($performance);
    }

    /**
     * 锁定绩效
     * @param $date
     * @throws MessageException
     */
    public function lockPerformance($date){
        $performance = DepartmentPerformance::query()->where('date', $date)->exists();
        if (!$performance){
            throw new MessageException('尚未生成本月绩效，无法锁定');
        }

        $hasLock = LockPerformance::query()->where('date', $date)->exists();
        if ($hasLock){
            throw new MessageException('本月绩效已经锁定，无需重复锁定');
        }
        LockPerformance::query()->insert(['date' => $date]);
    }

    /**
     * 核查绩效数据是否锁定
     * @param $date
     * @return array
     */
    public function checkLockStatus($date){
        $hasLock = LockPerformance::query()->where('date', $date)->exists();
        return [
          'data' =>  $hasLock
        ];
    }

}

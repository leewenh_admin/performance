<?php

namespace App\Repositories;

use App\Models\SurgeryNew;

class SurgeryNewRepositoryEloquent extends BaseRepository {
    protected $actionClass = SurgeryNew::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "subDepartment:id,name,code",
        ]);
        $query = $query->join('sub_departments', 'surgery_news.sub_departments_id', '=', 'sub_departments.id')
            ->leftjoin('lock_performances', 'surgery_news.date', '=', 'lock_performances.date');

        $sub_departments_id = request("sub_departments_id", null);
        if (!empty($sub_departments_id)){
            $query = $query->where("sub_departments_id", $sub_departments_id);
        }

        $surgery_time = request("surgery_time", null);
        if (!empty($surgery_time)){
            $query = $query->where("surgery_time", "like", "%$surgery_time%");
        }

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("surgery_news.date", $date);
        }

        $type_lv1 = request("type_lv1", null);
        if (!empty($type_lv1)){
            $query = $query->where("surgery_news.type_lv1", $type_lv1);
        }

        $query = $query->selectRaw("surgery_news.*, case when surgery_type = 1 then '不加急' when surgery_type = 2 then '加急' end as surgery_type_name")
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        return $query;
    }
}

<?php

namespace App\Repositories;

use App\Enums\DepartmentType;
use App\Enums\EmployeeProfessionalRankLevel;
use App\Enums\EmployeeProfessionalRankType;
use App\Models\Department;
use App\Models\DepartmentHeadPerformance;
use App\Models\Employee;
use App\Models\EmployeeAttendance;
use App\Models\EmployeePerformance;
use App\Models\MainIncome;
use App\Models\OtherPerformance;
use App\Models\PublicHealthPerformance;
use App\Models\TalentReserve;
use Carbon\Carbon;

/**
 * 个人绩效
 * Class EmployeePerformanceRepositoryEloquent
 * @package App\Repositories
 */
class EmployeePerformanceGenerateRepositoryEloquent extends BaseRepository {

    /**
     * 生成个人绩效
     * @param $date
     */
    public function generateEmployeeData($date){
        $datas = EmployeePerformance::where("date", $date)
            ->get();

        foreach ($datas as $data){
            $employee_id = $data->employees_id;
            $employee = $this->getEmployeeData($employee_id);
            if (empty($employee)){
                continue;
            }

            $work_title = $employee->professional_rank_title;
            $work_type = $employee->type;
            $bank_code = $employee->bank_code;
            $workload_performance = $data->workload_performance;
            $director_performance = $this->getDepartmentManagePerformance($date, $employee_id);
            $night_performance = $data->night_performance;
            $consultation_performance = $this->getConsultationPerformance($date, $employee_id);
            $expert_performance = $this->getVisitPerformance($date, $employee_id);
            $community_performance = $data->community_performance;
            $other_performance = $this->getOtherPerformance($date, $data->code);
            $parking_subsidy = $this->getParkingPerformance($date, $employee_id);
            $talent_reserves_performance = $this->getTalentReserve($date, $employee_id);
            $business_recovery_performance = $this->getBusinessRecoveryPerformance($date, $employee_id);
            $public_health_performance = $this->getPublicPerformance($date, $data->code);
            $deduct = $data->deduct;
            $should_sent = $workload_performance + $director_performance + $night_performance + $consultation_performance +
                $expert_performance+ $community_performance + $other_performance + $parking_subsidy + $talent_reserves_performance +
                $business_recovery_performance + $public_health_performance;

            $actual_sent = $should_sent - $deduct;

            EmployeePerformance::where("id", $data->id)
                ->update([
                    "work_title" => $work_title,
                    "work_type" => $work_type,
                    "bank_code" => $bank_code,
                    "director_performance" => $director_performance,
                    "consultation_performance" => $consultation_performance,
                    "expert_performance" => $expert_performance,
                    "other_performance" => $other_performance,
                    "parking_subsidy" => $parking_subsidy,
                    "talent_reserves_performance" => $talent_reserves_performance,
                    "business_recovery_performance" => $business_recovery_performance,
                    "public_health_performance" => $public_health_performance,
                    "should_sent" => $should_sent,
                    "actual_sent" => $actual_sent,
                ]);
        }
    }

    /**
     * 科主任绩效
     * @param $employee_id
     * @param $date
     * @return float|int
     */
    public function getDepartmentManagePerformance($date, $employee_id){
        $manageData = DepartmentHeadPerformance::where("employee_id", $employee_id)
            ->where("date", $date)
            ->first();
        if (empty($manageData)){
            return 0;
        }

        $attendance = EmployeeAttendance::where("employees_id", $employee_id)
            ->where("date", $date)
            ->where("departments_id", $manageData->department_id)
            ->first();

        if (empty($attendance)){
            return 0;
        }

        $department = Department::where("id", $manageData->department_id)->first();
        if ($department->subDepartment->type_lv1 == DepartmentType::FUNCTIONAL){
            $score = 100;
        } else {
            $obj = new NightPerformanceCalculateEloquent();
            $scoreData = $obj->getScore($date, $department->id, $department->subDepartments->type_lv1);
            if ($manageData->administrative_duties == "科主任" || $manageData->administrative_duties == "副主任") {
                $score = $scoreData->medical_quality;
            } elseif ($manageData->administrative_duties == "护士长") {
                $score = $scoreData->care_quality;
            }
        }

        //当月天数
        $days = date("t",strtotime($date));
        $percent = $attendance->actual_attendance / $days;

        if ($score >= 98) {
            $performance = $manageData->performance_standard * $percent;
        } else {
            $performance = ($manageData->performance_standard + ($manageData->performance_standard *
                    ($score - 98) / 100)) * $percent;
        }

        return $performance;

    }

    /**
     * 会诊绩效
     * @param $employee_id
     * @param $date
     * @return float|int
     */
    public function getConsultationPerformance($date, $employee_id){
        $arrDate = explode("-", $date);
        $count = MainIncome::date($date)->where("prescribe_doctor_id", $employee_id)
            ->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("item_cate_code", 36)//会诊费
            ->sum("quantity");

        $performance = $count * 10;

        return $performance;
    }

    /**
     * 专家号绩效
     * @param $employee_id
     * @param $date
     * @return float|int
     */
    public function getVisitPerformance($date, $employee_id){

        $arrDate = explode("-", $date);

        $chief_count = 0;
        $assistant_count = 0;

        //专家号数量
        $count = MainIncome::date($date)->whereYear("charging_time", $arrDate[0])
            ->whereMonth("charging_time", $arrDate[1])
            ->where("prescribe_doctor_id", $employee_id)
            ->select("serial_no", "quantity", "price")
            ->distinct()
            ->where("item_code", 1002)
            ->where("price", ">=", 10)
            ->get();

        foreach ($count as $value){
            if($value->price < 16 ){  //大于等于10 小于16 副主任
                $assistant_count += $value->quantity;
            } else { //大于等于16 主任
                $chief_count += $value->quantity;
            }
        }



        return $chief_count * 9.6 + $assistant_count * 6 ;

    }

    /**
     * 其他单项绩效
     * @param $code
     * @param $date
     * @return int
     */
    public function getOtherPerformance($date, $code){
        $data = OtherPerformance::where("code", $code)
            ->where("date", $date)
            ->first();

        if (empty($data)){
            return 0;
        } else {
            return $data->amount;
        }
    }

    /**
     * 停车补助
     * @param $employee_id
     * @param $date
     * @return float|int
     */
    public function getParkingPerformance($date, $employee_id){
        $attendanceCoefficient = EmployeeAttendance::where("date", $date)
            ->where("employees_id", $employee_id)
            ->sum("attendance_coefficient");

        $performance = 600 * $attendanceCoefficient ;

        return $performance;
    }

    /**
     * 人才储备绩效
     * @param $employee_id
     * @param $date
     * @return float|int
     */
    public function getTalentReserve($date, $employee_id) {
        $record = TalentReserve::where("date", $date)
            ->where("employees_id", $employee_id)
            ->get();

        if (empty($record)){
            return 0;
        }

        return $record->actual_sent;
    }

    /**
     * 业务恢复绩效
     * @param $employee_id
     * @param $date
     * @return false|float|int
     */
    public function getBusinessRecoveryPerformance($date, $employee_id){
        $employee = Employee::where("id", $employee_id)
            ->where("position", 4)
            ->first();
        if (empty($employee)){
            return 0;
        }
        $attendance = EmployeeAttendance::where("date", $date)
            ->where("employees_id", $employee_id)
            ->whereRaw("employee_attendances.actual_attendance = (select max(t.actual_attendance)
            from employee_attendances as t where
            employee_attendances.employees_id = t.employees_id and t.date = '$date') ")
            ->first();
        if (empty($attendance)){
            return 0;
        }

        $performance = (new NightPerformanceCalculateEloquent())
            ->getBusinessRecoveryPerformance($date, $attendance->departments_id);

        return $performance;
    }

    /**
     * 公卫绩效
     * @param $date
     * @param $code
     * @return mixed
     */
    public function getPublicPerformance($date, $code){
        $data = PublicHealthPerformance::where("date", $date)
            ->where("code", $code)
            ->sum("amount");

        return $data;
    }

    /**
     * 人员信息
     * @param $employee_id
     * @return mixed
     */
    public function getEmployeeData($employee_id){
        $data = Employee::leftjoin("employee_payrolls", "employees_id", "=", "employees.id")
            ->where("employees.id", $employee_id)
            ->select("professional_rank_title", "professional_rank_type", "bank_code")
            ->first();
        if (empty($data)){
            return $data;
        }
        $name = '';
        switch ($data->professional_rank_type){
            case EmployeeProfessionalRankType::NURSE:
                $name = "护理";
                break;
            case EmployeeProfessionalRankType::MEDICAL_TREATMENT:
                $name = "医疗";
                break;
            case EmployeeProfessionalRankType::ECONOMIC:
                $name = "经统会";
                break;
            case EmployeeProfessionalRankType::MEDICAL_SKILL:
                $name = "医技";
                break;
            case EmployeeProfessionalRankType::MEDICAMENT:
                $name = "药剂";
                break;
            case EmployeeProfessionalRankType::WORKER:
                $name = "工勤";
                break;
        }

        $data->type = $name;
        return $data;
    }
}

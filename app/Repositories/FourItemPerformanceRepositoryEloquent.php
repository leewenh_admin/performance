<?php

namespace App\Repositories;

use App\Models\FourItemPerformance;

class FourItemPerformanceRepositoryEloquent extends BaseRepository {
    protected $actionClass = FourItemPerformance::class;

    public function getList(){
        $query = parent::getList();
        $query = $query->with('department:id,name,code');

        $query = $query->join('departments', 'four_item_performances.department_id', '=', 'departments.id')
            ->leftjoin('lock_performances', 'four_item_performances.date', '=', 'lock_performances.date');
        $query->select('four_item_performances.*')
            ->selectRaw("IF(lock_performances.id is not null,'true','false') as haslock");

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("four_item_performances.date", $date);
        }

        $department_id = request("department_id", null);
        if (!empty($department_id)){
            $query = $query->where("department_id", $department_id);
        }

        return $query;
    }
}

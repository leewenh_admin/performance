<?php

namespace App\Repositories;

use App\Models\UnitPhysical;

class UnitPhysicalRepositoryEloquent extends BaseRepository {
    protected $actionClass = UnitPhysical::class;

    public function getList()
    {
        $query = parent::getList();

        $query = $query->with([
            "department:id,name,code",
            "subDepartment:id,name,code",
        ]);

        $department_id = request("department_id", null);
        if (!empty($department_id)){
            $query = $query->where("department_id", $department_id);
        }

        $sub_department_id = request("sub_department_id", null);
        if (!empty($sub_department_id)){
            $query = $query->where("sub_department_id", $sub_department_id);
        }

        $date = request("date", null);
        if (!empty($date)){
            $query = $query->where("date", $date);
        }
        return $query;
    }
}

<?php

namespace App\Jobs;

use App\Repositories\DepartmentPerformanceRepositoryEloquent;
use App\Repositories\DirectCostRepositoryEloquent;
use App\Repositories\EmployeePerformanceGenerateRepositoryEloquent;
use App\Repositories\GeneralizedIncomeRepositoryEloquent;
use App\Repositories\NightPerformanceCalculateEloquent;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class GeneratePerformanceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;
    /**
     * 任务运行的超时时间。
     *
     * @var int
     */
    public $timeout = 3600;

    public $type;
    public $date;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type, $date)
    {
        $this->type = $type;
        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->type){
            case 'generalizedIncome':
                (new GeneralizedIncomeRepositoryEloquent())->generateData($this->date);
                break;
            case 'directCost':
                (new DirectCostRepositoryEloquent())->generateData($this->date);
                break;
            case 'departmentPerformance':
                (new DepartmentPerformanceRepositoryEloquent())->generatePerformanceData($this->date);
                break;
            case 'specialPerformance':
                (new NightPerformanceCalculateEloquent())->generateSpecialPerformance($this->date);
                break;
            case 'employeePerformance':
                (new EmployeePerformanceGenerateRepositoryEloquent())->generateEmployeeData($this->date);
                break;
            default:
                break;
        }

        Cache::forget('generate');
    }

    /**
     * 要处理的失败任务。
     *
     *
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::error($exception->getMessage().'--'.$exception->getFile().'--'.$exception->getLine());
        Cache::forget('generate');
    }


}

<?php

namespace App\Enums;

// 导入数据类别
class ImportDataType {
    // 考勤统计数据
    const EMPLOYEE_ATTENDANCE = 1;
    // 固定资产
    const FIXED_ASSETS = 2;
    // 领用卫生材料
    const USED_SANITARY_MATERIAL = 3;
    // 领用行政材料
    const USED_GENERAL_MATERIAL = 4;
    // 洗衣费
    const LAUNDRY_COST = 5;
    // 消毒费
    const DISINFECT_COST = 6;
    // 职员工资
    const EMPLOYEE_PAYROLL = 7;
    // 床位利用
    const BED_USDING = 8;
    // 会诊
    const CONSULTATION = 9;
    // 专家号
    const EXPERT_DIAGNOSIS = 10;
    // 体检工作量统计
    const PHYSICAL_WORK = 11;
    // 劳务派遣员工工资表
    const EMPLOYEE_DISPATCH_PAYROLL = 12;
    // 病种点值
    const DIEASE_CATALOG = 13;
    // 特检胃镜药房统计
    const TOTAL_SPECIAL_INSPECTION_PHARMACY = 14;
    // 住院病人动态
    const INPATIENT = 15;
    // 彩超工作量
    const COLOR_ULTRASOUND_WORKLOAD = 16;
    // 检验统计
    const TOTAL_EXAMINATION = 17;
    // 影像统计
    const TOTAL_MEDICAL_IMAGE = 18;
    // 手术项目点值
    const SURGICAL_POINT_VALUE = 19;
    // 科室手术
    const SURGERY = 20;
    // 主要收入数据
    const MAIN_INCOME = 21;
    // 科室和科室归属
    const DEPARTMENT = 22;
    // 职员名单
    const EMPLOYEE = 23;
    // 职员保险基数
    const INSURANCE_BASE = 24;
    // 电费
    const ELECTRIC_COST = 25;
    // 护理时数
    const NURSING_HOUR = 26;
    // 2019年考勤
    const EMPLOYEE_ATTENDANCE_2019 = 27;
    // 医疗服务占比目标值
    const BASE_SERVICE_INCOME_TARGET = 28;
    // 平均住院日、出院药品例均费用、出院例均费用基准值
    const BASE_BED = 29;
    // 门诊例均费用基准值
    const BASE_AVERAGE_COST = 30;
    // 收治率基准值
    const BASE_ADMISSION_RATE = 31;
    // 基期可控变动成本
    const BASE_CONTROLLABLE_VARIABLE_COST = 32;
    // 基期科内有效收入
    const BASE_DEPARTMENT_INCOME = 33;
    // 目标出院病人均例耗材费用
    const BASE_AVERAGE_MATERIAL_COST = 34;
    // 医技综合考核分数表
    const EMPLOYEE_ATTENDANCE_SCORE = 35;
    // 医技项目点值
    const MEDICAL_POINT_VALUE = 36;
    // 目标手术率
    const TARGET_SURGERY_RATE = 37;
    // 新技术、新业务
    const NEW_TECHNOLOGY_ITEM = 38;
    // 单位体检绩效
    const UNIT_PHYSICAL = 39;
    // 科室管理系数
    const DEPART_COEFFICIENT = 40;
    // 新手术表
    const SURGERY_NEW = 41;
    // 夜班绩效
    const NIGHT_PERFORMANCE = 42;
    // 医共体影像中心绩效
    const RADIOLOGY_PERFORMANCE = 43;
    // 医共体检验中心绩效
    const CLINICAL_LABORATORY_PERFORMANCE = 44;
    // 医共体心电中心绩效
    const SPECIAL_SURVEY_PERFORMANCE = 45;
    // 医共体病理中心绩效
    const PATHOLOGY_PERFORMANCE = 46;
    // 公卫绩效
    const PUBLIC_HEALTH_PERFORMANCE = 47;
    // 人才储备绩效
    const TALENT_RESERVE = 48;
    // 药剂科增量绩效
    const PHARMACY_INCREMENTAL = 49;
    // 其他专项绩效
    const OTHER_PERFORMANCE = 50;
    // 管理绩效明细
    const DEPARTMENT_HEAD_PERFORMANCES = 51;
    // 临床综合考核分数表
    const CLINICAL_ATTENDANCE_SCORE = 52;
    // 个人绩效
    const EMPLOYEE_PERFORMANCE = 53;
    // 单位体检绩效、卒中绩效、疫情防控、科室跑账
    const FOUR_ITEM_PERFORMANCE = 54;
}

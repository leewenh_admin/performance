<?php

namespace App\Enums;

// 收入类型
class IncomeType {
    // 门诊
    const OUTPATIENT = 1;
    // 住院
    const INPATIENT = 2;
}

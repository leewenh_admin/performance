<?php

namespace App\Enums;

// 业务收入分类
class BusinessIncomeType {
    // 医疗组即临床科室
    const CLINICAL_DEPARTMENT = 1;
    // 医技科室
    const MEDICAL_DEPARTMENT = 2;
    // 护理组
    const NURSE_GROUP = 3;

}

<?php

namespace App\Enums;

// 配置表 配置值类型
class SettingValueType {
    const TYPE_INTEGER = 1;
    const TYPE_STRING = 2;
    const TYPE_DECIMAL = 3;
}

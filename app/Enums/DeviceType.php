<?php

namespace App\Enums;

// 设备类别
class DeviceType {
    // 一般设备
    const NORMAL = 1;
    // 专业设备
    const PROFESSIONAL = 2;
    // 计算机设备
    const COMPUTER = 3;
    // 家俱设备
    const FURNITURE = 4;
    // 制冷设备
    const COLD = 5;
    // 交通设备
    const TRAFFIC = 6;
    // 电器设备
    const ELECTRICAL = 7;
    // 其他设备
    const OTHER = 8;
}

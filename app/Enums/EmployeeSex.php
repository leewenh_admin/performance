<?php

namespace App\Enums;

// 职员性别
class EmployeeSex {
    # 男
    const MALE = 1;
    # 女
    const FEMALE = 2;
}

<?php

namespace App\Enums;

// 绩效详情选项
class PerformanceDetailType {
    // 成本效益绩效
    // 核心业务绩效
    const CORE_BUSINESS = 2;
    // 工作量绩效
    const WORKLOAD = 3;
    // 综合考核绩效
    const COMPREHENSIVE = 4;

    // 新技术业务绩效
    const NEWTECHNOLOGYITEM = 5;
    // 停车补助
    const PARKING_SUBSIDY = 6;
    // 科主任管理绩效
    const DEPARTMENT_HEADER = 7;
    // 夜班绩效
    const NIGHT_PERFORMANCE = 8;
    // 会诊绩效
    const CONSULTATION_PERFORMANCE = 9;
    // 专家号绩效
    const VISIT_PERFORMANCE = 10;
    // 医共体绩效
    const COMMUNITY_PERFORMANCE = 11;
    // 其他单项绩效
    const OTHER_PERFORMANCE = 12;
    // 公卫绩效
    const PUBLIC_HEALTH_PERFORMANCE = 13;
    // 人才储备绩效
    const TALENT_RESERVE_PERFORMANCE = 14;
    // 业务恢复绩效
    const BUSINESS_RECOVERY_PERFORMANCE = 15;
}

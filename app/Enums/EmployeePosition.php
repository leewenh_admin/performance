<?php

namespace App\Enums;

// 职员行政职务
class EmployeePosition {
    // 院长
    const DEAN = 1;
    // 副院长
    const SUB_DEAN = 2;
    // 院长助理
    const DEAN_AIDE = 3;
    // 科主任
    const DEPARTMENT_HEAD = 4;
    // 副主任
    const DEPARTMENT_SUB_HEAD = 5;
    // 护士长
    const NURESE_HEAD = 6;
    // 职员
    const STAFF = 7;
    // 干事
    const GANSHI = 8;
    // 工会主席
    const GHZX = 9;
    // 纪委书记
    const JWSJ = 9;
}

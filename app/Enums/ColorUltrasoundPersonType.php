<?php

namespace App\Enums;

// 彩超人员类别
class ColorUltrasoundPersonType {
    // 体检
    const PHYSICAL_EXAM = 1;
    // 门诊
    const OUTPATIENT = 2;
    // 住院
    const INPATIENT = 3;
}

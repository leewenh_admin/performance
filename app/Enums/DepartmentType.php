<?php

namespace App\Enums;

// 科室类别
class DepartmentType {
    # 临床科室
    const CLINICAL = 11;
    # 医技科室
    const MEDICAL_TECHNICAL = 12;
    # 职能科室
    const FUNCTIONAL = 13;

    # 病区科室
    const WARD = 21;
    # 门诊科室
    const OUTPATIENT = 22;

    # 非手术科室(内科)
    const MEDICINE = 31;
    # 手术科室(外科)
    const SURGICAL = 32;
}

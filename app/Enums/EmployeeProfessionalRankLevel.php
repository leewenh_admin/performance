<?php

namespace App\Enums;

// 职称级别
class EmployeeProfessionalRankLevel {
    // 无
    const NONE = 0;
    // 初级
    const JUNIOR = 1;
    // 中级
    const MIDDLE = 2;
    // 副高
    const DEPUTY_SENIOR = 3;
    // 正高
    const SENIOR = 4;
    // 返聘
    const REEMPLOY = 5;
}

<?php

namespace App\Enums;

// 职称分类
class EmployeeProfessionalRankType {
    // 护理
    const NURSE = 1;
    // 医疗
    const MEDICAL_TREATMENT = 2;
    // 经统会
    const ECONOMIC = 3;
    // 医技
    const MEDICAL_SKILL = 4;
    // 药剂
    const MEDICAMENT = 5;
    // 工勤
    const WORKER = 6;
}

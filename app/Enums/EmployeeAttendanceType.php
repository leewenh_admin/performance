<?php

namespace App\Enums;

// 职员考勤类别
class EmployeeAttendanceType {
    // 无
    const NONE = 0;
    // 医生
    const DOCTOR = 1;
    // 护理
    const NURSE = 2;
    // 技师
    const TECHNICIAN = 3;
    // 行管
    const MANAGER = 4;
    // 药师
    const PHARMACIST = 5;
}

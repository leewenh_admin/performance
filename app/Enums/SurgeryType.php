<?php

namespace App\Enums;

// 材料类型
class SurgeryType {
    // 不加急
    const NORMAL = 1;
    // 加急
    const URGENT = 2;

}

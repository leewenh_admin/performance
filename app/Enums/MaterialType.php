<?php

namespace App\Enums;

// 材料类型
class MaterialType {
    // 总务材料
    const GENERAL = 1;
    // 卫生材料
    const SANITARY = 2;

}

<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 2021/3/8
 * Time: 16:28
 */

namespace App\Exceptions;

use App\Http\Controllers\Traits\ApiResponse;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiHandler {
    use ApiResponse;

    protected $request;

    protected $exception;

    public $doReport = [
        AuthenticationException::class => array('登录信息失效,请退出后重新登录', ApiCodeConstants::NOT_LOGIN),
        ModelNotFoundException::class => array('该模型未找到', ApiCodeConstants::ERROR),
        NotFoundHttpException::class => array('该接口未找到', ApiCodeConstants::ERROR)
    ];

    public function __construct(Request $request, Exception $exception) {
        $this->request = $request;
        $this->exception = $exception;
    }

    public static function make(Exception $e) {
        return new static(request(), $e);
    }

    public function render() {
        Log::warning($this->exception->getMessage(), \request()->all());
        if ($this->exception instanceof ValidationException) {
            return $this->handleValidationExceptionToResponse($this->exception);
        }

        foreach (array_keys($this->doReport) as $report) {
            if ($this->exception instanceof $report) {
                $message = $this->doReport[$report];
                return $this->failed($message[0], $message[1]);
            }

            if ($this->exception instanceof QueryException) {
                return $this->failed('查询异常', ApiCodeConstants::Fail);
            }

            if ($this->exception instanceof MessageException) {
                return $this->failed($this->exception->getMessage(), ApiCodeConstants::Fail);
            }

            if ($this->exception instanceof FatalErrorException) {
                return $this->failed('服务器错误', ApiCodeConstants::Fail);
            }

            if ($this->exception instanceof  \ErrorException) {
                return $this->failed($this->exception->getMessage(), ApiCodeConstants::Fail);
            }
            if ($this->exception instanceof MethodNotAllowedHttpException) {
                return $this->failed('请求方法错误', ApiCodeConstants::Fail);
            }
            return $this->failed($this->exception->getMessage(), $this->exception->getCode());
        }
    }


    private function handleValidationExceptionToResponse(ValidationException $exception) {
        return $this->status('error', [
            'message' => implode(',', collect($exception->errors())->first()),
            'message1' => $exception->getMessage(),
            'error' => $exception->errors()
        ], ApiCodeConstants::Fail);
    }
}

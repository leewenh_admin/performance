<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 2021/3/9
 * Time: 11:16
 */

namespace App\Exceptions;

use Throwable;

class MessageException extends \Exception {
    public function __construct(string $message = "", int $code = ApiCodeConstants::Fail, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

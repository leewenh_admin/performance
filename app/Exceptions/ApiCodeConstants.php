<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 2021/3/8
 * Time: 17:30
 */

namespace App\Exceptions;

class ApiCodeConstants {
    //成功
    const SUCCESS = 200;

    //失败
    const Fail = 400;

    //登录授权失效
    const NOT_LOGIN = 401;

    //服务器错误
    const ERROR = 500;
}

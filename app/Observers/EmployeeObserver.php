<?php

namespace App\Observers;

use Illuminate\Support\Facades\Hash;
use App\Models\Employee;
use App\Models\EmployeePassword;

class EmployeeObserver
{
    public function created(Employee $employee)
    {
        // 创建人员信息后设定初始密码
        $pwd_obj = EmployeePassword::where('code', $employee->code)->first();
        if (empty($pwd_obj)) {
            EmployeePassword::create([
                'code' => $employee->code,
                'password' => Hash::make('88888888')
            ]);
        }
    }

}

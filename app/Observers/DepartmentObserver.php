<?php

namespace App\Observers;

use App\Models\Department;


class DepartmentObserver
{
    public function creating(Department $department)
    {
        $department->medical_point_price = $department->point_price ?? 0;
    }

    public function updating(Department $department)
    {
        $department->medical_point_price = $department->point_price ?? 0;
    }

}

<?php

namespace App\Providers;

use App\Models\Department;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use App\Models\Employee;
use App\Observers\EmployeeObserver;
use App\Observers\DepartmentObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::listen(function($query) {
            if (env('DEBUG_LOG_SQL', false)) {
                Log::info(
                    $query->sql,
                    $query->bindings,
                    $query->time
                );
            }
        });
        Employee::observe(EmployeeObserver::class);
        Department::observe(DepartmentObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

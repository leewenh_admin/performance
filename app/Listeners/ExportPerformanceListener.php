<?php

namespace App\Listeners;

use App\Events\ExportPerformanceEvent;
use Illuminate\Support\Facades\Cache;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class ExportPerformanceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExportPerformanceEvent  $event
     * @return void
     */
    public function handle(ExportPerformanceEvent $event)
    {
        if (Cache::get('generate')){
            return ;
        }
        Cache::put('generate', 'exportPerformance', 60);
        try {
            $data = $event->repository->export($event->date);
            if (!is_dir(public_path("export"))){
                mkdir(public_path("export"));
            }
            $zip_file = public_path("export/管理绩效明细{$event->date}.zip");
            @unlink($zip_file);
            $zip = new \ZipArchive();
            $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

            $path = storage_path('excel/exports/'.$data);
            $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
            foreach ($files as $name => $file)
            {
                // 我们要跳过所有子目录
                if (!$file->isDir()) {
                    $filePath     = $file->getRealPath();

                    // 用 substr/strlen 获取文件扩展名
                    $relativePath = $data.'/' . substr($filePath, strlen($path) + 1);

                    $zip->addFile($filePath, $relativePath);
                }
            }
            $zip->close();
            delDirAndFile($path);
            Cache::forget('generate');
        }catch (\Exception $exception){
            Cache::forget('generate');
            Log::error($exception->getMessage().'--'.$exception->getFile().'--'.$exception->getLine());
        }

    }
}

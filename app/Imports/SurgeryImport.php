<?php

namespace App\Imports;

use App\Models\Surgery;
use Maatwebsite\Excel\Facades\Excel;

class SurgeryImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){
                if (empty($row["姓名"])) {
                    return;
                }
                $department = $this->getOrCreateDepartment(
                    $row["科室"]
                );
                if (empty($department)){
                    return ;
                }
                $request_doctor = $this->getOrCreateEmployee(
                    $row["申请医师"], $row["工号"]
                );
    
                Surgery::create([
                    "date" => request("date", date("Y-m")),
                    "sub_departments_id" => $department->id,
                    "inpatient_code" => $row["住院号"],
                    "inpatient_name" => $row["姓名"],
                    "schedule_time" => (string) $row["预定时间"],
                    "diagnose_before" => $row["术前诊断"],
                    "diagnose_after" => $row["术后诊断"],
                    "request_time" => (string) $row["申请时间"],
                    "request_doctor_id" => $request_doctor->id,
                    "surgery_time" => (string) $row["手术日期"],
                ]);
            });
        });
    }
}
<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\EmployeePayroll;
use App\Models\Employee;
use App\Enums\EmployeeType;

class EmployeePayrollImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["姓名"])) {
                    return;
                }
                /* 
                $department = $this->getOrCreateDepartment($row["部门"]);
                $employee_type = [
                    "实名编制人员" => EmployeeType::REGULAR,
                    "编制备案人员" => EmployeeType::PREPARE,
                    "招聘人员" => EmployeeType::CONTRACT,
                    "自聘人员" => EmployeeType::SELF_CONTRACT,
                ];
                $employee = Employee::firstOrCreate(
                    ["name" => trim($row["姓名"])],
                    [
                        "sub_departments_id" => $department->id,
                        "code" => $row["人员编号"],
                        "type" => $employee_type[
                            trim($row["人员类别"])
                        ],
                        "position" => 0
                    ]
                ); 
                */

                $employee = $this->getOrCreateEmployee(
                    trim($row["姓名"]),
                    $row["人员编号"]
                );
                
                EmployeePayroll::updateOrCreate(
                    [
                        "date" => request("date", date("Y-m")),
                        "employees_id" => $employee->id,
                    ],
                    [
                        "bank_code" => $row["银行账号"] ?? null,
                        "post_salary" => isset($row["岗位工资"]) ? (float) $row["岗位工资"] : 0,
                        "level_salary" => isset($row["薪级工资"]) ? (float) $row["薪级工资"] : 0,
                        "standard_performance" => isset($row["标准绩效"]) ? (float) $row["标准绩效"] : 0,
                        "nurse_subsidy" => isset($row["护贴"]) ? (float) $row["护贴"] : 0,
                        "car_subsidy" => isset($row["车贴"]) ? (float) $row["车贴"] : 0,
                        "health_subsidy" => isset($row["卫生津贴"]) ? (float) $row["卫生津贴"] : 0,
                        "labour_safety_subsidy" => isset($row["卫生劳保费"]) ? (float) $row["卫生劳保费"] : 0,
                        "total_payable" => isset($row["应发合计"]) ? (float) $row["应发合计"] : 0,
                        "endowment_insurance" => isset($row["养老保险"]) ? (float) $row["养老保险"] : 0,
                        "occupation_pension" => isset($row["职业年金"]) ? (float) $row["职业年金"] : 0,
                        "medical_insurance" => isset($row["医疗保险"]) ? (float) $row["医疗保险"] : 0,
                        "unemployment_insurance" => isset($row["失业保险"]) ? (float) $row["失业保险"] : 0,
                        "house_fund" => isset($row["住房公积金"]) ? (float) $row["住房公积金"] : 0,
                        "insurance_patch" => isset($row["补扣保险"]) ? (float) $row["补扣保险"] : 0,
                        "deduct" => isset($row["扣款"]) ? (float) $row["扣款"] : 0,
                        "utilities" => isset($row["水电费"]) ? (float) $row["水电费"] : 0,
                        "other" => isset($row["其它"]) ? (float) $row["其它"] : 0,
                        "union_fee" => isset($row["会费"]) ? (float) $row["会费"] : 0,
                        "total_deduct" => isset($row["扣款合计"]) ? (float) $row["扣款合计"] : 0,
                        "paid_salary" => isset($row["工资实发"]) ? (float) $row["工资实发"] : 0,
                    ]
                );
            });
        });
    }
}
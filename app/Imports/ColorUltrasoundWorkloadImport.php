<?php

namespace App\Imports;

use App\Enums\ColorUltrasoundPersonType;
use App\Models\ColorUltrasoundWorkload;
use Maatwebsite\Excel\Facades\Excel;

class ColorUltrasoundWorkloadImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){

                if ($row["开单科室编号"] ?? null){
                    $request_department = $this->getDepartmentByCode(2, $row["开单科室编号"]);
                } else {
                    $request_department = $this->getOrCreateDepartment(
                        $row["开单科室"]
                    );
                }

                if (empty($request_department)){
                    return ;
                }
                $request_doctor = $this->getOrCreateEmployee(
                    $row["开单医生"], $row["开单医生工号"]
                );

                if ($row["执行科室编号"] ?? null){
                    $exec_department = $this->getDepartmentByCode(2, $row["执行科室编号"]);
                } else {
                    $exec_department = $this->getOrCreateDepartment(
                        $row["执行科室"]
                    );
                }

                if (empty($exec_department)){
                    return ;
                }
                $person_type = [
                    "体检" => ColorUltrasoundPersonType::PHYSICAL_EXAM,
                    "门诊" => ColorUltrasoundPersonType::OUTPATIENT,
                    "住院" => ColorUltrasoundPersonType::INPATIENT,
                ];
    
                ColorUltrasoundWorkload::create([
                    "date" => request("date", date("Y-m")),
                    "card" => $row["卡号"] ?? '无',
                    "request_note" => $row["申请单"] ?? '无',
                    "check_code" => $row["检查编号"] ?? '无',
                    "check_name" => $row["检查项目"] ?? '无',
                    "request_department_id" => $request_department->id,
                    "request_doctor_id" => $request_doctor->id,
                    "execute_department_id" => $exec_department->id,
                    "check_date" => $row["检查日期"] ?? '无',
                    "fee" => isset($row["费用"]) ? (float) $row["费用"] : 0,
                    "person_type" => array_key_exists($row["人员类型"], $person_type) ? $person_type[$row["人员类型"]] : 0,
                ]); 
            });
        });
    }
}
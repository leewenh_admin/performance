<?php

namespace App\Imports;

use App\Models\SurgicalPointValue;
use Maatwebsite\Excel\Facades\Excel;

class SurgicalPointValueImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){
                if (empty($row["项目"])) {
                    return;
                }

                if ($row["所在科室编号"] ?? null){
                    $belong_department = $this->getDepartmentByCode(2, $row["所在科室编号"]);
                } else {
                    $belong_department = $this->getOrCreateDepartment(
                        $row["所在科室"]
                    );
                }

                if (empty($belong_department)){
                    $belong_department_id = 1;
                } else {
                    $belong_department_id = $belong_department->id;
                }

                if ($row["执行科室编号"] ?? null){
                    $exec_department = $this->getDepartmentByCode(2, $row["执行科室编号"]);
                } else {
                    $exec_department = $this->getOrCreateDepartment(
                        $row["执行科室"]
                    );
                }

                if (empty($exec_department)){
                    return ;
                }
                if ($row["难度风险权重"] == "#VALUE!") {
                    $difficulty_risk_weights = 0;
                } else {
                    $difficulty_risk_weights = round($row["难度风险权重"], 2);
                }

                if ($row["绩效权重"] == "#VALUE!") {
                    $performance_weights = 0;
                } else {
                    $performance_weights = round($row["绩效权重"], 2);
                }

                SurgicalPointValue::updateOrCreate(
                    [
                        "code" => $row["编码"],
                    ],
                    [
                        "belong_department_id" => $belong_department_id,
                        "execute_department_id" => $exec_department->id,
                        "project" => $row["项目"],
                        "gb_code" => $row["国标码"],
                        "price" => $row["价格"],
                        "price_type" => $row["类别"],
                        "project_type" => $row["分类"],
                        "project_code" => $row["项目编码"],
                        "name" => $row["项目名称"],
                        "project_include" => $row["项目内涵"],
                        "include_consumable" => $row["内涵一次性耗材"],
                        "exclude_consumable" => $row["除外内容"],
                        "lowvalue_consumable" => $row["低值耗材"] ?? 0,
                        "consume_explain" => $row["基本人力消耗及耗时"],
                        "technical_difficulty1" => $row["技术难度1"] ?? "0",
                        "risk_degree1" => $row["风险程度1"] ?? "0",
                        "charge_unit" => $row["计价单位"],
                        "charge_explain" => $row["计价说明"],
                        "duration" => $row["时间"] ?? 0,
                        "technical_difficulty2" => $row["技术难度2"] ?? 0,
                        "risk_degree2" => $row["风险程度2"] ?? 0,
                        "difficulty_risk_weights" => $difficulty_risk_weights,
                        "duration_weights" => round($row["时间权重"], 2),
                        "performance_weights" => $performance_weights,
                ]);
            });
        });
    }
}
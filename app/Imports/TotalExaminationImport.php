<?php

namespace App\Imports;

use App\Models\TotalExamination;
use Maatwebsite\Excel\Facades\Excel;

class TotalExaminationImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){
                if (empty($row["科室名称"])){
                    return ;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        $row["科室名称"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                TotalExamination::create([
                    "date" => request("date", date("Y-m")),
                    "sub_departments_id" => $department->id,
                    "total_quantity" => $row["检验合计"]
                ]);
            });
        });
    }
}
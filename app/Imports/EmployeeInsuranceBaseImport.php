<?php

namespace App\Imports;

use App\Models\Employee;
use App\Models\EmployeeInsuranceBase;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeInsuranceBaseImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $results = $reader->get();
            $results->each(function($row){
                $employee = $this->getOrCreateEmployee($row["姓名"], $row["工号"]);
                $record = EmployeeInsuranceBase::where(
                    "employees_id", $employee->id
                )->first();

                if ($record) {
                    $record->endowment_insurance = isset($row["养老保险基数"]) ? (float) $row["养老保险基数"] : 0;
                    $record->occupation_pension = isset($row["职业年金基数"]) ? (float) $row["职业年金基数"] : 0;
                    $record->four_insurance = isset($row["四险基数"]) ? (float) $row["四险基数"] : 0;
                    $record->save();
                } else {
                    EmployeeInsuranceBase::updateOrCreate(
                        [
                            "employees_id" => $employee->id,
                        ],
                        [
                            "name" => $row["姓名"],
                            "code" => $row["工号"],
                            "endowment_insurance" => isset($row["养老保险基数"]) ? (float) $row["养老保险基数"] : 0,
                            "occupation_pension" => isset($row["职业年金基数"]) ? (float) $row["职业年金基数"] : 0,
                            "four_insurance" => isset($row["四险基数"]) ? (float) $row["四险基数"] : 0,
                        ]
                    );
                }

            });
        });
    }
}

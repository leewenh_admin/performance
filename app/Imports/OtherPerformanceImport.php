<?php

namespace App\Imports;

use App\Models\OtherPerformance;
use Maatwebsite\Excel\Facades\Excel;

class OtherPerformanceImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["工号"]) || empty($row["姓名"])) {
                    return;
                }


                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        $row["科室"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                OtherPerformance::create([
                    "date" => request("date", date("Y-m")),
                    "code" => $row["工号"],
                    "name" => $row["姓名"],
                    "sub_department_id" => $department->id,
                    "new_years_day" => $row["元旦天数"] ?? 0,
                    "spring_festival_day" => $row["春节天数"] ?? 0,
                    "days" => $row["天数"] ?? 0,
                    "amount" => $row["金额"],
                    "description" => $row["备注"],

                ]);

            });
        });
    }
}
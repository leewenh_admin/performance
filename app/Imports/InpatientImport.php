<?php

namespace App\Imports;

use App\Models\Inpatient;
use Maatwebsite\Excel\Facades\Excel;

class InpatientImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室名称"])){
                    return ;
                }
                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        $row["科室名称"]
                    );
                }


                if (empty($department)){
                    return ;
                }
                Inpatient::create([
                    "date" => request("date", date("Y-m")),
                    "sub_departments_id" => $department->id,
                    "end_beds" => isset($row["期末实有床位"]) ? (float) $row["期末实有床位"] : 0,
                    "start_in_hospital" => isset($row["期初在院"]) ? (float) $row["期初在院"] : 0,
                    "admission" => isset($row["入院人数"]) ? (float) $row["入院人数"] : 0,
                    "transfer_in" => isset($row["转入人数"]) ? (float) $row["转入人数"] : 0,
                    "leave" => isset($row["出院人数"]) ? (float) $row["出院人数"] : 0,
                    "cure" => isset($row["治愈人数"]) ? (float) $row["治愈人数"] : 0,
                    "better" => isset($row["好转人数"]) ? (float) $row["好转人数"] : 0,
                    "unsettled" => isset($row["未愈人数"]) ? (float) $row["未愈人数"] : 0,
                    "dead" => isset($row["死亡人数"]) ? (float) $row["死亡人数"] : 0,
                    "other" => isset($row["其他人数"]) ? (float) $row["其他人数"] : 0,
                    "transfer_out" => isset($row["转出"]) ? (float) $row["转出"] : 0,
                    "end_in_hospital" => isset($row["期末在院"]) ? (float) $row["期末在院"] : 0,
                ]);
            });
        });
    }
}
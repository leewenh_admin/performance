<?php

namespace App\Imports;

use App\Models\PublicHealthPerformance;
use Maatwebsite\Excel\Facades\Excel;

class PublicHealthPerformanceImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["工号"])) {
                    return;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        $row["科室"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                PublicHealthPerformance::create([
                    "date" => request("date", date("Y-m")),
                    "code" => $row["工号"],
                    "name" => $row["姓名"],
                    "sub_department_id" => $department->id,
                    "amount" => $row["金额"],
                    "description" => $row["备注"],

                ]);

            });
        });
    }
}
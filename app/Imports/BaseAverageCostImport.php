<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;

class BaseAverageCostImport extends BaseImport {

    /**
     * 门诊例均费用基准值
     */
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室名称"])) {
                    return;
                }
                Department::where("name", $row["科室名称"])->update([
                    "target_average_cost" => round($row["门诊例均费用"], 2),
                    "target_patient_count" => $row["门诊总人次"]?? 0,
                ]);

            });
        });
    }

}
<?php

namespace App\Imports;

use App\Models\TotalSpecialInspectionPharmacy;
use Maatwebsite\Excel\Facades\Excel;

class TotalSpecialInspectionPharmacyImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室名称"])){
                    return ;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        $row["科室名称"]
                    );
                }


                if (empty($department)){
                    return ;
                }
                TotalSpecialInspectionPharmacy::create([
                    "date" => request("date", date("Y-m")),
                    "sub_departments_id" => $department->id,
                    "ecg" => $row["心电图"] ?? 0,
                    "eeg" => $row["脑电图"] ?? 0,
                    "long_range_ecg" => $row["长程心电图"] ?? 0,
                    "pulmonary_function" => $row["肺功能"] ?? 0,
                    "moveable_plate" => $row["活动平板"] ?? 0,
                    "colonoscope" => $row["结肠镜"] ?? 0,
                    "gastroscope" => $row["胃镜"] ?? 0,
                    "mammogram" => $row["乳透"] ?? 0,
                    "carbon_breath_test" => $row["碳呼吸试验"] ?? 0,
                    "gastroscope_other" => $row["胃镜其他"] ?? 0,
                    "bronchoscopy" => $row["纤支镜检查"] ?? 0,
                    "bronchoscopy_other" => $row["纤支镜其他"] ?? 0,
                    "total_prescription" => $row["处方合计"] ?? 0,
                    "outpatient_western_medicine_prescription" => $row["门诊西药处方"] ?? 0,
                    "hospitalization_western_medicine_prescription" => $row["住院西药处方"] ?? 0,
                    "outpatient_chinese_medicine_prescription" => $row["门诊中药处方"] ?? 0
                ]);
            });
        });
    }
}
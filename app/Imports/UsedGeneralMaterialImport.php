<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Enums\MaterialType;
use App\Models\Department;
use App\Models\UsedMaterial;

class UsedGeneralMaterialImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();

        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["部门名称"])) {
                    return;
                }

                if ($row["部门编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["部门编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        $row["部门名称"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                UsedMaterial::updateOrCreate(
                    [
                        "date" => request("date", date("Y-m")),
                        "sub_departments_id" => $department->id,
                        "material_type" => MaterialType::GENERAL
                    ],
                    [
                        "total_amount" => $row["汇总金额"],
                    ]
                );

            });
        });
    }
}
<?php

namespace App\Imports;

use App\Enums\SurgeryType;
use App\Models\SurgeryNew;
use Maatwebsite\Excel\Facades\Excel;

class SurgeryNewImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["姓名"])) {
                    return;
                }


                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        $row["科室"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                if ($row["类别"] == "不加急"){
                    $type = SurgeryType::NORMAL;
                } elseif ($row["类别"] == "加急"){
                    $type = SurgeryType::URGENT;
                }
                $row["编号"] = str_replace(" \n", ',', $row["编号"]);
                $row["手术名称"] = str_replace(" \n", ',', $row["手术名称"]);
                $row["手术级别"] = str_replace(" \n", ',', $row["手术级别"]);
                $row["手术医师"] = str_replace(" \n", ',', $row["手术医师"]);
                $row["手术一助"] = str_replace(" \n", ',', $row["手术一助"]);
                $row["手术二助"] = str_replace(" \n", ',', $row["手术二助"]);
                $row["麻醉医师"] = str_replace(" \n", ',', $row["麻醉医师"]);
                $row["手术护士"] = str_replace(" \n", ',', $row["手术护士"]);

    
                SurgeryNew::create([
                    "date" => request("date", date("Y-m")),
                    "surgery_time" => $row["手术时间"],
                    "sub_departments_id" => $department->id,
                    "inpatient_code" => $row["住院号"],
                    "record_code" => $row["档案号"],
                    "bed_code" => $row["床号"],
                    "inpatient_name" => $row["姓名"],
                    "age" => $row["年龄"],
                    "surgery_type" =>  $type,
                    "code" => $row["编号"],
                    "title" =>  $row["手术名称"],
                    "level" =>  $row["手术级别"],
                    "incision" =>  $row["切口"],
                    "execute_doctor" =>  $row["手术医师"],
                    "surgery_assistant1" =>  $row["手术一助"],
                    "surgery_assistant2" =>  $row["手术二助"],
                    "anesthesia_doctor" =>  $row["麻醉医师"],
                    "anesthesia_type" =>  $row["麻醉方式"],
                    "surgery_nurse" =>  $row["手术护士"],
                    "clinical_diagnose" =>  $row["临床诊断"],
                    "HIV" =>  $row["hiv"],
                    "HCV" =>  $row["hcv"],
                    "HBSAG" =>  $row["hbsag"],
                    "syphilis" =>  $row["梅毒"],
                ]);
            });
        });
    }
}
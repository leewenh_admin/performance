<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\NightPerformance;

class NightPerformanceImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["归属科室"])) {
                    return;
                }
                if ($row["归属科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(1, $row["归属科室编号"]);
                } else {
                    $department = $this->getOrCreateLv1Department(
                        $row["归属科室"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                NightPerformance::create([
                    "date" => request("date", date("Y-m")),
                    "department_id" => $department->id,
                    "sub_department_name" => $row["科室"],
                    "department_type" => $row["科室类别"],
                    "daily_fixed_number" => $row["每日固定人数"] ?? 0,
                    "before_night_number" => $row["护理人员"] ?? 0,
                    "before_night_price" => $row["上夜班标准"] ?? 0,
                    "after_night_number" => $row["下夜班人数"] ?? 0,
                    "after_night_price" => $row["下夜班标准"] ?? 0,
                    "whole_night_number" => $row["整夜班人数"] ?? 0,
                    "whole_night_price" => $row["整夜班标准"] ?? 0,
                    "month_day" => $row["月天数"] ?? 0,
                    "dispatch_amount" => $row["劳务派遣人员夜班"] ?? 0,
                    "float_performance" => $row["浮动夜班绩效"] ?? 0,
                    "total_night_performance" => $row["实发夜班绩效合计"] ?? 0,
                ]);

            });
        });
    }
}
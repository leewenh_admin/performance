<?php

namespace App\Imports;

use App\Models\TotalMedicalImage;
use Maatwebsite\Excel\Facades\Excel;

class TotalMedicalImageImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){
                if (empty($row["科室名称"])){
                    return ;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        $row["科室名称"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                TotalMedicalImage::create([
                    "date" => request("date", date("Y-m")),
                    "sub_departments_id" => $department->id,
                    "radiate_person" => $row["放射人次"] ?? 0,
                    "radiate_count" => $row["拍片"] ?? 0,
                    "radiate_side_bed" => $row["床边拍片"] ?? 0,
                    "barium_meal" => $row["钡餐"] ?? 0,
                    "molybdenum_palladium" => $row["钼钯"] ?? 0,
                    "ct" => $row["ct"] ?? 0,
                    "mri" => $row["mri"] ?? 0,
                    "color_ultrasound_person" => $row["彩超检查人次"] ?? 0,
                    "tcd" => $row["tcd"] ?? 0,
                    "pathological_examination" => $row["病理检查"] ?? 0,
                    "tct" => $row["tct"] ?? 0,
                    "frozen_section" => $row["冰冻切片"] ?? 0,
                    "specific_stain" => $row["特异染色"] ?? 0,
                    "pathological_hpv" => $row["病理hpv"] ?? 0,
                    "radiotherapy_person" => $row["放疗人次"] ?? 0,
                    "interventional_angiography" => $row["介入造影"] ?? 0,
                    "interventional_surgery" => $row["介入手术"] ?? 0,
                ]);
            });
        });
    }
}
<?php

namespace App\Imports;

use App\Exceptions\MessageException;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;
use App\Models\SubDepartment;
use App\Models\Employee;

class BaseImport {
    public $model = null;

    public function getFile()
    {

        $file = Input::file('file');
        $random = str_random(10);
        $ext = $file->getClientOriginalExtension();
        $name = $random.".".$ext;

        $filename = $file->storeAs("upload/excel", $name);

        return storage_path("app/" . $filename);
    }

    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->skip(14);
            $reader->setHeaderRow(1);
            $results = $reader->get();
            dd($results[0]);

        });
    }

    public function getOrCreateDepartment($name) {
        $department = SubDepartment::where(
            "name", $name ?? "系统"
        )->first();

//        if (!$department){
//            $departments = SubDepartment::get()->pluck('name')->all();
//            $same = $this->getSameName($departments, $name);
//            $department = SubDepartment::where("name", $same[0] ?? "系统")->first();
//        }

        return $department;

    }

    public function getOrCreateLv1Department($name) {
        $department = Department::where(
            "name", $name ?? "系统"
        )->first();

        return $department;
    }

    public function getOrCreateEmployee($name, $code) {
        $name = $name ?? "管理员";
        $code = $code ?? 0;
        if ($code == 1100) {
            $code = 9644;
        }
        $employee = Employee::where(
            "code", $code
        )->first();

        if ($employee) {
            return $employee;
        } else {
            return Employee::Create([
                'name' => $name,
                'sub_departments_id' => 1,
                'code' => $code,
                'type' => 0,
                'position' => 0,
                'birth_date' => '2019-01-01',
                'join_date' => '2019-01-01'
            ]);

        }
    }

    public function getSameName($array_name, $name){
        $result = array();
        $i = 0;
        foreach ($array_name as $department){
            $percent = similar_text($department, $name);
            $result[$i]['name'] = $department;
            $result[$i]['percent'] = $percent;
            $i++;
        }
        $last_names = array_column($result,'percent');
        array_multisort($last_names,SORT_DESC,$result);
        if (count($result) > 1){
            return array_column(array_slice($result, 0, 1), 'name');
        }

        return array_column($result, 'name');
    }


    /**
     * 根据科室编号获取科室
     * @param $level
     * @param $code
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getDepartmentByCode($level, $code){
        if ($level === 1){
            return Department::query()->where("code", $code)->first();
        } else {
            return SubDepartment::query()->where("code", $code)->first();
        }
    }
}

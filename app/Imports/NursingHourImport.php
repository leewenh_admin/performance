<?php

namespace App\Imports;

use App\Models\NursingHour;
use Maatwebsite\Excel\Facades\Excel;

class NursingHourImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            // $reader->setHeaderRow(3);
            // $reader->setSelectedSheetIndices([2]);
            // $reader->skip(1);
            $results = $reader->get();

            $results->each(function($row){

                $department = $this->getOrCreateDepartment($row["科室"]);
                if (empty($department)){
                    return ;
                }
                NursingHour::create([
                    "date" => request("date", date("Y-m")),
                    "sub_departments_id" => $department->id,
                    "part_result" => $row["本月病房护理等级分部结果"] ?? 0,
                    "special_class_count" => $row["本月特级护理人数"] ?? 0,
                    "special_class_hour" => $row["特级护理耗时"] ?? 0,
                    "one_class_count" => $row["本月一级护理人数"] ?? 0,
                    "one_class_hour" => $row["一级护理耗时"] ?? 0,
                    "two_class_count" => $row["本月二级护理人数"] ?? 0,
                    "two_class_hour" => $row["二级护理耗时"] ?? 0,
                    "three_class_count" => $row["本月三级护理人数"] ?? 0,
                    "three_class_hour" => $row["三级护理耗时"] ?? 0,
                ]);
            });
        });
    }
}
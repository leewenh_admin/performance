<?php

namespace App\Imports;

use App\Enums\DepartmentType;
use App\Models\DepartmentManagementCoefficient;
use Maatwebsite\Excel\Facades\Excel;

class DepartCoefficientImport extends BaseImport {

    /**
     * 科室管理系数
     */
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["行标签"])) {
                    return;
                }

                $department = $this->getOrCreateLv1Department(
                    $row["行标签"]
                );
                if (empty($department)){
                    return ;
                }

                if($row["系列"] == "医技"){
                    $level_type = DepartmentType::MEDICAL_TECHNICAL;
                } elseif ($row["系列"] == "临床"){
                    $level_type = DepartmentType::CLINICAL;
                } elseif ($row["系列"] == "职能"){
                    $level_type = DepartmentType::FUNCTIONAL;
                } else {
                    $level_type = 0;
                }

                DepartmentManagementCoefficient::updateOrCreate(
                    [
                        "date" => request("date", date("Y-m")),
                        "department_id" => $department->id,
                    ],
                    [
                        "post_coefficient" => isset($row["岗位系数"]) ? (float) $row["岗位系数"] : 0,
                        "employees_number" => isset($row["员工数"]) ? (float) $row["员工数"] : 0,
                        "type_lv1" => $level_type,
                        "scale_coefficient" => isset($row["规模系数"]) ? (float) $row["规模系数"] : 0,
                        "college_level" => $row["专科级别"] ??  '',
                        "brand_coefficient" => isset($row["品牌系数"]) ? (float) $row["品牌系数"] : 0,
                        "expert_number_rate" => isset($row["专家号绩效占比"]) ? (float) $row["专家号绩效占比"] : 0,
                    ]);

            });
        });
    }

}
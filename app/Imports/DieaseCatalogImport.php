<?php

namespace App\Imports;

use App\Models\DieaseCatalog;
use Maatwebsite\Excel\Facades\Excel;

class DieaseCatalogImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);

            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["疾病编码"]) || empty($row["疾病名称"]) || empty($row["权重"])) {
                    return ;
                }

                DieaseCatalog::updateOrCreate([
                    "code" => $row["疾病编码"]
                ],[
                    "name" => $row["疾病名称"],
                    "weights" => (float) $row["权重"] ,
                ]);
            });

        });
    }
}
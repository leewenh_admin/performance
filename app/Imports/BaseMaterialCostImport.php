<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;

class BaseMaterialCostImport extends BaseImport {

    /**
     * 目标出院病人均例耗材费用基准值
     */
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室名称"])) {
                    return;
                }
                Department::where("name", $row["科室名称"])->update([
                    "target_average_material_cost" => round($row["例均耗材费用"], 2),
                ]);

            });
        });
    }

}
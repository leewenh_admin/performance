<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;

class BaseServiceIncomeImport extends BaseImport {
    /**
     * 医疗服务收入占比导入
     */
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室名称"])) {
                    return;
                }
                Department::where("name", $row["科室名称"])->update([
                    "service_income_target_value" => round($row["医疗服务收入占比"], 2),
                ]);

            });
        });
    }


}
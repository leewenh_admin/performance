<?php

namespace App\Imports;

use App\Models\BaseEmployeeAttendance;
use App\Models\ClinicalAttendanceScore;
use App\Models\MedicalAttendanceScore;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeAttendanceScoreImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室"])) {
                    return;
                }

                if(strstr($row["科室"], '合计') || strstr($row["科室"], '医技科室')){
                    return;
                }


                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(1, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateLv1Department(
                        make_semiangle($row["科室"])
                    );
                }

                if (empty($department)){
                    return ;
                }

                MedicalAttendanceScore::updateOrCreate(
                    [
                        "date" => request("date", date("Y-m")),
                        "departments_id" => $department->id,
                    ],
                    [
                        "employees_count" => isset($row["人数"]) ? (float) $row["人数"] : 0,
                        "medical_quality" => isset($row["医疗质量维度"]) ? (float) $row["医疗质量维度"] : 0,
                        "care_quality" => isset($row["护理质量维度"]) ? (float) $row["护理质量维度"] : 0,
                        "medical_ethics_quality" => isset($row["医德医风维度"]) ? (float) $row["医德医风维度"] : 0,
                        "health_management" => isset($row["医保管理"]) ? (float) $row["医保管理"] : 0,
                        "scientific_research" => isset($row["科研教学"]) ? (float) $row["科研教学"] : 0,
                        "workload_management" => isset($row["工作量管理"]) ? (float) $row["工作量管理"] : 0,
                        "total_score" => isset($row["得分"]) ? (float) $row["得分"] : 0,
                        "comprehensive_performance" => isset($row["综合目标绩效"]) ? (float) $row["综合目标绩效"] : 0,
                    ]
                );
            });

        });

    }
}
<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;
use App\Models\Employee;
use App\Models\ElectricCost;
use App\Enums\EmployeeType;

class ElectricCostImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["序号"])) {
                    return;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        $row["科室名称"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                ElectricCost::create(
                    [
                        "date" => request("date", date("Y-m")),
                        "sub_departments_id" => $department->id,
                        "location" => $row["地点"] ?? null,
                        "used_for" => $row["用途"] ?? null,
                        "start_code" => isset($row["期初码"]) ? (float) $row["期初码"] : 0,
                        "end_code" => isset($row["期末码"]) ? (float) $row["期末码"] : 0,
                        "rate" => isset($row["倍数"]) ? (float) $row["倍数"] : 1,
                        "dosage" => isset($row["电量"]) ? (float) $row["电量"] : 0,
                    ]
                );
            });
        });
    }
}
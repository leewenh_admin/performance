<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;

class BaseBedValueImport extends BaseImport {

    /**
     * 平均住院日、出院药品例均费用、出院例均费用基准值
     */
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->skip(4);
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室名称"])) {
                    return;
                }
                Department::where("name", $row["科室名称"])->update([
                    "target_in_hospital_day" => round($row["出院者平均住院日"], 1),
                    "target_average_drug_cost" => round($row["出院病人药品例均费用"], 2),
                    "target_average_leave_cost" => round($row["出院者人均医疗费用"], 2),
                    "base_use_bed_day" => round($row["实际占用总床日数"] / 12, 1),
                    "base_out_hospital" => round($row["出院人数"] / 12, 1),
                ]);

            });
        });
    }


}
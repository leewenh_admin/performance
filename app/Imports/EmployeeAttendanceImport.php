<?php

namespace App\Imports;

use App\Models\EmployeeAttendance;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeAttendanceImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
//            $reader->setHeaderRow(2);
            $results = $reader->get();
            $results->each(function($row){
                if ($row["所属科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(1, $row["所属科室编号"]);
                } else {
                    $department = $this->getOrCreateLv1Department(
                        make_semiangle($row["所属科室"])
                    );
                }

                if (empty($department)){
                    return ;
                }

                if ($row["科室编号"] ?? null){
                    $sub_department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $sub_department = $this->getOrCreateDepartment(
                        make_semiangle($row["科室"])
                    );
                }

                if (empty($sub_department)){
                    $sub_department = $department->subDepartment()->first();
                    if(empty($sub_department)){
                        return ;
                    }
                }
                $employee = $this->getOrCreateEmployee(
                    $row["姓名"], $row["工号"]
                );


                EmployeeAttendance::create([
                    "date" => request("date", date("Y-m")),
                    "departments_id" => $department->id,
                    "sub_departments_id" => $sub_department->id,
                    "employees_id" => $employee->id,
                    "code" => $row["工号"] ?? 0,
                    "actual_attendance" => (float)$row["实际出勤天数"] ?? 0,
                    "workday" => (float)$row["上班天数"] ?? 0,
                    "front_line_rest" => (float)$row["一线人员休整"] ?? 0,
                    "unemployed" => (float)$row["待岗"] ?? 0,
                    "business_trip" => (float)$row["外差"] ?? 0,
                    "study" => (float)$row["进修"] ?? 0,
                    "birth_control" => (float)$row["计生假"] ?? 0,
                    "birth" => (float)$row["产假"] ?? 0,
                    "annual" => (float)$row["年休假"] ?? 0,
                    "absence" => (float)$row["事假"] ?? 0,
                    "sick" => (float)$row["病假"] ?? 0,
                    "work_injury" => (float)$row["工伤"] ?? 0,
                    "family_visit" => (float)$row["探亲"] ?? 0,
                    "wedding" => (float)$row["婚假"] ?? 0,
                    "bereavement" => (float)$row["丧假"] ?? 0,
                    "radiate" => (float)$row["放射假"] ?? 0,
                    "coefficient" => (float)$row["系数"] ?? 1,
                    "attendance_coefficient" => (float)$row["结合考勤系数"] ?? 1,
                    "remark" => $row["备注"]
                ]);
            });

        });
    }
}
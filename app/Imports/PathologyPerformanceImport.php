<?php

namespace App\Imports;

use App\Models\ClinicalLaboratoryPerformance;
use App\Models\PathologyPerformance;
use App\Models\SpecialSurveyPerformance;
use Maatwebsite\Excel\Facades\Excel;

class PathologyPerformanceImport extends BaseImport
{
    public function import()
    {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader) {
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function ($row) {
                if (empty($row["科室"])) {
                    return;
                }
                if ($row["绩效占收入比"] == "#DIV/0!") {
                    $row["绩效占收入比"] = 0;
                }
                if ($row["占实收金额"] == "#DIV/0!") {
                    $row["占实收金额"] = 0;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(1, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateLv1Department(
                        $row["科室"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                $month = str_replace("月", "", $row["月份"]);
                if ($month < 10) {
                    $month = "0" . $month;
                }
                $year = date("Y", strtotime(request("date", date("Y"))));
                $date = $year . "-" . $month;

                PathologyPerformance::updateOrCreate(
                    [
                        "date" => $date,
                        "department_id" => $department->id,
                    ],
                    [
                        "point_value" => $row["点值合计"] ?? 0,
                        "point_price" => $row["点值单价"] ?? 0,
                        "check_num" => $row["检查人次"] ?? 0,
                        "check_price" => $row["检查单价"] ?? 0,
                        "performance" => $row["绩效合计"] ?? 0,
                        "amount" => $row["收入"] ?? 0,
                        "rate" => $row["绩效占收入比"] ?? 0,
                        "actual_amount" => $row["实收金额"] ?? 0,
                        "actual_rate" => $row["占实收金额"] ?? 0,
                    ]
                );
            });
        });
    }
}
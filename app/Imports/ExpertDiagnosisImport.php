<?php

namespace App\Imports;

use App\Models\ExpertDiagnosis;
use Maatwebsite\Excel\Facades\Excel;

class ExpertDiagnosisImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){
                if (empty($row["医生"])) {
                    return;
                }
                $employee = $this->getOrCreateEmployee(
                    $row["医生"],
                    $row["工号"]
                );

                ExpertDiagnosis::create([
                    "date" => request("date", date("Y-m")),
                    "employees_id" => $employee->id,
                    "assistant_count" => isset($row["副主任号人次"]) ? (float) $row["副主任号人次"] : 0,
                    "assistant_amount" => isset($row["副主任号金额"]) ? (float) $row["副主任号金额"] : 0,
                    "chief_count" => isset($row["主任号人次"]) ? (float) $row["主任号人次"] : 0,
                    "chief_amount" => isset($row["主任号金额"]) ? (float) $row["主任号金额"] : 0,
                    "total_count" => isset($row["总人次"]) ? (float) $row["总人次"] : 0,
                    "total_amount" => isset($row["总金额"]) ? (float) $row["总金额"] : 0,
                ]);
            });
        });
    }
}
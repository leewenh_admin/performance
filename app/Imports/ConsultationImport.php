<?php

namespace App\Imports;

use App\Models\Consultation;
use Maatwebsite\Excel\Facades\Excel;

class ConsultationImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){
                $employee = $this->getOrCreateEmployee(
                    $row["医生"],
                    $row["工号"]
                );

                Consultation::create([
                    "date" => request("date", date("Y-m")),
                    "employees_id" => $employee->id,
                    "count" => isset($row["数量"]) ? (float) $row["数量"] : 0,
                    "amount" => isset($row["金额"]) ? (float) $row["金额"] : 0,
                ]);
            });
        });
    }
}
<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\FixedAssets;
use App\Models\SubDepartment;
use App\Enums\DeviceType;

class FixedAssetsImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){


                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        make_semiangle($row["科室"])
                    );
                }

                if (empty($department) || empty($row["科室"])){
                    return ;
                }
                $data = FixedAssets::where("code", $row["编码"])
                    ->where("card", $row["卡片号"])
                    ->first();

                if ($data){
                    return ;
                }

                $device_type = [
                    "一般设备" => DeviceType::NORMAL,
                    "专业设备" => DeviceType::PROFESSIONAL,
                    "计算机设备" => DeviceType::COMPUTER,
                    "家俱设备" => DeviceType::FURNITURE,
                    "电器设备" => DeviceType::ELECTRICAL,
                    "交通设备" => DeviceType::TRAFFIC,
                    "制冷设备" => DeviceType::COLD,
                    "其他设备" => DeviceType::OTHER
                ];
                FixedAssets::create([
                    "code" => $row["编码"] ?? '无',
                    "card" => $row["卡片号"] ?? '无',
                    "name" => $row["品名"] ?? '无',
                    "specification" => $row["规格"] ?? null,
                    "unit" => $row["单位"] ?? '无',
                    "price" => isset($row["单价"]) ? (float) $row["单价"] : 0,
                    "count" => isset($row["数量"]) ? (float) $row["数量"] : 0,
                    "amount" => isset($row["金额"]) ? (float) $row["金额"] : 0,
                    "sub_departments_id" => $department->id,
                    "store_date" => $row["入库日期"],
                    "device_type" => isset($row["设备类型"]) && array_key_exists($row['设备类型'], $device_type) ? $device_type[$row["设备类型"]] : null
                ]);

            });
        });
    }
}
<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;
use App\Models\SubDepartment;

class DepartmentImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setHeaderRow(2);
            $results = $reader->get();
            $results->each(function($row){
                if (!empty($row["所属科室"])) {
                    $department = Department::firstOrCreate([
                        "name" => $row["所属科室"]
                    ]);
                } else {
                    $department = Department::firstOrCreate([
                        "name" => $row["科室"]
                    ]);
                }

                if (empty($department)){
                    return ;
                }

                SubDepartment::firstOrCreate([
                    "name" => $row["科室"],
                ],[
                    "parent_id" => $department->id,
                    "code" => 1,
                    "type_lv1" => 0,
                    "type_lv2" => 0,
                    "type_lv3" => 0
                ]);
            });
        });
    }
}
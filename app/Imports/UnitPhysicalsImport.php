<?php

namespace App\Imports;

use App\Models\UnitPhysical;
use App\Models\NewTechnologyItem;
use Maatwebsite\Excel\Facades\Excel;

class UnitPhysicalsImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
//            $reader->setHeaderRow(3);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["所属科室"]) || empty($row["科室"])) {
                    return;
                }
                $department = $this->getOrCreateLv1Department(
                    $row["所属科室"]
                );
                if (empty($department)){
                    return ;
                }
                $sub_department = $this->getOrCreateDepartment(
                    $row["科室"]
                );
                if (empty($sub_department)){
                    return ;
                }
                UnitPhysical::create([
                    "date" => request("date", date("Y-m")),
                    "department_id" => $department->id,
                    "sub_department_id" => $sub_department->id,
                    "point" => $row["点值"],
                    "point_price" => $row["点值单价"],
                    "check_count" => $row["检查人次"],
                    "check_income" => $row["体检收入"],

                ]);

            });
        });
    }
}

<?php

namespace App\Imports;

use App\Enums\IncomeType;
use App\Models\MainIncome;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Support\Facades\DB;
ini_set('memory_limit',-1);

class MainIncomeImport extends BaseImport {
//     public function getFile() {
//         return storage_path("app/shouru.xlsx");
//     }

    public function import() {
        /**
         * 使用了fast-excel进行导入
         * 40000条数据导入在10m左右
         * 需要xlsx格式的数据
         */
        $date = request("date", date("Y-m"));
//        $date = "2021-06";
        $arrDate = explode('-', $date);
        $new_table = "main_incomes" . $arrDate[0] . $arrDate[1];
        MainIncome::import($new_table);

        $filename = $this->getFile();
//        $filename = public_path("QYSL202106.xlsx");
        (new FastExcel)->import($filename, function ($row) use($new_table){
            $income_type = [
                "住院" => IncomeType::INPATIENT,
                "门诊" => IncomeType::OUTPATIENT
            ];

            $prescribe_doctor = $this->getOrCreateEmployee(
                $row["开单医生"],
                $row["开单医生工号"]
            );

            if (empty($row["执行人"])) {
                $execute_doctor_id = null;
            } else {
                $execute_doctor_id = $this->getOrCreateEmployee(
                    $row["执行人"],
                    $row["执行人工号"]
                )->id;
            }

            $prescribe_department = $this->getOrCreateDepartment(
                $row["开单科室"]
            );

            if(!$prescribe_department){
                return ;
            }

            $execute_department = $this->getOrCreateDepartment(
                $row["执行科室"]
            );

            if(!$execute_department){
                return ;
            }

            if (empty($row["入院科室"])) {
                $admission_department_id = null;
            } else {
                $admission_department = $this->getOrCreateDepartment(
                    $row["入院科室"]
                );

                if(!$admission_department){
                    return ;
                }

                $admission_department_id = $this->getOrCreateDepartment(
                    $row["入院科室"]
                )->id;
            }

            if (empty($row["出院科室"])) {
                $leave_department_id = null;
            } else {
                $leave_department = $this->getOrCreateDepartment(
                    $row["出院科室"]
                );

                if(!$leave_department){
                    return ;
                }

                $leave_department_id = $this->getOrCreateDepartment(
                    $row["出院科室"]
                )->id;
            }

            if ($row["病历标志"] == "有病历"){
                $is_have_case = 1;
            }else {
                $is_have_case = 0;
            }

            $current = new Carbon();

            DB::table($new_table)->insert([
                "income_type" => $income_type[
                trim($row["类型"])
                ],
                "serial_no" => $row["流水号"],
                "card_no" => $row["卡号"],
                "name" => $row["姓名"],
                "prescribe_doctor_id" => $prescribe_doctor->id,
                "execute_doctor_id" => $execute_doctor_id,
                "prescribe_department_id" => $prescribe_department->id,
                "execute_department_id" => $execute_department->id,
                "item_code" => $row["项目编码"],
                "item_name" => $row["项目名称"],
                "gb_code" => $row["国标码"],
                "item_cate_code" => $row["项目类别编号"],
                "item_cate" => $row["项目类别"],
                "price" => $row["单价"],
                "quantity" => $row["数量"],
                "amount" => $row["金额"],
                "admission_department_id" => $admission_department_id,
                "leave_department_id" => $leave_department_id,
                "admission_time" => (string) $row["入院时间"],
                "leave_time" => (string) $row["出院时间"],
                "outpatient_diagnosis_code" => $row["门诊诊断编码"],
                "outpatient_diagnosis" => $row["门诊诊断"],
                "leave_diagnosis_code" => $row["出院诊断编码"],
                "leave_diagnosis" => $row["出院诊断"],
                "sub_diagnosis_code" => $row["次诊编码"],
                "sub_diagnosis" => $row["次诊"],
                "charging_time" => $row["收费时间"],
                "is_have_case" => $is_have_case,
                "created_at" => $current->toDateTimeString(),
                "updated_at" => $current->toDateTimeString(),
            ]);
        });
    }
}
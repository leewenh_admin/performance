<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\EmployeePerformance;

class EmployeePerformanceImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                $employee = $this->getOrCreateEmployee(
                    $row["姓名"], $row["工号"]
                );

                EmployeePerformance::create([
                    'date' => request("date", date("Y-m")),
                    'department' => $row['科室'] ?? null,
                    'employees_id' => $employee->id,
                    'code' => $row['工号'] ?? '无',
                    'name' => $row['姓名'] ?? '无',
                    'workload_performance' => $row['工作量绩效'],
                    'night_performance' => $row['夜班绩效'],
                    'community_performance' => $row['医共体绩效'],
                    'deduct' => $row['扣款'],
                    'tax' => $row['个人所得税'],
                    'decuct_remark' => $row['扣款备注'] ?? null
                ]);
            });
        });
    }
}

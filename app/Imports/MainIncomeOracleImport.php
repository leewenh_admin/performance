<?php

namespace App\Imports;

use App\Enums\IncomeType;
use App\Models\MainIncome;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class MainIncomeOracleImport extends BaseImport {
//     public function getFile() {
//         return storage_path("app/shouru.xlsx");
//     }

    public function import_from_oracle($date, $table_name) {
        /**
         *  源数据来自Oracle数据库中
         *  数据库链接信息可见 config/oracle.conf
         *  参数$date样例: 2021-04
         */
        $perPageCount = 10000;

        // 根据提交信息创建数据库表
        $arrDate = explode('-', $date);
        $new_table = "main_incomes" . $arrDate[0] . $arrDate[1];
        MainIncome::import($new_table);

        // 根据数据库数据总量拆分每批次数据量

        $all_count = DB::connection('oracle')->table($table_name)
            ->count();

        $page_count = ceil($all_count / $perPageCount);
        Log::useFiles(storage_path('logs/import'.date('Ymdhis').'.log'), 'info');

        // 外层循环，控制每批次数据
        for($i=0; $i<$page_count; $i++) {
            $records = DB::connection('oracle')->table($table_name)
                ->skip($i * $perPageCount)
                ->take($perPageCount)
                ->get();

            var_dump('已导入' . $i * $perPageCount. '条数据');
            // 内层循环，控制每批次数据中的每条数据
            foreach ($records as $record) {
                $row = get_object_vars($record);

                $income_type = [
                    '住院' => IncomeType::INPATIENT,
                    '门诊' => IncomeType::OUTPATIENT
                ];

                $prescribe_doctor = $this->getOrCreateEmployee(
                    $row['开单医生'],
                    $row['开单医生工号']
                );

                if (empty($prescribe_doctor)) {
                    Log::info('跳过开单医生' . $row['开单医生'] . $row['开单医生工号'] .'rn=' . $row['rn']);
                    continue;
                }

                if (empty($row['执行人'])) {
                    $execute_doctor_id = null;
                } else {
                    $execute_doctor_id = $this->getOrCreateEmployee(
                        $row['执行人'],
                        $row['执行人工号']
                    )->id;
                    if (empty($execute_doctor_id)){
                        Log::info('跳过执行人' . $row['执行人'] . $row['执行人工号'] .'rn=' . $row['rn']);
                        continue;
                    }
                }

                $prescribe_department = $this->getOrCreateDepartment(
                    $row['开单科室']
                );
                if (empty($prescribe_department)){
                    Log::info('跳过开单科室' . $row['开单科室'] .'rn=' . $row['rn']);
                    continue;
                }

                $execute_department = $this->getOrCreateDepartment(
                    $row['执行科室']
                );
                if (empty($execute_department)){
                    Log::info('跳过执行科室' . $row['执行科室'] .'rn=' . $row['rn']);
                    continue;
                }

                if (empty($row['入院科室'])) {
                    $admission_department_id = null;
                } else {
                    $admission_department = $this->getOrCreateDepartment(
                        $row['入院科室']
                    );
                    if (empty($admission_department)){
                        Log::info('跳过入院科室 ' . $row['入院科室'] .'rn=' . $row['rn']);
                        continue;
                    } else {
                        $admission_department_id = $admission_department->id;
                    }
                }

                if (empty($row['出院科室'])) {
                    $leave_department_id = null;
                } else {
                    $leave_department = $this->getOrCreateDepartment(
                        $row['出院科室']
                    );
                    if (empty($leave_department)){
                        Log::info('跳过出院科室' . $row['出院科室'] .'rn=' . $row['rn']);
                        continue;
                    } else {
                        $leave_department_id = $leave_department->id;
                    }
                }

                if ($row['病历标志'] == '有病历'){
                    $is_have_case = 1;
                }else {
                    $is_have_case = 0;
                }


                $current = new Carbon();

                DB::table($new_table)->insert([
                    'income_type' => $income_type[
                        trim($row["类型"])
                    ],
                    'serial_no' => $row['流水号'],
                    'card_no' => $row['卡号'],
                    'name' => $row['姓名'],
                    'prescribe_doctor_id' => $prescribe_doctor->id,
                    'execute_doctor_id' => $execute_doctor_id,
                    'prescribe_department_id' => $prescribe_department->id,
                    'execute_department_id' => $execute_department->id,
                    'item_code' => $row['项目编码'],
                    'item_name' => $row['项目名称'],
                    'gb_code' => $row['国标码'],
                    'item_cate_code' => $row['项目类别编号'],
                    'item_cate' => $row['项目类别'],
                    'price' => $row['单价'],
                    'quantity' => $row['数量'],
                    'amount' => $row['金额'],
                    'admission_department_id' => $admission_department_id,
                    'leave_department_id' => $leave_department_id,
                    'admission_time' => (string) $row['入院时间'],
                    'leave_time' => (string) $row['出院时间'],
                    'outpatient_diagnosis_code' => $row['门诊诊断编码'],
                    'outpatient_diagnosis' => $row['门诊诊断'],
                    'leave_diagnosis_code' => $row['出院诊断编码'],
                    'leave_diagnosis' => $row['出院诊断'],
                    'sub_diagnosis_code' => $row['次诊编码'],
                    'sub_diagnosis' => $row['次诊'],
                    'charging_time' => $row['收费时间'],
                    'is_have_case' => $is_have_case,
                    'created_at' => $current->toDateTimeString(),
                    'updated_at' => $current->toDateTimeString(),
                ]);
//                Log::info('插入一条数据, rn=' . $row['rn']);
            }
        }

    }
}

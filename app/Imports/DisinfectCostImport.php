<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;
use App\Models\Employee;
use App\Models\DisinfectCost;
use App\Enums\EmployeeType;

class DisinfectCostImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室"])){
                    return ;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        $row["科室"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                DisinfectCost::updateOrCreate(
                    [
                        "date" => request("date", date("Y-m")),
                        "sub_departments_id" => $department->id,
                    ],
                    [
                        "count" => isset($row["数量"]) ? (float) $row["数量"] : 0,
                        "amount" => isset($row["金额"]) ? (float) $row["金额"] : 0,
                    ]
                );
            });
        });
    }
}
<?php

namespace App\Imports;

use App\Models\NewTechnologyDetail;
use App\Models\NewTechnologyItem;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class NewTechnologyItemImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室"])) {
                    return;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(1, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateLv1Department(
                        $row["科室"]
                    );
                }

                if (empty($department)){
                    return ;
                }

                DB::beginTransaction();
                try {
                    $record = NewTechnologyItem::firstOrCreate([
                        "department_id" => $department->id,
                        "item_code" => $row["项目编码"],
                    ],
                        [
                            "apply_year" => request("date", date("Y")),
                            "name" => $row["新技术名称"],
                            "code" => $row["医院编码"],
                            "item_code" => $row["项目编码"],
                            "count" => $row["数量"] ?? 0,
                            "price" => $row["单价"] ?? 0,
                            "increase_count" => $row["新业务增量"] ?? 0,
                        ]);

                    NewTechnologyDetail::updateorcreate([
                        'technology_id' => $record->id,
                        'date' => request("date", date("Y-m")),
                    ],[
                        'count' => $row["数量"] ?? 0,
                        "price" => $row["单价"] ?? 0,
                        "increase_count" => $row["新业务增量"] ?? 0,
                    ]);
                    DB::commit();
                } catch (\Exception $exception){
                    DB::rollBack();
                }


            });
        });
    }
}
<?php

namespace App\Imports;

use App\Models\PhysicalWork;
use Maatwebsite\Excel\Facades\Excel;

class PhysicalWorkImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){
                $department_names = [
                    "心电图" => "心电图室",
                    "检验" => "检验科",
                    "妇科" => "妇科"
                ];
    
                $department = $this->getOrCreateDepartment(
                    $department_names[$row["科室"]]
                );
                if (empty($department)){
                    return ;
                }
                PhysicalWork::create([
                    "date" => request("date", date("Y-m")),
                    "sub_departments_id" => $department->id,
                    "person_name" => $row["姓名"],
                    "item_group" => $row["分组"],
                    "item_name" => $row["项目名"],
                    "exam_time" => $row["检查日期"],
                    "amount" => $row["价格"]
                ]);
            });
        });
    }
}
<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;

class BaseSurgeryRateImport extends BaseImport {

    /**
     * 目标手术率
     */
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室名称"])) {
                    return;
                }
                Department::where("name", $row["科室名称"])->update([
                    "target_surgery_rate" => round($row["手术率"] * 100, 2),
                    "base_surgery_count" => round($row["手术人次开单科室"] / 12, 0),
                ]);

            });
        });
    }

}
<?php

namespace App\Imports;

use App\Models\MedicalPointValue;
use Maatwebsite\Excel\Facades\Excel;

class MedicalPointValueImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){

            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){
                if (empty($row["项目"])) {
                    return;
                }


                if ($row["执行科室编号"] ?? null){
                    $exec_department = $this->getDepartmentByCode(2, $row["执行科室编号"]);
                } else {
                    $exec_department = $this->getOrCreateDepartment(
                        $row["执行科室"]
                    );
                }

                if (empty($exec_department)){
                    return ;
                }

                MedicalPointValue::updateOrCreate(
                    [
                        "code" => $row["编码"],
                    ],
                    [
                        "execute_department_id" => $exec_department->id,
                        "project" => $row["项目"] ?? '无',
                        "gb_code" => $row["国标码"] ?? '无',
                        "project_type" => $row["一级分类"] ?? null,
                        "project_code" => $row["项目编码"] ?? '无',
                        "name" => $row["项目名称"] ?? '无',
                        "project_include" => $row["项目内涵"] ?? '无',
                        "include_consumable" => $row["内涵一次性耗材"] ?? null,
                        "exclude_consumable" => $row["除外内容"] ?? null,
                        "lowvalue_consumable" => isset($row["低值耗材"]) ? (float) $row["低值耗材"] : 0,
                        "consume_explain" => $row["基本人力消耗及耗时"] ?? null,
                        "technical_difficulty1" => $row["技术难度"] ?? "0",
                        "risk_degree1" => $row["风险程度"] ?? "0",
                        "charge_unit" => $row["计价单位"] ?? '无',
                        "charge_explain" => $row["计价说明"] ?? null,
                        "duration" => $row["时间"] ?? 0,
                        "technical_difficulty2" => $row["技术难度2"] ?? 0,
                        "risk_degree2" => $row["风险程度2"] ?? 0,
                        "difficulty_risk_weights" => $row["难度风险权重"] ?? 0,
                        "duration_weights" => round($row["时间权重"], 2),
                        "performance_weights" => $row["绩效点数"] ?? 0,
                ]);
            });
        });
    }
}

<?php

namespace App\Imports;

use App\Enums\EmployeeAttendanceType;
use App\Enums\EmployeePosition;
use App\Enums\EmployeeProfessionalRankLevel;
use App\Enums\EmployeeProfessionalRankType;
use App\Enums\EmployeeSex;
use App\Models\Employee;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setHeaderRow(3);
            $results = $reader->get();
            $header = $results->getHeading();
            if (!in_array('工号', $header)){
                $reader->setHeaderRow(1);
                $results = $reader->get();
            }
            $results->each(function($row){
                if (empty($row["工号"])){
                    return ;
                }

                $position_type = [
                    "院长" => EmployeePosition::DEAN,
                    "副院长" => EmployeePosition::SUB_DEAN,
                    "院长助理" => EmployeePosition::DEAN_AIDE,
                    "科主任" => EmployeePosition::DEPARTMENT_HEAD,
                    "副主任" => EmployeePosition::DEPARTMENT_SUB_HEAD,
                    "护士长" => EmployeePosition::NURESE_HEAD,
                    "职员" => EmployeePosition::STAFF,
                    "干事" => EmployeePosition::GANSHI,
                    "工会主席" => EmployeePosition::GHZX,
                    "纪委书记" => EmployeePosition::JWSJ,
                ];
                $professional_rank_type = [
                    "无" => 0,
                    "护理" => EmployeeProfessionalRankType::NURSE,
                    "医疗" => EmployeeProfessionalRankType::MEDICAL_TREATMENT,
                    "经统会" => EmployeeProfessionalRankType::ECONOMIC,
                    "医技" => EmployeeProfessionalRankType::MEDICAL_SKILL,
                    "药剂" => EmployeeProfessionalRankType::MEDICAMENT,
                    "工勤" => EmployeeProfessionalRankType::WORKER,

                ];
                $employee_sex = [
                    "男" => EmployeeSex::MALE,
                    "女" => EmployeeSex::FEMALE
                ];

                $professional_rank_level = [
                    "无" => null,
                    "初级" => EmployeeProfessionalRankLevel::JUNIOR,
                    "中级" => EmployeeProfessionalRankLevel::MIDDLE,
                    "副高" => EmployeeProfessionalRankLevel::DEPUTY_SENIOR,
                    "正高" => EmployeeProfessionalRankLevel::SENIOR,
                    "返聘" => EmployeeProfessionalRankLevel::REEMPLOY
                ];

                $attendance_type = [
                    "无" => null,
                    "医生" => EmployeeAttendanceType::DOCTOR,
                    "护理" => EmployeeAttendanceType::NURSE,
                    "技师" => EmployeeAttendanceType::TECHNICIAN,
                    "行管" => EmployeeAttendanceType::MANAGER,
                    "药师" => EmployeeAttendanceType::PHARMACIST,

                ];

                $row["职称级别"] = $row["职称级别"] ?? "无";
                if (!$row["职称级别"]) {
                    $row["职称级别"] = "无";
                }

                if (array_key_exists($row["职称级别"], $professional_rank_level)){
                    $rank_level = $professional_rank_level[$row["职称级别"]];
                } else {
                    $rank_level = null;
                }


                $row["职称分类"] = $row["职称分类"] ?? "无";
                if (!$row["职称分类"]) {
                    $row["职称分类"] = "无";
                }
                if (array_key_exists($row["职称分类"], $professional_rank_type)){
                    $rank_type = $professional_rank_type[$row["职称分类"]];
                } else {
                    $rank_type = 0;
                }


                Employee::updateOrCreate([
                    "code" => $row["工号"],
                    ],
                    [
                    "name" => trim($row["姓名"]),
                    "coefficient" => isset($row["系数"]) ? (float) $row["系数"] : 0,
                    "position" => $position_type[$row["行政职务"] ?? "职员"],
                    "professional_rank_type" => $rank_type,
                    "sex" => $row["性别"] ? $employee_sex[$row["性别"]] : 0,
                    "birth_date" => $row["出生日期"],
                    "join_date" => $row["到职日期"] ?? null,
                    "first_education" => $row["第一学历"] ?? null,
                    "working_education" => $row["在职学历"] ?? null,
                    "position_detail" => $row["行政职务明细"] ?? null,
                    "position_date" => $row["行政职务任职时间"] ?? null,
                    "position_code" => $row["行政职务任职文号"] ?? null,
                    "professional_rank_level" => $rank_level,
                    "professional_rank_title" => $row["技术职称"] ?? null,
                    "professional_rank_date" => $row["技术职称任职时间"] ?? null,
                    "professional_rank_code" => $row["技术职称任职文号"] ?? null,
                    "coefficient_desc" => $row["系数说明"] ?? null,
                    "employ_status" => $row["雇佣状态"] ?? null,
                    "employ_type" => $row["人员类别"] ?? null,
                    "card_no" => $row["证件号码"] ?? null,
                    "fist_work_time" => $row["参加工作时间"] ?? null,
                    "this_work_time" => $row["进入本行业时间"] ?? null,
                    "positive_time" => $row["转正时间"] ?? null,
                    "mobile" => $row["手机"] ?? null,
                    "leave_time" => $row["离职日期"] ?? null,
                    "attendances_type" => $row["所在科室人员分类"] ? $attendance_type[$row["所在科室人员分类"]] : 0,
                   ]);

                // Employee::where("code", $row["工号"])->update([
                //     "sex" => $employee_sex[$row["性别"]],
                //     "birth_date" => $row["出生年月"],
                //     "join_date" => $row["进院时间"],
                //     "first_education" => $row["第一学历"],
                //     "working_education" => $row["在职学历"],
                //     "position_detail" => $row["行政职务明细"],
                //     "position_date" => $row["行政职务任职时间"],
                //     "position_code" => $row["行政职务任职文号"],
                //     "professional_rank_level" => $professional_rank_level[$row["职称级别"] ?? "无"],
                //     "professional_rank_title" => $row["技术职称"],
                //     "professional_rank_date" => $row["技术职称任职时间"],
                //     "professional_rank_code" => $row["技术职称任职文号"],

                // ]);
                
            });
        });
    }  
}
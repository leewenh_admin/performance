<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;

class BaseDepartmentIncomeImport extends BaseImport {

    /**
     * 基期科内有效收入基准值
     */
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室"])) {
                    return;
                }
                Department::where("name", $row["科室"])->update([
                    "base_department_income" => round($row["科内收入"], 2),
                ]);

            });
        });
    }

}
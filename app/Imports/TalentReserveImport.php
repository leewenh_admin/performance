<?php

namespace App\Imports;

use App\Models\TalentReserve;
use Maatwebsite\Excel\Facades\Excel;

class TalentReserveImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $reader->setHeaderRow(3);
            $results = $reader->get();

            $results->each(function($row){
                if (empty($row["姓名"])) {
                    return;
                }

                if (empty($row["所属科室"])) {
                    return;
                }


                if ($row["所属科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(1, $row["所属科室编号"]);
                } else {
                    $department = $this->getOrCreateLv1Department(
                        $row["所属科室"]
                    );
                }

                if (empty($department)){
                    return ;
                }

                $employee = $this->getOrCreateEmployee(
                    $row["姓名"], $row["工号"]
                );

                TalentReserve::updateOrCreate(
                    [
                        "date" => request("date", date("Y-m")),
                        "employees_id" => $employee->id,
                        "department_id" => $department->id,
                    ],
                    [
                        "code" => $row["工号"],
                        "name" => $row["姓名"],
                        "start_date" => $row["开始发放时间"],
                        "end_date" => $row["终止时间"],
                        'remark' => $row["备注"],
                        'professional_rank_level' => $row['职称级别'],
                        'professional_rank_title' => $row['技术职称'],
                        'standard' => $row['发放标准元'],
                        'actual_sent' => $row['本月应发元'],
                        'leave_day' => $row['请假天数'],
                ]);
            });
        });
    }
}

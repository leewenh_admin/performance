<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;

class BaseAdmissionRateImport extends BaseImport {

    /**
     * 收治率基准值
     */
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){
                if (empty($row["科室"])) {
                    return;
                }
                Department::where("name", $row["科室"])->update([
                    "target_admission_rate" => round($row["收治率"], 2),
                ]);

            });
        });
    }
}
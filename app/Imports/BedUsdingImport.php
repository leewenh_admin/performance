<?php

namespace App\Imports;

use App\Models\BedUsing;
use Maatwebsite\Excel\Facades\Excel;

class BedUsdingImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $results = $reader->get();

            $results->each(function($row){
                if (empty($row["科室名称"])){
                    return ;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment($row["科室名称"]);
                }


                if (empty($department)){
                    return ;
                }

                BedUsing::create([
                    "date" => request("date", date("Y-m")),
                    "sub_departments_id" => $department->id,
                    "begin_open" => isset($row["期初开放床位数"]) ? (float) $row["期初开放床位数"] : 0,
                    "finish_open" => isset($row["期未开放床位数"]) ? (float) $row["期未开放床位数"] : 0,
                    "avg_open" => isset($row["平均开放床位数"]) ? (float) $row["平均开放床位数"] : 0,
                    "total_open_bed_day" => isset($row["实际开放总床日数"]) ? (float) $row["实际开放总床日数"] : 0,
                    "total_use_bed_day" => isset($row["实际占用总床日数"]) ? (float) $row["实际占用总床日数"] : 0,
                    "avg_bed_work" => isset($row["平均病床工作日"]) ? (float) $row["平均病床工作日"] : 0,
                    "used_rate" => isset($row["病床使用率"]) ? (float) $row["病床使用率"] : 0,
                    "trunover_count" => isset($row["病床周转次"]) ? (float) $row["病床周转次"] : 0,
                    "leaver_count" => isset($row["出院人数"]) ? (float) $row["出院人数"] : 0,
                    "leaver_used_bed_day" => isset($row["出院者占用总床日数"]) ? (float) $row["出院者占用总床日数"] : 0,
                    "leaver_avg_day" => isset($row["出院者平均住院日"]) ? (float) $row["出院者平均住院日"] : 0,
                    "leaver_avg_cost" => isset($row["出院者平均住院费"]) ? (float) $row["出院者平均住院费"] : 0,
                    "medicine_in_cost_rate" => isset($row["药费占住院费比例"]) ? (float) $row["药费占住院费比例"] : 0,
                    "leaver_cost_per_person" => isset($row["出院者人均医疗费用"]) ? (float) $row["出院者人均医疗费用"] : 0,
                    "leaver_cost_per_day" => isset($row["出院者日均费用"]) ? (float) $row["出院者日均费用"] : 0,
                ]);
            });
        });
    }
}
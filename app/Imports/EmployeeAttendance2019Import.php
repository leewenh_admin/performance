<?php

namespace App\Imports;

use App\Models\BaseEmployeeAttendance;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeAttendance2019Import extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setHeaderRow(1);
            $results = $reader->get();
            $results->each(function($row){

                $department = $this->getOrCreateLv1Department(
                    make_semiangle($row["归属科室"])
                );
                if (empty($department)){
                    return ;
                }
                $sub_department = $this->getOrCreateDepartment(
                    make_semiangle($row["科室"])
                );
                if (empty($sub_department)){
                    return ;
                }
                $employee = $this->getOrCreateEmployee(
                    $row["姓名"], $row["工号"]
                );

                BaseEmployeeAttendance::create([
                    "date" => $row["所属月份"]->format("Y-m"),
                    "departments_id" => $department->id,
                    "sub_departments_id" => $sub_department->id,
                    "employees_id" => $employee->id,
                    "code" => $row["工号"] ?? 0,
                    "workday" => $row["上班"] ?? 0,
                    "business_trip" => $row["开会_外差"] ?? 0,
                    "study" => $row["学习"] ?? 0,
                    "up_study" => $row["进修"] ?? 0,
                    "birth_control" => $row["计生假"] ?? 0,
                    "birth" => $row["产假"] ?? 0,
                    "annual" => $row["年休"] ?? 0,
                    "absence" => $row["事假"] ?? 0,
                    "sick" => $row["病假"] ?? 0,
                    "wedding" => $row["婚假"] ?? 0,
                    "remark" => $row["备注"]
                ]);
            });

        });
    }
}
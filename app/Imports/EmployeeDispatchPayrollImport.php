<?php

namespace App\Imports;

use App\Models\EmployeeDispatchPayroll;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeDispatchPayrollImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["序号"]) || empty($row["科室"])) {
                    return;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment($row["科室"]);
                }

                if (empty($department)){
                    return ;
                }
                EmployeeDispatchPayroll::create([
                    "date" => request("date", date("Y-m")),
                    "sub_departments_id" => $department->id,
                    "name" => $row["姓名"] ?? '无',
                    "dispatcher_total" => isset($row["派遣公司总合计"]) ? (float) $row["派遣公司总合计"] : 0,
                    "base_salary" => isset($row["工资合计"]) ? (float) $row["工资合计"] : 0,
                    "standard_performance" => isset($row["绩效工资标准"]) ? (float) $row["绩效工资标准"] : 0,
                    "night_salary" => isset($row["夜班费"]) ? (float) $row["夜班费"] : 0,
                    "total_payable" => isset($row["应发合计"]) ? (float) $row["应发合计"] : 0,
                    "management_cost" => isset($row["管理费"]) ? (float) $row["管理费"] : 0,
                    "employer_insurance" => isset($row["单位缴保险合计"]) ? (float) $row["单位缴保险合计"] : 0,
                ]);
            });

        });
    }
}
<?php

namespace App\Imports;

use App\Models\ClinicalLaboratoryPerformance;
use Maatwebsite\Excel\Facades\Excel;

class ClinicalLaboratoryPerformanceImport extends BaseImport
{
    public function import()
    {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader) {
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function ($row) {
                if (empty($row["科室"])) {
                    return;
                }
                if ($row["绩效占收入比"] == "#DIV/0!") {
                    $row["绩效占收入比"] = 0;
                }
                if ($row["占实收金额"] == "#DIV/0!") {
                    $row["占实收金额"] = 0;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(1, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateLv1Department(
                        $row["科室"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                $row["月份"] = date("Y-m", strtotime($row["月份"]));

                ClinicalLaboratoryPerformance::updateOrCreate(
                    [
                        "date" => $row["月份"],
                        "department_id" => $department->id,
                    ],
                    [
                        "point_value" => isset($row["点值合计"]) ? (float) $row["点值合计"] : 0,
                        "point_price" => isset($row["点值单价"]) ? (float) $row["点值单价"] : 0,
                        "check_num" => isset($row["检查人次"]) ? (float) $row["检查人次"] : 0,
                        "check_price" => isset($row["检查单价"]) ? (float) $row["检查单价"] : 0,
                        "performance" => isset($row["绩效合计"]) ? (float) $row["绩效合计"] : 0,
                        "amount" => isset($row["收入"]) ? (float) $row["收入"] : 0,
                        "rate" => isset($row["绩效占收入比"]) ? (float) $row["绩效占收入比"] : 0,
                        "actual_amount" => isset($row["实收金额"]) ? (float) $row["实收金额"] : 0,
                        "actual_rate" => isset($row["占实收金额"]) ? (float) $row["占实收金额"] : 0,
                    ]
                );

            });
        });
    }
}
<?php

namespace App\Imports;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\Department;

class BaseControllableCostImport extends BaseImport {

    /**
     * 基期可控变动成本基准值
     */
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setHeaderRow(4);
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if (empty($row["科室"])) {
                    return;
                }
                Department::where("name", $row["科室"])->update([
                    "base_period_controllable_variable_cost" => round($row["小计"], 2),
                ]);

            });
        });
    }

}
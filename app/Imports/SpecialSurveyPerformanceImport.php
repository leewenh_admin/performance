<?php

namespace App\Imports;

use App\Models\SpecialSurveyPerformance;
use Maatwebsite\Excel\Facades\Excel;

class SpecialSurveyPerformanceImport extends BaseImport
{
    public function import()
    {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader) {
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function ($row) {
                if (empty($row["科室"])) {
                    return;
                }

                $month = str_replace("月", "", $row["月份"]);
                if ($month < 10) {
                    $month = "0" . $month;
                }
                $year = date("Y", strtotime(request("date", date("Y"))));
                $date = $year . "-" . $month;

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(1, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateLv1Department(
                        $row["科室"]
                    );
                }

                if (empty($department)){
                    return ;
                }
                SpecialSurveyPerformance::updateOrCreate(
                    [
                        "date" => $date,
                        "department_id" => $department->id,
                    ],
                    [
                        "normal_number" => $row["普通心电图例数"] ?? 0,
                        "normal_price" => $row["普通心电图单价"] ?? 0,
                        "changcheng_number" => $row["长程心电图例数"] ?? 0,
                        "changcheng_price" => $row["长程心电图单价"] ?? 0,
                        "performance" => $row["绩效合计"] ?? 0,
                    ]
                );

            });
        });
    }
}
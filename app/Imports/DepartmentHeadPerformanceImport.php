<?php

namespace App\Imports;

use App\Models\DepartmentHeadPerformance;
use Maatwebsite\Excel\Facades\Excel;

class DepartmentHeadPerformanceImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function($row){
                if ($row["所在科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(1, $row["所在科室编号"]);
                } else {
                    $department = $this->getOrCreateLv1Department(
                        make_semiangle($row["所在科室"])
                    );
                }

                if (empty($department)){
                    return ;
                }

                $employee = $this->getOrCreateEmployee(
                    $row["姓名"], $row["工号"]
                );

                DepartmentHeadPerformance::create([
                    "date" => request("date", date("Y-m")),
                    "department_id" => $department->id,
                    "employee_id" => $employee->id,
                    "code" => isset($row["工号"]) ? (float) $row["工号"] : 0,
                    "administrative_duties" => $row["行政职务"] ?? null,
                    "administrative_time" => $row["行政职务任职时间"] ?? null,
                    "actual_attendance" => isset($row["出勤天数"]) ? (float) $row["出勤天数"] : 0,
                    "check_day" => isset($row["核减天数"]) ? (float) $row["核减天数"] : 0,
                    "medical_percent" => isset($row["药占比考核"]) ? (float) $row["药占比考核"] : 0,
                    "expend_percent" => isset($row["耗占比考核"]) ? (float) $row["耗占比考核"] : 0,
                    "performance_standard" => isset($row["管理绩效标准"]) ? (float) $row["管理绩效标准"] : 0,
                ]);
            });

        });
    }
}
<?php

namespace App\Imports;

use App\Models\LaundryCost;
use Maatwebsite\Excel\Facades\Excel;

class LaundryCostImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){
                if (empty($row["科室"])){
                    return ;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(2, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateDepartment(
                        $row["科室"]
                    );
                }


                if (empty($department)){
                    return ;
                }
                $quilt = isset($row["被套"]) ? (float) $row["被套"] : 0;
                $coverlet = isset($row["床单"]) ? (float) $row["床单"] : 0;
                $pillowcase = isset($row["枕套"]) ? (float) $row["枕套"] : 0;
                $worker_clothe = isset($row["工作服"]) ? (float) $row["工作服"] : 0;
                $sick_clothes = isset($row["病服其他"]) ? (float) $row["病服其他"] : 0;
                $surgery_count = isset($row["手术台次"]) ? (float) $row["手术台次"] : 0;
                $amount = $quilt * 3.5 + $coverlet * 3 + $pillowcase * 1.5
                    + $worker_clothe * 3 + $sick_clothes * 3 + $surgery_count * 30;
                LaundryCost::updateOrCreate(
                    [
                        "date" => request("date", date("Y-m")),
                        "sub_departments_id" => $department->id,
                    ],
                    [
                        "quilt" => $quilt,
                        "coverlet" => $coverlet,
                        "pillowcase" => $pillowcase,
                        "worker_clothe" => $worker_clothe,
                        "sick_clothes" => $sick_clothes,
                        "total_count" => isset($row["合计"]) ? (float) $row["合计"] : 0,
                        "surgery_count" => isset($row["手术台次"]) ? (float) $row["手术台次"] : 0,
                        "used_bed_day" => isset($row["实际占床日"]) ? (float) $row["实际占床日"] : 0,
                        "per_wash_fee" => isset($row["每件洗涤费"]) ? (float) $row["每件洗涤费"] : 0,
                        "amount" => $amount,
                    ]
                );
            });
        });
    }
}
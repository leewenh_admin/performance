<?php

namespace App\Imports;

use App\Models\DepartmentPerformance;
use App\Models\FourItemPerformance;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class FourItemPerformanceImport extends BaseImport {
    public function import() {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader){
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();

            $results->each(function($row){
                if (empty($row["科室"])){
                    return ;
                }

                if ($row["科室编号"] ?? null){
                    $department = $this->getDepartmentByCode(1, $row["科室编号"]);
                } else {
                    $department = $this->getOrCreateLv1Department(
                        $row["科室"]
                    );
                }

                if (empty($department)){
                    return ;
                }

                $physical_performance = isset($row["单位体检绩效"]) ? (float) $row["单位体检绩效"] : 0;
                $stroke_performance = isset($row["卒中绩效"]) ? (float) $row["卒中绩效"] : 0;
                $control_performance = isset($row["疫情防控"]) ? (float) $row["疫情防控"] : 0;
                $running_performance = isset($row["科室跑账"]) ? (float) $row["科室跑账"] : 0;

                DB::beginTransaction();

                try {
                    FourItemPerformance::updateOrCreate(
                        [
                            "date" => request("date", date("Y-m")),
                            "department_id" => $department->id,
                        ],
                        [
                            "physical_performance" => $physical_performance,
                            "stroke_performance" => $stroke_performance,
                            "control_performance" => $control_performance,
                            "running_performance" => $running_performance,
                        ]
                    );

                    $performance = DepartmentPerformance::where("department_id" , $department->id)->where("date", request("date", date("Y-m")))->first();

                    //更新已生成的绩效数据
                    if ($performance) {
                        $performance->total_performances = $performance->total_performances - $performance->physical_performance -
                            $performance->control_performance - $performance->running_performance +  $physical_performance +
                            $control_performance + $running_performance;

                        $performance->physical_performance = $physical_performance;
                        $performance->control_performance = $control_performance;
                        $performance->running_performance = $running_performance;
                        $performance->save();
                    }

                    DB::commit();
                }catch (\Exception $exception){
                    DB::rollBack();
                }


            });
        });
    }
}
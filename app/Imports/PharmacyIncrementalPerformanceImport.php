<?php

namespace App\Imports;

use App\Models\PharmacyIncrementalPerformance;
use Maatwebsite\Excel\Facades\Excel;

class PharmacyIncrementalPerformanceImport extends BaseImport
{
    public function import()
    {
        $filename = $this->getFile();
        Excel::load($filename, function ($reader) {
            $reader->setSelectedSheetIndices([0]);
            $results = $reader->get();
            $results->each(function ($row) {
                $key = $row["指标"] ?? null;
  
                if (empty($key)) {
                    return;
                }


                PharmacyIncrementalPerformance::create([
                    "date" => request("date", date("Y-m")),
                    "quota" => $key,
                    "standard" => $row["标准值"] ?? 0,
                    "reviews" => $row["点评数"] ?? 0,
                    "total" => $row["总数"] ?? 0,
                    "actual" => $row["实际值"] ?? 0,
                    "incremental" => $row["绩效增量"] ?? 0
                ]);
            });
        });
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 2021/3/8
 * Time: 16:30
 */

namespace App\Http\Controllers\Traits;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as FoundationResponse;

trait ApiResponse {
    /**
     * status Code
     * @var int
     */
    protected $statusCode = FoundationResponse::HTTP_OK;

    /**
     * @return int
     */
    public function getStatusCode():int {
        return 200;
    }

    /**
     * @param int $statusCode
     *
     * @return ApiResponse
     */
    public function setStatusCode(int $statusCode) {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param $data
     * @param array $header
     *
     * @return mixed
     */
    public function response($data, $header = []) {
        return Response::json($data, $this->getStatusCode(), $header);
    }

    public function status($status, array $data, $code = null) {
        if ($code) {
            $this->setStatusCode($code);
        }
        $status = [
            'status' => $status,
            'code' => $this->statusCode
        ];
        $data = array_merge($status, $data);
        return $this->response($data);
    }

    public function failed($message, $code = FoundationResponse::HTTP_BAD_REQUEST, $status = 'error') {
        return $this->status($status, ['message' => $message], $code);
    }

    public function message($message, $status = 'success') {
        return $this->status($status, ['message' => $message]);
    }

    public function internalError($message = 'Internal Error') {
        return $this->setStatusCode(FoundationResponse::HTTP_INTERNAL_SERVER_ERROR)->failed($message);
    }

    /**
     * @param AbstractPaginator|mixed $data
     * @param string                  $status
     * @param array                   $extends
     *
     * @return mixed
     */
    public function success($data, $status = 'success', $extends = []) {
        if ($data instanceof Paginator) {
            $page = Arr::except($data->toArray(), ['data']);
            $data = $data->getCollection();
        } elseif ($data instanceof ResourceCollection) {
            $page = Arr::except($data->resource->toArray(), ['data']);
        }

        if (isset($page)) {
            $data = compact('page', 'data');
        } else {
            $data =compact('data');
        }

        if ($extends && is_array($data)) {
            $data = array_merge($data, $extends);
        }

        return $this->status($status, $data);
    }

    public function resource($resource) {
        $status = [
            'status' => 'success',
            'code' => $this->statusCode
        ];
        return $resource->additional($status);
    }

    public function notFound($message = 'Not Found!') {
        return $this->setStatusCode(FoundationResponse::HTTP_NOT_FOUND)->failed($message);
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SurgicalPointValuesRepositoryEloquent;
use App\Http\Requests\SurgicalPointValues\GetListRequest;
use App\Http\Requests\SurgicalPointValues\GetItemRequest;
use App\Http\Requests\SurgicalPointValues\CreateRequest;
use App\Http\Requests\SurgicalPointValues\UpdateRequest;
use App\Http\Requests\SurgicalPointValues\DeleteRequest;
use App\Http\Requests\SurgicalPointValues\CheckRequest;

/**
 * 21.手术项目点值
 *
 * Class SurgicalPointValuesController
 * @package App\Http\Controllers\Api
 */
class SurgicalPointValuesController extends Controller {
    public $repository = null;

    public function __construct(SurgicalPointValuesRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }


    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        $this->repository->delete($id);

        return $this->success(null);
    }

    /**
     * 核对缺失数据
     * @return mixed
     */
    public function getDiff(CheckRequest $request){
        $data = $this->repository->getDiff();

        return $this->success($data);
    }
}

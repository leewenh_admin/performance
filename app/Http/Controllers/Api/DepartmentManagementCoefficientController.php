<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\DepartmentManagementCoefficientRepositoryEloquent;
use App\Http\Requests\DepartmentManagementCoefficient\GetListRequest;
use App\Http\Requests\DepartmentManagementCoefficient\GetItemRequest;
use App\Http\Requests\DepartmentManagementCoefficient\CreateRequest;
use App\Http\Requests\DepartmentManagementCoefficient\UpdateRequest;
use App\Http\Requests\DepartmentManagementCoefficient\DeleteRequest;

/**
 * 39.科室管理系数
 *
 * Class DepartmentManagementCoefficientController
 * @package App\Http\Controllers\Api
 */
class DepartmentManagementCoefficientController extends Controller {
    public $repository = null;

    public function __construct(DepartmentManagementCoefficientRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }


    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        $this->repository->delete($id);

        return $this->success(null);
    }
}

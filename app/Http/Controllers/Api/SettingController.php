<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SettingRepositoryEloquent;
use App\Http\Requests\Setting\GetListRequest;
use App\Http\Requests\Setting\GetItemRequest;
use App\Http\Requests\Setting\CreateRequest;
use App\Http\Requests\Setting\UpdateRequest;
use App\Http\Requests\Setting\DeleteRequest;

/**
 * 11.配置项管理
 *
 * Class SettingController
 * @package App\Http\Controllers\Api
 */
class SettingController extends Controller {
    public $repository = null;
    
    public function __construct(SettingRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }

    
    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        $this->repository->delete($id);

        return $this->success(null);
    }

    public function administrativeAverage(Request $request){
        $this->repository->administrativeAverage($request->all());
        return $this->success(null);
    }


    public function getAdministrativeAverage(Request $request){
        $data = $this->repository->getAdministrativeAverage($request->all());
        return $this->success(["administrativeAverage" => $data]);
    }

    public function specialPerformance(Request $request){
        $res = $this->repository->specialPerformance($request->all());
        if($res){
            return $this->failed($res);
        }
        return $this->success($res);
    }


    public function getSpecialPerformance(Request $request){
        $data = $this->repository->getSpecialPerformance($request->all());
        return $this->success(["performance" => $data]);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Repositories\CheckDataRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\DataRepositoryEloquent;
use App\Http\Requests\Data\ImportRequest;
use App\Http\Requests\Data\DeleteRequest;

/**
 * 10.基础数据管理
 *
 * Class DataController
 * @package App\Http\Controllers\Api
 */
class DataController extends Controller {
    public $repository = null;
    
    public function __construct(DataRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 数据导入
     *
     * @param ImportRequest $request
     * @return mixed
     */
    public function import(ImportRequest $request) {
        $importer = $this->repository->getDataImporter(
            $request->get("data_type", 0)
        );
        $startTime = date("Y-m-d H:i:s",time());
        $importer->import();
        $endTime = date("Y-m-d H:i:s",time());

        $importCount = $this->repository->getDataCount(
            $request->get("data_type", 0),
            $startTime,
            $endTime
        );

        $data = [
            "import_count" => $importCount
        ];

        return $this->success($data);
    }

    /**
     * 数据删除
     *
     * @param DeleteRequest $request
     * @return mixed
     * @throws \App\Exceptions\MessageException
     */
    public function delete(DeleteRequest $request) {
        if (request('password')){
            (new CheckDataRepositoryEloquent())->checkPassword(request('password'));
        }
        $model = $this->repository->getModel(request("data_type", 0));
        $this->repository->deleteData($model, request('date'));
        return $this->success(null);
    }

    public function export(ImportRequest $request){

    }
}

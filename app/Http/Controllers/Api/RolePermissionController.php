<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\RolePermissionRepositoryEloquent;
use App\Http\Requests\RolePermission\GetListRequest;
use App\Http\Requests\RolePermission\GetItemRequest;
use App\Http\Requests\RolePermission\CreateRequest;
use App\Http\Requests\RolePermission\UpdateRequest;
use App\Http\Requests\RolePermission\DeleteRequest;
use App\Http\Requests\RolePermission\DetailRequest;

/**
 * 7.角色权限绑定关系
 *
 * Class RolePermissionController
 * @package App\Http\Controllers\Api
 */
class RolePermissionController extends Controller {
    public $repository = null;
    
    public function __construct(RolePermissionRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }

    
    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        $this->repository->delete($id);

        return $this->success(null);
    }


    /**
     * 查询角色权限
     * @param DetailRequest $request
     * @return mixed
     */
    public function getRoleDetail(DetailRequest $request){
        $data = $this->repository->getRolePermission();

        return $this->success($data);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\NewTechnologyItemRepositoryEloquent;

class TestController extends Controller {

    protected $repository;

    public function __construct(
        NewTechnologyItemRepositoryEloquent $repository
    ){
        $this->repository = $repository;
    }

    public function do(Request $request)
    {
        $records = DB::connection('oracle')->table('QYSL202106')
            ->orderBy('收费时间', 'asc')
            ->skip(10)
            ->take(10)
            ->get();
        dd($records);

    }
}

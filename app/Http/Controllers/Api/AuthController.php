<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Auth\PasswordCheckRequest;
use App\Repositories\CheckDataRepositoryEloquent;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\MessageException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\LogoutRequest;
use App\Http\Requests\Auth\PasswordChangeRequest;
use App\Http\Requests\Auth\PasswordResetRequest;
use App\Repositories\EmployeeRepositoryEloquent;
use App\Repositories\PermissionRepositoryEloquent;

/**
 * 1.认证
 *
 * Class AuthController
 * @package App\Http\Controllers\Api
 */
class AuthController extends Controller {

    public $employeeRepository;
    public $permissionRepository;

    public function __construct(
        EmployeeRepositoryEloquent $employeeRepository,
        PermissionRepositoryEloquent $permissionRepository
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * 登录
     *
     * @param LoginRequest $request
     * @return mixed
     * @throws MessageException
     */
    public function login(LoginRequest $request) {
        $this->employeeRepository->getItem(1);
        $credentials = request(['code', 'password']);
        $token = auth()->attempt($credentials);

        if (!$token) {
            throw new MessageException("用户或者密码错误");
        }

        $data = ["token" => $token];
    
        $user = auth()->user()->employee;

        $userPermIds = $this->employeeRepository
            ->getPermissionIdList($user->id)
            ->pluck("permissions_id")->all();
        $userPerms = $this->permissionRepository
            ->getNameListById($userPermIds);

        $data["employee"] = [
            "name" => $user->name,
            "code" => $user->code,
            "permissions" => $userPerms
    	];

        return $this->success($data);
    }

    /**
     * 退出登录
     *
     * @param LogoutRequest $request
     * @return mixed
     */
    public function logout(LogoutRequest $request) {
        auth()->logout();
        return $this->success(null);
    }

    /**
     * 修改密码
     *
     * @param PasswordChangeRequest $request
     * @return mixed
     * @throws MessageException
     */
    public function changePassword(PasswordChangeRequest $request) {
        $passwords = request(['old_password', 'new_password']);

        if (!Hash::check($passwords["old_password"], auth()->user()->password)) {
            throw new MessageException("输入的旧密码与当前密码不匹配");
        }

        $this->employeeRepository->setPassword(
            auth()->user()->code,
            $passwords["new_password"]
        );

        return $this->success(null);
    }

    /**
     * 管理员重设密码
     *
     * @param PasswordResetRequest $request
     * @return mixed
     */
    public function resetPassword(PasswordResetRequest $request) {
        $this->employeeRepository->setPassword(
            $request->input("employee_code"),
            $request->input("new_password")
        );

        return $this->success(null);
    }

    /**
     * 核对登录密码
     * @param PasswordCheckRequest $request
     * @return mixed
     * @throws MessageException
     */
    public function checkPassword(PasswordCheckRequest $request){
        (new CheckDataRepositoryEloquent())->checkPassword(request('password'));

        return $this->success(null);
    }
}

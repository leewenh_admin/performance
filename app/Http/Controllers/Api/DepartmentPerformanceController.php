<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\DepartmentPerformance\ShowDetailRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\DepartmentPerformanceRepositoryEloquent;
use App\Http\Requests\DepartmentPerformance\GetListRequest;
use App\Http\Requests\DepartmentPerformance\GetItemRequest;
use App\Http\Requests\DepartmentPerformance\CreateRequest;
use App\Http\Requests\DepartmentPerformance\UpdateRequest;
use App\Http\Requests\DepartmentPerformance\DeleteRequest;

/**
 * 19.科室绩效核算
 *
 * Class DepartmentPerformanceController
 * @package App\Http\Controllers\Api
 */
class DepartmentPerformanceController extends Controller {
    public $repository = null;

    public function __construct(DepartmentPerformanceRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }


    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        $this->repository->delete($id);

        return $this->success(null);
    }

    /**
     * 查看科室绩效详细数据
     *
     * @param ShowDetailRequest $request
     * @param $id  int 绩效数据记录ID
     * @return mixed
     */
    public function showDetail(ShowDetailRequest $request,int $id) {
        $detail_type = $request->get("detail_type", 0);
        $data = $this->repository->getDetail(
            $request->date, $id, $detail_type
        );
        return $this->success($data);
    }

    /**
     * 更新特殊科室绩效
     * @param Request $request
     * @return mixed
     */
    public function specialPerformance(Request $request){
        $this->repository->specialPerformance($request->all());
        return $this->success(null);
    }
}

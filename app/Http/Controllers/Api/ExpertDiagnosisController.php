<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ExpertDiagnosisRepositoryEloquent;
use App\Http\Requests\ExpertDiagnosis\GetListRequest;
use App\Http\Requests\ExpertDiagnosis\GetItemRequest;
use App\Http\Requests\ExpertDiagnosis\CreateRequest;
use App\Http\Requests\ExpertDiagnosis\UpdateRequest;
use App\Http\Requests\ExpertDiagnosis\DeleteRequest;

/**
 * 30. 专家号统计数据
 *
 * Class ExpertDiagnosisController
 * @package App\Http\Controllers\Api
 */
class ExpertDiagnosisController extends Controller {
    public $repository = null;

    public function __construct(ExpertDiagnosisRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }


    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        $this->repository->delete($id);

        return $this->success(null);
    }
}

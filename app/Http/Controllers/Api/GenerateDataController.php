<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\GenerateData\GenerateRequest;
use App\Jobs\GeneratePerformanceJob;
use App\Repositories\CheckDataRepositoryEloquent;
use App\Repositories\DepartmentPerformanceRepositoryEloquent;
use App\Http\Controllers\Controller;
use App\Repositories\DirectCostRepositoryEloquent;
use App\Repositories\NightPerformanceCalculateEloquent;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

/**
 * 35.生成综合考核绩效
 *
 * Class GenerateDataController
 * @package App\Http\Controllers\Api
 */
class GenerateDataController extends Controller {

    protected $departmentPerformance;
    protected $directCost;
    protected $specialPerformance;
    protected $checkData;

    public function __construct(
        DepartmentPerformanceRepositoryEloquent $departmentPerformance,
        DirectCostRepositoryEloquent $directCost,
        NightPerformanceCalculateEloquent $specialPerformance,
        CheckDataRepositoryEloquent $checkData
    ){
        $this->departmentPerformance = $departmentPerformance;
        $this->directCost = $directCost;
        $this->specialPerformance = $specialPerformance;
        $this->checkData = $checkData;
    }

    /**
     * 生成固定成本
     *
     * @param GenerateRequest $request
     * @return mixed
     */
    public function directCost(GenerateRequest $request)
    {
        if (request('password')){
            $this->checkData->checkPassword(request('password'));
        }
        $date = request("date", date("Y-m"));
        $res = $this->checkData->checkDirectCost($date);
        if ($res != "success"){
            $data = [
                "status" => "filed",
                "data" => $res,
            ];

            return $data;
        }
        Cache::put('generate', 'directCost', 30);
        try {
            GeneratePerformanceJob::dispatch('directCost', $date);
//            $this->directCost->generateData($date);
//            Cache::forget('generate');
        }catch (\Exception $exception){
            Cache::forget('generate');
            Log::error($exception->getMessage().'--'.$exception->getFile().'--'.$exception->getLine());
            return $this->success(["msg" =>"数据生成失败"], 'error');
        }

        return $this->success(null);
    }

    /**
     * 生成科室绩效
     *
     * @param GenerateRequest $request
     * @return mixed
     */
    public function departmentPerformance(GenerateRequest $request)
    {
        if (request()->exists('password')){
            $this->checkData->checkPassword(request('password'));
        }
        $date = request("date", date("Y-m"));
        $res = $this->checkData->checkDepartmentPerformance($date);
        if ($res != "success"){
            $data = [
                "status" => "filed",
                "data" => $res,
            ];

            return $data;
        }
        Cache::put('generate', 'departmentPerformance', 30);
        try {
            GeneratePerformanceJob::dispatch('departmentPerformance', $date);
//            $this->departmentPerformance->generatePerformanceData($date);
//            Cache::forget('generate');
        }catch (\Exception $exception){
            Cache::forget('generate');
            Log::error($exception->getMessage().'--'.$exception->getFile().'--'.$exception->getLine());
            return $this->success(["msg" =>"数据生成失败"], 'error');
        }
        return $this->success(null);
    }

    /**
     * 生成专项绩效
     *
     * @param GenerateRequest $request
     * @return mixed
     */
    public function specialPerformance(GenerateRequest $request)
    {
        if (request()->exists('password')){
            $this->checkData->checkPassword(request('password'));
        }
        $date = request("date", date("Y-m"));
        $res = $this->checkData->checkSpecialPerformance($date);
        if ($res != "success"){
            $data = [
                "status" => "filed",
                "data" => $res,
            ];

            return $data;
        }
        Cache::put('generate', 'specialPerformance', 30);
        try {
            GeneratePerformanceJob::dispatch('specialPerformance', $date);
//            $this->specialPerformance->generateSpecialPerformance($date);
//            Cache::forget('generate');
        }catch (\Exception $e){
            Cache::forget('generate');
            Log::error($e->getMessage().'--'.$e->getFile().'--'.$e->getLine());
            return $this->success(["msg" =>"数据生成失败"], 'error');
        }
        return $this->success(null);
    }


    /**
     * 清空绩效数据
     *
     * @param GenerateRequest $request
     * @return mixed
     */
    public function clearPerformance(GenerateRequest $request)
    {
        if (request()->exists('password')){
            $this->checkData->checkPassword(request('password'));
        }
        $date = request("date", date("Y-m"));
        $this->departmentPerformance->clearPerformance($date);

        return $this->success(null);
    }

    /**
     * 查询数据生成状态
     * @return mixed
     */
    public function checkStatus(){
        $value = Cache::get('generate');
        $data = [
          "type" => $value
        ];
        return $this->success($data);
    }

    /**
     * 锁定绩效数据
     * @param GenerateRequest $request
     * @return mixed
     * @throws \App\Exceptions\MessageException
     */
    public function lockPerformance(GenerateRequest $request){
        $date = request("date", date("Y-m"));
        $this->departmentPerformance->lockPerformance($date);

        return $this->success(null);
    }

    /**
     * 检查绩效是否锁定
     * @param GenerateRequest $request
     * @return mixed
     */
    public function checkLockStatus(GenerateRequest $request){
        $date = request("date", date("Y-m"));
        $data = $this->departmentPerformance->checkLockStatus($date);
        return $this->success($data);

    }


}
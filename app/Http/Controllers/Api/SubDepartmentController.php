<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SubDepartmentRepositoryEloquent;
use App\Http\Requests\SubDepartment\GetListRequest;
use App\Http\Requests\SubDepartment\GetItemRequest;
use App\Http\Requests\SubDepartment\CreateRequest;
use App\Http\Requests\SubDepartment\UpdateRequest;
use App\Http\Requests\SubDepartment\DeleteRequest;

/**
 * 3.二级科室管理
 *
 * Class SubDepartmentController
 * @package App\Http\Controllers\Api
 */
class SubDepartmentController extends Controller {
    public $repository = null;
    
    public function __construct(SubDepartmentRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }

    
    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        $this->repository->delete($id);

        return $this->success(null);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\EmployeePerformance\GenerateRequest;
use App\Jobs\GeneratePerformanceJob;
use App\Repositories\CheckDataRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\EmployeePerformanceRepositoryEloquent;
use App\Http\Requests\EmployeePerformance\GetListRequest;
use App\Http\Requests\EmployeePerformance\GetItemRequest;
use App\Http\Requests\EmployeePerformance\CreateRequest;
use App\Http\Requests\EmployeePerformance\UpdateRequest;
use App\Http\Requests\EmployeePerformance\DeleteRequest;
use Illuminate\Support\Facades\Cache;

/**
 * 53.个人绩效
 *
 * Class EmployeePerformanceController
 * @package App\Http\Controllers\Api
 */
class EmployeePerformanceController extends Controller {
    public $repository = null;

    public function __construct(EmployeePerformanceRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }


    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        $this->repository->delete($id);

        return $this->success(null);
    }

    /**
     * 生成个人绩效数据
     * @param GenerateRequest $request
     * @return mixed
     */
    public function generate(GenerateRequest $request){
        if (request('password')){
            (new CheckDataRepositoryEloquent())->checkPassword(request('password'));
        }
        $date = request("date", date("Y-m"));
        $res = $this->repository->checkData($date);
        if ($res != "success"){
            $data = [
                "status" => "filed",
                "data" => $res,
            ];

            return $data;
        }
        Cache::put('generate', 'employeePerformance', 30);
        try {
            GeneratePerformanceJob::dispatch('employeePerformance', $date);
        }catch (\Exception $e){
            Cache::forget('generate');
            return $this->success(["msg" =>"数据生成失败"], 'error');
        }

        return $this->success(null);
    }
}

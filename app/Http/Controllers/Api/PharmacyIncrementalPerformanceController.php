<?php

namespace App\Http\Controllers\Api;

use App\Repositories\CheckDataRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PharmacyIncrementalPerformanceRepositoryEloquent;
use App\Http\Requests\PharmacyIncrementalPerformance\GetListRequest;
use App\Http\Requests\PharmacyIncrementalPerformance\GetItemRequest;
use App\Http\Requests\PharmacyIncrementalPerformance\CreateRequest;
use App\Http\Requests\PharmacyIncrementalPerformance\UpdateRequest;
use App\Http\Requests\PharmacyIncrementalPerformance\DeleteRequest;

/**
 * 48.药剂科增量绩效
 *
 * Class PharmacyIncrementalPerformanceController
 * @package App\Http\Controllers\Api
 */
class PharmacyIncrementalPerformanceController extends Controller {
    public $repository = null;

    public function __construct(PharmacyIncrementalPerformanceRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        if (request('password')){
            (new CheckDataRepositoryEloquent())->checkPassword(request('password'));
        }
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }


    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        if (request('password')){
            (new CheckDataRepositoryEloquent())->checkPassword(request('password'));
        }
        $this->repository->delete($id);

        return $this->success(null);
    }
}

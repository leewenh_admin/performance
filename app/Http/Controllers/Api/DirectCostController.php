<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\DirectCost\DelRequest;
use App\Http\Requests\DirectCost\GetListRequest;
use App\Http\Requests\DirectCost\GetDetailRequest;
use App\Repositories\CheckDataRepositoryEloquent;
use App\Repositories\DirectCostRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


/**
 * 12.直接费用
 *
 * Class DirectCostController
 * @package App\Http\Controllers\Api
 */
class DirectCostController extends Controller
{
    public $repository = null;

    public function __construct(DirectCostRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 直接费用详情
     * @param GetDetailRequest $request
     * @return mixed
     */
    public function getDirectCostDetail(GetDetailRequest $request){
        //$detail_type = $request->get("detail_type", 0);
        $department_id = request("departments_id", null);
        $date = request("date", null);
        $data = $this->repository->getBaseSalaryDetail($date, $department_id);

        return $this->success($data);
    }

    /**
     * 五险一金详情
     * @param GetDetailRequest $request
     * @return mixed
     */
    public function getSocialDetail(GetDetailRequest $request){
        //$detail_type = $request->get("detail_type", 0);
        $department_id = request("departments_id", null);
        $date = request("date", null);
        $data = $this->repository->getSocialDetail($date, $department_id);

        return $this->success($data);
    }

    /**
     * 清空固定成本
     * @param DelRequest $request
     * @return mixed
     */
    public function delData(DelRequest $request){
        if (request('password')){
            (new CheckDataRepositoryEloquent())->checkPassword(request('password'));
        }
        $this->repository->delData();

        return $this->success(null);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Repositories\CheckDataRepositoryEloquent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\NewTechnologyItemRepositoryEloquent;
use App\Http\Requests\NewTechnologyItem\GetListRequest;
use App\Http\Requests\NewTechnologyItem\GetItemRequest;
use App\Http\Requests\NewTechnologyItem\CreateRequest;
use App\Http\Requests\NewTechnologyItem\UpdateRequest;
use App\Http\Requests\NewTechnologyItem\DeleteRequest;

/**
 * 37.新技术业务绩效
 *
 * Class NewTechnologyItemController
 * @package App\Http\Controllers\Api
 */
class NewTechnologyItemController extends Controller {
    public $repository = null;

    public function __construct(NewTechnologyItemRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        if (request('password')){
            (new CheckDataRepositoryEloquent())->checkPassword(request('password'));
        }
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }


    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        if (request('password')){
            (new CheckDataRepositoryEloquent())->checkPassword(request('password'));
        }
        $this->repository->delete($id);

        return $this->success(null);
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MainIncomeRepositoryEloquent;
use App\Http\Requests\MainIncome\GetListRequest;
use App\Http\Requests\MainIncome\GetItemRequest;
use App\Http\Requests\MainIncome\CreateRequest;
use App\Http\Requests\MainIncome\UpdateRequest;
use App\Http\Requests\MainIncome\DeleteRequest;

/**
 * 13.主要收入数据
 *
 * Class MainIncomeController
 * @package App\Http\Controllers\Api
 */
class MainIncomeController extends Controller {
    public $repository = null;

    public function __construct(MainIncomeRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);
        $data->each(function ($item){
            $item->prescribe_doctor_name = $item->prescribe_doctor->name ?? '';
            $item->execute_doctor_name = $item->execute_doctor->name ?? '';
            $item->prescribe_department_name = $item->prescribe_department->name ?? '';
            $item->execute_department_name = $item->execute_department->name ?? '';
            $item->admission_department_name = $item->admission_department->name ?? '';
            $item->leave_department_name = $item->leave_department->name ?? '';
            return $item;
        });


        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }


    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        $this->repository->delete($id);

        return $this->success(null);
    }
}

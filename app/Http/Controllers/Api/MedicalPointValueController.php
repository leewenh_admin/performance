<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\SurgicalPointValues\CheckRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MedicalPointValueRepositoryEloquent;
use App\Http\Requests\MedicalPointValue\GetListRequest;
use App\Http\Requests\MedicalPointValue\GetItemRequest;
use App\Http\Requests\MedicalPointValue\CreateRequest;
use App\Http\Requests\MedicalPointValue\UpdateRequest;
use App\Http\Requests\MedicalPointValue\DeleteRequest;

/**
 * 22.医疗服务项目点值
 *
 * Class MedicalPointValueController
 * @package App\Http\Controllers\Api
 */
class MedicalPointValueController extends Controller {
    public $repository = null;

    public function __construct(MedicalPointValueRepositoryEloquent $repository) {
        $this->repository = $repository;
    }

    /**
     * 列表
     *
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $query = $this->repository->getList();
        $data = $this->repository->parseListResult($query);

        return $this->success($data);
    }

    /**
     * 详情
     *
     * @param GetItemRequest $request
     * @return mixed
     */
    public function getItem(GetItemRequest $request, $id) {
        $item = $this->repository->getItem($id);

        return $this->success($item);
    }

    /**
     * 新建
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request) {
        $this->repository->insert($request->all());

        return $this->success(null);
    }

    /**
     * 修改
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function update(UpdateRequest $request, $id) {
        $this->repository->update($id, $request->all());

        return $this->success(null);
    }


    /**
     * 删除
     *
     * @param DeleteRequest $request
     * @return mixed
     */
    public function delete(DeleteRequest $request, $id) {
        $this->repository->delete($id);

        return $this->success(null);
    }

    /**
     * 核对缺失数据
     * @return mixed
     */
    public function getDiff(CheckRequest $request){
        $data = $this->repository->getDiff();

        return $this->success($data);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Events\ExportPerformanceEvent;
use App\Http\Requests\Export\ExportRequest;
use App\Repositories\ExportDetail\BaseExportDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

/**
 * 52.导出详情数据
 *
 * Class ExportDataController
 * @package App\Http\Controllers\Api
 */
class ExportDataController extends Controller {
    public $repository = null;

    public function __construct() {
        $this->repository = new BaseExportDetail();
    }

    /**
     * 判断是否已生成绩效文件
     * @param ExportRequest $request
     * @param $date
     * @return mixed
     */
    public function checkFile(ExportRequest $request, $date){
        $file = file_exists(public_path("export/管理绩效明细{$date}.zip"));
        $data = [
            'hasfile' => $file
        ];

        return $this->success($data);
    }

    /**
     * 导出绩效详情数据
     * @param ExportRequest $request
     * @param $date
     * @return mixed
     */
    public function export(ExportRequest $request, $date){
        $status = $this->repository->checkData($date);
        if (!empty($status)){
            return $status;
        }
        event(New ExportPerformanceEvent($this->repository, $date));

        return $this->success(null);
    }
}

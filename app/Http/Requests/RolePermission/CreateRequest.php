<?php

namespace App\Http\Requests\RolePermission;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "roles_id" => "required|integer",
            "permissions_id" => "array"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "roles_id" => "角色ID",
            "permissions_id" => "权限ID"
        ];
    }
}

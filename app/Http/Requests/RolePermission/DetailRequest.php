<?php

namespace App\Http\Requests\RolePermission;

use App\Http\Requests\BaseRequest;

class DetailRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "roles_id" => "integer",
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "roles_id" => "角色ID",
        ];
    }
}

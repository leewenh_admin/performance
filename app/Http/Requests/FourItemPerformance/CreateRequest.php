<?php

namespace App\Http\Requests\FourItemPerformance;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                'department_id' => 'integer',
                'date' => 'string',
                'physical_performance' => 'nullable|numeric',
                'stroke_performance' => 'nullable|numeric',
                'control_performance' => 'nullable|numeric',
                'running_performance' => 'nullable|numeric',
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
                'department_id' => '科室ID，必填',
                'date' => '年月日期，必填',
                'physical_performance' => '单位体检绩效',
                'stroke_performance' => '卒中绩效',
                'control_performance' => '疫情防控',
                'running_performance' => '科室跑账',
            ];
    }
}

<?php

namespace App\Http\Requests\DieaseCatalog;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "name" => "required|string",
            "code" => "required|string",
            "weights" => "required|numeric",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
            "name" => "疾病名称,必填",
            "code" => "疾病编码,必填",
            "weights" => "权重,必填",
            ];
    }
}

<?php

namespace App\Http\Requests\SubDepartment;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "name" => "required|string",
            "parent_id" => "required|integer",
            "code" => "required|string",
            "type_lv1" => "integer",
            "type_lv2" => "integer",
            "type_lv3" => "integer"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "name" => "科室名称",
            "parent_id" => "一级科室ID",
            "code" => "科室号",
            "type_lv1" => "科室层级1",
            "type_lv2" => "科室层级2",
            "type_lv3" => "科室层级3"
        ];
    }
}

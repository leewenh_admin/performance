<?php

namespace App\Http\Requests\SubDepartment;

use App\Http\Requests\BaseRequest;

class GetListRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = true;

    public function rules() {
        return parent::rules() + [
            "department_id" => "integer",
            "name" => "string",
            "code" => "string",
            "type_lv1" => "integer",
            "type_lv2" => "integer",
            "type_lv3" => "integer",
            "is_performance" => "boolean"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "department_id" => "一级科室ID",
            "name" => "科室名称",
            "code" => "工号",
            "type_lv1" => "科室类型1",
            "type_lv2" => "科室类型2",
            "type_lv3" => "科室类型3",
            "is_performance" => "是否参与绩效"
        ];
    }

}

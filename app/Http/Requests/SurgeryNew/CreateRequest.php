<?php

namespace App\Http\Requests\SurgeryNew;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
              "date" => "required|string",
              "surgery_time" => "required|string",
              "sub_departments_id" =>"required|integer",
              "inpatient_code" => "required|string",
              "record_code" => "required|string",
              "bed_code" => "string|nullable",
              "inpatient_name" => "required|string",
              "age" => "required|string",
              "surgery_type" => "required|integer",
              "code" => "required|string",
              "title" => "required|string",
              "level" => "required|string",
              "incision" => "string|nullable",
              "execute_doctor" => "required|string",
              "surgery_assistant1" => "string|nullable",
              "surgery_assistant2" => "string|nullable",
              "anesthesia_doctor" => "string|nullable",
              "anesthesia_type" => "string|nullable",
              "surgery_nurse" => "string|nullable",
              "clinical_diagnose" => "string|nullable",
              "HIV" => "required|string",
              "HCV" => "required|string",
              "HBSAG" => "required|string",
              "syphilis" => "required|string",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
              "date" => "年月日期",
              "surgery_time" => "手术日期",
              "sub_departments_id" => "二级科室ID",
              "inpatient_code" => "住院号",
              "record_code" => "档案号",
              "bed_code" => "床号,选填",
              "inpatient_name" => "姓名",
              "age" => "年龄",
              "surgery_type" => "类别",
              "code" => "编号",
              "title" => "手术名称",
              "level" => "手术级别",
              "incision" => "切口,选填",
              "execute_doctor" => "手术医师",
              "surgery_assistant1" => "手术一助,选填",
              "surgery_assistant2" => "手术二助,选填",
              "anesthesia_doctor" => "麻醉医师,选填",
              "anesthesia_type" => "麻醉方式,选填",
              "surgery_nurse" => "手术护士,选填",
              "clinical_diagnose" => "临床诊断,选填",
              "HIV" => "HIV",
              "HCV" => "HCV",
              "HBSAG" => "HBSAG",
              "syphilis" => "梅毒",
            ];
    }
}

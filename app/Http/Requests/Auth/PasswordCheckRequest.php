<?php


namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;

class PasswordCheckRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            'password' => 'required|string',
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            'password' => '密码',
        ];
    }
}

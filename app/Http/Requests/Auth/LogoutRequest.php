<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;

class LogoutRequest extends BaseRequest {
    protected $is_need_auth = true;
}

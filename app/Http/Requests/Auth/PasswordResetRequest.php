<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;

class PasswordResetRequest extends BaseRequest
{
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "employee_code" => "required|string",
            "new_password" => "required|string|confirmed",
            "new_password_confirmation" => "required|string"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "employee_code" => "职员工号",
            "new_password" => "新密码",
            "new_password_confirmation" => "重复新密码",
        ];
    }
}

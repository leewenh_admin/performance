<?php


namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;

class PasswordChangeRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            'old_password' => 'required|string',
            'new_password' => 'required|string|confirmed',
            "new_password_confirmation" => "required|string"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            'old_password' => '旧密码',
            'new_password' => '新密码',
            'new_password_confirmation' => '重复新密码',
        ];
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 2021/3/9
 * Time: 11:39
 */

namespace App\Http\Requests\Auth;

use App\Http\Requests\BaseRequest;

class LoginRequest extends BaseRequest {
    public function rules() {
        return parent::rules() + [
                'code' => 'required|string',
                'password' => 'required|string',
            ];
    }

    public function attributes() {
        return parent::attributes() + [
                'code' => '职员工号',
                'password' => '密码',
            ];
    }
}

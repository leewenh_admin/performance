<?php

namespace App\Http\Requests\SurgicalPointValues;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "belong_department_id" => "required|integer",
            "execute_department_id" => "required|integer",
            "project" => "required|string",
            "code" => "required|string",
            "gb_code" => "required|string",
            "price" => "required|string",
            "price_type" => "required|string",
            "project_type" => "required|string",
            "project_code" => "required|string",
            "name" => "required|string",
            "project_include" => "string|nullable",
            "include_consumable" => "string|nullable",
            "exclude_consumable" => "string|nullable",
            "lowvalue_consumable" => "required|integer",
            "consume_explain" => "string|nullable",
            "technical_difficulty1" => "required|string",
            "risk_degree1" => "required|string",
            "charge_unit" => "required|string",
            "charge_explain" => "string|nullable",
            "duration" => "required|integer",
            "technical_difficulty2" => "required|numeric",
            "risk_degree2" => "required|numeric",
            "difficulty_risk_weights" => "required|numeric",
            "duration_weights" => "required|numeric",
            "performance_weights" => "required|numeric",
            ];
    }

    public function attributes() {
        return parent::attributes() + [
            "belong_department_id" => "所在二级科室ID,必填",
            "execute_department_id" => "执行二级科室ID,必填",
            "project" => "项目,必填",
            "code" => "编码,必填",
            "gb_code" => "国标码,必填",
            "price" => "价格,必填",
            "price_type" => "价格类别,必填",
            "project_type" => "项目分类,必填",
            "project_code" => "项目编码,必填",
            "name" => "项目名称",
            "project_include" => "项目内涵",
            "include_consumable" => "内涵一次性耗材",
            "exclude_consumable" => "除外耗材,必填",
            "lowvalue_consumable" => "低值耗材,必填",
            "consume_explain" => "基本人力消耗及耗时",
            "technical_difficulty1" => "技术难度1,必填",
            "risk_degree1" => "风险程度1,必填",
            "charge_unit" => "计价单位,必填",
            "charge_explain" => "计价说明",
            "duration" => "时间(分钟),必填",
            "technical_difficulty2" => "技术难度2,必填",
            "risk_degree2" => "风险程度2,必填",
            "difficulty_risk_weights" => "难度风险权重,必填",
            "duration_weights" => "时间权重,必填",
            "performance_weights" => "绩效权重,必填",
            ];
    }
}

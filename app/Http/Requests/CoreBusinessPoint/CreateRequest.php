<?php

namespace App\Http\Requests\CoreBusinessPoint;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                "date" => "required|string",
                "department_id" => "required|numeric",
                "point" => "required|numeric",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
                "date" => "年月日期，必填",
                "department_id" => "科室id，必填",
                "point" => "核心业务点值，必填",
            ];
    }
}

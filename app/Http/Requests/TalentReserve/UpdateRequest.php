<?php

namespace App\Http\Requests\TalentReserve;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                "employees_id" => "required|integer",
                "code" => "required|integer",
                "name" => "required|string",
                "start_date" => "required|string",
                "end_date" => "nullable|string",
                "remark" => "nullable|string",
            ];
    }

    public function attributes() {
        return parent::attributes() + [
                "employees_id" => "职员id",
                "code" => "工号",
                "name" => "姓名",
                "start_date" => "开始发放时间",
                "end_date" => "终止时间",
                "remark" => "备注,选填",
            ];
    }
}

<?php

namespace App\Http\Requests\SpecialPerformanceSetting;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                "date" => "required|string",
                "department_id" => "required|numeric",
                "performance" => "required|numeric",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
                "date" => "年月日期，必填",
                "department_id" => "科室id，必填",
                "performance" => "特殊科室绩效，必填",
            ];
    }
}

<?php

namespace App\Http\Requests\Role;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "name" => "required|string",
            "display_name" => "required|string",
            "description" => "required|string"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "name" => "角色名称",
            "display_name" => "角色显示名称",
            "description" => "角色描述"
        ];
    }
}

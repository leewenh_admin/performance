<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "key" => "required|string",
            "title" => "required|string",
            "value" => "required|string",
            "value_type" => "required|integer"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "key" => "键",
            "title" => "描述",
            "value" => "值",
            "value_type" => "值类型"
        ];
    }
}

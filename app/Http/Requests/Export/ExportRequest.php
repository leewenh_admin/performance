<?php

namespace App\Http\Requests\Export;

use App\Http\Requests\BaseRequest;

class ExportRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules();
    }

    public function attributes() {
        return parent::attributes();
    }
}

<?php

namespace App\Http\Requests\MedicalAttendanceScore;

use App\Http\Requests\BaseRequest;

class DeleteRequest extends BaseRequest {
    protected $is_need_auth = true;
}

<?php

namespace App\Http\Requests\MedicalAttendanceScore;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules();
    }

    public function attributes()
    {
        return parent::attributes();
    }
}

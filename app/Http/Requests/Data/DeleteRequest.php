<?php

namespace App\Http\Requests\Data;

use App\Http\Requests\BaseRequest;

class DeleteRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "date" => "required|string",
            "data_type" => "required|integer"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "date" => "年月日期, YYYY-mm",
            "data_type" => "数据类型"
        ];
    }
}

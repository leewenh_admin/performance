<?php

namespace App\Http\Requests\Data;

use App\Http\Requests\BaseRequest;

class ImportRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "file" => "required|file",
            "date" => "required|string",
            "data_type" => "required|integer"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "file" => "数据文件",
            "date" => "年月日期, YYYY-mm",
            "data_type" => "数据类型"
        ];
    }
}

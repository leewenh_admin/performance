<?php
/**
 * Created by PhpStorm.
 * User: Lee
 * Date: 2021/3/8
 * Time: 16:17
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest {
    protected $is_need_auth = false;
    protected $is_list_request = false;

    public function authorize() {
        $result = true;
    
        if ($this->is_need_auth) {
            $result = auth()->check();
        }

        return $result;
    }

    public function rules() {
        $result = [];

        if ($this->is_need_auth) {
            $result = $result + [
                'token' => 'string'
            ];
        }

        if ($this->is_list_request) {
            $result = $result + [
                "perPage" => "integer",
                "sort_str" => "string",
                "sort_order" => "string",
                "allPage" => "integer"
            ];
        }

        return $result;
    }

    public function attributes() {
        $result = [];
        if ($this->is_need_auth) {
            $result = $result + [
                'Token' => '用户Token'
            ];
        }

        if ($this->is_list_request) {
            $result = $result + [
                "perPage" => "每页条数, 可选, 默认25条",
                "sort_str" => "排序字段, 可选, 默认created_at",
                "sort_order" => "排序规则, 可选, asc, desc 默认desc",
                "allPage" => "是否分页, 可选, 1分页2不分页, 默认1"
            ];
        }
    
        return $result;
    }
}

<?php

namespace App\Http\Requests\EmployeeAttendance;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                'date' => 'required|string',
                'departments_id' => 'required|integer',
                'sub_departments_id' => 'required|integer',
                'employees_id' => 'required|integer',
                'code' => 'required|string',
                'actual_attendance' => 'integer',
                'workday' => 'integer',
                'front_line_rest' => 'integer',
                'unemployed' => 'integer',
                'business_trip' => 'integer',
                'study' => 'integer',
                'birth_control' => 'integer',
                'birth' => 'integer',
                'annual' => 'integer',
                'absence' => 'integer',
                'sick' => 'integer',
                'work_injury' => 'integer',
                'family_visit' => 'integer',
                'wedding' => 'integer',
                'bereavement' => 'integer',
                'radiate' => 'integer',
                'coefficient' => 'numeric',
                'attendance_coefficient' => 'numeric',
                'remark' => 'string|nullable',
            ];
    }

    public function attributes() {
        return parent::attributes() + [
                'date' => '年月日期(必填)YYYY-mm',
                'departments_id' => '一级科室ID(必填)',
                'sub_departments_id' => '二级科室ID(必填)',
                'employees_id' => '职员ID(必填)',
                'code' => '职员工号(必填)',
                'actual_attendance' => '实际出勤天数',
                'workday' => '上班天数',
                'front_line_rest' => '一线人员休整天数',
                'unemployed' => '待岗天数',
                'business_trip' => '外差天数',
                'study' => '进修天数',
                'birth_control' => '计生假天数',
                'birth' => '产假天数',
                'annual' => '年假天数',
                'absence' => '事假天数',
                'sick' => '病假天数',
                'work_injury' => '工伤天数',
                'family_visit' => '探亲假天数',
                'wedding' => '婚假天数',
                'bereavement' => '丧假天数',
                'radiate' => '放射假天数',
                'coefficient' => '系数',
                'attendance_coefficient' => '结合考勤系数',
                'remark' => '备注',
            ];
    }
}

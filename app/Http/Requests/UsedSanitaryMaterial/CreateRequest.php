<?php

namespace App\Http\Requests\UsedSanitaryMaterial;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                'date' => 'string',
                'sub_departments_id' => 'integer',
                'total_amount' => 'numeric',
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
                'date' => '领用时间 YYYY-mm',
                'sub_departments_id' => '二级科室ID',
                'total_amount' => '汇总金额',
            ];
    }
}

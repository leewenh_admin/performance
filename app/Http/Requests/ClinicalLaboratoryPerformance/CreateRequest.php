<?php

namespace App\Http\Requests\ClinicalLaboratoryPerformance;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
              "date" => "required|string",
              "department_id" => "required|integer",
              "point_value" => "required|numeric",
              "point_price" => "required|numeric",
              "check_num" => "required|numeric",
              "check_price" => "required|numeric",
              "performance" => "required|numeric",
              "amount" => "required|numeric",
              "rate" => "required|numeric",
              "actual_amount" => "required|numeric",
              "actual_rate" => "required|numeric",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
              "date" => "年月日期",
              "department_id" => "科室",
              "point_value" => "点值合计",
              "point_price" => "点值单价",
              "check_num" => "检查人次",
              "check_price" => "检查单价",
              "performance" => "绩效合计",
              "amount" => "收入",
              "rate" => "绩效占收入比%",
              "actual_amount" => "实收金额",
              "actual_rate" => "占实收金额%",
            ];
    }
}

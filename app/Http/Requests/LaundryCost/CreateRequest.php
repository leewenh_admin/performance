<?php

namespace App\Http\Requests\LaundryCost;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "date" => "required|string",
            "sub_departments_id" => "required|integer",
            "quilt" => "integer",
            "coverlet" => "integer",
            "pillowcase" => "integer",
            "worker_clothe" => "integer",
            "sick_clothes" => "integer",
            "total_count" => "integer",
            "surgery_count" => "integer",
            "used_bed_day" => "integer",
            "per_wash_fee" => "numeric",
            "amount" => "numeric",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
            "date" => "年月日期 Y-m 必填",
            "sub_departments_id" => "二级科室id，必填",
            "quilt" => "被套",
            "coverlet" => "床单",
            "pillowcase" => "枕套",
            "worker_clothe" => "工作服",
            "sick_clothes" => "病服",
            "total_count" => "合计件数",
            "surgery_count" => "手术台次",
            "used_bed_day" => "实际占床日",
            "per_wash_fee" => "每件洗涤费",
            "amount" => "金额",
            ];
    }
}

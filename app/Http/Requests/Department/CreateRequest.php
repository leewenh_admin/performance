<?php

namespace App\Http\Requests\Department;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "name" => "required|string",
            "house_price" => "numeric",
            "house_space" => "numeric",
            "service_income_target_value" => "numeric",
            "device_maintain" => "numeric",
            "current_period_controllable_variable_cost" => "numeric",
            "base_department_income" => "numeric",
            "base_period_controllable_variable_cost" => "numeric",
            "variable_cost_control_deduction_rate" => "numeric",
            "is_performance" => "boolean"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "name" => "科室名称",
            "house_price" => "房屋使用单价,选填, 默认0",
            "house_space" => "房屋面积,选填, 默认0",
            "service_income_target_value" => "医疗服务收入目标值,选填, 默认0",
            "device_maintain" => "设备维护费用,选填, 默认0",
            "current_period_controllable_variable_cost" => "本期可控变动成本,选填, 默认0",
            "base_department_income" => "基期科内有效收入,选填, 默认0",
            "base_period_controllable_variable_cost" => "基期可控变动成本,选填, 默认0",
            "variable_cost_control_deduction_rate" => "动成本控制扣罚比率,选填, 默认1",
            "is_performance" => "是否参与绩效"
        ];
    }
}

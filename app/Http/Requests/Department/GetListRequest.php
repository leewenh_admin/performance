<?php

namespace App\Http\Requests\Department;

use App\Http\Requests\BaseRequest;

class GetListRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = true;

    public function rules() {
        return parent::rules() + [
            "name" => "string",
            "is_performance" => "boolean"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "name" => "科室名称",
            "is_performance" => "是否参与绩效"
        ];
    }
}

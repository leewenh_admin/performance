<?php

namespace App\Http\Requests\DepartmentHeadSubtract;

use App\Http\Requests\BaseRequest;

class DeleteRequest extends BaseRequest {
    protected $is_need_auth = true;
}

<?php

namespace App\Http\Requests\AvgPerformanceSetting;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                "date" => "required|string",
                "performance" => "required|numeric",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
                "date" => "年月日期，必填",
                "performance" => "行政平均绩效，必填",
            ];
    }
}

<?php

namespace App\Http\Requests\Basedata;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                'department_id' => 'integer',
                'date' => 'date',
                'base_department_income' => 'numeric',
                'base_period_controllable_variable_cost' => 'numeric',
                'target_patient_count' => 'numeric',
                'base_use_bed_day' => 'numeric',
                'base_out_hospital' => 'numeric',
                'base_surgery_count' => 'numeric',
                'base_special_surgery_rate' => 'numeric',
                'base_special_surgery_count' => 'numeric',
                'point_price' => 'numeric',
                'cost_effective_price' => 'numeric',
                'outpatient_price' => 'numeric',
                'admission_price' => 'numeric',
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
                'department_id' => '科室id，必填',
                'date' => '年月日期，必填',
                'base_department_income' => '基期科内有效收入，必填',
                'base_period_controllable_variable_cost' => '基期可控变动成本，必填',
                'target_patient_count' => '科室基期门急诊人次，必填',
                'base_use_bed_day' => '科室基期占床日，必填',
                'base_out_hospital' => '基期出院人数，必填',
                'base_surgery_count' => '基期手术台次，必填',
                'base_special_surgery_rate' => '基期三、四级手术率，必填',
                'base_special_surgery_count' => '基期三、四级手术台次，必填',
                'point_price' => '核心业务单价，必填',
                'cost_effective_price' => '成本效益单价，必填',
                'outpatient_price' => '门诊工作量单价，必填',
                'admission_price' => '住院工作量单价，必填',
            ];
    }
}

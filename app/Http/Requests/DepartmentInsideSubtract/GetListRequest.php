<?php

namespace App\Http\Requests\DepartmentInsideSubtract;

use App\Http\Requests\BaseRequest;

class GetListRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = true;

    public function rules() {
        return parent::rules();
    }

    public function attributes() {
        return parent::attributes();
    }
}

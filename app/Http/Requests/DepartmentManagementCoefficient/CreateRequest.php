<?php

namespace App\Http\Requests\DepartmentManagementCoefficient;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
              "date" => "required|string",
              "department_id" =>"required|integer",
              "post_coefficient" => "required|numeric",
              "employees_number" => "required|integer",
              "type_lv1" => "required|integer",
              "scale_coefficient" => "required|numeric",
              "college_level" => "string|nullable",
              "brand_coefficient" => "required|numeric",
              "efficiency_ranking" => "integer|nullable",
              "expert_number_rate" =>"required|numeric",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
              "date" => "年月日期",
              "department_id" => "一级科室id",
              "post_coefficient" => "岗位系数",
              "employees_number" => "员工数量",
              "type_lv1" => "科室类型",
              "scale_coefficient" => "规模系数",
              "college_level" => "专科级别,选填",
              "brand_coefficient" => "品牌系数",
              "efficiency_ranking" => "人均效率排名,选填",
              "expert_number_rate" => "专家号绩效占比",
            ];
    }
}

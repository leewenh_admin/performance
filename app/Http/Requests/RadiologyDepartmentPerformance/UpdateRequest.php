<?php

namespace App\Http\Requests\RadiologyDepartmentPerformance;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "date" => "required|string",
            "department_id" => "required|integer",
            "radiation_number" => "required|integer",
            "radiation_price" => "required|numeric",
            "performance" => "required|numeric",
            ];
    }

    public function attributes() {
        return parent::attributes() + [
            "date" => "年月日期",
            "department_id" => "科室",
            "radiation_number" => "放射列数",
            "radiation_price" => "单价",
            "performance" => "绩效合计",
            ];
    }
}

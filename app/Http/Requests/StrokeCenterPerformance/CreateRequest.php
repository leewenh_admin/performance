<?php

namespace App\Http\Requests\StrokeCenterPerformance;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                "date" => "required|string",
                "sub_department_id" => "required|integer",
                "employee_id" => "required|integer",
                "punish_amount" => "required|numeric",
                "punish_reason" => "string|nullable",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
                "date" => "年月日期 YYYY-mm",
                "sub_department_id" => "二级科室id",
                "employee_id" => "员工id",
                "punish_amount" => "奖罚金额，可为负数",
                "punish_reason" => "奖罚原因，选填",
            ];
    }
}

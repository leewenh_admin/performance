<?php

namespace App\Http\Requests\Employee;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "name" => "required|string",
            "sub_departments_id" => "integer",
            "code" => "required|string",
            "coefficient" => "required|numeric",
            "position" => "required|integer",
            "professional_rank_type" => "required|integer"
        ];
    }

    public function attributes()
    {
        return parent::attributes() + [
            "name" => "职员姓名",
            "sub_departments_id" => "二级科室ID",
            "code" => "职员工号",
            "coefficient" => "系数,选填, 默认1",
            "position" => "行政职务",
            "professional_rank_type" => "职称分类"
        ];
    }
}

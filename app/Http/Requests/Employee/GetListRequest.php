<?php

namespace App\Http\Requests\Employee;

use App\Http\Requests\BaseRequest;

class GetListRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = true;

    public function rules() {
        return parent::rules() + [
            "code" => "string",
            "name" => "string",
            "department_id" => "integer",
            "sub_department_id" => "integer",
            "position" => "integer",
            "professional_rank_type" => "integer"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "code" => "工号, 选填",
            "name" => "职员名称, 选填",
            "department_id" => "一级科室, 选填",
            "sub_department_id" => "二级科室, 选填",
            "position" => "行政职务, 选填",
            "professional_rank_type" => "职称分类, 选填"
        ];
    }
}

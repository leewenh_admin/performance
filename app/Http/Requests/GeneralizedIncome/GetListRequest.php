<?php

namespace App\Http\Requests\GeneralizedIncome;

use App\Http\Requests\BaseRequest;

class GetListRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = true;

    public function rules() {
        return parent::rules() + [
            "type" => "integer",
            "date" => "string",
            "sub_department_id" => "integer",
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "type" => "收入类型 1.医疗组 2.医技科室 3.护理组 默认1",
            "date" => "收入时间",
            "sub_department_id" => "二级科室id"
        ];
    }
}

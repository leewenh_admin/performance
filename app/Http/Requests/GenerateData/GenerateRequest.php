<?php

namespace App\Http\Requests\GenerateData;

use App\Http\Requests\BaseRequest;

class GenerateRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = false;

    public function rules() {
        return parent::rules() + [
            'date' => 'string',
            ];
    }

    public function attributes() {
        return parent::attributes() + [
                'date' => '年月日期, YYYY-mm 必填',
            ];
    }
}

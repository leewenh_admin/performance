<?php

namespace App\Http\Requests\EmployeePerformance;

use App\Http\Requests\BaseRequest;

class GenerateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "date" => "required|string"
            ];
    }

    public function attributes() {
        return parent::attributes() + [
                "date" => "年月日期"
            ];
    }
}

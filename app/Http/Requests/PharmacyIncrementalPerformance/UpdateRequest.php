<?php

namespace App\Http\Requests\PharmacyIncrementalPerformance;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                "date" => "required|string",
                "quota" => "required|string",
                "standard" => "required|numeric",
                "reviews" => "required|numeric",
                "total" => "required|numeric",
                "actual" => "required|numeric",
                "incremental" => "required|numeric",
            ];
    }

    public function attributes() {
        return parent::attributes() + [
                "date" => "年月日期",
                "quota" => "指标",
                "standard" => "标准值%",
                "reviews" => "点评数",
                "total" => "总数",
                "actual" => "实际值%",
                "incremental" => "绩效增量",
            ];
    }
}

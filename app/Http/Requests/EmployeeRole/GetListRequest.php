<?php

namespace App\Http\Requests\EmployeeRole;

use App\Http\Requests\BaseRequest;

class GetListRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = true;
}

<?php

namespace App\Http\Requests\EmployeeRole;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "employees_id" => "required|integer",
            "roles_id" => "required|integer",
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "employees_id" => "职员ID",
            "roles_id" => "角色ID",
        ];
    }
}

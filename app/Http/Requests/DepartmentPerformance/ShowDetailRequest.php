<?php

namespace App\Http\Requests\DepartmentPerformance;

use App\Http\Requests\BaseRequest;

class ShowDetailRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                'date' => 'required|string',
                "detail_type" => "integer",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
                'date' => '查看科室绩效日期,格式:Y-m',
                "detail_type" => "详情数据类型"
            ];
    }
}

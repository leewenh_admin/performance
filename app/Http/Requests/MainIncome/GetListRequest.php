<?php

namespace App\Http\Requests\MainIncome;

use App\Http\Requests\BaseRequest;

class GetListRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = true;

    public function rules() {
        return parent::rules() + [
                "patient" => "string",
                "prescribe_doctor" => "string",
                "execute_doctor" => "string",
                "charging_time" => "string",
            ];
    }

    public function attributes() {
        return parent::attributes() + [
                "patient" => "病人名称,可选",
                "prescribe_doctor" => "开单医生名称,可选",
                "execute_doctor" => "执行医生名称,可选",
                "charging_time" => "收费时间,可选",
            ];
    }
}

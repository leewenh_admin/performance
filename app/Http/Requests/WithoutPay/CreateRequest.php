<?php

namespace App\Http\Requests\WithoutPay;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                'date' => 'string|required',
                'code' => 'string',
                'name' => 'string',
            ];
    }

    public function attributes()
    {
        return parent::attributes()  + [
                'date' => '年月日期',
                'code' => '工号',
                'name' => '姓名',
            ];
    }
}

<?php

namespace App\Http\Requests\OtherPerformance;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                "date" => "required|string",
                "code" => "required|integer",
                "name" => "required|string",
                "sub_department_id" => "required|integer",
                "new_years_day" => "required|integer",
                "spring_festival_day" => "required|integer",
                "days" => "required|integer",
                "amount" => "required|numeric",
                "description" => "nullable|string",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
                "date" => "年月日期",
                "code" => "工号",
                "name" => "姓名",
                "sub_department_id" => "二级科室",
                "new_years_day" => "元旦天数",
                "spring_festival_day" => "春节天数",
                "days" => "天数",
                "amount" => "金额",
                "description" => "备注",
            ];
    }
}

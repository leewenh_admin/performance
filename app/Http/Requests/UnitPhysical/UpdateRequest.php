<?php

namespace App\Http\Requests\UnitPhysical;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "date" => "required|string",
            "department_id" => "required|integer",
            "sub_department_id" => "required|integer",
            "point" => "required|numeric",
            "point_price" => "required|numeric",
            "check_count" => "required|integer",
            "total_performance"=> "required|numeric",
            "check_income" => "required|numeric",
            "performance_percent" => "required|numeric",
            ];
    }

    public function attributes() {
        return parent::attributes() + [
            "date" => "年月日期",
            "department_id" => "一级科室ID",
            "sub_department_id" => "二级科室ID",
            "point" => "点值",
            "point_price" => "点数单价",
            "check_count" => "检查人次",
            "total_performance" => "绩效合计",
            "check_income" => "体检收入",
            "performance_percent" => "绩效占收入",
            ];
    }
}

<?php

namespace App\Http\Requests\SpecialSurveyPerformance;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                "date" => "required|string",
                "department_id" => "required|integer",
                "normal_number" => "required|integer",
                "normal_price" => "required|numeric",
                "changcheng_number" => "required|numeric",
                "changcheng_price" => "required|numeric",
                "performance" => "required|numeric",
            ];
    }

    public function attributes() {
        return parent::attributes() + [
                "date" => "年月日期",
                "department_id" => "科室",
                "normal_number" => "普通心电图例数",
                "normal_price" => "普通心电图单价",
                "changcheng_number" => "长程心电图列数",
                "changcheng_price" => "长程心电图单价",
                "performance" => "绩效合计",
            ];
    }
}

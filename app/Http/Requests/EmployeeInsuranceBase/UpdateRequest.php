<?php

namespace App\Http\Requests\EmployeeInsuranceBase;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "employees_id" => "required|integer",
            "name" => "required|string",
            "code" => "required|string",
            "endowment_insurance" => "required|numeric",
            "occupation_pension" => "required|numeric",
            "four_insurance" => "required|numeric",
            ];
    }

    public function attributes() {
        return parent::attributes() + [
            "employees_id" => "职员ID",
            "name" => "姓名",
            "code" => "工号",
            "endowment_insurance" => "养老保险基数",
            "occupation_pension" => "职业年金基数",
            "four_insurance" => "四险基数",
            ];
    }
}

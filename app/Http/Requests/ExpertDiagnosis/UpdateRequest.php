<?php

namespace App\Http\Requests\ExpertDiagnosis;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules();
    }

    public function attributes() {
        return parent::attributes();
    }
}

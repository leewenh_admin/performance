<?php

namespace App\Http\Requests\DisinfectCost;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "date" => "required|string",
            "sub_departments_id" => "required|integer",
            "count" => "integer",
            "amount" => "numeric",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
            "date" => "年月日期,必填",
            "sub_departments_id" => "二级科室id,必填",
            "count" => "数量",
            "amount" => "金额",
            ];
    }
}

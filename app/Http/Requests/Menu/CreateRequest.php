<?php

namespace App\Http\Requests\Menu;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "title" => "required|string",
            "parent_id" => "integer",
            "route" => "required|string",
            "permissions_id" => "required|integer",
            "icon" => "required|string",
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "title" => "标题",
            "parent_id" => "父目录ID, 选填, 一级菜单=null",
            "route" => "前端路由",
            "permissions_id" => "权限ID",
            "icon" => "图标",
        ];
    }
}

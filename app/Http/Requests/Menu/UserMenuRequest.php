<?php

namespace App\Http\Requests\Menu;

use App\Http\Requests\BaseRequest;

class UserMenuRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = true;

}
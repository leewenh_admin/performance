<?php

namespace App\Http\Requests\Menu;

use App\Http\Requests\BaseRequest;

class GetItemRequest extends BaseRequest {
    protected $is_need_auth = true;
}

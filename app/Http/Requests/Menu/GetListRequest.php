<?php

namespace App\Http\Requests\Menu;

use App\Http\Requests\BaseRequest;

class GetListRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = true;

    public function rules() {
        return parent::rules() + [
            "parent_id" => "integer"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "parent_id" => "父目录ID"
        ];
    }
}

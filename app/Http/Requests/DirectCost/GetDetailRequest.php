<?php

namespace App\Http\Requests\DirectCost;

use App\Http\Requests\BaseRequest;

class GetDetailRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = false;

    public function rules() {
        return parent::rules() + [
            'department_id' => 'integer',
            'date' => 'string',
            ];
    }

    public function attributes() {
        return parent::attributes() + [
                'department_id' => '一级部门id,可选',
                'date' => '年月日期, YYYY-mm',
            ];
    }
}

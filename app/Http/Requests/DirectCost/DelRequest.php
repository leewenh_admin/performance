<?php

namespace App\Http\Requests\DirectCost;

use App\Http\Requests\BaseRequest;

class DelRequest extends BaseRequest {
    protected $is_need_auth = true;
    protected $is_list_request = true;

    public function rules() {
        return parent::rules() + [
            'date' => 'string',
            ];
    }

    public function attributes() {
        return parent::attributes() + [
                'date' => '年月日期, YYYY-mm',
            ];
    }
}

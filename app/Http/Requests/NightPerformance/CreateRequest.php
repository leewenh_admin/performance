<?php

namespace App\Http\Requests\NightPerformance;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
              "date" => "required|string",
              "department_id" =>"required|integer",
              "sub_department_name" =>"string",
              "department_type" => "required|string",
              "daily_fixed_number" => "required|numeric",
              "before_night_number" => "required|numeric",
              "before_night_price" => "required|numeric",
              "after_night_number" => "required|numeric",
              "after_night_price" => "required|numeric",
              "whole_night_number" => "required|numeric",
              "whole_night_price" => "required|numeric",
              "month_day" =>"required|integer",
              "dispatch_amount" => "required|numeric",
              "total_night_performance" => "required|numeric",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
              "date" => "年月日期",
              "department_id" => "归属科室",
              "sub_department_name" => "科室,选填",
              "department_type" => "科室类别",
              "daily_fixed_number" => "每日固定人数",
              "before_night_number" => "上夜班人数",
              "before_night_price" => "上夜班标准",
              "after_night_number" => "下夜班人数",
              "after_night_price" => "下夜班标准",
              "whole_night_number" => "整夜班人数",
              "whole_night_price" => "整夜班标准",
              "month_day" => "月天数",
              "dispatch_amount" => "劳务派遣人员夜班",
              "total_night_performance" => "实发夜班绩效合计",
            ];
    }
}

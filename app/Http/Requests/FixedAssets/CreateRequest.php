<?php

namespace App\Http\Requests\FixedAssets;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                'code' => 'required|string',
                'card' => 'required|string',
                'name' => 'required|string',
                'specification' => 'required|string',
                'unit' => 'required|string',
                'price' => 'required|numeric',
                'count' => 'required|integer',
                'amount' => 'required|numeric',
                'sub_departments_id' => 'required|integer',
                'store_date' => 'required|date',
                'device_type' => 'required|integer',
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
                'code' => '编码',
                'card' => '卡号',
                'name' => '名称',
                'specification' => '规格',
                'unit' => '单位',
                'price' => '单价',
                'count' => '数量',
                'amount' => '金额',
                'sub_departments_id' => '二级科室ID',
                'store_date' => '入库日期 YYYY-mm-dd',
                'device_type' => '设备类型 1.一般设备 2.专业设备 3.计算机设备 4.家俱设备 5.制冷设备 6.交通设备 7.电器设备 8.其他设备',
            ];
    }
}

<?php

namespace App\Http\Requests\Permission;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
            "name" => "required|string",
            "display_name" => "required|string",
            "description" => "required|string"
        ];
    }

    public function attributes() {
        return parent::attributes() + [
            "name" => "权限名称",
            "display_name" => "权限显示名称",
            "description" => "权限描述"
        ];
    }
}

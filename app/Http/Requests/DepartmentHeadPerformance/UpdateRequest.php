<?php

namespace App\Http\Requests\DepartmentHeadPerformance;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                "date" => "required|string",
                "department_id" => "required|integer",
                "code" => "required|integer",
                "employee_id" => "required|integer",
                "administrative_duties" => "nullable|string",
                "performance_standard" => "required|numeric",
            ];
    }

    public function attributes() {
        return parent::attributes() + [
                "date" => "年月日期",
                "department_id" => "一级科室id",
                "code" => "工号",
                "employee_id" => "人员id",
                "administrative_duties" => "行政职务,选填",
                "performance_standard" => "绩效标准",
            ];
    }
}

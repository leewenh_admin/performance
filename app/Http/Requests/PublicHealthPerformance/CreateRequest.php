<?php

namespace App\Http\Requests\PublicHealthPerformance;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest {
    protected $is_need_auth = true;

    public function rules() {
        return parent::rules() + [
                "date" => "required|string",
                "code" => "required|integer",
                "name" => "required|string",
                "sub_department_id" => "required|integer",
                "amount" => "required|numeric",
                "description" => "nullable|string",
            ];
    }

    public function attributes()
    {
        return parent::attributes() + [
                "date" => "年月日期",
                "code" => "工号",
                "name" => "姓名",
                "sub_department_id" => "二级科室",
                "amount" => "金额",
                "description" => "备注，选填",
            ];
    }
}

<?php

namespace App\Events;

use App\Repositories\ExportDetail\BaseExportDetail;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ExportPerformanceEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $repository = null;
    public $date = null;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(BaseExportDetail $baseExportDetail, $date)
    {
        $this->repository = $baseExportDetail;
        $this->date = $date;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}

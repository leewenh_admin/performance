<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

if (!function_exists("make_semiangle")) {
    /**
     *  将一个字串中含有全角的数字字符、字母、空格或'%+-()'字符转换为相应半角字符
     *
     * @access  public
     * @param   string　$str　待转换字串
     * @return  string
     */
    function make_semiangle($str)
    {
        $arr = array('０' => '0', '１' => '1', '２' => '2', '３' => '3', '４' => '4',
                    '５' => '5', '６' => '6', '７' => '7', '８' => '8', '９' => '9',
                    'Ａ' => 'A', 'Ｂ' => 'B', 'Ｃ' => 'C', 'Ｄ' => 'D', 'Ｅ' => 'E',
                    'Ｆ' => 'F', 'Ｇ' => 'G', 'Ｈ' => 'H', 'Ｉ' => 'I', 'Ｊ' => 'J',
                    'Ｋ' => 'K', 'Ｌ' => 'L', 'Ｍ' => 'M', 'Ｎ' => 'N', 'Ｏ' => 'O',
                    'Ｐ' => 'P', 'Ｑ' => 'Q', 'Ｒ' => 'R', 'Ｓ' => 'S', 'Ｔ' => 'T',
                    'Ｕ' => 'U', 'Ｖ' => 'V', 'Ｗ' => 'W', 'Ｘ' => 'X', 'Ｙ' => 'Y',
                    'Ｚ' => 'Z', 'ａ' => 'a', 'ｂ' => 'b', 'ｃ' => 'c', 'ｄ' => 'd',
                    'ｅ' => 'e', 'ｆ' => 'f', 'ｇ' => 'g', 'ｈ' => 'h', 'ｉ' => 'i',
                    'ｊ' => 'j', 'ｋ' => 'k', 'ｌ' => 'l', 'ｍ' => 'm', 'ｎ' => 'n',
                    'ｏ' => 'o', 'ｐ' => 'p', 'ｑ' => 'q', 'ｒ' => 'r', 'ｓ' => 's',
                    'ｔ' => 't', 'ｕ' => 'u', 'ｖ' => 'v', 'ｗ' => 'w', 'ｘ' => 'x',
                    'ｙ' => 'y', 'ｚ' => 'z',
                    '（' => '(', '）' => ')', '〔' => '[', '〕' => ']', '【' => '[',
                    '】' => ']', '〖' => '[', '〗' => ']', '“' => '[', '”' => ']',
                    '‘' => '[', '’' => ']', '｛' => '{', '｝' => '}', '《' => '<',
                    '》' => '>',
                    '％' => '%', '＋' => '+', '—' => '-', '－' => '-', '～' => '-',
                    '：' => ':', '。' => '.', '、' => ',', '，' => '.', '、' => '.',
                    '；' => ',', '？' => '?', '！' => '!', '…' => '-', '‖' => '|',
                    '”' => '"', '’' => '`', '‘' => '`', '｜' => '|', '〃' => '"',
                    '　' => ' ', '•' => '·');

        return strtr($str, $arr);
    }
}

if (!function_exists("splited_table_name")) {
    function splited_table_name($origin, $date) {
        $arrDate = explode('-', $date);
        $new_table = $origin . $arrDate[0] . $arrDate[1];

        $is_table_exists = DB::getSchemaBuilder()->hasTable($new_table);
        if ($is_table_exists) {
            return $new_table;
        } else {
            return $origin;
        }
    }
}

if (!function_exists('diff_in_months')) {
    function diff_in_months($start_date, $end_date) {

        $ts1 = strtotime($start_date);
        $ts2 = strtotime($end_date);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
        return $diff;
    }
}

/**
 * 输出sql语句
 */
if (!function_exists('getSql')) {
    function getSql($builder) {

        $bindings = $builder->getBindings();
        $sql = str_replace('?', '%s', $builder->toSql());
        $sql = sprintf($sql, ...$bindings);

        return $sql;
    }
}

if (!function_exists('delDirAndFile')) {
     function delDirAndFile( $dirName='' ) {
        // opendir将目录打开，成功则返回句柄资源
        if ( $handle = opendir( "$dirName" ) ) {
            // while循环取出资源中的文件夹名称及文件名
            while ( false !== ( $item = readdir( $handle ) ) ) {
                // 过滤掉"."，".."
                if ( $item != "." && $item != ".." ) {
                    // 检查当次循环是否为文件夹
                    if ( is_dir( "$dirName/$item" ) ) {
                        // 递归继续打开下层目录直至文件
                        delDirAndFile( "$dirName/$item" );
                    } else {
                        // 删除文件
                        if( unlink( "$dirName/$item" ) );
                    }
                }
            }
            closedir( $handle ); // 释放句柄资源
            if(rmdir( $dirName )); // 删除空目录
        }
    }
}

//获取两个字母之前的所有字母
if (!function_exists('setSheetWidth')) {
    function setSheetWidth($start='A', $end='Z', $width='20') {
        if (ord($start) < 65 || ord($start) > 90){
            throw new Exception('只允许大写字母');
        }
        $data = [];
        for ($i = ord($start); $i <= ord($end); $i++){
            $data[chr($i)] = $width;
        }

        return $data;
    }
}
<?php

use App\Enums\EmployeeProfessionalRankLevel;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Enums\EmployeeSex;

class EmployeeAddExtraColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->tinyInteger("sex")->default(EmployeeSex::MALE)->comment("性别");
            $table->string("birth_date", 20)->nullable()->comment("生日");
            $table->string("join_date", 20)->nullable()->comment("入院时间");
            $table->string("first_education", 5)->nullable()->comment('第一学历');
            $table->string("working_education", 5)->nullable()->comment("在职学历");
            $table->string("position_detail", 20)->nullable()->comment("行政职务明细");
            $table->string("position_date", 20)->nullable()->comment("行政职务任职时间");
            $table->string("position_code", 50)->nullable()->comment("行政职务任职文号");
            $table->tinyInteger("professional_rank_level")
                ->nullable()
                ->default(EmployeeProfessionalRankLevel::JUNIOR)
                ->comment("职称级别");
            $table->string("professional_rank_title")
                ->nullable()
                ->comment("技术职称");
            $table->string("professional_rank_date", 20)->nullable()->comment("技术职称任职时间");
            $table->string("professional_rank_code", 50)->nullable()->comment("技术职称任职文号");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn("sex");
            $table->dropColumn("birth_date");
            $table->dropColumn("join_date");
            $table->dropColumn("first_education");
            $table->dropColumn("working_education");
            $table->dropColumn("position_detail");
            $table->dropColumn("position_date");
            $table->dropColumn("position_code");
            $table->dropColumn("professional_rank_level");
            $table->dropColumn("professional_rank_title");
            $table->dropColumn("professional_rank_date");
            $table->dropColumn("professional_rank_code");
        });
    }
}

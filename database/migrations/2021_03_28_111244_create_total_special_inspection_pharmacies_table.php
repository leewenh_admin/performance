<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTotalSpecialInspectionPharmaciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_special_inspection_pharmacies', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->integer("ecg")->default(0)->comment("心电图");
            $table->integer("eeg")->default(0)->comment("脑电图");
            $table->integer("long_range_ecg")->default(0)->comment("长程心电图");
            $table->integer("pulmonary_function")->default(0)->comment("肺功能");
            $table->integer("moveable_plate")->default(0)->comment("活动平板");
            $table->integer("colonoscope")->default(0)->comment("结肠镜");
            $table->integer("gastroscope")->default(0)->comment("胃镜");
            $table->integer("mammogram")->default(0)->comment("乳透");
            $table->integer("carbon_breath_test")->default(0)->comment("碳呼吸实验");
            $table->integer("gastroscope_other")->default(0)->comment("胃镜其他");
            $table->integer("bronchoscopy")->default(0)->comment("纤支镜检查");
            $table->integer("bronchoscopy_other")->default(0)->comment("纤支镜其他");
            $table->integer("total_prescription")->default(0)->comment("处方合计");
            $table->integer("outpatient_western_medicine_prescription")->default(0)->comment("门诊西药处方");
            $table->integer("hospitalization_western_medicine_prescription")->default(0)->comment("住院西药处方");
            $table->integer("outpatient_chinese_medicine_prescription")->default(0)->comment("门诊中药处方");
            $table->timestamps();

            $table->index(["date", "sub_departments_id"], "date_sub_department_id_index");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_special_inspection_pharmacies');
    }
}

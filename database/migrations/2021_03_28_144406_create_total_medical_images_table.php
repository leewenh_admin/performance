<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTotalMedicalImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_medical_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->integer("radiate_person")->default(0)->comment("放射人次");
            $table->integer("radiate_count")->default(0)->comment("拍片");
            $table->integer("radiate_side_bed")->default(0)->comment("床边拍片");
            $table->integer("barium_meal")->default(0)->comment("钡餐");
            $table->integer("molybdenum_palladium")->default(0)->comment("钼钯");
            $table->integer("ct")->default(0)->comment("CT");
            $table->integer("mri")->default(0)->comment("MRI");
            $table->integer("color_ultrasound_person")->default(0)->comment("彩超人次");
            $table->integer("tcd")->default(0)->comment("TCD");
            $table->integer("pathological_examination")->default(0)->comment("病理检查");
            $table->integer("tct")->default(0)->comment("TCT");
            $table->integer("frozen_section")->default(0)->comment("冰冻切片");
            $table->integer("specific_stain")->default(0)->comment("特异染色");
            $table->integer("pathological_hpv")->default(0)->comment("病理hpv");
            $table->integer("radiotherapy_person")->default(0)->comment("放疗人次");
            $table->integer("interventional_angiography")->default(0)->comment("介入造影");
            $table->integer("interventional_surgery")->default(0)->comment("介入手术");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_medical_images');
    }
}

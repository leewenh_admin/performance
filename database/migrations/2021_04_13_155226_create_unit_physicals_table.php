<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitPhysicalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_physicals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 7)->comment("年月日期");
            $table->unsignedInteger("department_id")->comment("一级科室ID");
            $table->unsignedInteger("sub_department_id")->comment("二级科室ID");
            $table->decimal("point", 10, 2)->default(0)->comment("点值");
            $table->decimal("point_price", 10, 2)->default(0)->comment("点数单价");
            $table->integer("check_count")->default(0)->comment("检查人次");
            $table->decimal("total_performance", 10, 2)->default(0)->comment("绩效合计");
            $table->decimal("check_income", 10, 2)->default(0)->comment("体检收入");
            $table->decimal("performance_percent", 10, 2)->default(0)->comment("绩效占收入%");


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_physicals');
    }
}

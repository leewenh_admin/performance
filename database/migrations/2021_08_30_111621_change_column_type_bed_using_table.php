<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnTypeBedUsingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bed_usings', function (Blueprint $table) {
            $table->decimal('avg_open', 10, 2)->change();
            $table->decimal('avg_bed_work', 10, 2)->change();
            $table->decimal('leaver_avg_day', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bed_usings', function (Blueprint $table) {
            //
        });
    }
}

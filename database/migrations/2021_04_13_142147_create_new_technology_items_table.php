<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTechnologyItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_technology_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string("apply_year", 4)->comment("申请时间");
            $table->unsignedInteger("department_id")->comment("科室id");
            $table->string("name", 100)->nullable()->comment("新技术名称");
            $table->string("code", 20)->comment("医院编码");
            $table->string("item_code", 20)->comment("项目编码");
            $table->string("item_name", 100)->comment("项目名称");
            $table->integer("count")->default(0)->comment("数量");
            $table->decimal("price", 5, 2)->comment("单价");
            $table->integer("increase_count")->default(0)->comment("新业务增量");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_technology_items');
    }
}

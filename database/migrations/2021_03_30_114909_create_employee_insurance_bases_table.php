<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeInsuranceBasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_insurance_bases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 7)->comment("年月日期");
            $table->unsignedInteger("employees_id")->comment("职员ID");
            $table->string("name", 10)->comment("姓名");
            $table->string("code", 10)->comment("工号");
            $table->decimal("endowment_insurance", 10, 2)->default(0)->comment("养老保险基数");
            $table->decimal("occupation_pension", 10, 2)->default(0)->comment("职业年金基数");
            $table->decimal("four_insurance", 10, 2)->default(0)->comment("四险基数");
            $table->timestamps();

            $table->index("employees_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_insurance_bases');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixedAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string("code", 10)->comment("编码");
            $table->string("card", 10)->comment("卡号");
            $table->string("name", 200)->comment("名称");
            $table->string("specification", 50)->nullable()->comment("规格");
            $table->string("unit", 10)->comment("单位");
            $table->decimal("price", 10, 2)->comment("单价");
            $table->integer("count")->comment("数量");
            $table->decimal("amount", 10, 2)->comment("金额");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->date("store_date")->comment("入库日期");
            $table->tinyInteger("device_type")->comment("设备类型");

            $table->timestamps();

            $table->index(["store_date", "sub_departments_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_assets');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSurgeryNewTimeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surgerys', function (Blueprint $table) {
            $table->string("schedule_time", 30)->nullable()->comment("预计时间");
            $table->string("request_time", 30)->nullable()->comment("申请时间");
            $table->string("surgery_time", 30)->nullable()->comment("手术日期");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surgerys', function (Blueprint $table) {
            $table->dropColumn("schedule_time");
            $table->dropColumn("request_time");
            $table->dropColumn("surgery_time");
        });
    }
}

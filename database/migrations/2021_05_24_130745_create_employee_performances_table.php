<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->string('department', 20)->nullable()->comment('科室');
            $table->unsignedInteger("employees_id")->comment("职员ID");
            $table->string('code', 20)->comment('工号');
            $table->string('name', 10)->comment('姓名');
            $table->string('work_title')->nullable()->comment('职务');
            $table->string('work_type')->nullable()->comment('类别');
            $table->string('bank_code')->nullable()->comment('银行帐号');
            $table->decimal('workload_performance', 8, 2)->default(0)->comment('工作量绩效');
            $table->decimal('director_performance', 8, 2)->default(0)->comment('科主任管理绩效');
            $table->decimal('night_performance', 8, 2)->default(0)->comment('夜班绩效');
            $table->decimal('consultation_performance', 8, 2)->default(0)->comment('会诊绩效');
            $table->decimal('expert_performance', 8, 2)->default(0)->comment('专家号绩效');
            $table->decimal('community_performance', 8, 2)->default(0)->comment('医共体绩效');
            $table->decimal('other_performance', 8, 2)->default(0)->comment('其他单项绩效');
            $table->decimal('parking_subsidy', 8, 2)->default(0)->comment('院外停车补助');
            $table->decimal('talent_reserves_performance', 8, 2)->default(0)->comment('人才储备绩效');
            $table->decimal('business_recovery_performance', 8, 2)->default(0)->comment('业务恢复绩效');
            $table->decimal('public_health_performance', 8, 2)->default(0)->comment('公卫绩效');
            $table->decimal('deduct', 8, 2)->default(0)->comment('扣款');
            $table->decimal('should_sent', 8, 2)->default(0)->comment('应发绩效合计');
            $table->decimal('tax', 8, 2)->default(0)->comment('个人所得税');
            $table->decimal('actual_sent', 8, 2)->default(0)->comment('实发绩效');
            $table->string('deduct_remark')->nullable()->comment('扣款备注');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_performances');
    }
}

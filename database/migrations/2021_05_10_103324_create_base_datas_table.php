<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("department_id")->comment("一级科室ID");
            $table->string("date", 7)->comment("年月日期");
            $table->decimal("base_department_income", 10, 2)->default(0)->comment("基期科内有效收入");
            $table->decimal("base_period_controllable_variable_cost", 10, 2)->default(0)->comment("基期可控变动成本");
            $table->decimal("target_patient_count", 10, 2)->default(0)->comment("科室基期门急诊人次");
            $table->decimal("base_use_bed_day", 10, 2)->default(0)->comment("科室基期占床日");
            $table->decimal("base_out_hospital", 10, 2)->default(0)->comment("基期出院人数");
            $table->decimal("base_surgery_count", 10, 2)->default(0)->comment("基期手术台次");
            $table->decimal("base_special_surgery_rate", 10, 2)->default(0)->comment("基期三、四级手术率");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_datas');
    }
}

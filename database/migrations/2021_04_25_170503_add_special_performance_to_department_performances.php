<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpecialPerformanceToDepartmentPerformances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('department_performances', function (Blueprint $table) {
            $table->decimal("special_performance", 10, 2)->default(0)->comment("特殊科室绩效");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('department_performances', function (Blueprint $table) {
            $table->dropColumn("special_performance");
        });
    }
}

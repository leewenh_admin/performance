<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToMianIncomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('main_incomes', function (Blueprint $table) {
            $table->index('prescribe_doctor_id');
            $table->index('item_cate_code');
            $table->index('item_code');
            $table->index('charging_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->dropIndex('main_incomes_prescribe_doctor_id_index');
            $table->dropIndex('main_incomes_item_cate_code_index');
            $table->dropIndex('main_incomes_item_code_index');
            $table->dropIndex('main_incomes_charging_time_index');
        });
    }
}

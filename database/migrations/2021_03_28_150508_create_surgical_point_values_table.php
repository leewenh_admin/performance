<?php

use Egulias\EmailValidator\Warning\Comment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurgicalPointValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surgical_point_values', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("belong_department_id")->comment("所在二级科室ID");
            $table->unsignedInteger("execute_department_id")->comment("执行二级科室ID");
            $table->string("project", 50)->comment("项目");
            $table->string("code", 10)->comment("编码");
            $table->string("gb_code", 20)->comment("国标码");
            $table->decimal("price", 10, 2)->default(0)->comment("价格");
            $table->string("price_type", 20)->comment("价格类别");
            $table->string("project_type", 20)->comment("项目分类");
            $table->string("project_code", 20)->comment("项目编码");
            $table->string("name", 100)->comment("项目名称");
            $table->text("project_include")->nullable()->comment("项目内涵");
            $table->string("include_consumable", 200)->nullable()->comment("内涵一次性耗材");
            $table->string("exclude_consumable", 200)->nullable()->comment("除外耗材");
            $table->integer("lowvalue_consumable")->default(0)->comment("低值耗材");
            $table->string("consume_explain", 200)->nullable()->comment("基本人力消耗及耗时");
            $table->string("technical_difficulty1", 10)->comment("技术难度1");
            $table->string("risk_degree1", 10)->comment("风险程度1");
            $table->string("charge_unit", 10)->comment("计价单位");
            $table->string("charge_explain", 100)->nullable()->comment("计价说明");
            $table->integer("duration")->default(0)->comment("时间(分钟)");
            $table->decimal("technical_difficulty2", 5, 2)->comment("技术难度2");
            $table->decimal("risk_degree2", 5, 2)->comment("风险程度2");
            $table->decimal("difficulty_risk_weights", 5, 2)->comment("难度风险权重");
            $table->decimal("duration_weights", 5, 2)->comment("时间权重");
            $table->decimal("performance_weights", 5, 2)->comment("绩效权重");

            $table->timestamps();

            $table->index("code");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgical_point_values');
    }
}

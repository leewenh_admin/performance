<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavourCollectInpatientFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favour_collect_inpatient_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月日期");
            $table->integer("amount")->default(0)->comment("代收住院病人人数");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favour_collect_inpatient_fees');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStayColumnToDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->decimal("target_stay_bed_day", 10, 2)->after('base_use_bed_day')->default(0)->comment("目标术前待床日");
            $table->decimal("punish_amount", 10, 2)->after('target_stay_bed_day')->default(0)->comment("扣罚金额");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->dropColumn('target_stay_bed_day');
            $table->dropColumn('punish_amount');
        });
    }
}

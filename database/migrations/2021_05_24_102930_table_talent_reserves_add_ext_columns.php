<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTalentReservesAddExtColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('talent_reserves', function(Blueprint $table){
            $table->string('professional_rank_level', 20)->nullable()->comment('职称级别');
            $table->string('professional_rank_title', 20)->nullable()->comment('技术职称');
            $table->decimal('standard', 8, 2)->default(0)->comment('发放标准');
            $table->decimal('actual_sent')->default(0)->comment('本月应发');
            $table->integer('leave_day')->default(0)->comment('请假天数');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('talent_reserves', function(Blueprint $table){
            $table->dropColumn('professional_rank_level');
            $table->dropColumn('professional_rank_title');
            $table->dropColumn('standard');
            $table->dropColumn('actual_sent');
            $table->dropColumn('leave_day');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDepartmentPerformanceAddPerformance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('department_performances', function (Blueprint $table) {
            $table->decimal('department_performance', 10 ,2)->default(0)->comment('科内绩效');
            $table->decimal('distribution_performance', 10 ,2)->default(0)->comment('科室分配绩效');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('department_performances', function (Blueprint $table) {
            $table->dropColumn('department_performance');
            $table->dropColumn('distribution_performance');
        });
    }
}

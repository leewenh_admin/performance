<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePharmacyIncrementalPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacy_incremental_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月日期");
            $table->string("quota", 50)->comment("指标");
            $table->decimal("standard", 5, 2)->default(0)->comment("标准值%");
            $table->decimal("reviews", 10, 2)->default(0)->comment("点评数");
            $table->decimal("total", 10, 2)->default(0)->comment("总数");
            $table->decimal("actual", 5, 2)->default(0)->comment("实际值%");
            $table->decimal("incremental", 10, 2)->default(0)->comment("绩效增量");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacy_incremental_performances');
    }
}

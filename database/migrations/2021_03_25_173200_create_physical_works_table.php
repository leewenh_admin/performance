<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_works', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->string("person_name", 0)->comment("体检人姓名");
            $table->string("item_group", 0)->comment("项目类别");
            $table->string("item_name", 0)->comment("项目名");
            $table->datetime("exam_time")->comment("检查日期");
            $table->decimal("amount", 10, 2)->default(0)->comment("金额");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_works');
    }
}

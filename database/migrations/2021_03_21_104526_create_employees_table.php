<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name", 30)->comment("职员姓名");
            // $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->string("code", 20)->comment("工号");
            // $table->tinyInteger("type")->comment("人员类别");
            $table->decimal("coefficient", 3, 2)->default(0)->comment("系数");
            $table->tinyInteger("position")->comment("职员岗位");
            $table->timestamps();

            $table->index("name");
            $table->index("code");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurgeryNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surgery_news', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->string("surgery_time", 8)->nullable()->comment("手术日期");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->string("inpatient_code", 20)->comment("住院号");
            $table->string("record_code", 20)->comment("档案号");
            $table->string("bed_code", 20)->nullable()->comment("床号");
            $table->string("inpatient_name", 10)->comment("姓名");
            $table->string("age", 10)->comment("年龄");
            $table->tinyInteger("surgery_type")->comment("类别");
            $table->string("code", 100)->comment("编号");
            $table->string("title", 100)->comment("手术名称");
            $table->string("level", 20)->comment("手术级别");
            $table->string("incision", 10)->nullable()->comment("切口");
            $table->string("execute_doctor", 20)->comment("手术医师");
            $table->string("surgery_assistant1", 20)->nullable()->comment("手术一助");
            $table->string("surgery_assistant2", 20)->nullable()->comment("手术二助");
            $table->string("anesthesia_doctor", 20)->nullable()->comment("麻醉医师");
            $table->string("anesthesia_type", 50)->nullable()->comment("麻醉方式");
            $table->string("surgery_nurse", 20)->nullable()->comment("手术护士");
            $table->string("clinical_diagnose", 100)->nullable()->comment("临床诊断");
            $table->string("HIV", 10)->comment("HIV");
            $table->string("HCV", 10)->comment("HCV");
            $table->string("HBSAG", 10)->comment("HBSAG");
            $table->string("syphilis", 10)->comment("梅毒");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgery_news');
    }
}

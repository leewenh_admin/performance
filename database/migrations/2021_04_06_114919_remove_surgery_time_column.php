<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSurgeryTimeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surgerys', function (Blueprint $table) {
            $table->dropColumn("schedule_time");
            $table->dropColumn("request_time");
            $table->dropColumn("surgery_time");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surgerys', function (Blueprint $table) {
            $table->dateTime("schedule_time")->nullable()->comment("预计时间");
            $table->dateTime("request_time")->nullable()->comment("申请时间");
            $table->dateTime("surgery_time")->nullable()->comment("手术日期");
        });
    }
}

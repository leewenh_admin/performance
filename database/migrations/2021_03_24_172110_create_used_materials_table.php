<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsedMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('used_materials', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->decimal("total_amount", 10, 2)->default(0)->comment("汇总金额");
            $table->tinyInteger("material_type")->comment("材料类型");
            $table->timestamps();

            $table->index(["date", "sub_departments_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('used_materials');
    }
}

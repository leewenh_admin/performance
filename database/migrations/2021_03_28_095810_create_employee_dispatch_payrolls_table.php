<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeDispatchPayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_dispatch_payrolls', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->string("name", 10)->comment("姓名");
            $table->decimal("base_salary", 10, 2)->default(0)->comment("工资合计");
            $table->decimal("standard_performance", 10, 2)->default(0)->comment("标准绩效");
            $table->decimal("night_salary", 10, 2)->default(0)->comment("夜班费");
            $table->decimal("total_payable", 10, 2)->default(0)->comment("应发合计");
            $table->decimal("management_cost", 10, 2)->default(0)->comment("管理费");

            $table->timestamps();

            $table->index("date");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_dispatch_payrolls');
    }
}

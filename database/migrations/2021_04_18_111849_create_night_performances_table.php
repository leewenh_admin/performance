<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNightPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('night_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("department_id")->comment("归属科室");
            $table->string("sub_department_name")->nullable()->comment("科室");
            $table->string("department_type", 10)->comment("科室类别");
            $table->decimal("daily_fixed_number", 10, 2)->default(0)->comment("每日固定人数");
            $table->decimal("before_night_number", 10, 2)->default(0)->comment("上夜班人数");
            $table->decimal("before_night_price", 10, 2)->default(0)->comment("上夜班标准");
            $table->decimal("after_night_number", 10, 2)->default(0)->comment("下夜班人数");
            $table->decimal("after_night_price", 10, 2)->default(0)->comment("下夜班标准");
            $table->decimal("whole_night_number", 10, 2)->default(0)->comment("整夜班人数");
            $table->decimal("whole_night_price", 10, 2)->default(0)->comment("整夜班标准");
            $table->tinyInteger("month_day")->default(0)->comment("月天数");
            $table->decimal("dispatch_amount", 10, 2)->default(0)->comment("劳务派遣人员夜班");
            $table->decimal("total_night_performance", 10, 2)->default(0)->comment("实发夜班绩效合计");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('night_performances');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFourItemPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('four_item_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('department_id')->comment('科室id');
            $table->string('date', 7)->comment('年月');
            $table->decimal('physical_performance', 10, 2)->default(0)->comment('单位体检绩效');
            $table->decimal('stroke_performance', 10, 2)->default(0)->comment('卒中绩效');
            $table->decimal('control_performance', 10, 2)->default(0)->comment('疫情防控');
            $table->decimal('running_performance', 10, 2)->default(0)->comment('科室跑账');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('four_item_performances');
    }
}

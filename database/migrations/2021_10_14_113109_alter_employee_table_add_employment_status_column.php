<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmployeeTableAddEmploymentStatusColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table){
            $table->string("employ_status", 50)->nullable()->comment("雇佣状态");
            $table->string("employ_type", 50)->nullable()->comment("人员类别");
            $table->string("card_no", 50)->nullable()->comment("证件号码");
            $table->date("fist_work_time")->nullable()->comment("参加工作时间");
            $table->date("this_work_time")->nullable()->comment("进入本行业时间");
            $table->date("positive_time")->nullable()->comment("转正时间");
            $table->string("mobile", 20)->nullable()->comment("手机");
            $table->date("leave_time")->nullable()->comment("离职日期");
            $table->string("coefficient_desc", 200)->nullable()->comment("系数说明");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('talent_reserves', function (Blueprint $table) {
            $table->dropColumn("employ_status");
            $table->dropColumn("employ_type");
            $table->dropColumn("card_no");
            $table->dropColumn("fist_work_time");
            $table->dropColumn("this_work_time");
            $table->dropColumn("positive_time");
            $table->dropColumn("mobile");
            $table->dropColumn("leave_time");
            $table->dropColumn("coefficient_desc");
        });
    }
}

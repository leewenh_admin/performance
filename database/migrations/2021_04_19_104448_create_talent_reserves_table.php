<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTalentReservesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talent_reserves', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("employees_id")->comment("职员ID");
            $table->string("code", 10)->comment("工号");
            $table->string("name", 10)->comment("姓名");
            $table->date("start_date")->nullable()->comment("开始发放时间");
            $table->date("end_date")->nullable()->comment("终止时间");
            $table->string("remark")->nullable()->comment("备注");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('talent_reserves');
    }
}

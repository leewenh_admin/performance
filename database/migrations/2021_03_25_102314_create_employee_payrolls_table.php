<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_payrolls', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("employees_id")->comment("职员ID");
            $table->string("bank_code", 100)->nullable()->comment("银行卡号");
            $table->decimal("post_salary", 10, 2)->default(0)->comment("岗位工资");
            $table->decimal("level_salary", 10, 2)->default(0)->comment("薪级工资");
            $table->decimal("standard_performance", 10, 2)->default(0)->comment("标准绩效");
            $table->decimal("nurse_subsidy", 10, 2)->default(0)->comment("护士补贴");
            $table->decimal("car_subsidy", 10, 2)->default(0)->comment("车贴");
            $table->decimal("health_subsidy", 10, 2)->default(0)->comment("卫生津贴");
            $table->decimal("labour_safety_subsidy", 10, 2)->default(0)->comment("劳保津贴");
            $table->decimal("total_payable", 10, 2)->default(0)->comment("应发合计");
            $table->decimal("endowment_insurance", 10, 2)->default(0)->comment("养老保险");
            $table->decimal("occupation_pension", 10, 2)->default(0)->comment("职业年金");
            $table->decimal("medical_insurance", 10, 2)->default(0)->comment("医疗保险");
            $table->decimal("unemployment_insurance", 10, 2)->default(0)->comment("失业保险");
            $table->decimal("house_fund", 10, 2)->default(0)->comment("住房公积金");
            $table->decimal("insurance_patch", 10, 2)->default(0)->comment("补扣社保");
            $table->decimal("deduct", 10, 2)->default(0)->comment("扣款");
            $table->decimal("utilities", 10, 2)->default(0)->comment("水电费");
            $table->decimal("other", 10, 2)->default(0)->comment("其他");
            $table->decimal("union_fee", 10, 2)->default(0)->comment("会费");
            $table->decimal("total_deduct", 10, 2)->default(0)->comment("扣款合计");
            $table->decimal("paid_salary", 10, 2)->default(0)->comment("工资实发");
            $table->timestamps();

            $table->index(["date", "employees_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_payrolls');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseEmployeeAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_attendances_2019', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 7)->comment("年月日期");
            $table->unsignedInteger("departments_id")->comment("一级科室ID");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->unsignedInteger("employees_id")->comment("职员ID");
            $table->string("code", 20)->comment("职员工号");
            $table->tinyInteger("workday")->default(0)->comment("上班");
            $table->tinyInteger("study")->default(0)->comment("学习");
            $table->tinyInteger("business_trip")->default(0)->comment("外差 开会");
            $table->tinyInteger("up_study")->default(0)->comment("进修");
            $table->tinyInteger("sick")->default(0)->comment("病假");
            $table->tinyInteger("birth_control")->default(0)->comment("计生假");
            $table->tinyInteger("wedding")->default(0)->comment("婚假");
            $table->tinyInteger("birth")->default(0)->comment("产假");
            $table->tinyInteger("annual")->default(0)->comment("年假");
            $table->tinyInteger("absence")->default(0)->comment("事假");
            $table->string("remark")->nullable()->comment("备注");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_attendances_2019');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_incomes', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger("income_type")->comment("收入类型");
            $table->string("serial_no", 20)->comment("流水号");
            $table->string("card_no", 20)->comment("卡号");
            $table->string("name", 20)->nullable()->comment("姓名");
            $table->unsignedInteger("prescribe_doctor_id")->comment("开方医生ID");
            $table->unsignedInteger("execute_doctor_id")->nullable()->comment("执行医生ID");
            $table->unsignedInteger("prescribe_department_id")->comment("开方二级科室ID");
            $table->unsignedInteger("execute_department_id")->comment("执行二级科室ID");
            $table->string("item_code", 20)->comment("项目编码");
            $table->string("item_name", 100)->comment("项目名称");
            $table->string("gb_code", 20)->nullable()->comment("国标码");
            $table->integer("item_cate_code")->comment("项目类别编码");
            $table->string("item_cate", 10)->nullable()->comment("项目类别");
            $table->integer("register_cate")->nullable()->comment("挂号类别");
            $table->decimal("price", 10, 2)->default(0)->comment("单价");
            $table->decimal("quantity", 10, 2)->comment("数量");
            $table->decimal("amount", 10, 2)->default(0)->comment("金额");
            $table->unsignedInteger("admission_department_id")->nullable()->comment("入院二级科室ID");
            $table->unsignedInteger("leave_department_id")->nullable()->comment("出院二级科室ID");
            $table->dateTime("admission_time")->nullable()->comment("出院时间");
            $table->dateTime("leave_time")->nullable()->comment("出院时间");
            $table->string("outpatient_diagnosis_code", 20)->nullable()->comment("门诊诊断编码");
            $table->string("outpatient_diagnosis", 50)->nullable()->comment("门诊诊断");
            $table->string("leave_diagnosis_code", 20)->nullable()->comment("出院诊断编码");
            $table->string("leave_diagnosis", 50)->nullable()->comment("出院诊断");
            $table->string("sub_diagnosis_code", 20)->nullable()->comment("次诊断编码");
            $table->string("sub_diagnosis", 50)->nullable()->comment("次诊断");
            $table->date("charging_time")->comment("收费时间");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_incomes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentManagementCoefficientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_management_coefficients', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月日期");
            $table->unsignedInteger("department_id")->comment("一级科室id");
            $table->decimal("post_coefficient", 10, 2)->default(0)->comment("岗位系数");
            $table->tinyInteger("employees_number")->default(0)->comment("员工数量");
            $table->tinyInteger("type_lv1")->default(0)->comment("科室类型");
            $table->decimal("scale_coefficient", 10, 2)->default(0)->comment("规模系数");
            $table->string("college_level", 100)->nullable()->comment("专科级别");
            $table->decimal("brand_coefficient", 10, 2)->default(0)->comment("品牌系数");
            $table->tinyInteger("efficiency_ranking")->nullable()->comment("人均效率排名");
            $table->decimal("expert_number_rate", 10, 2)->default(0)->comment("专家号绩效占比");


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_management_coefficients');
    }
}

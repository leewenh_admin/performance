<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRadiologyDepartmentPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('radiology_department_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("department_id")->comment("科室");
            $table->integer("radiation_number")->defautl(0)->comment("放射列数");
            $table->decimal("radiation_price", 10, 2)->defautl(0)->comment("单价");
            $table->decimal("performance", 10, 2)->defautl(0)->comment("绩效合计");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('radiology_department_performances');
    }
}

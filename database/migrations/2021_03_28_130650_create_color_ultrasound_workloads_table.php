<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColorUltrasoundWorkloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('color_ultrasound_workloads', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->string("card", 20)->comment("卡号");
            $table->string("request_note", 50)->comment("申请单");
            $table->string("check_code", 50)->comment("检查编号");
            $table->string("check_name", 100)->nullable()->comment("检查项目");
            $table->unsignedInteger("request_department_id")->comment("开单二级科室ID");
            $table->unsignedInteger("request_doctor_id")->comment("开单医生ID");
            $table->unsignedInteger("execute_department_id")->comment("执行二级科室ID");
            $table->string("check_date", 10)->comment("检查日期");
            $table->decimal("fee", 10, 2)->default(0)->comment("费用");
            $table->tinyInteger("person_type")->comment("人员类型");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('color_ultrasound_workloads');
    }
}

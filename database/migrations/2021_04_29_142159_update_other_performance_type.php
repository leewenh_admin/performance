<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOtherPerformanceType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('other_performances', function (Blueprint $table) {
            DB::statement("ALTER TABLE other_performances modify amount decimal(10, 2) COMMENT '金额'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('other_performances', function (Blueprint $table) {
            //
        });
    }
}

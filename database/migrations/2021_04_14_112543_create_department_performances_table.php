<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月日期");
            $table->unsignedInteger("department_id")->comment("一级科室id");
            $table->tinyInteger("type")->comment("科室类型");
            $table->decimal("cost_effective", 10, 2)->default(0)->comment("成本效益绩效");
            $table->decimal("workload", 10, 2)->default(0)->comment("工作量绩效");
            $table->decimal("core_business", 10, 2)->default(0)->comment("核心业务绩效");
            $table->decimal("comprehensive", 10, 2)->default(0)->comment("综合考核绩效");
            $table->decimal("individual", 10, 2)->default(0)->comment("单项类绩效");
            $table->decimal("total_performances", 10, 2)->default(0)->comment("科室总绩效");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_performances');
    }
}

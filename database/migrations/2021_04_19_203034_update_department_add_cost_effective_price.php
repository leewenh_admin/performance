<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDepartmentAddCostEffectivePrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->decimal("cost_effective_price", 10, 2)->default(0)->comment("成本效益单价默认值");
            $table->decimal("outpatient_price", 10, 2)->default(0)->comment("门诊工作量单价默认值");
            $table->decimal("admission_price", 10, 2)->default(0)->comment("住院工作量单价默认值");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('departments', function (Blueprint $table) {
            //
        });
    }
}

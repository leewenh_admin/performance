<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurgeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surgerys', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->string("inpatient_code", 20)->comment("住院号");
            $table->string("inpatient_name", 10)->comment("姓名");
            $table->dateTime("schedule_time")->nullable()->comment("预计时间");
            $table->string("diagnose_before", 100)->comment("术前诊断");
            $table->string("diagnose_after", 100)->nullable()->comment("术后诊断");
            $table->dateTime("request_time")->nullable()->comment("申请时间");
            $table->unsignedInteger("request_doctor_id")->comment("申请医生ID");
            $table->dateTime("surgery_time")->nullable()->comment("手术日期");
            $table->timestamps();

            $table->index(["date", "sub_departments_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surgerys');
    }
}

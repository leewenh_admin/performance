<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInpatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inpatients', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->integer("end_beds")->default(0)->comment("期末实有床位");
            $table->integer("start_in_hospital")->default(0)->comment("期初在院");
            $table->integer("admission")->default(0)->comment("入院人数");
            $table->integer("transfer_in")->default(0)->comment("转入人数");
            $table->integer("leave")->default(0)->comment("出院人数");
            $table->integer("cure")->default(0)->comment("治愈人数");
            $table->integer("better")->default(0)->comment("好转人数");
            $table->integer("unsettled")->default(0)->comment("未愈人数");
            $table->integer("dead")->default(0)->comment("死亡人数");
            $table->integer("other")->default(0)->comment("其他人数");
            $table->integer("transfer_out")->default(0)->comment("转出");
            $table->integer("end_in_hospital")->default(0)->comment("期末在院");
            $table->timestamps();

            $table->index(["date", "sub_departments_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inpatients');
    }
}

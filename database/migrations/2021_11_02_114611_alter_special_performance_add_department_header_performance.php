<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSpecialPerformanceAddDepartmentHeaderPerformance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('special_performances', function (Blueprint $table) {
            $table->decimal('department_header_performance', 10 ,2)->default(0)->comment('科主任科室绩效');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('special_performances', function (Blueprint $table) {
            $table->dropColumn('department_header_performance');
        });
    }
}

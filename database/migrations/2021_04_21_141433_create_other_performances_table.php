<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("code")->comment("工号");
            $table->string("name", 10)->comment("姓名");
            $table->unsignedInteger("sub_department_id")->comment("二级科室id");
            $table->integer("new_years_day")->defautl(0)->comment("元旦天数");
            $table->integer("spring_festival_day")->defautl(0)->comment("春节天数");
            $table->integer("days")->defautl(0)->comment("天数");
            $table->integer("amount")->defautl(0)->comment("金额");
            $table->string("description", 100)->nullable()->comment("备注");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_performances');
    }
}

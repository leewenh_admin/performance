<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMainincomePrescribeDepartmentIdIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('main_incomes', function (Blueprint $table) {
            $table->index('prescribe_department_id');
            $table->index('execute_department_id');
            $table->index('admission_department_id');
            $table->index('leave_department_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('main_incomes', function (Blueprint $table) {
            $table->dropIndex('main_incomes_prescribe_department_id_index');
            $table->dropIndex('main_incomes_execute_department_id_index');
            $table->dropIndex('main_incomes_admission_department_id_index');
            $table->dropIndex('main_incomes_leave_department_id_index');
        });
    }
}

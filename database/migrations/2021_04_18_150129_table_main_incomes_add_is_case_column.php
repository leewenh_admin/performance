<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableMainIncomesAddIsCaseColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('main_incomes', function (Blueprint $table) {
            $table->boolean("is_have_case")->default(false)->comment("有无病例");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('main_incomes', function (Blueprint $table) {
            $table->dropColumn("is_have_case");
        });
    }
}

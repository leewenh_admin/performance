<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBaseDataAddPointPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('base_datas', function (Blueprint $table) {
            $table->decimal('point_price', 10, 2)->default(0)->comment('核心业务单价');
            $table->decimal('cost_effective_price', 10, 2)->default(0)->comment('成本效益单价');
            $table->decimal('outpatient_price', 10, 2)->default(0)->comment('门诊工作量单价');
            $table->decimal('admission_price', 10, 2)->default(0)->comment('住院工作量单价');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('base_datas', function (Blueprint $table) {
            $table->dropColumn(['point_price', 'cost_effective_price', 'outpatient_price', 'admission_price']);
        });
    }
}

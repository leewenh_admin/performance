<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNightPerformancesAddFloatPerformanceColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('night_performances', function (Blueprint $table) {
            $table->decimal('float_performance', 10, 2)->default(0)->comment('浮动夜班绩效');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('night_performances', function (Blueprint $table) {
            $table->dropColumn('float_performance');
        });
    }
}

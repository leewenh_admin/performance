<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialSurveyPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_survey_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("department_id")->comment("科室");
            $table->integer("normal_number")->defautl(0)->comment("普通心电图例数");
            $table->decimal("normal_price", 10, 2)->defautl(0)->comment("普通心电图单价");
            $table->decimal("changcheng_number", 10, 2)->defautl(0)->comment("长程心电图列数");
            $table->decimal("changcheng_price", 10, 2)->defautl(0)->comment("长程心电图单价");
            $table->decimal("performance", 10, 2)->defautl(0)->comment("绩效合计");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_survey_performances');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectricCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electric_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 7)->comment("年月日期");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->string("location", 20)->nullable()->comment("地点");
            $table->string("used_for", 20)->nullable()->comment("用途");
            $table->integer("start_code")->default(0)->comment("期初码");
            $table->integer("end_code")->default(0)->comment("期末码");
            $table->integer("rate")->default(1)->comment("倍率");
            $table->integer("dosage")->default(0)->comment("用量");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electric_costs');
    }
}

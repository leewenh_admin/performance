<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月日期");
            $table->unsignedInteger("department_id")->comment("一级科室id");
            $table->tinyInteger("type")->comment("科室类型");
            $table->decimal("people_count", 10, 2)->default(0)->comment("科室考勤人数");
            $table->decimal("management_performance", 10, 2)->default(0)->comment("管理绩效");
            $table->decimal("night_performance", 10, 2)->default(0)->comment("夜班绩效");
            $table->decimal("consultation_performance", 10, 2)->default(0)->comment("会诊绩效");
            $table->decimal("visit_performance", 10, 2)->default(0)->comment("专家号绩效");
            $table->decimal("community_performance", 10, 2)->default(0)->comment("医共体绩效");
            $table->decimal("public_health_performance", 10, 2)->default(0)->comment("公卫绩效");
            $table->decimal("talent_performance", 10, 2)->default(0)->comment("人才储备绩效");
            $table->decimal("business_recovery_performance", 10, 2)->default(0)->comment("业务恢复绩效");
            $table->decimal("parking_performance", 10, 2)->default(0)->comment("停车补助");
            $table->decimal("other_performance", 10, 2)->default(0)->comment("其他专项绩效");
            $table->decimal("total_performance", 10, 2)->default(0)->comment("总绩效");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_performances');
    }
}

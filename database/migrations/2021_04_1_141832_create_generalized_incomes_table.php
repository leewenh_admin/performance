<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralizedIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generalized_incomes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("sub_department_id")->comment("二级科室ID");
            $table->tinyInteger("type")->comment("收入类型");
            $table->string("date", 7)->comment("年月");
            $table->decimal("diagnosis_income", 10, 2)->default(0)->comment("诊察收入");
            $table->decimal("check_income", 10, 2)->default(0)->comment("检查收入");
            $table->decimal("treatment_income", 10, 2)->default(0)->comment("治疗收入");
            $table->decimal("assay_income", 10, 2)->default(0)->comment("化验收入");
            $table->decimal("bed_income", 10, 2)->default(0)->comment("床位收入");
            $table->decimal("nursing_income", 10, 2)->default(0)->comment("护理收入");
            $table->decimal("material_income", 10, 2)->default(0)->comment("材料收入");
            $table->decimal("drug_income", 10, 2)->default(0)->comment("药品收入");
            $table->decimal("surgery_income", 10, 2)->default(0)->comment("手术收入");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generalized_incomes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNursingHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nursing_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->decimal("part_result", 5, 2)->default(0)->comment("分部结果");
            $table->integer("special_class_count")->default(0)->comment("特级护理人数");
            $table->decimal("special_class_hour")->default(0)->comment("特级护理耗时");
            $table->integer("one_class_count")->default(0)->comment("一级护理人数");
            $table->decimal("one_class_hour")->default(0)->comment("一级护理耗时");
            $table->integer("two_class_count")->default(0)->comment("二级护理人数");
            $table->decimal("two_class_hour")->default(0)->comment("二级护理耗时");
            $table->integer("three_class_count")->default(0)->comment("三级护理人数");
            $table->decimal("three_class_hour")->default(0)->comment("三级护理耗时");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nursing_hours');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name", 100)->comment("科室名称");
            $table->integer("code")->nullable()->comment("科室编号");
            $table->decimal("house_price", 6, 2)->default(0)->comment("房屋使用单价");
            $table->decimal("house_space", 10, 2)->default(0)->comment("房屋使用面积");
            $table->decimal("service_income_target_value", 10, 2)->default(0)->comment("医疗服务收入目标值");
            $table->decimal("device_maintain", 10, 2)->default(0)->comment("设备维护费用");
            $table->decimal("current_period_controllable_variable_cost", 10, 2)->default(0)->comment("本期可控变动成本");
            $table->decimal("base_department_income", 10, 2)->default(0)->comment("基期科内有效收入");
            $table->decimal("base_period_controllable_variable_cost", 10, 2)->default(0)->comment("基期可控变动成本");
            $table->decimal("variable_cost_control_deduction_rate", 3, 2)->default(1)->comment("变动成本控制扣罚比率");
            $table->boolean("is_performance")->default(false)->comment("是否参与绩效计算");

            $table->index("name");
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}

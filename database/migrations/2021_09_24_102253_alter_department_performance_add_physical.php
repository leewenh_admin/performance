<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDepartmentPerformanceAddPhysical extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('department_performances', function (Blueprint $table) {
            $table->decimal('physical_performance',10,2)->default('0.00')->comment('体检绩效')->after("individual");
            $table->decimal('control_performance',10,2)->default('0.00')->comment('疫情防控绩效')->after("physical_performance");
            $table->decimal('running_performance',10,2)->default('0.00')->comment('科室跑账绩效')->after("control_performance");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

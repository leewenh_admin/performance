<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicalLaboratoryPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinical_laboratory_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("department_id")->comment("科室");
            $table->decimal("point_value", 10, 2)->defautl(0)->comment("点值合计");
            $table->decimal("point_price", 10, 2)->defautl(0)->comment("点值单价");
            $table->decimal("check_num", 10, 2)->defautl(0)->comment("检查人次");
            $table->decimal("check_price", 10, 2)->defautl(0)->comment("检查单价");
            $table->decimal("performance", 10, 2)->defautl(0)->comment("绩效合计");
            $table->decimal("amount", 10, 2)->defautl(0)->comment("收入");
            $table->decimal("rate", 10, 2)->defautl(0)->comment("绩效占收入比%");
            $table->decimal("actual_amount", 10, 2)->defautl(0)->comment("实收金额");
            $table->decimal("actual_rate", 10, 2)->defautl(0)->comment("占实收金额%");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinical_laboratory_performances');
    }
}

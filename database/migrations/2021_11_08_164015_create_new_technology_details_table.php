<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTechnologyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_technology_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('technology_id')->comment('新技术id');
            $table->string('date', 7)->comment('月份');
            $table->integer('count')->default(0)->comment('数量');
            $table->decimal('price', 10, 2)->default(0)->comment('单价');
            $table->decimal('increase_count', 10, 2)->default(0)->comment('新业务增量');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_technology_details');
    }
}

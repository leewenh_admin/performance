<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direct_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 7)->comment("年月日期");
            $table->unsignedInteger("departments_id")->comment("一级科室ID");
            $table->decimal("base_salary", 10, 2)->default(0)->comment("基本工资");
            $table->decimal("social_security", 10, 2)->default(0)->comment("五险一金");
            $table->decimal("loabor_dispatch", 10, 2)->default(0)->comment("劳动派遣人员经费");
            $table->decimal("canteen", 10, 2)->default(0)->comment("食堂费");
            $table->decimal("house_fee", 10, 2)->default(0)->comment("房屋使用费");
            $table->decimal("device_depreciation", 10, 2)->default(0)->comment("设备折旧费");
            $table->decimal("property_management", 10, 2)->default(0)->comment("物业管理费");
            $table->decimal("device_maintenance", 10, 2)->default(0)->comment("设备维护费");
            $table->decimal("sewage_disposal", 10, 2)->default(0)->comment("污水处理费");
            $table->decimal("sanitary_material", 10, 2)->default(0)->comment("非计价卫生材料");
            $table->decimal("affairs_material", 10, 2)->default(0)->comment("总务材料");
            $table->decimal("disinfect", 10, 2)->default(0)->comment("消毒费");
            $table->decimal("laundry", 10, 2)->default(0)->comment("洗涤费");
            $table->decimal("utilities", 10, 2)->default(0)->comment("水电气费");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direct_costs');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTalentReservesAddDepartmentIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('talent_reserves', function (Blueprint $table) {
            $table->unsignedInteger("department_id")->comment("所属科室");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('talent_reserves', function (Blueprint $table) {
            $table->dropColumn('department_id');
        });
    }
}

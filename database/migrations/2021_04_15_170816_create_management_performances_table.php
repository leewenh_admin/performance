<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagementPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('management_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("department_id")->comment("一级科室ID");
            $table->tinyInteger("type_lv1")->default(0)->comment("科室类型");
            $table->decimal("department_head_award", 10, 2)->default(0)->comment("科室负责人管理奖");
            $table->decimal("department_sub_head_award", 10, 2)->default(0)->comment("科室副主任管理奖");
            $table->decimal("department_nurse_head_award", 10, 2)->default(0)->comment("科室护士长管理奖");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('management_performances');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewMainIncomeTimeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('main_incomes', function (Blueprint $table) {
            $table->string("admission_time")->nullable()->comment("入院时间");
            $table->string("leave_time")->nullable()->comment("出院时间");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('main_incomes', function (Blueprint $table) {
            $table->dropColumn("admission_time");
            $table->dropColumn("leave_time");
        });
    }
}

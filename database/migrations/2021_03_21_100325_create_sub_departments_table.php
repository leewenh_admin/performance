<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_departments', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name", 100)->comment("科室名称");
            $table->unsignedInteger("parent_id")->nullable()->comment("关联一级科室ID");
            $table->string("code", 20)->comment("科室号");
            $table->tinyInteger("type_lv1")->comment("科室类型1");
            $table->tinyInteger("type_lv2")->comment("科室类型2");
            $table->tinyInteger("type_lv3")->comment("科室类型3");
            $table->boolean("is_performance")->default(false)->comment("是否参与绩效计算");
            $table->timestamps();

            $table->index("name");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_departments');
    }
}

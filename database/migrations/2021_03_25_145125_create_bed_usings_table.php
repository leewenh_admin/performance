<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBedUsingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bed_usings', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->integer("begin_open")->default(0)->comment("期初开放床位数");
            $table->integer("finish_open")->default(0)->comment("期末开放床位数");
            $table->integer("avg_open")->default(0)->comment("平均开放床位数");
            $table->integer("total_open_bed_day")->default(0)->comment("实际开放总床日数");
            $table->integer("total_use_bed_day")->default(0)->comment("实际占用总床日数");
            $table->integer("avg_bed_work")->default(0)->comment("平均病床工作日");
            $table->decimal("used_rate", 5, 2)->default(0)->comment("病床使用率%");
            $table->integer("trunover_count")->default(0)->comment("周转次数");
            $table->integer("leaver_count")->default(0)->comment("出院人数");
            $table->integer("leaver_used_bed_day")->default(0)->comment("出院者占用总床日数");
            $table->integer("leaver_avg_day")->default(0)->comment("出院者平均住院日");
            $table->decimal("leaver_avg_cost", 10, 2)->default(0)->comment("出院者平均住院费");
            $table->decimal("medicine_in_cost_rate", 5, 2)->default(0)->comment("药费占住院费比例%");
            $table->decimal("leaver_cost_per_person", 10, 2)->default(0)->comment("出院者人均医疗费用");
            $table->decimal("leaver_cost_per_day", 10, 2)->default(0)->comment("出院者日均费用");
            $table->timestamps();

            $table->index(["date", "sub_departments_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bed_usings');
    }
}

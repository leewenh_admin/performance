<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPriceSurgicalPointValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surgical_point_values', function (Blueprint $table) {
            DB::statement("ALTER TABLE `surgical_point_values` 
MODIFY COLUMN `price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '价格' AFTER `gb_code`");
            DB::statement("ALTER TABLE `surgical_point_values` 
MODIFY COLUMN `price_type` varchar(20) NULL DEFAULT 0.00 COMMENT '类别' AFTER `price`");
            DB::statement("ALTER TABLE `surgical_point_values` 
MODIFY COLUMN `project_type` varchar(20) NULL DEFAULT 0.00 COMMENT '项目类别' AFTER `price_type`");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surgical_point_values', function (Blueprint $table) {
            //
        });
    }
}

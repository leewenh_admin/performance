<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpertDiagnosesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expert_diagnoses', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("employees_id")->comment("职员ID");
            $table->integer("assistant_count")->default(0)->comment("副主任号人次");
            $table->decimal("assistant_amount", 10, 2)->default(0)->comment("副主任号金额");
            $table->integer("chief_count")->default(0)->comment("主任号人次");
            $table->decimal("chief_amount", 10, 2)->default(0)->comment("主任号金额");
            $table->integer("total_count")->default(0)->comment("总数量");
            $table->decimal("total_amount", 10, 2)->default(0)->comment("总金额");
            $table->timestamps();

            $table->index(["date", "employees_id"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expert_diagnoses');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewToDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->decimal("target_average_cost", 10, 2)->after('is_performance')->default(0)->comment("目标(基期)门诊均例费用");
            $table->integer("target_patient_count")->after('target_average_cost')->default(0)->comment("科室基期门急诊人次");
            $table->decimal("target_in_hospital_day", 10, 1)->after('target_patient_count')->default(0)->comment("目标平均住院日");
            $table->decimal("target_average_leave_cost", 10, 2)->after('target_in_hospital_day')->default(0)->comment("目标出院病人均例费用");
            $table->decimal("target_average_drug_cost", 10, 2)->after('target_average_leave_cost')->default(0)->comment("目标出院病人均例药品费用");
            $table->decimal("target_average_material_cost", 10, 2)->after('target_average_drug_cost')->default(0)->comment("目标出院病人均例耗材费用");
            $table->decimal("base_use_bed_day", 10, 1)->after('target_average_material_cost')->default(0)->comment("科室基期占床日");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->dropColumn('target_average_cost');
            $table->dropColumn('target_patient_count');
            $table->dropColumn('target_in_hospital_day');
            $table->dropColumn('target_average_leave_cost');
            $table->dropColumn('target_average_drug_cost');
            $table->dropColumn('target_average_material_cost');
            $table->dropColumn('base_use_bed_day');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDepartmentHeadPerformancesAddAttendanceDayColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('department_head_performances', function (Blueprint $table){
            $table->decimal("medical_percent", 10, 2)->nullable()->default(0)->comment("药占比考核");
            $table->decimal("expend_percent", 10, 2)->nullable()->default(0)->comment("耗占比考核");
            $table->string("administrative_time", 100)->nullable()->comment("行政职务任职时间");
            $table->decimal("actual_attendance", 10, 2)->nullable()->default(0)->comment("出勤天数");
            $table->decimal("check_day", 10, 2)->nullable()->default(0)->comment("核减天数");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('department_head_performances', function (Blueprint $table){
           $table->dropColumn(['medical_percent', 'expend_percent', 'administrative_time', 'actual_attendance']);
        });
    }
}

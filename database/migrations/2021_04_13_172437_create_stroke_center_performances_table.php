<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStrokeCenterPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stroke_center_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月日期");
            $table->unsignedInteger("sub_department_id")->comment("二级科室id");
            $table->unsignedInteger("employee_id")->comment("职员id");
            $table->decimal("punish_amount", 10, 2)->default(0)->comment("奖惩金额");
            $table->string("punish_reason", 100)->nullable()->comment("奖惩原因");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stroke_center_performances');
    }
}

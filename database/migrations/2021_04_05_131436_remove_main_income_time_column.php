<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveMainIncomeTimeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('main_incomes', function (Blueprint $table) {
            $table->dropColumn("admission_time");
            $table->dropColumn("leave_time");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('main_incomes', function (Blueprint $table) {
            $table->dateTime("admission_time")->nullable()->comment("入院时间");
            $table->dateTime("leave_time")->nullable()->comment("出院时间");
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 7)->comment("年月日期");
            $table->unsignedInteger("departments_id")->comment("一级科室ID");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->unsignedInteger("employees_id")->comment("职员ID");
            $table->string("code", 20)->comment("职员工号");
            $table->tinyInteger("actual_attendance")->default(0)->comment("实际出勤天数");
            $table->tinyInteger("workday")->default(0)->comment("上班天数");
            $table->tinyInteger("front_line_rest")->default(0)->comment("一线人员休整天数");
            $table->tinyInteger("unemployed")->default(0)->comment("待岗天数");
            $table->tinyInteger("business_trip")->default(0)->comment("外差天数");
            $table->tinyInteger("study")->default(0)->comment("进修天数");
            $table->tinyInteger("birth_control")->default(0)->comment("计生假天数");
            $table->tinyInteger("birth")->default(0)->comment("产假天数");
            $table->tinyInteger("annual")->default(0)->comment("年假天数");
            $table->tinyInteger("absence")->default(0)->comment("事假天数");
            $table->tinyInteger("sick")->default(0)->comment("病假天数");
            $table->tinyInteger("work_injury")->default(0)->comment("工伤天数");
            $table->tinyInteger("family_visit")->default(0)->comment("探亲假天数");
            $table->tinyInteger("wedding")->default(0)->comment("婚假天数");
            $table->tinyInteger("bereavement")->default(0)->comment("丧假天数");
            $table->tinyInteger("radiate")->default(0)->comment("放射假天数");
            $table->decimal("coefficient", 4, 2)->default(1)->comment("系数");
            $table->decimal("attendance_coefficient", 4, 2)->default(1)->comment("结合考勤系数");
            $table->string("remark")->nullable()->comment("备注");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_attendances');
    }
}

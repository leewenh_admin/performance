<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaundryCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laundry_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 7)->comment("年月日期");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->integer("quilt")->default(0)->comment("被套");
            $table->integer("coverlet")->default(0)->comment("床单");
            $table->integer("pillowcase")->default(0)->comment("枕套");
            $table->integer("worker_clothe")->default(0)->comment("工作服");
            $table->integer("sick_clothes")->default(0)->comment("病服");
            $table->integer("total_count")->default(0)->comment("合计件数");
            $table->integer("surgery_count")->default(0)->comment("手术台次");
            $table->integer("used_bed_day")->default(0)->comment("实际占床日");
            $table->decimal("per_wash_fee", 10, 2)->default(0)->comment("每件洗涤费");
            $table->decimal("amount", 10, 2)->default(0)->comment("金额");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laundry_costs');
    }
}

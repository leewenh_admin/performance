<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDepartmentAddBaseSurgeryCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->integer("base_surgery_count")->default(0)->comment("基期手术台次");
            $table->decimal("base_special_surgery_rate", 10, 2)->default(0)->comment("基期三、四级手术率");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->dropColumn("base_surgery_count");
            $table->dropColumn("base_special_surgery_rate");
        });
    }
}

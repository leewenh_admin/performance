<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceColumnToDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->decimal("point_price", 10, 2)->after('punish_amount')->default(0)->comment("点数单价");
            $table->decimal("target_surgery_rate", 10, 2)->after('target_admission_rate')->default(0)->comment("目标手术率");
            $table->decimal("reward_amount", 10, 2)->after('punish_amount')->default(0)->comment("奖励金额");
            $table->decimal("target_medical_workload", 10, 2)->after('target_surgery_rate')->default(0)->comment("医技目标工作量");
            $table->decimal("medical_point_price", 10, 2)->after('target_medical_workload')->default(0)->comment("医技工作量绩效点值");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->dropColumn('point_price');
            $table->dropColumn('target_surgery_rate');
            $table->dropColumn('reward_amount');
            $table->dropColumn('target_medical_workload');
            $table->dropColumn('medical_point_price');
        });
    }
}

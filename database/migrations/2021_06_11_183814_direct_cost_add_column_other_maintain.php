<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DirectCostAddColumnOtherMaintain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('direct_costs', function (Blueprint $table) {
            $table->decimal("other_maintain", 10, 2)->default(0)->comment("其它维护费");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('direct_costs', function (Blueprint $table) {
            $table->dropColumn('other_maintain');
        });
    }
}

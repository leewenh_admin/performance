<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisinfectCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disinfect_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月");
            $table->unsignedInteger("sub_departments_id")->comment("二级科室ID");
            $table->integer("count")->default(0)->comment("数量");
            $table->decimal("amount", 10, 2)->deafult(0)->comment("金额");
            $table->timestamps();

            $table->index(["date", "sub_departments_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disinfect_costs');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePasswordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_passwords', function (Blueprint $table) {
            $table->increments('id');
            $table->string("code", 20)->comment("工号");
            $table->string("password", 60)->comment("加密密码");
            $table->timestamps();

            $table->index("code");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_passwords');
    }
}

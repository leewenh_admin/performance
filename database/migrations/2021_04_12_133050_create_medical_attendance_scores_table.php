<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalAttendanceScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_attendance_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date', 7)->comment("年月日期");
            $table->unsignedInteger("departments_id")->comment("一级科室ID");
            $table->decimal("employees_count")->comment("考勤人数");
            $table->decimal("medical_quality", 10, 2)->default(0)->comment("医疗质量维度（60分）");
            $table->decimal("care_quality", 10, 2)->default(0)->comment("护理质量维度（5分）");
            $table->decimal("medical_ethics_quality", 10, 2)->default(0)->comment("医德医风维度（5分）");
            $table->decimal("health_management", 10, 2)->default(0)->comment("医保管理（5分）");
            $table->decimal("scientific_research", 10, 2)->default(0)->comment("科研教学（10分）");
            $table->decimal("workload_management", 10, 2)->default(0)->comment("工作量管理（15分）");
            $table->decimal("total_score", 10, 2)->default(0)->comment("总得分");
            $table->decimal("comprehensive_performance", 10, 2)->default(0)->comment("综合绩效");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_attendance_scores');
    }
}

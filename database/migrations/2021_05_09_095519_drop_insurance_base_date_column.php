<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropInsuranceBaseDateColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_insurance_bases', function (Blueprint $table) {
            $table->dropColumn("date");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_insurance_bases', function (Blueprint $table) {
            $table->string('date', 7)->comment("年月日期");
        });
    }
}

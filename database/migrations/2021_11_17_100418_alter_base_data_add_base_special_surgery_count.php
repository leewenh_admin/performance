<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBaseDataAddBaseSpecialSurgeryCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('base_datas', function (Blueprint $table) {
            $table->integer('base_special_surgery_count')->default(0)->comment('基期三、四级手术台次');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('base_datas', function (Blueprint $table) {
            $table->dropColumn('base_special_surgery_count');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClinicalAttendanceScoresColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinical_attendance_scores', function (Blueprint $table) {
            DB::statement("ALTER TABLE clinical_attendance_scores CHANGE workload_management medical_record_quality decimal(10, 2) COMMENT '病案质量（10分）'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinical_attendance_scores', function (Blueprint $table) {
            //
        });
    }
}

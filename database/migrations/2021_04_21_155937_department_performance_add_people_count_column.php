<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DepartmentPerformanceAddPeopleCountColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('department_performances', function (Blueprint $table) {
            $table->decimal("people_count")->default(0)->comment("科室考勤人数");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('department_performances', function (Blueprint $table) {
            $table->dropColumn("people_count");
        });
    }
}

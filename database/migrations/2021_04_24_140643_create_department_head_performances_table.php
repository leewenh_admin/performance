<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentHeadPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_head_performances', function (Blueprint $table) {
            $table->increments('id');
            $table->string("date", 7)->comment("年月日期");
            $table->unsignedInteger("department_id")->comment("一级科室id");
            $table->unsignedInteger("code")->comment("工号");
            $table->unsignedInteger("employee_id")->comment("人员id");
            $table->string("administrative_duties", 10)->nullable()->comment("行政职务");
            $table->decimal("performance_standard", 10, 2)->default(0)->comment("绩效标准");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_head_performances');
    }
}

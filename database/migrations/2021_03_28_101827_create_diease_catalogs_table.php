<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDieaseCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diease_catalogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name", 200)->comment("疾病名称");
            $table->string("code", 20)->comment("疾病编码");
            $table->decimal("weights", 4, 2)->default(0)->comment("权重");
            $table->timestamps();

            $table->index("code");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diease_catalogs');
    }
}

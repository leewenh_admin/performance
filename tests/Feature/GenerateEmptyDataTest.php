<?php

namespace Tests\Feature;

use App\Models\Employee;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GenerateEmptyDataTest extends TestCase
{
    public $date;

    public function getOrCreateEmployee($code) {
        $code = $code ?? 0;
        $employee = Employee::where(
            "code", $code
        )->first();

        if ($employee) {
            return $employee;
        } else {
            return Employee::Create([
                'name' => "测试用户",
                'code' => $code,
                'type' => 0,
                'position' => 0,
                'birth_date' => '2019-01-01',
                'join_date' => '2019-01-01'
            ]);

        }
    }

    public function getToken(){
        $code = rand(1, 20);
        $this->getOrCreateEmployee($code);
        $password = "88888888";
        $token = auth()->attempt(['code' => $code, "password" => $password]);
        $this->date = env("TEST_DATE", date("Y-m"));

        $this->withHeaders(['Authorization' => 'Bearer '.$token]);

        return $this;
    }

    /**
     * 业务收入生成测试
     *
     * @return void
     */
    public function testGeneralizedIncomeTest()
    {
        $response = $this->getToken()->json('GET','api/generalizedincome/generateData', [
            "date" => $this->date
        ]);

        $response->assertStatus(200)->assertJson([
            "status" => "filed",
        ]);
    }

    /**
     * 业务收入清空测试
     *
     * @return void
     */
    public function testDeleteGeneralizedIncomeTest()
    {
        $response = $this->getToken()->json('GET','api/generalizedincome/delData', [
            "date" => $this->date
        ]);

        $response->assertStatus(200);
    }

    /**
     * 固定成本生成测试
     *
     * @return void
     */
    public function testDirectCostTest()
    {
        $response = $this->getToken()->json('GET','api/generate/directCost', [
            "date" => $this->date
        ]);

        $response->assertStatus(200)->assertJson([
            "status" => "filed",
        ]);
    }

    /**
     * 科室综合绩效生成测试
     *
     * @return void
     */
    public function testDepartmentPerformanceTest()
    {
        $response = $this->getToken()->json('GET','api/generate/departmentPerformance', [
            "date" => $this->date
        ]);

        $response->assertStatus(200)->assertJson([
            "status" => "filed",
        ]);
    }

    /**
     * 专项绩效绩效生成测试
     *
     * @return void
     */
    public function testSpecialPerformanceTest()
    {
        $response = $this->getToken()->json('GET','api/generate/specialPerformance', [
            "date" => $this->date
        ]);

        $response->assertStatus(200)->assertJson([
            "status" => "filed",
        ]);
    }


    /**
     * 清空固定成本数据测试
     *
     * @return void
     */
    public function testClearDirectCostTest()
    {
        $response = $this->getToken()->json('DELETE','api/directcost/delData', [
            "date" => $this->date
        ]);

        $response->assertStatus(200);
    }

    /**
     * 清空绩效数据测试
     *
     * @return void
     */
    public function testClearPerformanceTest()
    {
        $response = $this->getToken()->json('DELETE','api/generate/clearPerformance', [
            "date" => $this->date
        ]);

        $response->assertStatus(200);
    }

    /**
     * 查询绩效生成状态测试
     *
     * @return void
     */
    public function testCheckStatusTest()
    {
        $response = $this->getToken()->json('GET','api/generate/checkStatus');

        $response->assertStatus(200);
    }


    /**
     * 核对手术项目点值缺失数据测试
     *
     * @return void
     */
    public function testCheckSurgicalDiffTest()
    {
        $response = $this->getToken()->json('GET','api/surgicalpointvalues/check/diff');

        $response->assertStatus(200);
    }

    /**
     * 核对医技科室项目点值缺失数据测试
     *
     * @return void
     */
    public function testCheckMedicalDiffTest()
    {
        $response = $this->getToken()->json('GET','api/medicalpointvalue/check/diff');

        $response->assertStatus(200);
    }

    /**
     * 个人绩效数据生成测试
     *
     * @return void
     */
    public function testEmployeePerformanceTest()
    {
        $response = $this->getToken()->json('GET','api/employeeperformance/data/generate', [
            "date" => $this->date
        ]);

        $response->assertStatus(200)->assertJson([
            "status" => "filed",
        ]);
    }
}
